note
	description: "Summary description for {ROLL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ROLL
	create
		creator
	feature
		first:INTEGER
		second:INTEGER
		rolled:INTEGER
		next:ROLL
		shoot: ARRAY[INTEGER]

		is_strike:BOOLEAN
		do
			result:= first=10
		end

		is_spare:BOOLEAN
		do
			result:= (not is_strike) and ((first+second)=10)
		end

		knocked_pins:INTEGER
		do
			result:= first+second
		end

		is_end:BOOLEAN
		do
			result:= is_strike() or rolled>2
		end

		roll(n:INTEGER)
		do
			if is_end() then
				next.roll(n)
			else
				shoot[rolled]:=n;
				rolled:=rolled+1
			end
		end

		creator(n:ROLL)
		do
			first:=0
			second:=0
			next:=n
			rolled:=0
			shoot:=array.make_filled(0,0,1)
		end
end
