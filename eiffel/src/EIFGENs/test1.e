note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	TEST1

inherit
	EQA_TEST_SET

feature -- Test routines

	bowling_test1
			-- New test routine
		local
			g: GAME
		do
			create g.game
			assert ("nome",g.pippo=0)
		end

end


