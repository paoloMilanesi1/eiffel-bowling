#include "eif_eiffel.h"

#ifdef __cplusplus
extern "C" {
#endif

extern const char *names7[];
static const uint32 types7 [] =
{
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags7 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype7_0 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype7_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype7_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype7_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes7 [] = {
g_atype7_0,
g_atype7_1,
g_atype7_2,
g_atype7_3,
};

static const int32 cn_attr7 [] =
{
53,
54,
55,
52,
};

extern const char *names10[];
static const uint32 types10 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags10 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype10_0 [] = {614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_1 [] = {701,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_2 [] = {737,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_3 [] = {902,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_4 [] = {374,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_5 [] = {665,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_6 [] = {544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_7 [] = {410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_8 [] = {328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_9 [] = {273,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_10 [] = {455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_11 [] = {629,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_12 [] = {773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype10_13 [] = {794,224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes10 [] = {
g_atype10_0,
g_atype10_1,
g_atype10_2,
g_atype10_3,
g_atype10_4,
g_atype10_5,
g_atype10_6,
g_atype10_7,
g_atype10_8,
g_atype10_9,
g_atype10_10,
g_atype10_11,
g_atype10_12,
g_atype10_13,
};

static const int32 cn_attr10 [] =
{
67,
68,
69,
70,
71,
72,
73,
74,
75,
76,
77,
78,
79,
80,
};

extern const char *names11[];
static const uint32 types11 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags11 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype11_0 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_1 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_2 [] = {0xFF01,832,0xFF01,0xFFF9,9,186,0xFF01,232,0xFF01,232,221,221,221,221,200,200,200,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_3 [] = {0xFF01,855,0xFF01,0xFFF9,9,186,0xFF01,232,0xFF01,232,221,221,221,221,200,200,200,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype11_9 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes11 [] = {
g_atype11_0,
g_atype11_1,
g_atype11_2,
g_atype11_3,
g_atype11_4,
g_atype11_5,
g_atype11_6,
g_atype11_7,
g_atype11_8,
g_atype11_9,
};

static const int32 cn_attr11 [] =
{
94,
95,
96,
97,
88,
89,
90,
91,
92,
93,
};

extern const char *names14[];
static const uint32 types14 [] =
{
SK_REF,
};

static const uint16 attr_flags14 [] =
{0,};

static const EIF_TYPE_INDEX g_atype14_0 [] = {0xFF01,238,0xFFFF};

static const EIF_TYPE_INDEX *gtypes14 [] = {
g_atype14_0,
};

static const int32 cn_attr14 [] =
{
119,
};

extern const char *names16[];
static const uint32 types16 [] =
{
SK_REF,
};

static const uint16 attr_flags16 [] =
{0,};

static const EIF_TYPE_INDEX g_atype16_0 [] = {175,0xFFFF};

static const EIF_TYPE_INDEX *gtypes16 [] = {
g_atype16_0,
};

static const int32 cn_attr16 [] =
{
249,
};

extern const char *names18[];
static const uint32 types18 [] =
{
SK_UINT32,
};

static const uint16 attr_flags18 [] =
{1,};

static const EIF_TYPE_INDEX g_atype18_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes18 [] = {
g_atype18_0,
};

static const int32 cn_attr18 [] =
{
369,
};

extern const char *names30[];
static const uint32 types30 [] =
{
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags30 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype30_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_1 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_2 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype30_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes30 [] = {
g_atype30_0,
g_atype30_1,
g_atype30_2,
g_atype30_3,
g_atype30_4,
g_atype30_5,
g_atype30_6,
g_atype30_7,
};

static const int32 cn_attr30 [] =
{
772,
766,
768,
770,
773,
767,
769,
771,
};

extern const char *names34[];
static const uint32 types34 [] =
{
SK_REF,
};

static const uint16 attr_flags34 [] =
{0,};

static const EIF_TYPE_INDEX g_atype34_0 [] = {0xFF01,248,0xFF01,0xFFF9,5,186,0xFF01,245,0,232,232,221,200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes34 [] = {
g_atype34_0,
};

static const int32 cn_attr34 [] =
{
871,
};

extern const char *names36[];
static const uint32 types36 [] =
{
SK_UINT32,
};

static const uint16 attr_flags36 [] =
{0,};

static const EIF_TYPE_INDEX g_atype36_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes36 [] = {
g_atype36_0,
};

static const int32 cn_attr36 [] =
{
958,
};

extern const char *names37[];
static const uint32 types37 [] =
{
SK_REF,
};

static const uint16 attr_flags37 [] =
{0,};

static const EIF_TYPE_INDEX g_atype37_0 [] = {253,0xFF01,13,0xFFFF};

static const EIF_TYPE_INDEX *gtypes37 [] = {
g_atype37_0,
};

static const int32 cn_attr37 [] =
{
967,
};

extern const char *names39[];
static const uint32 types39 [] =
{
SK_BOOL,
};

static const uint16 attr_flags39 [] =
{0,};

static const EIF_TYPE_INDEX g_atype39_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes39 [] = {
g_atype39_0,
};

static const int32 cn_attr39 [] =
{
995,
};

extern const char *names40[];
static const uint32 types40 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags40 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype40_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype40_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes40 [] = {
g_atype40_0,
g_atype40_1,
g_atype40_2,
g_atype40_3,
g_atype40_4,
g_atype40_5,
g_atype40_6,
g_atype40_7,
};

static const int32 cn_attr40 [] =
{
1040,
1041,
995,
1037,
1045,
1042,
1043,
1046,
};

extern const char *names41[];
static const uint32 types41 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags41 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype41_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype41_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes41 [] = {
g_atype41_0,
g_atype41_1,
g_atype41_2,
g_atype41_3,
g_atype41_4,
g_atype41_5,
g_atype41_6,
g_atype41_7,
};

static const int32 cn_attr41 [] =
{
1040,
1041,
995,
1037,
1045,
1042,
1043,
1046,
};

extern const char *names48[];
static const uint32 types48 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags48 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype48_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_1 [] = {248,0xFF01,0xFFF9,1,186,0xFF01,132,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_2 [] = {248,0xFF01,0xFFF9,2,186,0xFF01,132,0xFF01,132,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_3 [] = {248,0xFF01,0xFFF9,1,186,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_4 [] = {253,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_5 [] = {878,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype48_11 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes48 [] = {
g_atype48_0,
g_atype48_1,
g_atype48_2,
g_atype48_3,
g_atype48_4,
g_atype48_5,
g_atype48_6,
g_atype48_7,
g_atype48_8,
g_atype48_9,
g_atype48_10,
g_atype48_11,
};

static const int32 cn_attr48 [] =
{
1098,
1099,
1100,
1101,
1102,
1103,
1104,
1107,
1108,
1109,
1110,
1111,
};

extern const char *names49[];
static const uint32 types49 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags49 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype49_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_1 [] = {248,0xFF01,0xFFF9,1,186,0xFF01,132,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_2 [] = {248,0xFF01,0xFFF9,2,186,0xFF01,132,0xFF01,132,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_3 [] = {248,0xFF01,0xFFF9,1,186,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_4 [] = {253,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_5 [] = {878,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype49_11 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes49 [] = {
g_atype49_0,
g_atype49_1,
g_atype49_2,
g_atype49_3,
g_atype49_4,
g_atype49_5,
g_atype49_6,
g_atype49_7,
g_atype49_8,
g_atype49_9,
g_atype49_10,
g_atype49_11,
};

static const int32 cn_attr49 [] =
{
1098,
1099,
1100,
1101,
1102,
1103,
1104,
1107,
1108,
1109,
1110,
1111,
};

extern const char *names50[];
static const uint32 types50 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags50 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype50_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_1 [] = {248,0xFF01,0xFFF9,1,186,0xFF01,132,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_2 [] = {248,0xFF01,0xFFF9,2,186,0xFF01,132,0xFF01,132,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_3 [] = {248,0xFF01,0xFFF9,1,186,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_4 [] = {253,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_5 [] = {878,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype50_11 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes50 [] = {
g_atype50_0,
g_atype50_1,
g_atype50_2,
g_atype50_3,
g_atype50_4,
g_atype50_5,
g_atype50_6,
g_atype50_7,
g_atype50_8,
g_atype50_9,
g_atype50_10,
g_atype50_11,
};

static const int32 cn_attr50 [] =
{
1098,
1099,
1100,
1101,
1102,
1103,
1104,
1107,
1108,
1109,
1110,
1111,
};

extern const char *names51[];
static const uint32 types51 [] =
{
SK_REF,
SK_BOOL,
SK_POINTER,
};

static const uint16 attr_flags51 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype51_0 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype51_2 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes51 [] = {
g_atype51_0,
g_atype51_1,
g_atype51_2,
};

static const int32 cn_attr51 [] =
{
1133,
1128,
1132,
};

extern const char *names55[];
static const uint32 types55 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags55 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype55_0 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_1 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_2 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype55_3 [] = {0xFF01,794,224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes55 [] = {
g_atype55_0,
g_atype55_1,
g_atype55_2,
g_atype55_3,
};

static const int32 cn_attr55 [] =
{
1171,
1172,
1173,
1174,
};

extern const char *names56[];
static const uint32 types56 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags56 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype56_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype56_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes56 [] = {
g_atype56_0,
g_atype56_1,
g_atype56_2,
g_atype56_3,
g_atype56_4,
g_atype56_5,
g_atype56_6,
};

static const int32 cn_attr56 [] =
{
1177,
1178,
1175,
1176,
1184,
1192,
1193,
};

extern const char *names57[];
static const uint32 types57 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_UINT64,
};

static const uint16 attr_flags57 [] =
{0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype57_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_2 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_9 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype57_10 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes57 [] = {
g_atype57_0,
g_atype57_1,
g_atype57_2,
g_atype57_3,
g_atype57_4,
g_atype57_5,
g_atype57_6,
g_atype57_7,
g_atype57_8,
g_atype57_9,
g_atype57_10,
};

static const int32 cn_attr57 [] =
{
1177,
1178,
1207,
1175,
1176,
1208,
1184,
1192,
1193,
1210,
1211,
};

extern const char *names58[];
static const uint32 types58 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL64,
SK_REAL64,
SK_REAL64,
};

static const uint16 attr_flags58 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype58_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_12 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_13 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype58_14 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes58 [] = {
g_atype58_0,
g_atype58_1,
g_atype58_2,
g_atype58_3,
g_atype58_4,
g_atype58_5,
g_atype58_6,
g_atype58_7,
g_atype58_8,
g_atype58_9,
g_atype58_10,
g_atype58_11,
g_atype58_12,
g_atype58_13,
g_atype58_14,
};

static const int32 cn_attr58 [] =
{
1177,
1178,
1175,
1176,
1223,
1224,
1225,
1226,
1184,
1192,
1193,
1222,
1219,
1220,
1221,
};

extern const char *names59[];
static const uint32 types59 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
SK_UINT64,
};

static const uint16 attr_flags59 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype59_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_8 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype59_9 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes59 [] = {
g_atype59_0,
g_atype59_1,
g_atype59_2,
g_atype59_3,
g_atype59_4,
g_atype59_5,
g_atype59_6,
g_atype59_7,
g_atype59_8,
g_atype59_9,
};

static const int32 cn_attr59 [] =
{
1177,
1178,
1175,
1176,
1243,
1184,
1192,
1193,
1241,
1242,
};

extern const char *names63[];
static const uint32 types63 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags63 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype63_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype63_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes63 [] = {
g_atype63_0,
g_atype63_1,
g_atype63_2,
g_atype63_3,
g_atype63_4,
g_atype63_5,
g_atype63_6,
};

static const int32 cn_attr63 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names64[];
static const uint32 types64 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags64 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype64_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype64_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes64 [] = {
g_atype64_0,
g_atype64_1,
g_atype64_2,
g_atype64_3,
g_atype64_4,
g_atype64_5,
g_atype64_6,
};

static const int32 cn_attr64 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names65[];
static const uint32 types65 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags65 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype65_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype65_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes65 [] = {
g_atype65_0,
g_atype65_1,
g_atype65_2,
g_atype65_3,
g_atype65_4,
g_atype65_5,
g_atype65_6,
};

static const int32 cn_attr65 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names66[];
static const uint32 types66 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags66 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype66_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype66_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes66 [] = {
g_atype66_0,
g_atype66_1,
g_atype66_2,
g_atype66_3,
g_atype66_4,
g_atype66_5,
g_atype66_6,
};

static const int32 cn_attr66 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names67[];
static const uint32 types67 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags67 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype67_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype67_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes67 [] = {
g_atype67_0,
g_atype67_1,
g_atype67_2,
g_atype67_3,
g_atype67_4,
g_atype67_5,
g_atype67_6,
};

static const int32 cn_attr67 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names68[];
static const uint32 types68 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags68 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype68_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype68_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes68 [] = {
g_atype68_0,
g_atype68_1,
g_atype68_2,
g_atype68_3,
g_atype68_4,
g_atype68_5,
g_atype68_6,
};

static const int32 cn_attr68 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names69[];
static const uint32 types69 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags69 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype69_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_5 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype69_9 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes69 [] = {
g_atype69_0,
g_atype69_1,
g_atype69_2,
g_atype69_3,
g_atype69_4,
g_atype69_5,
g_atype69_6,
g_atype69_7,
g_atype69_8,
g_atype69_9,
};

static const int32 cn_attr69 [] =
{
1290,
1291,
1300,
1305,
1309,
1316,
1307,
1292,
1310,
1311,
};

extern const char *names70[];
static const uint32 types70 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags70 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype70_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype70_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes70 [] = {
g_atype70_0,
g_atype70_1,
g_atype70_2,
g_atype70_3,
g_atype70_4,
g_atype70_5,
g_atype70_6,
g_atype70_7,
};

static const int32 cn_attr70 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
1327,
};

extern const char *names71[];
static const uint32 types71 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags71 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype71_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype71_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes71 [] = {
g_atype71_0,
g_atype71_1,
g_atype71_2,
g_atype71_3,
g_atype71_4,
g_atype71_5,
g_atype71_6,
g_atype71_7,
};

static const int32 cn_attr71 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
1329,
};

extern const char *names72[];
static const uint32 types72 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags72 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype72_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype72_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes72 [] = {
g_atype72_0,
g_atype72_1,
g_atype72_2,
g_atype72_3,
g_atype72_4,
g_atype72_5,
g_atype72_6,
};

static const int32 cn_attr72 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names73[];
static const uint32 types73 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags73 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype73_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype73_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes73 [] = {
g_atype73_0,
g_atype73_1,
g_atype73_2,
g_atype73_3,
g_atype73_4,
g_atype73_5,
g_atype73_6,
};

static const int32 cn_attr73 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names74[];
static const uint32 types74 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags74 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype74_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype74_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes74 [] = {
g_atype74_0,
g_atype74_1,
g_atype74_2,
g_atype74_3,
g_atype74_4,
g_atype74_5,
g_atype74_6,
};

static const int32 cn_attr74 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names75[];
static const uint32 types75 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags75 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype75_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype75_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes75 [] = {
g_atype75_0,
g_atype75_1,
g_atype75_2,
g_atype75_3,
g_atype75_4,
g_atype75_5,
g_atype75_6,
};

static const int32 cn_attr75 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names76[];
static const uint32 types76 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags76 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype76_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype76_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes76 [] = {
g_atype76_0,
g_atype76_1,
g_atype76_2,
g_atype76_3,
g_atype76_4,
g_atype76_5,
g_atype76_6,
};

static const int32 cn_attr76 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names77[];
static const uint32 types77 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags77 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype77_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype77_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes77 [] = {
g_atype77_0,
g_atype77_1,
g_atype77_2,
g_atype77_3,
g_atype77_4,
g_atype77_5,
g_atype77_6,
g_atype77_7,
};

static const int32 cn_attr77 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
1332,
};

extern const char *names78[];
static const uint32 types78 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags78 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype78_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype78_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes78 [] = {
g_atype78_0,
g_atype78_1,
g_atype78_2,
g_atype78_3,
g_atype78_4,
g_atype78_5,
g_atype78_6,
};

static const int32 cn_attr78 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names79[];
static const uint32 types79 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags79 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype79_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype79_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes79 [] = {
g_atype79_0,
g_atype79_1,
g_atype79_2,
g_atype79_3,
g_atype79_4,
g_atype79_5,
g_atype79_6,
};

static const int32 cn_attr79 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names80[];
static const uint32 types80 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags80 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype80_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype80_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes80 [] = {
g_atype80_0,
g_atype80_1,
g_atype80_2,
g_atype80_3,
g_atype80_4,
g_atype80_5,
g_atype80_6,
};

static const int32 cn_attr80 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names81[];
static const uint32 types81 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags81 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype81_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype81_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes81 [] = {
g_atype81_0,
g_atype81_1,
g_atype81_2,
g_atype81_3,
g_atype81_4,
g_atype81_5,
g_atype81_6,
};

static const int32 cn_attr81 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names82[];
static const uint32 types82 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags82 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype82_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype82_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes82 [] = {
g_atype82_0,
g_atype82_1,
g_atype82_2,
g_atype82_3,
g_atype82_4,
g_atype82_5,
g_atype82_6,
g_atype82_7,
};

static const int32 cn_attr82 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
1334,
};

extern const char *names83[];
static const uint32 types83 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags83 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype83_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype83_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes83 [] = {
g_atype83_0,
g_atype83_1,
g_atype83_2,
g_atype83_3,
g_atype83_4,
g_atype83_5,
g_atype83_6,
};

static const int32 cn_attr83 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names84[];
static const uint32 types84 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags84 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype84_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype84_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes84 [] = {
g_atype84_0,
g_atype84_1,
g_atype84_2,
g_atype84_3,
g_atype84_4,
g_atype84_5,
g_atype84_6,
};

static const int32 cn_attr84 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names85[];
static const uint32 types85 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags85 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype85_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype85_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes85 [] = {
g_atype85_0,
g_atype85_1,
g_atype85_2,
g_atype85_3,
g_atype85_4,
g_atype85_5,
g_atype85_6,
};

static const int32 cn_attr85 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names86[];
static const uint32 types86 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags86 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype86_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype86_8 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes86 [] = {
g_atype86_0,
g_atype86_1,
g_atype86_2,
g_atype86_3,
g_atype86_4,
g_atype86_5,
g_atype86_6,
g_atype86_7,
g_atype86_8,
};

static const int32 cn_attr86 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
1335,
1338,
};

extern const char *names87[];
static const uint32 types87 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags87 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype87_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype87_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes87 [] = {
g_atype87_0,
g_atype87_1,
g_atype87_2,
g_atype87_3,
g_atype87_4,
g_atype87_5,
g_atype87_6,
};

static const int32 cn_attr87 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names88[];
static const uint32 types88 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags88 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype88_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype88_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes88 [] = {
g_atype88_0,
g_atype88_1,
g_atype88_2,
g_atype88_3,
g_atype88_4,
g_atype88_5,
g_atype88_6,
};

static const int32 cn_attr88 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names89[];
static const uint32 types89 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags89 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype89_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype89_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes89 [] = {
g_atype89_0,
g_atype89_1,
g_atype89_2,
g_atype89_3,
g_atype89_4,
g_atype89_5,
g_atype89_6,
};

static const int32 cn_attr89 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names90[];
static const uint32 types90 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags90 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype90_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_5 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_6 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype90_8 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes90 [] = {
g_atype90_0,
g_atype90_1,
g_atype90_2,
g_atype90_3,
g_atype90_4,
g_atype90_5,
g_atype90_6,
g_atype90_7,
g_atype90_8,
};

static const int32 cn_attr90 [] =
{
1290,
1291,
1300,
1305,
1309,
1339,
1340,
1307,
1292,
};

extern const char *names91[];
static const uint32 types91 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags91 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype91_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype91_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes91 [] = {
g_atype91_0,
g_atype91_1,
g_atype91_2,
g_atype91_3,
g_atype91_4,
g_atype91_5,
g_atype91_6,
};

static const int32 cn_attr91 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names92[];
static const uint32 types92 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags92 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype92_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype92_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes92 [] = {
g_atype92_0,
g_atype92_1,
g_atype92_2,
g_atype92_3,
g_atype92_4,
g_atype92_5,
g_atype92_6,
};

static const int32 cn_attr92 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names93[];
static const uint32 types93 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags93 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype93_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype93_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes93 [] = {
g_atype93_0,
g_atype93_1,
g_atype93_2,
g_atype93_3,
g_atype93_4,
g_atype93_5,
g_atype93_6,
};

static const int32 cn_attr93 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names94[];
static const uint32 types94 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags94 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype94_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype94_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes94 [] = {
g_atype94_0,
g_atype94_1,
g_atype94_2,
g_atype94_3,
g_atype94_4,
g_atype94_5,
g_atype94_6,
};

static const int32 cn_attr94 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names95[];
static const uint32 types95 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags95 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype95_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype95_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes95 [] = {
g_atype95_0,
g_atype95_1,
g_atype95_2,
g_atype95_3,
g_atype95_4,
g_atype95_5,
g_atype95_6,
};

static const int32 cn_attr95 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names96[];
static const uint32 types96 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags96 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype96_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype96_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes96 [] = {
g_atype96_0,
g_atype96_1,
g_atype96_2,
g_atype96_3,
g_atype96_4,
g_atype96_5,
g_atype96_6,
};

static const int32 cn_attr96 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names97[];
static const uint32 types97 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags97 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype97_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype97_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes97 [] = {
g_atype97_0,
g_atype97_1,
g_atype97_2,
g_atype97_3,
g_atype97_4,
g_atype97_5,
g_atype97_6,
};

static const int32 cn_attr97 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names98[];
static const uint32 types98 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags98 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype98_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype98_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes98 [] = {
g_atype98_0,
g_atype98_1,
g_atype98_2,
g_atype98_3,
g_atype98_4,
g_atype98_5,
g_atype98_6,
};

static const int32 cn_attr98 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names99[];
static const uint32 types99 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags99 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype99_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype99_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes99 [] = {
g_atype99_0,
g_atype99_1,
g_atype99_2,
g_atype99_3,
g_atype99_4,
g_atype99_5,
g_atype99_6,
};

static const int32 cn_attr99 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names100[];
static const uint32 types100 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags100 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype100_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype100_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes100 [] = {
g_atype100_0,
g_atype100_1,
g_atype100_2,
g_atype100_3,
g_atype100_4,
g_atype100_5,
g_atype100_6,
};

static const int32 cn_attr100 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1292,
};

extern const char *names101[];
static const uint32 types101 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags101 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype101_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_2 [] = {62,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_3 [] = {124,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_4 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype101_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes101 [] = {
g_atype101_0,
g_atype101_1,
g_atype101_2,
g_atype101_3,
g_atype101_4,
g_atype101_5,
g_atype101_6,
g_atype101_7,
};

static const int32 cn_attr101 [] =
{
1290,
1291,
1300,
1305,
1309,
1307,
1344,
1292,
};

extern const char *names102[];
static const uint32 types102 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags102 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype102_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype102_1 [] = {273,0xFF01,614,221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes102 [] = {
g_atype102_0,
g_atype102_1,
};

static const int32 cn_attr102 [] =
{
1353,
1354,
};

extern const char *names103[];
static const uint32 types103 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags103 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype103_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype103_1 [] = {273,0xFF01,614,221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes103 [] = {
g_atype103_0,
g_atype103_1,
};

static const int32 cn_attr103 [] =
{
1353,
1354,
};

extern const char *names104[];
static const uint32 types104 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags104 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype104_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype104_1 [] = {273,0xFF01,614,221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes104 [] = {
g_atype104_0,
g_atype104_1,
};

static const int32 cn_attr104 [] =
{
1353,
1354,
};

extern const char *names108[];
static const uint32 types108 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags108 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype108_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_1 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_3 [] = {0xFF01,47,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype108_7 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes108 [] = {
g_atype108_0,
g_atype108_1,
g_atype108_2,
g_atype108_3,
g_atype108_4,
g_atype108_5,
g_atype108_6,
g_atype108_7,
};

static const int32 cn_attr108 [] =
{
1375,
1383,
1384,
1385,
1386,
1377,
1392,
1389,
};

extern const char *names109[];
static const uint32 types109 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags109 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype109_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype109_1 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype109_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype109_3 [] = {0xFF01,47,0xFFFF};
static const EIF_TYPE_INDEX g_atype109_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype109_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype109_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype109_7 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes109 [] = {
g_atype109_0,
g_atype109_1,
g_atype109_2,
g_atype109_3,
g_atype109_4,
g_atype109_5,
g_atype109_6,
g_atype109_7,
};

static const int32 cn_attr109 [] =
{
1375,
1383,
1384,
1385,
1386,
1377,
1392,
1389,
};

extern const char *names110[];
static const uint32 types110 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags110 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype110_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_1 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_3 [] = {0xFF01,47,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype110_7 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes110 [] = {
g_atype110_0,
g_atype110_1,
g_atype110_2,
g_atype110_3,
g_atype110_4,
g_atype110_5,
g_atype110_6,
g_atype110_7,
};

static const int32 cn_attr110 [] =
{
1375,
1383,
1384,
1385,
1386,
1377,
1392,
1389,
};

extern const char *names111[];
static const uint32 types111 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags111 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype111_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_1 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_2 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_3 [] = {0xFF01,47,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_4 [] = {0xFF01,160,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype111_7 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes111 [] = {
g_atype111_0,
g_atype111_1,
g_atype111_2,
g_atype111_3,
g_atype111_4,
g_atype111_5,
g_atype111_6,
g_atype111_7,
};

static const int32 cn_attr111 [] =
{
1375,
1383,
1384,
1385,
1386,
1377,
1392,
1389,
};

extern const char *names114[];
static const uint32 types114 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags114 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype114_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_2 [] = {253,0xFF01,13,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_3 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_5 [] = {0xFF01,273,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_6 [] = {614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype114_8 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes114 [] = {
g_atype114_0,
g_atype114_1,
g_atype114_2,
g_atype114_3,
g_atype114_4,
g_atype114_5,
g_atype114_6,
g_atype114_7,
g_atype114_8,
};

static const int32 cn_attr114 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
1460,
1462,
};

extern const char *names115[];
static const uint32 types115 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags115 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype115_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_2 [] = {253,0xFF01,13,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_3 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_5 [] = {0xFF01,273,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_6 [] = {614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype115_8 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes115 [] = {
g_atype115_0,
g_atype115_1,
g_atype115_2,
g_atype115_3,
g_atype115_4,
g_atype115_5,
g_atype115_6,
g_atype115_7,
g_atype115_8,
};

static const int32 cn_attr115 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
1460,
1462,
};

extern const char *names116[];
static const uint32 types116 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags116 [] =
{0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype116_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_2 [] = {253,0xFF01,13,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_3 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_5 [] = {0xFF01,273,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_6 [] = {614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_7 [] = {273,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype116_9 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes116 [] = {
g_atype116_0,
g_atype116_1,
g_atype116_2,
g_atype116_3,
g_atype116_4,
g_atype116_5,
g_atype116_6,
g_atype116_7,
g_atype116_8,
g_atype116_9,
};

static const int32 cn_attr116 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
1499,
1460,
1462,
};

extern const char *names120[];
static const uint32 types120 [] =
{
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags120 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype120_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_1 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_2 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_3 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype120_12 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes120 [] = {
g_atype120_0,
g_atype120_1,
g_atype120_2,
g_atype120_3,
g_atype120_4,
g_atype120_5,
g_atype120_6,
g_atype120_7,
g_atype120_8,
g_atype120_9,
g_atype120_10,
g_atype120_11,
g_atype120_12,
};

static const int32 cn_attr120 [] =
{
772,
766,
768,
1540,
770,
773,
1536,
1538,
1539,
767,
769,
771,
1537,
};

extern const char *names122[];
static const uint32 types122 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags122 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype122_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype122_8 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes122 [] = {
g_atype122_0,
g_atype122_1,
g_atype122_2,
g_atype122_3,
g_atype122_4,
g_atype122_5,
g_atype122_6,
g_atype122_7,
g_atype122_8,
};

static const int32 cn_attr122 [] =
{
1040,
1041,
995,
1037,
1045,
1609,
1042,
1043,
1046,
};

extern const char *names123[];
static const uint32 types123 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags123 [] =
{0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype123_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_1 [] = {0xFF01,171,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype123_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes123 [] = {
g_atype123_0,
g_atype123_1,
g_atype123_2,
g_atype123_3,
g_atype123_4,
g_atype123_5,
g_atype123_6,
g_atype123_7,
};

static const int32 cn_attr123 [] =
{
1040,
1041,
995,
1037,
1045,
1042,
1043,
1046,
};

extern const char *names125[];
static const uint32 types125 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags125 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype125_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype125_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes125 [] = {
g_atype125_0,
g_atype125_1,
};

static const int32 cn_attr125 [] =
{
1640,
1643,
};

extern const char *names127[];
static const uint32 types127 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags127 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype127_0 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype127_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype127_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype127_3 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes127 [] = {
g_atype127_0,
g_atype127_1,
g_atype127_2,
g_atype127_3,
};

static const int32 cn_attr127 [] =
{
1133,
1128,
1661,
1132,
};

extern const char *names128[];
static const uint32 types128 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags128 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype128_0 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype128_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype128_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype128_3 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes128 [] = {
g_atype128_0,
g_atype128_1,
g_atype128_2,
g_atype128_3,
};

static const int32 cn_attr128 [] =
{
1133,
1128,
1679,
1132,
};

extern const char *names133[];
static const uint32 types133 [] =
{
SK_INT32,
};

static const uint16 attr_flags133 [] =
{0,};

static const EIF_TYPE_INDEX g_atype133_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes133 [] = {
g_atype133_0,
};

static const int32 cn_attr133 [] =
{
1854,
};

extern const char *names135[];
static const uint32 types135 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags135 [] =
{0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype135_0 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_1 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_2 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_3 [] = {341,0xFF01,0xFFF9,2,186,0xFF01,138,253,0xFF01,0xFFF9,2,186,0xFF01,140,0xFF01,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype135_10 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes135 [] = {
g_atype135_0,
g_atype135_1,
g_atype135_2,
g_atype135_3,
g_atype135_4,
g_atype135_5,
g_atype135_6,
g_atype135_7,
g_atype135_8,
g_atype135_9,
g_atype135_10,
};

static const int32 cn_attr135 [] =
{
1971,
1972,
1998,
2000,
1968,
1969,
1970,
1996,
1997,
1966,
1967,
};

extern const char *names136[];
static const uint32 types136 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags136 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype136_0 [] = {0xFF01,132,0xFFFF};
static const EIF_TYPE_INDEX g_atype136_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype136_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype136_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes136 [] = {
g_atype136_0,
g_atype136_1,
g_atype136_2,
g_atype136_3,
};

static const int32 cn_attr136 [] =
{
2013,
1854,
2012,
2014,
};

extern const char *names137[];
static const uint32 types137 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags137 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype137_0 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype137_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype137_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes137 [] = {
g_atype137_0,
g_atype137_1,
g_atype137_2,
};

static const int32 cn_attr137 [] =
{
2020,
1854,
2021,
};

extern const char *names139[];
static const uint32 types139 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags139 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype139_0 [] = {0xFF01,134,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_2 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_3 [] = {138,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_4 [] = {0xFF01,556,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_5 [] = {253,138,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_6 [] = {253,0xFF01,140,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_7 [] = {0xFF01,0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype139_14 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes139 [] = {
g_atype139_0,
g_atype139_1,
g_atype139_2,
g_atype139_3,
g_atype139_4,
g_atype139_5,
g_atype139_6,
g_atype139_7,
g_atype139_8,
g_atype139_9,
g_atype139_10,
g_atype139_11,
g_atype139_12,
g_atype139_13,
g_atype139_14,
};

static const int32 cn_attr139 [] =
{
2025,
2026,
2030,
2032,
2033,
2034,
2035,
2038,
2037,
2043,
2044,
2045,
2028,
2029,
2031,
};

extern const char *names141[];
static const uint32 types141 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags141 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype141_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype141_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype141_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes141 [] = {
g_atype141_0,
g_atype141_1,
g_atype141_2,
};

static const int32 cn_attr141 [] =
{
2084,
2082,
2083,
};

extern const char *names144[];
static const uint32 types144 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags144 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype144_0 [] = {0xFF01,142,0xFFFF};
static const EIF_TYPE_INDEX g_atype144_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype144_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes144 [] = {
g_atype144_0,
g_atype144_1,
g_atype144_2,
};

static const int32 cn_attr144 [] =
{
2135,
2136,
2137,
};

extern const char *names145[];
static const uint32 types145 [] =
{
SK_INT32,
};

static const uint16 attr_flags145 [] =
{0,};

static const EIF_TYPE_INDEX g_atype145_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes145 [] = {
g_atype145_0,
};

static const int32 cn_attr145 [] =
{
2139,
};

extern const char *names146[];
static const uint32 types146 [] =
{
SK_INT32,
};

static const uint16 attr_flags146 [] =
{0,};

static const EIF_TYPE_INDEX g_atype146_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes146 [] = {
g_atype146_0,
};

static const int32 cn_attr146 [] =
{
2148,
};

extern const char *names148[];
static const uint32 types148 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags148 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype148_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype148_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes148 [] = {
g_atype148_0,
g_atype148_1,
};

static const int32 cn_attr148 [] =
{
2173,
2177,
};

extern const char *names150[];
static const uint32 types150 [] =
{
SK_INT32,
};

static const uint16 attr_flags150 [] =
{0,};

static const EIF_TYPE_INDEX g_atype150_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes150 [] = {
g_atype150_0,
};

static const int32 cn_attr150 [] =
{
2201,
};

extern const char *names151[];
static const uint32 types151 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags151 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype151_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype151_1 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype151_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype151_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype151_4 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes151 [] = {
g_atype151_0,
g_atype151_1,
g_atype151_2,
g_atype151_3,
g_atype151_4,
};

static const int32 cn_attr151 [] =
{
2156,
2267,
2268,
2241,
2242,
};

extern const char *names152[];
static const uint32 types152 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags152 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype152_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype152_1 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype152_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype152_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype152_4 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes152 [] = {
g_atype152_0,
g_atype152_1,
g_atype152_2,
g_atype152_3,
g_atype152_4,
};

static const int32 cn_attr152 [] =
{
2156,
2267,
2268,
2241,
2242,
};

extern const char *names155[];
static const uint32 types155 [] =
{
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags155 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype155_0 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype155_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype155_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes155 [] = {
g_atype155_0,
g_atype155_1,
g_atype155_2,
};

static const int32 cn_attr155 [] =
{
2327,
2737,
2738,
};

extern const char *names156[];
static const uint32 types156 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags156 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype156_0 [] = {871,0xFF01,0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype156_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype156_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype156_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes156 [] = {
g_atype156_0,
g_atype156_1,
g_atype156_2,
g_atype156_3,
};

static const int32 cn_attr156 [] =
{
2742,
2327,
2737,
2738,
};

extern const char *names158[];
static const uint32 types158 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT32,
};

static const uint16 attr_flags158 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype158_0 [] = {0xFF01,38,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_2 [] = {253,0xFF01,13,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_3 [] = {0xFF01,129,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_4 [] = {0xFF01,136,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_5 [] = {0xFF01,273,0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_6 [] = {614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_7 [] = {273,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_8 [] = {346,0xFF01,0xFFF9,1,186,0xFF01,232,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_9 [] = {346,0xFF01,0xFFF9,2,186,0xFF01,232,221,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_10 [] = {0xFF01,855,0xFF01,10,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_11 [] = {0xFF01,253,0xFF01,0xFFF9,2,186,0xFF01,0,0xFF01,158,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_12 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_13 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_14 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_15 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_16 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype158_17 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes158 [] = {
g_atype158_0,
g_atype158_1,
g_atype158_2,
g_atype158_3,
g_atype158_4,
g_atype158_5,
g_atype158_6,
g_atype158_7,
g_atype158_8,
g_atype158_9,
g_atype158_10,
g_atype158_11,
g_atype158_12,
g_atype158_13,
g_atype158_14,
g_atype158_15,
g_atype158_16,
g_atype158_17,
};

static const int32 cn_attr158 [] =
{
1448,
1449,
1451,
1455,
1456,
1457,
1463,
2944,
2945,
2946,
2947,
2948,
1460,
2949,
2950,
2951,
2952,
1462,
};

extern const char *names159[];
static const uint32 types159 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags159 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype159_0 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_1 [] = {0xFF01,273,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_2 [] = {0xFF01,273,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_3 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_4 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_5 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_6 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_7 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_8 [] = {240,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_16 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_17 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype159_18 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes159 [] = {
g_atype159_0,
g_atype159_1,
g_atype159_2,
g_atype159_3,
g_atype159_4,
g_atype159_5,
g_atype159_6,
g_atype159_7,
g_atype159_8,
g_atype159_9,
g_atype159_10,
g_atype159_11,
g_atype159_12,
g_atype159_13,
g_atype159_14,
g_atype159_15,
g_atype159_16,
g_atype159_17,
g_atype159_18,
};

static const int32 cn_attr159 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
3064,
3065,
2327,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names160[];
static const uint32 types160 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags160 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype160_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_1 [] = {0xFF01,273,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_2 [] = {0xFF01,273,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_3 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_4 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_5 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_6 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype160_16 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes160 [] = {
g_atype160_0,
g_atype160_1,
g_atype160_2,
g_atype160_3,
g_atype160_4,
g_atype160_5,
g_atype160_6,
g_atype160_7,
g_atype160_8,
g_atype160_9,
g_atype160_10,
g_atype160_11,
g_atype160_12,
g_atype160_13,
g_atype160_14,
g_atype160_15,
g_atype160_16,
};

static const int32 cn_attr160 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
2327,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names161[];
static const uint32 types161 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT32,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags161 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype161_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_1 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_2 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_3 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_7 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_8 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_9 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_16 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype161_17 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes161 [] = {
g_atype161_0,
g_atype161_1,
g_atype161_2,
g_atype161_3,
g_atype161_4,
g_atype161_5,
g_atype161_6,
g_atype161_7,
g_atype161_8,
g_atype161_9,
g_atype161_10,
g_atype161_11,
g_atype161_12,
g_atype161_13,
g_atype161_14,
g_atype161_15,
g_atype161_16,
g_atype161_17,
};

static const int32 cn_attr161 [] =
{
3009,
3010,
3011,
3012,
2327,
3008,
3014,
958,
2972,
3024,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
3025,
};

extern const char *names163[];
static const uint32 types163 [] =
{
SK_POINTER,
};

static const uint16 attr_flags163 [] =
{0,};

static const EIF_TYPE_INDEX g_atype163_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes163 [] = {
g_atype163_0,
};

static const int32 cn_attr163 [] =
{
3163,
};

extern const char *names164[];
static const uint32 types164 [] =
{
SK_BOOL,
SK_INT32,
SK_POINTER,
SK_UINT64,
};

static const uint16 attr_flags164 [] =
{0,0,1,1,};

static const EIF_TYPE_INDEX g_atype164_0 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype164_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype164_2 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype164_3 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes164 [] = {
g_atype164_0,
g_atype164_1,
g_atype164_2,
g_atype164_3,
};

static const int32 cn_attr164 [] =
{
3177,
3176,
3175,
3259,
};

extern const char *names165[];
static const uint32 types165 [] =
{
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags165 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype165_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype165_1 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes165 [] = {
g_atype165_0,
g_atype165_1,
};

static const int32 cn_attr165 [] =
{
3275,
3269,
};

extern const char *names167[];
static const uint32 types167 [] =
{
SK_POINTER,
};

static const uint16 attr_flags167 [] =
{0,};

static const EIF_TYPE_INDEX g_atype167_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes167 [] = {
g_atype167_0,
};

static const int32 cn_attr167 [] =
{
3321,
};

extern const char *names168[];
static const uint32 types168 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_INT32,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags168 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype168_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_4 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype168_5 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes168 [] = {
g_atype168_0,
g_atype168_1,
g_atype168_2,
g_atype168_3,
g_atype168_4,
g_atype168_5,
};

static const int32 cn_attr168 [] =
{
3350,
3365,
3367,
3368,
3362,
3363,
};

extern const char *names169[];
static const uint32 types169 [] =
{
SK_POINTER,
};

static const uint16 attr_flags169 [] =
{0,};

static const EIF_TYPE_INDEX g_atype169_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes169 [] = {
g_atype169_0,
};

static const int32 cn_attr169 [] =
{
3393,
};

extern const char *names170[];
static const uint32 types170 [] =
{
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags170 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype170_0 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype170_1 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes170 [] = {
g_atype170_0,
g_atype170_1,
};

static const int32 cn_attr170 [] =
{
3400,
3408,
};

extern const char *names171[];
static const uint32 types171 [] =
{
SK_INT32,
};

static const uint16 attr_flags171 [] =
{0,};

static const EIF_TYPE_INDEX g_atype171_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes171 [] = {
g_atype171_0,
};

static const int32 cn_attr171 [] =
{
3418,
};

extern const char *names172[];
static const uint32 types172 [] =
{
SK_REF,
SK_CHAR8,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags172 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype172_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_1 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_2 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_3 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_4 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_5 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_6 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_9 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_10 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_11 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype172_12 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes172 [] = {
g_atype172_0,
g_atype172_1,
g_atype172_2,
g_atype172_3,
g_atype172_4,
g_atype172_5,
g_atype172_6,
g_atype172_7,
g_atype172_8,
g_atype172_9,
g_atype172_10,
g_atype172_11,
g_atype172_12,
};

static const int32 cn_attr172 [] =
{
3428,
3427,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3439,
3434,
3431,
3440,
};

extern const char *names173[];
static const uint32 types173 [] =
{
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags173 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype173_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_1 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_3 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_4 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_5 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_6 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_7 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_12 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_13 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_14 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_15 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype173_16 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes173 [] = {
g_atype173_0,
g_atype173_1,
g_atype173_2,
g_atype173_3,
g_atype173_4,
g_atype173_5,
g_atype173_6,
g_atype173_7,
g_atype173_8,
g_atype173_9,
g_atype173_10,
g_atype173_11,
g_atype173_12,
g_atype173_13,
g_atype173_14,
g_atype173_15,
g_atype173_16,
};

static const int32 cn_attr173 [] =
{
3428,
3427,
3522,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3510,
3511,
3439,
3514,
3434,
3431,
3440,
};

extern const char *names174[];
static const uint32 types174 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags174 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype174_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_3 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_4 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_7 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_8 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_9 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_10 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_11 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_15 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_16 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_17 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_18 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype174_19 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes174 [] = {
g_atype174_0,
g_atype174_1,
g_atype174_2,
g_atype174_3,
g_atype174_4,
g_atype174_5,
g_atype174_6,
g_atype174_7,
g_atype174_8,
g_atype174_9,
g_atype174_10,
g_atype174_11,
g_atype174_12,
g_atype174_13,
g_atype174_14,
g_atype174_15,
g_atype174_16,
g_atype174_17,
g_atype174_18,
g_atype174_19,
};

static const int32 cn_attr174 [] =
{
3428,
3616,
3618,
3427,
3534,
2327,
3680,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names175[];
static const uint32 types175 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags175 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype175_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_3 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_4 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_5 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_8 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_9 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_11 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_12 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_16 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_17 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_18 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_19 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype175_20 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes175 [] = {
g_atype175_0,
g_atype175_1,
g_atype175_2,
g_atype175_3,
g_atype175_4,
g_atype175_5,
g_atype175_6,
g_atype175_7,
g_atype175_8,
g_atype175_9,
g_atype175_10,
g_atype175_11,
g_atype175_12,
g_atype175_13,
g_atype175_14,
g_atype175_15,
g_atype175_16,
g_atype175_17,
g_atype175_18,
g_atype175_19,
g_atype175_20,
};

static const int32 cn_attr175 [] =
{
3428,
3616,
3618,
3685,
3427,
3534,
2327,
3680,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names176[];
static const uint32 types176 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags176 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype176_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_3 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_4 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_8 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_9 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_11 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_12 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_16 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_17 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_18 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_19 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype176_20 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes176 [] = {
g_atype176_0,
g_atype176_1,
g_atype176_2,
g_atype176_3,
g_atype176_4,
g_atype176_5,
g_atype176_6,
g_atype176_7,
g_atype176_8,
g_atype176_9,
g_atype176_10,
g_atype176_11,
g_atype176_12,
g_atype176_13,
g_atype176_14,
g_atype176_15,
g_atype176_16,
g_atype176_17,
g_atype176_18,
g_atype176_19,
g_atype176_20,
};

static const int32 cn_attr176 [] =
{
3428,
3616,
3618,
3427,
3534,
2327,
3680,
3694,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names177[];
static const uint32 types177 [] =
{
SK_UINT8,
SK_POINTER,
};

static const uint16 attr_flags177 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype177_0 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype177_1 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes177 [] = {
g_atype177_0,
g_atype177_1,
};

static const int32 cn_attr177 [] =
{
3711,
3712,
};

extern const char *names178[];
static const uint32 types178 [] =
{
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags178 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype178_0 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype178_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes178 [] = {
g_atype178_0,
g_atype178_1,
g_atype178_2,
g_atype178_3,
g_atype178_4,
};

static const int32 cn_attr178 [] =
{
2327,
2491,
3719,
3727,
3728,
};

extern const char *names179[];
static const uint32 types179 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags179 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype179_0 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype179_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype179_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype179_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes179 [] = {
g_atype179_0,
g_atype179_1,
g_atype179_2,
g_atype179_3,
};

static const int32 cn_attr179 [] =
{
3736,
3734,
3737,
3738,
};

extern const char *names180[];
static const uint32 types180 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags180 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype180_0 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype180_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes180 [] = {
g_atype180_0,
g_atype180_1,
};

static const int32 cn_attr180 [] =
{
2327,
2491,
};

extern const char *names181[];
static const uint32 types181 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags181 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype181_0 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype181_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes181 [] = {
g_atype181_0,
g_atype181_1,
};

static const int32 cn_attr181 [] =
{
2327,
2491,
};

extern const char *names183[];
static const uint32 types183 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags183 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype183_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_1 [] = {0xFF01,237,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype183_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes183 [] = {
g_atype183_0,
g_atype183_1,
g_atype183_2,
g_atype183_3,
g_atype183_4,
};

static const int32 cn_attr183 [] =
{
3791,
3799,
3792,
3794,
3798,
};

extern const char *names184[];
static const uint32 types184 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags184 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype184_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_1 [] = {0xFF01,230,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype184_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes184 [] = {
g_atype184_0,
g_atype184_1,
g_atype184_2,
g_atype184_3,
g_atype184_4,
};

static const int32 cn_attr184 [] =
{
3791,
3802,
3792,
3794,
3801,
};

extern const char *names186[];
static const uint32 types186 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags186 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype186_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype186_1 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype186_2 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes186 [] = {
g_atype186_0,
g_atype186_1,
g_atype186_2,
};

static const int32 cn_attr186 [] =
{
3851,
3855,
3850,
};

extern const char *names188[];
static const uint32 types188 [] =
{
SK_INT8,
};

static const uint16 attr_flags188 [] =
{0,};

static const EIF_TYPE_INDEX g_atype188_0 [] = {188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes188 [] = {
g_atype188_0,
};

static const int32 cn_attr188 [] =
{
4014,
};

extern const char *names189[];
static const uint32 types189 [] =
{
SK_INT8,
};

static const uint16 attr_flags189 [] =
{0,};

static const EIF_TYPE_INDEX g_atype189_0 [] = {188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes189 [] = {
g_atype189_0,
};

static const int32 cn_attr189 [] =
{
4014,
};

extern const char *names190[];
static const uint32 types190 [] =
{
SK_INT8,
};

static const uint16 attr_flags190 [] =
{0,};

static const EIF_TYPE_INDEX g_atype190_0 [] = {188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes190 [] = {
g_atype190_0,
};

static const int32 cn_attr190 [] =
{
4014,
};

extern const char *names191[];
static const uint32 types191 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags191 [] =
{0,};

static const EIF_TYPE_INDEX g_atype191_0 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes191 [] = {
g_atype191_0,
};

static const int32 cn_attr191 [] =
{
4066,
};

extern const char *names192[];
static const uint32 types192 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags192 [] =
{0,};

static const EIF_TYPE_INDEX g_atype192_0 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes192 [] = {
g_atype192_0,
};

static const int32 cn_attr192 [] =
{
4066,
};

extern const char *names193[];
static const uint32 types193 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags193 [] =
{0,};

static const EIF_TYPE_INDEX g_atype193_0 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes193 [] = {
g_atype193_0,
};

static const int32 cn_attr193 [] =
{
4066,
};

extern const char *names194[];
static const uint32 types194 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags194 [] =
{0,};

static const EIF_TYPE_INDEX g_atype194_0 [] = {194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes194 [] = {
g_atype194_0,
};

static const int32 cn_attr194 [] =
{
4098,
};

extern const char *names195[];
static const uint32 types195 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags195 [] =
{0,};

static const EIF_TYPE_INDEX g_atype195_0 [] = {194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes195 [] = {
g_atype195_0,
};

static const int32 cn_attr195 [] =
{
4098,
};

extern const char *names196[];
static const uint32 types196 [] =
{
SK_CHAR8,
};

static const uint16 attr_flags196 [] =
{0,};

static const EIF_TYPE_INDEX g_atype196_0 [] = {194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes196 [] = {
g_atype196_0,
};

static const int32 cn_attr196 [] =
{
4098,
};

extern const char *names197[];
static const uint32 types197 [] =
{
SK_INT64,
};

static const uint16 attr_flags197 [] =
{0,};

static const EIF_TYPE_INDEX g_atype197_0 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes197 [] = {
g_atype197_0,
};

static const int32 cn_attr197 [] =
{
4139,
};

extern const char *names198[];
static const uint32 types198 [] =
{
SK_INT64,
};

static const uint16 attr_flags198 [] =
{0,};

static const EIF_TYPE_INDEX g_atype198_0 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes198 [] = {
g_atype198_0,
};

static const int32 cn_attr198 [] =
{
4139,
};

extern const char *names199[];
static const uint32 types199 [] =
{
SK_INT64,
};

static const uint16 attr_flags199 [] =
{0,};

static const EIF_TYPE_INDEX g_atype199_0 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes199 [] = {
g_atype199_0,
};

static const int32 cn_attr199 [] =
{
4139,
};

extern const char *names200[];
static const uint32 types200 [] =
{
SK_BOOL,
};

static const uint16 attr_flags200 [] =
{0,};

static const EIF_TYPE_INDEX g_atype200_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes200 [] = {
g_atype200_0,
};

static const int32 cn_attr200 [] =
{
4190,
};

extern const char *names201[];
static const uint32 types201 [] =
{
SK_BOOL,
};

static const uint16 attr_flags201 [] =
{0,};

static const EIF_TYPE_INDEX g_atype201_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes201 [] = {
g_atype201_0,
};

static const int32 cn_attr201 [] =
{
4190,
};

extern const char *names202[];
static const uint32 types202 [] =
{
SK_BOOL,
};

static const uint16 attr_flags202 [] =
{0,};

static const EIF_TYPE_INDEX g_atype202_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes202 [] = {
g_atype202_0,
};

static const int32 cn_attr202 [] =
{
4190,
};

extern const char *names203[];
static const uint32 types203 [] =
{
SK_REAL64,
};

static const uint16 attr_flags203 [] =
{0,};

static const EIF_TYPE_INDEX g_atype203_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes203 [] = {
g_atype203_0,
};

static const int32 cn_attr203 [] =
{
4202,
};

extern const char *names204[];
static const uint32 types204 [] =
{
SK_REAL64,
};

static const uint16 attr_flags204 [] =
{0,};

static const EIF_TYPE_INDEX g_atype204_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes204 [] = {
g_atype204_0,
};

static const int32 cn_attr204 [] =
{
4202,
};

extern const char *names205[];
static const uint32 types205 [] =
{
SK_REAL64,
};

static const uint16 attr_flags205 [] =
{0,};

static const EIF_TYPE_INDEX g_atype205_0 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes205 [] = {
g_atype205_0,
};

static const int32 cn_attr205 [] =
{
4202,
};

extern const char *names206[];
static const uint32 types206 [] =
{
SK_UINT8,
};

static const uint16 attr_flags206 [] =
{0,};

static const EIF_TYPE_INDEX g_atype206_0 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes206 [] = {
g_atype206_0,
};

static const int32 cn_attr206 [] =
{
4229,
};

extern const char *names207[];
static const uint32 types207 [] =
{
SK_UINT8,
};

static const uint16 attr_flags207 [] =
{0,};

static const EIF_TYPE_INDEX g_atype207_0 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes207 [] = {
g_atype207_0,
};

static const int32 cn_attr207 [] =
{
4229,
};

extern const char *names208[];
static const uint32 types208 [] =
{
SK_UINT8,
};

static const uint16 attr_flags208 [] =
{0,};

static const EIF_TYPE_INDEX g_atype208_0 [] = {206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes208 [] = {
g_atype208_0,
};

static const int32 cn_attr208 [] =
{
4229,
};

extern const char *names209[];
static const uint32 types209 [] =
{
SK_REAL32,
};

static const uint16 attr_flags209 [] =
{0,};

static const EIF_TYPE_INDEX g_atype209_0 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes209 [] = {
g_atype209_0,
};

static const int32 cn_attr209 [] =
{
4278,
};

extern const char *names210[];
static const uint32 types210 [] =
{
SK_REAL32,
};

static const uint16 attr_flags210 [] =
{0,};

static const EIF_TYPE_INDEX g_atype210_0 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes210 [] = {
g_atype210_0,
};

static const int32 cn_attr210 [] =
{
4278,
};

extern const char *names211[];
static const uint32 types211 [] =
{
SK_REAL32,
};

static const uint16 attr_flags211 [] =
{0,};

static const EIF_TYPE_INDEX g_atype211_0 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes211 [] = {
g_atype211_0,
};

static const int32 cn_attr211 [] =
{
4278,
};

extern const char *names212[];
static const uint32 types212 [] =
{
SK_INT16,
};

static const uint16 attr_flags212 [] =
{0,};

static const EIF_TYPE_INDEX g_atype212_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes212 [] = {
g_atype212_0,
};

static const int32 cn_attr212 [] =
{
4305,
};

extern const char *names213[];
static const uint32 types213 [] =
{
SK_INT16,
};

static const uint16 attr_flags213 [] =
{0,};

static const EIF_TYPE_INDEX g_atype213_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes213 [] = {
g_atype213_0,
};

static const int32 cn_attr213 [] =
{
4305,
};

extern const char *names214[];
static const uint32 types214 [] =
{
SK_INT16,
};

static const uint16 attr_flags214 [] =
{0,};

static const EIF_TYPE_INDEX g_atype214_0 [] = {212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes214 [] = {
g_atype214_0,
};

static const int32 cn_attr214 [] =
{
4305,
};

extern const char *names215[];
static const uint32 types215 [] =
{
SK_UINT32,
};

static const uint16 attr_flags215 [] =
{0,};

static const EIF_TYPE_INDEX g_atype215_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes215 [] = {
g_atype215_0,
};

static const int32 cn_attr215 [] =
{
4357,
};

extern const char *names216[];
static const uint32 types216 [] =
{
SK_UINT32,
};

static const uint16 attr_flags216 [] =
{0,};

static const EIF_TYPE_INDEX g_atype216_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes216 [] = {
g_atype216_0,
};

static const int32 cn_attr216 [] =
{
4357,
};

extern const char *names217[];
static const uint32 types217 [] =
{
SK_UINT32,
};

static const uint16 attr_flags217 [] =
{0,};

static const EIF_TYPE_INDEX g_atype217_0 [] = {215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes217 [] = {
g_atype217_0,
};

static const int32 cn_attr217 [] =
{
4357,
};

extern const char *names218[];
static const uint32 types218 [] =
{
SK_UINT16,
};

static const uint16 attr_flags218 [] =
{0,};

static const EIF_TYPE_INDEX g_atype218_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes218 [] = {
g_atype218_0,
};

static const int32 cn_attr218 [] =
{
4405,
};

extern const char *names219[];
static const uint32 types219 [] =
{
SK_UINT16,
};

static const uint16 attr_flags219 [] =
{0,};

static const EIF_TYPE_INDEX g_atype219_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes219 [] = {
g_atype219_0,
};

static const int32 cn_attr219 [] =
{
4405,
};

extern const char *names220[];
static const uint32 types220 [] =
{
SK_UINT16,
};

static const uint16 attr_flags220 [] =
{0,};

static const EIF_TYPE_INDEX g_atype220_0 [] = {218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes220 [] = {
g_atype220_0,
};

static const int32 cn_attr220 [] =
{
4405,
};

extern const char *names221[];
static const uint32 types221 [] =
{
SK_INT32,
};

static const uint16 attr_flags221 [] =
{0,};

static const EIF_TYPE_INDEX g_atype221_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes221 [] = {
g_atype221_0,
};

static const int32 cn_attr221 [] =
{
4454,
};

extern const char *names222[];
static const uint32 types222 [] =
{
SK_INT32,
};

static const uint16 attr_flags222 [] =
{0,};

static const EIF_TYPE_INDEX g_atype222_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes222 [] = {
g_atype222_0,
};

static const int32 cn_attr222 [] =
{
4454,
};

extern const char *names223[];
static const uint32 types223 [] =
{
SK_INT32,
};

static const uint16 attr_flags223 [] =
{0,};

static const EIF_TYPE_INDEX g_atype223_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes223 [] = {
g_atype223_0,
};

static const int32 cn_attr223 [] =
{
4454,
};

extern const char *names224[];
static const uint32 types224 [] =
{
SK_UINT64,
};

static const uint16 attr_flags224 [] =
{0,};

static const EIF_TYPE_INDEX g_atype224_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes224 [] = {
g_atype224_0,
};

static const int32 cn_attr224 [] =
{
4506,
};

extern const char *names225[];
static const uint32 types225 [] =
{
SK_UINT64,
};

static const uint16 attr_flags225 [] =
{0,};

static const EIF_TYPE_INDEX g_atype225_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes225 [] = {
g_atype225_0,
};

static const int32 cn_attr225 [] =
{
4506,
};

extern const char *names226[];
static const uint32 types226 [] =
{
SK_UINT64,
};

static const uint16 attr_flags226 [] =
{0,};

static const EIF_TYPE_INDEX g_atype226_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes226 [] = {
g_atype226_0,
};

static const int32 cn_attr226 [] =
{
4506,
};

extern const char *names227[];
static const uint32 types227 [] =
{
SK_POINTER,
};

static const uint16 attr_flags227 [] =
{0,};

static const EIF_TYPE_INDEX g_atype227_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes227 [] = {
g_atype227_0,
};

static const int32 cn_attr227 [] =
{
4554,
};

extern const char *names228[];
static const uint32 types228 [] =
{
SK_POINTER,
};

static const uint16 attr_flags228 [] =
{0,};

static const EIF_TYPE_INDEX g_atype228_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes228 [] = {
g_atype228_0,
};

static const int32 cn_attr228 [] =
{
4554,
};

extern const char *names229[];
static const uint32 types229 [] =
{
SK_POINTER,
};

static const uint16 attr_flags229 [] =
{0,};

static const EIF_TYPE_INDEX g_atype229_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes229 [] = {
g_atype229_0,
};

static const int32 cn_attr229 [] =
{
4554,
};

extern const char *names230[];
static const uint32 types230 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags230 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype230_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype230_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes230 [] = {
g_atype230_0,
g_atype230_1,
};

static const int32 cn_attr230 [] =
{
4714,
4715,
};

extern const char *names231[];
static const uint32 types231 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags231 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype231_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype231_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype231_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype231_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes231 [] = {
g_atype231_0,
g_atype231_1,
g_atype231_2,
g_atype231_3,
};

static const int32 cn_attr231 [] =
{
4745,
4714,
4715,
4748,
};

extern const char *names232[];
static const uint32 types232 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags232 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype232_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype232_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes232 [] = {
g_atype232_0,
g_atype232_1,
};

static const int32 cn_attr232 [] =
{
4714,
4715,
};

extern const char *names233[];
static const uint32 types233 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags233 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype233_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype233_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes233 [] = {
g_atype233_0,
g_atype233_1,
g_atype233_2,
g_atype233_3,
g_atype233_4,
};

static const int32 cn_attr233 [] =
{
4745,
2327,
4714,
4715,
4748,
};

extern const char *names234[];
static const uint32 types234 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags234 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype234_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype234_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes234 [] = {
g_atype234_0,
g_atype234_1,
g_atype234_2,
g_atype234_3,
g_atype234_4,
g_atype234_5,
};

static const int32 cn_attr234 [] =
{
4745,
2327,
4714,
4715,
4748,
4833,
};

extern const char *names235[];
static const uint32 types235 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags235 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype235_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype235_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes235 [] = {
g_atype235_0,
g_atype235_1,
g_atype235_2,
g_atype235_3,
g_atype235_4,
};

static const int32 cn_attr235 [] =
{
4745,
2327,
4714,
4715,
4748,
};

extern const char *names236[];
static const uint32 types236 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags236 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype236_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype236_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes236 [] = {
g_atype236_0,
g_atype236_1,
g_atype236_2,
g_atype236_3,
g_atype236_4,
};

static const int32 cn_attr236 [] =
{
4745,
2327,
4714,
4715,
4748,
};

extern const char *names237[];
static const uint32 types237 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags237 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype237_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype237_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes237 [] = {
g_atype237_0,
g_atype237_1,
g_atype237_2,
g_atype237_3,
g_atype237_4,
};

static const int32 cn_attr237 [] =
{
4745,
2327,
4714,
4715,
4748,
};

extern const char *names238[];
static const uint32 types238 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags238 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype238_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype238_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype238_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype238_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes238 [] = {
g_atype238_0,
g_atype238_1,
g_atype238_2,
g_atype238_3,
};

static const int32 cn_attr238 [] =
{
4888,
4714,
4715,
4891,
};

extern const char *names239[];
static const uint32 types239 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags239 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype239_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype239_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes239 [] = {
g_atype239_0,
g_atype239_1,
g_atype239_2,
g_atype239_3,
g_atype239_4,
};

static const int32 cn_attr239 [] =
{
4888,
2327,
4714,
4715,
4891,
};

extern const char *names240[];
static const uint32 types240 [] =
{
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags240 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype240_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype240_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes240 [] = {
g_atype240_0,
g_atype240_1,
};

static const int32 cn_attr240 [] =
{
4714,
4715,
};

extern const char *names241[];
static const uint32 types241 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags241 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype241_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype241_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes241 [] = {
g_atype241_0,
g_atype241_1,
g_atype241_2,
g_atype241_3,
g_atype241_4,
};

static const int32 cn_attr241 [] =
{
4745,
4714,
4715,
4748,
4935,
};

extern const char *names242[];
static const uint32 types242 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags242 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype242_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype242_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes242 [] = {
g_atype242_0,
g_atype242_1,
g_atype242_2,
g_atype242_3,
g_atype242_4,
};

static const int32 cn_attr242 [] =
{
4888,
4714,
4715,
4891,
4942,
};

extern const char *names243[];
static const uint32 types243 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags243 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype243_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_3 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_4 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_8 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_9 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_10 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_11 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_12 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_16 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_17 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_18 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_19 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype243_20 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes243 [] = {
g_atype243_0,
g_atype243_1,
g_atype243_2,
g_atype243_3,
g_atype243_4,
g_atype243_5,
g_atype243_6,
g_atype243_7,
g_atype243_8,
g_atype243_9,
g_atype243_10,
g_atype243_11,
g_atype243_12,
g_atype243_13,
g_atype243_14,
g_atype243_15,
g_atype243_16,
g_atype243_17,
g_atype243_18,
g_atype243_19,
g_atype243_20,
};

static const int32 cn_attr243 [] =
{
3428,
3616,
3618,
3427,
3534,
2327,
3680,
3694,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names244[];
static const uint32 types244 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags244 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype244_0 [] = {253,0xFF01,13,0xFFFF};
static const EIF_TYPE_INDEX g_atype244_1 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype244_2 [] = {0xFF01,238,0xFFFF};
static const EIF_TYPE_INDEX g_atype244_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype244_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes244 [] = {
g_atype244_0,
g_atype244_1,
g_atype244_2,
g_atype244_3,
g_atype244_4,
};

static const int32 cn_attr244 [] =
{
967,
4962,
4964,
4965,
4963,
};

extern const char *names245[];
static const uint32 types245 [] =
{
SK_REF,
};

static const uint16 attr_flags245 [] =
{0,};

static const EIF_TYPE_INDEX g_atype245_0 [] = {0xFF01,544,200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes245 [] = {
g_atype245_0,
};

static const int32 cn_attr245 [] =
{
2156,
};

extern const char *names246[];
static const uint32 types246 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags246 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype246_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype246_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes246 [] = {
g_atype246_0,
g_atype246_1,
};

static const int32 cn_attr246 [] =
{
3889,
3890,
};

extern const char *names247[];
static const uint32 types247 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags247 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype247_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype247_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes247 [] = {
g_atype247_0,
g_atype247_1,
};

static const int32 cn_attr247 [] =
{
3889,
3890,
};

extern const char *names248[];
static const uint32 types248 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags248 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype248_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype248_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype248_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype248_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes248 [] = {
g_atype248_0,
g_atype248_1,
g_atype248_2,
g_atype248_3,
};

static const int32 cn_attr248 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names249[];
static const uint32 types249 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags249 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype249_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_2 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_3 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_9 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype249_11 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes249 [] = {
g_atype249_0,
g_atype249_1,
g_atype249_2,
g_atype249_3,
g_atype249_4,
g_atype249_5,
g_atype249_6,
g_atype249_7,
g_atype249_8,
g_atype249_9,
g_atype249_10,
g_atype249_11,
};

static const int32 cn_attr249 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names250[];
static const uint32 types250 [] =
{
SK_POINTER,
};

static const uint16 attr_flags250 [] =
{0,};

static const EIF_TYPE_INDEX g_atype250_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes250 [] = {
g_atype250_0,
};

static const int32 cn_attr250 [] =
{
4554,
};

extern const char *names251[];
static const uint32 types251 [] =
{
SK_POINTER,
};

static const uint16 attr_flags251 [] =
{0,};

static const EIF_TYPE_INDEX g_atype251_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes251 [] = {
g_atype251_0,
};

static const int32 cn_attr251 [] =
{
4554,
};

extern const char *names252[];
static const uint32 types252 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags252 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype252_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype252_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes252 [] = {
g_atype252_0,
g_atype252_1,
};

static const int32 cn_attr252 [] =
{
3889,
3890,
};

extern const char *names253[];
static const uint32 types253 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags253 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype253_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_2 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_3 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_9 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype253_11 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes253 [] = {
g_atype253_0,
g_atype253_1,
g_atype253_2,
g_atype253_3,
g_atype253_4,
g_atype253_5,
g_atype253_6,
g_atype253_7,
g_atype253_8,
g_atype253_9,
g_atype253_10,
g_atype253_11,
};

static const int32 cn_attr253 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names254[];
static const uint32 types254 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags254 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype254_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype254_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes254 [] = {
g_atype254_0,
g_atype254_1,
g_atype254_2,
};

static const int32 cn_attr254 [] =
{
2156,
2327,
3094,
};

extern const char *names256[];
static const uint32 types256 [] =
{
SK_BOOL,
};

static const uint16 attr_flags256 [] =
{0,};

static const EIF_TYPE_INDEX g_atype256_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes256 [] = {
g_atype256_0,
};

static const int32 cn_attr256 [] =
{
2327,
};

extern const char *names259[];
static const uint32 types259 [] =
{
SK_BOOL,
};

static const uint16 attr_flags259 [] =
{0,};

static const EIF_TYPE_INDEX g_atype259_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes259 [] = {
g_atype259_0,
};

static const int32 cn_attr259 [] =
{
2327,
};

extern const char *names260[];
static const uint32 types260 [] =
{
SK_BOOL,
};

static const uint16 attr_flags260 [] =
{0,};

static const EIF_TYPE_INDEX g_atype260_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes260 [] = {
g_atype260_0,
};

static const int32 cn_attr260 [] =
{
2327,
};

extern const char *names261[];
static const uint32 types261 [] =
{
SK_BOOL,
};

static const uint16 attr_flags261 [] =
{0,};

static const EIF_TYPE_INDEX g_atype261_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes261 [] = {
g_atype261_0,
};

static const int32 cn_attr261 [] =
{
2327,
};

extern const char *names262[];
static const uint32 types262 [] =
{
SK_BOOL,
};

static const uint16 attr_flags262 [] =
{0,};

static const EIF_TYPE_INDEX g_atype262_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes262 [] = {
g_atype262_0,
};

static const int32 cn_attr262 [] =
{
2327,
};

extern const char *names265[];
static const uint32 types265 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags265 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype265_0 [] = {0xFF01,262,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype265_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype265_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype265_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype265_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype265_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype265_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes265 [] = {
g_atype265_0,
g_atype265_1,
g_atype265_2,
g_atype265_3,
g_atype265_4,
g_atype265_5,
g_atype265_6,
};

static const int32 cn_attr265 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names267[];
static const uint32 types267 [] =
{
SK_BOOL,
};

static const uint16 attr_flags267 [] =
{0,};

static const EIF_TYPE_INDEX g_atype267_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes267 [] = {
g_atype267_0,
};

static const int32 cn_attr267 [] =
{
2327,
};

extern const char *names268[];
static const uint32 types268 [] =
{
SK_BOOL,
};

static const uint16 attr_flags268 [] =
{0,};

static const EIF_TYPE_INDEX g_atype268_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes268 [] = {
g_atype268_0,
};

static const int32 cn_attr268 [] =
{
2327,
};

extern const char *names269[];
static const uint32 types269 [] =
{
SK_BOOL,
};

static const uint16 attr_flags269 [] =
{0,};

static const EIF_TYPE_INDEX g_atype269_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes269 [] = {
g_atype269_0,
};

static const int32 cn_attr269 [] =
{
2327,
};

extern const char *names270[];
static const uint32 types270 [] =
{
SK_BOOL,
};

static const uint16 attr_flags270 [] =
{0,};

static const EIF_TYPE_INDEX g_atype270_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes270 [] = {
g_atype270_0,
};

static const int32 cn_attr270 [] =
{
2327,
};

extern const char *names271[];
static const uint32 types271 [] =
{
SK_BOOL,
};

static const uint16 attr_flags271 [] =
{0,};

static const EIF_TYPE_INDEX g_atype271_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes271 [] = {
g_atype271_0,
};

static const int32 cn_attr271 [] =
{
2327,
};

extern const char *names272[];
static const uint32 types272 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags272 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype272_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_2 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_3 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_11 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype272_12 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes272 [] = {
g_atype272_0,
g_atype272_1,
g_atype272_2,
g_atype272_3,
g_atype272_4,
g_atype272_5,
g_atype272_6,
g_atype272_7,
g_atype272_8,
g_atype272_9,
g_atype272_10,
g_atype272_11,
g_atype272_12,
};

static const int32 cn_attr272 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4615,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names273[];
static const uint32 types273 [] =
{
SK_BOOL,
};

static const uint16 attr_flags273 [] =
{0,};

static const EIF_TYPE_INDEX g_atype273_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes273 [] = {
g_atype273_0,
};

static const int32 cn_attr273 [] =
{
2327,
};

extern const char *names275[];
static const uint32 types275 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags275 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype275_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype275_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes275 [] = {
g_atype275_0,
g_atype275_1,
g_atype275_2,
};

static const int32 cn_attr275 [] =
{
3791,
3792,
3794,
};

extern const char *names276[];
static const uint32 types276 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags276 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype276_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype276_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype276_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes276 [] = {
g_atype276_0,
g_atype276_1,
g_atype276_2,
};

static const int32 cn_attr276 [] =
{
3791,
3792,
3794,
};

extern const char *names277[];
static const uint32 types277 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags277 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype277_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype277_1 [] = {0xFF01,247,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype277_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype277_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype277_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes277 [] = {
g_atype277_0,
g_atype277_1,
g_atype277_2,
g_atype277_3,
g_atype277_4,
};

static const int32 cn_attr277 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names278[];
static const uint32 types278 [] =
{
SK_REF,
};

static const uint16 attr_flags278 [] =
{0,};

static const EIF_TYPE_INDEX g_atype278_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes278 [] = {
g_atype278_0,
};

static const int32 cn_attr278 [] =
{
2156,
};

extern const char *names279[];
static const uint32 types279 [] =
{
SK_BOOL,
};

static const uint16 attr_flags279 [] =
{0,};

static const EIF_TYPE_INDEX g_atype279_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes279 [] = {
g_atype279_0,
};

static const int32 cn_attr279 [] =
{
2327,
};

extern const char *names280[];
static const uint32 types280 [] =
{
SK_BOOL,
};

static const uint16 attr_flags280 [] =
{0,};

static const EIF_TYPE_INDEX g_atype280_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes280 [] = {
g_atype280_0,
};

static const int32 cn_attr280 [] =
{
2327,
};

extern const char *names281[];
static const uint32 types281 [] =
{
SK_BOOL,
};

static const uint16 attr_flags281 [] =
{0,};

static const EIF_TYPE_INDEX g_atype281_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes281 [] = {
g_atype281_0,
};

static const int32 cn_attr281 [] =
{
2327,
};

extern const char *names282[];
static const uint32 types282 [] =
{
SK_BOOL,
};

static const uint16 attr_flags282 [] =
{0,};

static const EIF_TYPE_INDEX g_atype282_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes282 [] = {
g_atype282_0,
};

static const int32 cn_attr282 [] =
{
2327,
};

extern const char *names283[];
static const uint32 types283 [] =
{
SK_BOOL,
};

static const uint16 attr_flags283 [] =
{0,};

static const EIF_TYPE_INDEX g_atype283_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes283 [] = {
g_atype283_0,
};

static const int32 cn_attr283 [] =
{
2327,
};

extern const char *names284[];
static const uint32 types284 [] =
{
SK_BOOL,
};

static const uint16 attr_flags284 [] =
{0,};

static const EIF_TYPE_INDEX g_atype284_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes284 [] = {
g_atype284_0,
};

static const int32 cn_attr284 [] =
{
2327,
};

extern const char *names285[];
static const uint32 types285 [] =
{
SK_BOOL,
};

static const uint16 attr_flags285 [] =
{0,};

static const EIF_TYPE_INDEX g_atype285_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes285 [] = {
g_atype285_0,
};

static const int32 cn_attr285 [] =
{
2327,
};

extern const char *names286[];
static const uint32 types286 [] =
{
SK_BOOL,
};

static const uint16 attr_flags286 [] =
{0,};

static const EIF_TYPE_INDEX g_atype286_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes286 [] = {
g_atype286_0,
};

static const int32 cn_attr286 [] =
{
2327,
};

extern const char *names287[];
static const uint32 types287 [] =
{
SK_BOOL,
};

static const uint16 attr_flags287 [] =
{0,};

static const EIF_TYPE_INDEX g_atype287_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes287 [] = {
g_atype287_0,
};

static const int32 cn_attr287 [] =
{
2327,
};

extern const char *names288[];
static const uint32 types288 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags288 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype288_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype288_1 [] = {0xFF01,253,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype288_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype288_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes288 [] = {
g_atype288_0,
g_atype288_1,
g_atype288_2,
g_atype288_3,
};

static const int32 cn_attr288 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names289[];
static const uint32 types289 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags289 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype289_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype289_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes289 [] = {
g_atype289_0,
g_atype289_1,
};

static const int32 cn_attr289 [] =
{
3889,
3890,
};

extern const char *names290[];
static const uint32 types290 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags290 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype290_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype290_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes290 [] = {
g_atype290_0,
g_atype290_1,
};

static const int32 cn_attr290 [] =
{
3889,
3890,
};

extern const char *names291[];
static const uint32 types291 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags291 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype291_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype291_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes291 [] = {
g_atype291_0,
g_atype291_1,
};

static const int32 cn_attr291 [] =
{
3889,
3890,
};

extern const char *names292[];
static const uint32 types292 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags292 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype292_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype292_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes292 [] = {
g_atype292_0,
g_atype292_1,
};

static const int32 cn_attr292 [] =
{
3889,
3890,
};

extern const char *names293[];
static const uint32 types293 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags293 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype293_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype293_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes293 [] = {
g_atype293_0,
g_atype293_1,
};

static const int32 cn_attr293 [] =
{
3889,
3890,
};

extern const char *names294[];
static const uint32 types294 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags294 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype294_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype294_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes294 [] = {
g_atype294_0,
g_atype294_1,
};

static const int32 cn_attr294 [] =
{
3889,
3890,
};

extern const char *names295[];
static const uint32 types295 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags295 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype295_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype295_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes295 [] = {
g_atype295_0,
g_atype295_1,
};

static const int32 cn_attr295 [] =
{
3889,
3890,
};

extern const char *names296[];
static const uint32 types296 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags296 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype296_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype296_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes296 [] = {
g_atype296_0,
g_atype296_1,
};

static const int32 cn_attr296 [] =
{
3889,
3890,
};

extern const char *names297[];
static const uint32 types297 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags297 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype297_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype297_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes297 [] = {
g_atype297_0,
g_atype297_1,
};

static const int32 cn_attr297 [] =
{
3889,
3890,
};

extern const char *names298[];
static const uint32 types298 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags298 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype298_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype298_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes298 [] = {
g_atype298_0,
g_atype298_1,
};

static const int32 cn_attr298 [] =
{
3889,
3890,
};

extern const char *names299[];
static const uint32 types299 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags299 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype299_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype299_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes299 [] = {
g_atype299_0,
g_atype299_1,
};

static const int32 cn_attr299 [] =
{
3889,
3890,
};

extern const char *names300[];
static const uint32 types300 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags300 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype300_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype300_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes300 [] = {
g_atype300_0,
g_atype300_1,
};

static const int32 cn_attr300 [] =
{
3889,
3890,
};

extern const char *names301[];
static const uint32 types301 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags301 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype301_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype301_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes301 [] = {
g_atype301_0,
g_atype301_1,
};

static const int32 cn_attr301 [] =
{
3889,
3890,
};

extern const char *names302[];
static const uint32 types302 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags302 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype302_0 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype302_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype302_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype302_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes302 [] = {
g_atype302_0,
g_atype302_1,
g_atype302_2,
g_atype302_3,
};

static const int32 cn_attr302 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names303[];
static const uint32 types303 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags303 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype303_0 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype303_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype303_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes303 [] = {
g_atype303_0,
g_atype303_1,
g_atype303_2,
};

static const int32 cn_attr303 [] =
{
2156,
2327,
3094,
};

extern const char *names304[];
static const uint32 types304 [] =
{
SK_BOOL,
};

static const uint16 attr_flags304 [] =
{0,};

static const EIF_TYPE_INDEX g_atype304_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes304 [] = {
g_atype304_0,
};

static const int32 cn_attr304 [] =
{
2327,
};

extern const char *names305[];
static const uint32 types305 [] =
{
SK_BOOL,
};

static const uint16 attr_flags305 [] =
{0,};

static const EIF_TYPE_INDEX g_atype305_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes305 [] = {
g_atype305_0,
};

static const int32 cn_attr305 [] =
{
2327,
};

extern const char *names306[];
static const uint32 types306 [] =
{
SK_BOOL,
};

static const uint16 attr_flags306 [] =
{0,};

static const EIF_TYPE_INDEX g_atype306_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes306 [] = {
g_atype306_0,
};

static const int32 cn_attr306 [] =
{
2327,
};

extern const char *names307[];
static const uint32 types307 [] =
{
SK_BOOL,
};

static const uint16 attr_flags307 [] =
{0,};

static const EIF_TYPE_INDEX g_atype307_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes307 [] = {
g_atype307_0,
};

static const int32 cn_attr307 [] =
{
2327,
};

extern const char *names309[];
static const uint32 types309 [] =
{
SK_BOOL,
};

static const uint16 attr_flags309 [] =
{0,};

static const EIF_TYPE_INDEX g_atype309_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes309 [] = {
g_atype309_0,
};

static const int32 cn_attr309 [] =
{
2327,
};

extern const char *names311[];
static const uint32 types311 [] =
{
SK_BOOL,
};

static const uint16 attr_flags311 [] =
{0,};

static const EIF_TYPE_INDEX g_atype311_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes311 [] = {
g_atype311_0,
};

static const int32 cn_attr311 [] =
{
2327,
};

extern const char *names314[];
static const uint32 types314 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags314 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype314_0 [] = {0xFF01,311,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype314_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype314_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype314_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype314_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype314_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype314_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes314 [] = {
g_atype314_0,
g_atype314_1,
g_atype314_2,
g_atype314_3,
g_atype314_4,
g_atype314_5,
g_atype314_6,
};

static const int32 cn_attr314 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names316[];
static const uint32 types316 [] =
{
SK_BOOL,
};

static const uint16 attr_flags316 [] =
{0,};

static const EIF_TYPE_INDEX g_atype316_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes316 [] = {
g_atype316_0,
};

static const int32 cn_attr316 [] =
{
2327,
};

extern const char *names317[];
static const uint32 types317 [] =
{
SK_BOOL,
};

static const uint16 attr_flags317 [] =
{0,};

static const EIF_TYPE_INDEX g_atype317_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes317 [] = {
g_atype317_0,
};

static const int32 cn_attr317 [] =
{
2327,
};

extern const char *names318[];
static const uint32 types318 [] =
{
SK_BOOL,
};

static const uint16 attr_flags318 [] =
{0,};

static const EIF_TYPE_INDEX g_atype318_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes318 [] = {
g_atype318_0,
};

static const int32 cn_attr318 [] =
{
2327,
};

extern const char *names319[];
static const uint32 types319 [] =
{
SK_BOOL,
};

static const uint16 attr_flags319 [] =
{0,};

static const EIF_TYPE_INDEX g_atype319_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes319 [] = {
g_atype319_0,
};

static const int32 cn_attr319 [] =
{
2327,
};

extern const char *names320[];
static const uint32 types320 [] =
{
SK_BOOL,
};

static const uint16 attr_flags320 [] =
{0,};

static const EIF_TYPE_INDEX g_atype320_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes320 [] = {
g_atype320_0,
};

static const int32 cn_attr320 [] =
{
2327,
};

extern const char *names321[];
static const uint32 types321 [] =
{
SK_BOOL,
};

static const uint16 attr_flags321 [] =
{0,};

static const EIF_TYPE_INDEX g_atype321_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes321 [] = {
g_atype321_0,
};

static const int32 cn_attr321 [] =
{
2327,
};

extern const char *names322[];
static const uint32 types322 [] =
{
SK_BOOL,
};

static const uint16 attr_flags322 [] =
{0,};

static const EIF_TYPE_INDEX g_atype322_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes322 [] = {
g_atype322_0,
};

static const int32 cn_attr322 [] =
{
2327,
};

extern const char *names323[];
static const uint32 types323 [] =
{
SK_BOOL,
};

static const uint16 attr_flags323 [] =
{0,};

static const EIF_TYPE_INDEX g_atype323_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes323 [] = {
g_atype323_0,
};

static const int32 cn_attr323 [] =
{
2327,
};

extern const char *names324[];
static const uint32 types324 [] =
{
SK_BOOL,
};

static const uint16 attr_flags324 [] =
{0,};

static const EIF_TYPE_INDEX g_atype324_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes324 [] = {
g_atype324_0,
};

static const int32 cn_attr324 [] =
{
2327,
};

extern const char *names325[];
static const uint32 types325 [] =
{
SK_BOOL,
};

static const uint16 attr_flags325 [] =
{0,};

static const EIF_TYPE_INDEX g_atype325_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes325 [] = {
g_atype325_0,
};

static const int32 cn_attr325 [] =
{
2327,
};

extern const char *names326[];
static const uint32 types326 [] =
{
SK_BOOL,
};

static const uint16 attr_flags326 [] =
{0,};

static const EIF_TYPE_INDEX g_atype326_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes326 [] = {
g_atype326_0,
};

static const int32 cn_attr326 [] =
{
2327,
};

extern const char *names327[];
static const uint32 types327 [] =
{
SK_BOOL,
};

static const uint16 attr_flags327 [] =
{0,};

static const EIF_TYPE_INDEX g_atype327_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes327 [] = {
g_atype327_0,
};

static const int32 cn_attr327 [] =
{
2327,
};

extern const char *names328[];
static const uint32 types328 [] =
{
SK_REF,
};

static const uint16 attr_flags328 [] =
{0,};

static const EIF_TYPE_INDEX g_atype328_0 [] = {0xFF01,328,227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes328 [] = {
g_atype328_0,
};

static const int32 cn_attr328 [] =
{
2156,
};

extern const char *names330[];
static const uint32 types330 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags330 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype330_0 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype330_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype330_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes330 [] = {
g_atype330_0,
g_atype330_1,
g_atype330_2,
};

static const int32 cn_attr330 [] =
{
3791,
3792,
3794,
};

extern const char *names331[];
static const uint32 types331 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags331 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype331_0 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype331_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype331_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes331 [] = {
g_atype331_0,
g_atype331_1,
g_atype331_2,
};

static const int32 cn_attr331 [] =
{
3791,
3792,
3794,
};

extern const char *names333[];
static const uint32 types333 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags333 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype333_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype333_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes333 [] = {
g_atype333_0,
g_atype333_1,
};

static const int32 cn_attr333 [] =
{
3889,
3890,
};

extern const char *names334[];
static const uint32 types334 [] =
{
SK_BOOL,
};

static const uint16 attr_flags334 [] =
{0,};

static const EIF_TYPE_INDEX g_atype334_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes334 [] = {
g_atype334_0,
};

static const int32 cn_attr334 [] =
{
2327,
};

extern const char *names335[];
static const uint32 types335 [] =
{
SK_BOOL,
};

static const uint16 attr_flags335 [] =
{0,};

static const EIF_TYPE_INDEX g_atype335_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes335 [] = {
g_atype335_0,
};

static const int32 cn_attr335 [] =
{
2327,
};

extern const char *names336[];
static const uint32 types336 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags336 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype336_0 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype336_1 [] = {0xFF01,302,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype336_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype336_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes336 [] = {
g_atype336_0,
g_atype336_1,
g_atype336_2,
g_atype336_3,
};

static const int32 cn_attr336 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names337[];
static const uint32 types337 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags337 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype337_0 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype337_1 [] = {0xFF01,301,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype337_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype337_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype337_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes337 [] = {
g_atype337_0,
g_atype337_1,
g_atype337_2,
g_atype337_3,
g_atype337_4,
};

static const int32 cn_attr337 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names338[];
static const uint32 types338 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags338 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype338_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype338_5 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes338 [] = {
g_atype338_0,
g_atype338_1,
g_atype338_2,
g_atype338_3,
g_atype338_4,
g_atype338_5,
};

static const int32 cn_attr338 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names339[];
static const uint32 types339 [] =
{
SK_POINTER,
};

static const uint16 attr_flags339 [] =
{0,};

static const EIF_TYPE_INDEX g_atype339_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes339 [] = {
g_atype339_0,
};

static const int32 cn_attr339 [] =
{
4554,
};

extern const char *names340[];
static const uint32 types340 [] =
{
SK_POINTER,
};

static const uint16 attr_flags340 [] =
{0,};

static const EIF_TYPE_INDEX g_atype340_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes340 [] = {
g_atype340_0,
};

static const int32 cn_attr340 [] =
{
4554,
};

extern const char *names341[];
static const uint32 types341 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags341 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype341_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype341_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes341 [] = {
g_atype341_0,
g_atype341_1,
};

static const int32 cn_attr341 [] =
{
3889,
3890,
};

extern const char *names342[];
static const uint32 types342 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags342 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype342_0 [] = {342,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype342_1 [] = {342,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype342_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype342_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype342_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype342_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes342 [] = {
g_atype342_0,
g_atype342_1,
g_atype342_2,
g_atype342_3,
g_atype342_4,
g_atype342_5,
};

static const int32 cn_attr342 [] =
{
2881,
2885,
2327,
2889,
2890,
2891,
};

extern const char *names343[];
static const uint32 types343 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags343 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype343_0 [] = {0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype343_1 [] = {342,0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes343 [] = {
g_atype343_0,
g_atype343_1,
};

static const int32 cn_attr343 [] =
{
1718,
1722,
};

extern const char *names344[];
static const uint32 types344 [] =
{
SK_REF,
};

static const uint16 attr_flags344 [] =
{0,};

static const EIF_TYPE_INDEX g_atype344_0 [] = {0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes344 [] = {
g_atype344_0,
};

static const int32 cn_attr344 [] =
{
1718,
};

extern const char *names345[];
static const uint32 types345 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags345 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype345_0 [] = {0xFF01,341,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype345_1 [] = {342,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype345_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype345_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype345_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype345_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype345_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype345_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes345 [] = {
g_atype345_0,
g_atype345_1,
g_atype345_2,
g_atype345_3,
g_atype345_4,
g_atype345_5,
g_atype345_6,
g_atype345_7,
};

static const int32 cn_attr345 [] =
{
3784,
3790,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names346[];
static const uint32 types346 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags346 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype346_0 [] = {342,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype346_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype346_2 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes346 [] = {
g_atype346_0,
g_atype346_1,
g_atype346_2,
};

static const int32 cn_attr346 [] =
{
2150,
2151,
2152,
};

extern const char *names347[];
static const uint32 types347 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags347 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype347_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_2 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_3 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_4 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_11 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype347_12 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes347 [] = {
g_atype347_0,
g_atype347_1,
g_atype347_2,
g_atype347_3,
g_atype347_4,
g_atype347_5,
g_atype347_6,
g_atype347_7,
g_atype347_8,
g_atype347_9,
g_atype347_10,
g_atype347_11,
g_atype347_12,
};

static const int32 cn_attr347 [] =
{
4579,
4594,
4598,
4606,
4615,
4587,
4601,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names348[];
static const uint32 types348 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags348 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype348_0 [] = {0xFF01,374,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype348_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes348 [] = {
g_atype348_0,
g_atype348_1,
g_atype348_2,
g_atype348_3,
};

static const int32 cn_attr348 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names349[];
static const uint32 types349 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags349 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype349_0 [] = {0xFF01,374,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype349_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype349_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes349 [] = {
g_atype349_0,
g_atype349_1,
g_atype349_2,
};

static const int32 cn_attr349 [] =
{
2156,
2327,
3094,
};

extern const char *names350[];
static const uint32 types350 [] =
{
SK_BOOL,
};

static const uint16 attr_flags350 [] =
{0,};

static const EIF_TYPE_INDEX g_atype350_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes350 [] = {
g_atype350_0,
};

static const int32 cn_attr350 [] =
{
2327,
};

extern const char *names351[];
static const uint32 types351 [] =
{
SK_BOOL,
};

static const uint16 attr_flags351 [] =
{0,};

static const EIF_TYPE_INDEX g_atype351_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes351 [] = {
g_atype351_0,
};

static const int32 cn_attr351 [] =
{
2327,
};

extern const char *names352[];
static const uint32 types352 [] =
{
SK_BOOL,
};

static const uint16 attr_flags352 [] =
{0,};

static const EIF_TYPE_INDEX g_atype352_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes352 [] = {
g_atype352_0,
};

static const int32 cn_attr352 [] =
{
2327,
};

extern const char *names353[];
static const uint32 types353 [] =
{
SK_BOOL,
};

static const uint16 attr_flags353 [] =
{0,};

static const EIF_TYPE_INDEX g_atype353_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes353 [] = {
g_atype353_0,
};

static const int32 cn_attr353 [] =
{
2327,
};

extern const char *names355[];
static const uint32 types355 [] =
{
SK_BOOL,
};

static const uint16 attr_flags355 [] =
{0,};

static const EIF_TYPE_INDEX g_atype355_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes355 [] = {
g_atype355_0,
};

static const int32 cn_attr355 [] =
{
2327,
};

extern const char *names357[];
static const uint32 types357 [] =
{
SK_BOOL,
};

static const uint16 attr_flags357 [] =
{0,};

static const EIF_TYPE_INDEX g_atype357_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes357 [] = {
g_atype357_0,
};

static const int32 cn_attr357 [] =
{
2327,
};

extern const char *names360[];
static const uint32 types360 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags360 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype360_0 [] = {0xFF01,357,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype360_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype360_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype360_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype360_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype360_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype360_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes360 [] = {
g_atype360_0,
g_atype360_1,
g_atype360_2,
g_atype360_3,
g_atype360_4,
g_atype360_5,
g_atype360_6,
};

static const int32 cn_attr360 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names362[];
static const uint32 types362 [] =
{
SK_BOOL,
};

static const uint16 attr_flags362 [] =
{0,};

static const EIF_TYPE_INDEX g_atype362_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes362 [] = {
g_atype362_0,
};

static const int32 cn_attr362 [] =
{
2327,
};

extern const char *names363[];
static const uint32 types363 [] =
{
SK_BOOL,
};

static const uint16 attr_flags363 [] =
{0,};

static const EIF_TYPE_INDEX g_atype363_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes363 [] = {
g_atype363_0,
};

static const int32 cn_attr363 [] =
{
2327,
};

extern const char *names364[];
static const uint32 types364 [] =
{
SK_BOOL,
};

static const uint16 attr_flags364 [] =
{0,};

static const EIF_TYPE_INDEX g_atype364_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes364 [] = {
g_atype364_0,
};

static const int32 cn_attr364 [] =
{
2327,
};

extern const char *names365[];
static const uint32 types365 [] =
{
SK_BOOL,
};

static const uint16 attr_flags365 [] =
{0,};

static const EIF_TYPE_INDEX g_atype365_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes365 [] = {
g_atype365_0,
};

static const int32 cn_attr365 [] =
{
2327,
};

extern const char *names366[];
static const uint32 types366 [] =
{
SK_BOOL,
};

static const uint16 attr_flags366 [] =
{0,};

static const EIF_TYPE_INDEX g_atype366_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes366 [] = {
g_atype366_0,
};

static const int32 cn_attr366 [] =
{
2327,
};

extern const char *names367[];
static const uint32 types367 [] =
{
SK_BOOL,
};

static const uint16 attr_flags367 [] =
{0,};

static const EIF_TYPE_INDEX g_atype367_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes367 [] = {
g_atype367_0,
};

static const int32 cn_attr367 [] =
{
2327,
};

extern const char *names368[];
static const uint32 types368 [] =
{
SK_BOOL,
};

static const uint16 attr_flags368 [] =
{0,};

static const EIF_TYPE_INDEX g_atype368_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes368 [] = {
g_atype368_0,
};

static const int32 cn_attr368 [] =
{
2327,
};

extern const char *names369[];
static const uint32 types369 [] =
{
SK_BOOL,
};

static const uint16 attr_flags369 [] =
{0,};

static const EIF_TYPE_INDEX g_atype369_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes369 [] = {
g_atype369_0,
};

static const int32 cn_attr369 [] =
{
2327,
};

extern const char *names370[];
static const uint32 types370 [] =
{
SK_BOOL,
};

static const uint16 attr_flags370 [] =
{0,};

static const EIF_TYPE_INDEX g_atype370_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes370 [] = {
g_atype370_0,
};

static const int32 cn_attr370 [] =
{
2327,
};

extern const char *names371[];
static const uint32 types371 [] =
{
SK_BOOL,
};

static const uint16 attr_flags371 [] =
{0,};

static const EIF_TYPE_INDEX g_atype371_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes371 [] = {
g_atype371_0,
};

static const int32 cn_attr371 [] =
{
2327,
};

extern const char *names372[];
static const uint32 types372 [] =
{
SK_BOOL,
};

static const uint16 attr_flags372 [] =
{0,};

static const EIF_TYPE_INDEX g_atype372_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes372 [] = {
g_atype372_0,
};

static const int32 cn_attr372 [] =
{
2327,
};

extern const char *names373[];
static const uint32 types373 [] =
{
SK_BOOL,
};

static const uint16 attr_flags373 [] =
{0,};

static const EIF_TYPE_INDEX g_atype373_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes373 [] = {
g_atype373_0,
};

static const int32 cn_attr373 [] =
{
2327,
};

extern const char *names374[];
static const uint32 types374 [] =
{
SK_REF,
};

static const uint16 attr_flags374 [] =
{0,};

static const EIF_TYPE_INDEX g_atype374_0 [] = {0xFF01,374,209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes374 [] = {
g_atype374_0,
};

static const int32 cn_attr374 [] =
{
2156,
};

extern const char *names376[];
static const uint32 types376 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags376 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype376_0 [] = {0xFF01,374,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype376_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype376_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes376 [] = {
g_atype376_0,
g_atype376_1,
g_atype376_2,
};

static const int32 cn_attr376 [] =
{
3791,
3792,
3794,
};

extern const char *names377[];
static const uint32 types377 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags377 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype377_0 [] = {0xFF01,374,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype377_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype377_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes377 [] = {
g_atype377_0,
g_atype377_1,
g_atype377_2,
};

static const int32 cn_attr377 [] =
{
3791,
3792,
3794,
};

extern const char *names379[];
static const uint32 types379 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags379 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype379_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype379_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes379 [] = {
g_atype379_0,
g_atype379_1,
};

static const int32 cn_attr379 [] =
{
3889,
3890,
};

extern const char *names380[];
static const uint32 types380 [] =
{
SK_BOOL,
};

static const uint16 attr_flags380 [] =
{0,};

static const EIF_TYPE_INDEX g_atype380_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes380 [] = {
g_atype380_0,
};

static const int32 cn_attr380 [] =
{
2327,
};

extern const char *names381[];
static const uint32 types381 [] =
{
SK_BOOL,
};

static const uint16 attr_flags381 [] =
{0,};

static const EIF_TYPE_INDEX g_atype381_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes381 [] = {
g_atype381_0,
};

static const int32 cn_attr381 [] =
{
2327,
};

extern const char *names382[];
static const uint32 types382 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags382 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype382_0 [] = {0xFF01,374,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype382_1 [] = {0xFF01,348,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype382_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype382_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes382 [] = {
g_atype382_0,
g_atype382_1,
g_atype382_2,
g_atype382_3,
};

static const int32 cn_attr382 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names383[];
static const uint32 types383 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags383 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype383_0 [] = {0xFF01,374,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype383_1 [] = {0xFF01,347,209,0xFFFF};
static const EIF_TYPE_INDEX g_atype383_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype383_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype383_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes383 [] = {
g_atype383_0,
g_atype383_1,
g_atype383_2,
g_atype383_3,
g_atype383_4,
};

static const int32 cn_attr383 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names384[];
static const uint32 types384 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags384 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype384_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype384_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype384_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype384_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes384 [] = {
g_atype384_0,
g_atype384_1,
g_atype384_2,
g_atype384_3,
};

static const int32 cn_attr384 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names385[];
static const uint32 types385 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags385 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype385_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype385_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype385_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes385 [] = {
g_atype385_0,
g_atype385_1,
g_atype385_2,
};

static const int32 cn_attr385 [] =
{
2156,
2327,
3094,
};

extern const char *names386[];
static const uint32 types386 [] =
{
SK_BOOL,
};

static const uint16 attr_flags386 [] =
{0,};

static const EIF_TYPE_INDEX g_atype386_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes386 [] = {
g_atype386_0,
};

static const int32 cn_attr386 [] =
{
2327,
};

extern const char *names387[];
static const uint32 types387 [] =
{
SK_BOOL,
};

static const uint16 attr_flags387 [] =
{0,};

static const EIF_TYPE_INDEX g_atype387_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes387 [] = {
g_atype387_0,
};

static const int32 cn_attr387 [] =
{
2327,
};

extern const char *names388[];
static const uint32 types388 [] =
{
SK_BOOL,
};

static const uint16 attr_flags388 [] =
{0,};

static const EIF_TYPE_INDEX g_atype388_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes388 [] = {
g_atype388_0,
};

static const int32 cn_attr388 [] =
{
2327,
};

extern const char *names389[];
static const uint32 types389 [] =
{
SK_BOOL,
};

static const uint16 attr_flags389 [] =
{0,};

static const EIF_TYPE_INDEX g_atype389_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes389 [] = {
g_atype389_0,
};

static const int32 cn_attr389 [] =
{
2327,
};

extern const char *names391[];
static const uint32 types391 [] =
{
SK_BOOL,
};

static const uint16 attr_flags391 [] =
{0,};

static const EIF_TYPE_INDEX g_atype391_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes391 [] = {
g_atype391_0,
};

static const int32 cn_attr391 [] =
{
2327,
};

extern const char *names393[];
static const uint32 types393 [] =
{
SK_BOOL,
};

static const uint16 attr_flags393 [] =
{0,};

static const EIF_TYPE_INDEX g_atype393_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes393 [] = {
g_atype393_0,
};

static const int32 cn_attr393 [] =
{
2327,
};

extern const char *names396[];
static const uint32 types396 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags396 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype396_0 [] = {0xFF01,393,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype396_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes396 [] = {
g_atype396_0,
g_atype396_1,
g_atype396_2,
g_atype396_3,
g_atype396_4,
g_atype396_5,
g_atype396_6,
};

static const int32 cn_attr396 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names398[];
static const uint32 types398 [] =
{
SK_BOOL,
};

static const uint16 attr_flags398 [] =
{0,};

static const EIF_TYPE_INDEX g_atype398_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes398 [] = {
g_atype398_0,
};

static const int32 cn_attr398 [] =
{
2327,
};

extern const char *names399[];
static const uint32 types399 [] =
{
SK_BOOL,
};

static const uint16 attr_flags399 [] =
{0,};

static const EIF_TYPE_INDEX g_atype399_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes399 [] = {
g_atype399_0,
};

static const int32 cn_attr399 [] =
{
2327,
};

extern const char *names400[];
static const uint32 types400 [] =
{
SK_BOOL,
};

static const uint16 attr_flags400 [] =
{0,};

static const EIF_TYPE_INDEX g_atype400_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes400 [] = {
g_atype400_0,
};

static const int32 cn_attr400 [] =
{
2327,
};

extern const char *names401[];
static const uint32 types401 [] =
{
SK_BOOL,
};

static const uint16 attr_flags401 [] =
{0,};

static const EIF_TYPE_INDEX g_atype401_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes401 [] = {
g_atype401_0,
};

static const int32 cn_attr401 [] =
{
2327,
};

extern const char *names402[];
static const uint32 types402 [] =
{
SK_BOOL,
};

static const uint16 attr_flags402 [] =
{0,};

static const EIF_TYPE_INDEX g_atype402_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes402 [] = {
g_atype402_0,
};

static const int32 cn_attr402 [] =
{
2327,
};

extern const char *names403[];
static const uint32 types403 [] =
{
SK_BOOL,
};

static const uint16 attr_flags403 [] =
{0,};

static const EIF_TYPE_INDEX g_atype403_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes403 [] = {
g_atype403_0,
};

static const int32 cn_attr403 [] =
{
2327,
};

extern const char *names404[];
static const uint32 types404 [] =
{
SK_BOOL,
};

static const uint16 attr_flags404 [] =
{0,};

static const EIF_TYPE_INDEX g_atype404_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes404 [] = {
g_atype404_0,
};

static const int32 cn_attr404 [] =
{
2327,
};

extern const char *names405[];
static const uint32 types405 [] =
{
SK_BOOL,
};

static const uint16 attr_flags405 [] =
{0,};

static const EIF_TYPE_INDEX g_atype405_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes405 [] = {
g_atype405_0,
};

static const int32 cn_attr405 [] =
{
2327,
};

extern const char *names406[];
static const uint32 types406 [] =
{
SK_BOOL,
};

static const uint16 attr_flags406 [] =
{0,};

static const EIF_TYPE_INDEX g_atype406_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes406 [] = {
g_atype406_0,
};

static const int32 cn_attr406 [] =
{
2327,
};

extern const char *names407[];
static const uint32 types407 [] =
{
SK_BOOL,
};

static const uint16 attr_flags407 [] =
{0,};

static const EIF_TYPE_INDEX g_atype407_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes407 [] = {
g_atype407_0,
};

static const int32 cn_attr407 [] =
{
2327,
};

extern const char *names408[];
static const uint32 types408 [] =
{
SK_BOOL,
};

static const uint16 attr_flags408 [] =
{0,};

static const EIF_TYPE_INDEX g_atype408_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes408 [] = {
g_atype408_0,
};

static const int32 cn_attr408 [] =
{
2327,
};

extern const char *names409[];
static const uint32 types409 [] =
{
SK_BOOL,
};

static const uint16 attr_flags409 [] =
{0,};

static const EIF_TYPE_INDEX g_atype409_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes409 [] = {
g_atype409_0,
};

static const int32 cn_attr409 [] =
{
2327,
};

extern const char *names410[];
static const uint32 types410 [] =
{
SK_REF,
};

static const uint16 attr_flags410 [] =
{0,};

static const EIF_TYPE_INDEX g_atype410_0 [] = {0xFF01,410,194,0xFFFF};

static const EIF_TYPE_INDEX *gtypes410 [] = {
g_atype410_0,
};

static const int32 cn_attr410 [] =
{
2156,
};

extern const char *names412[];
static const uint32 types412 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags412 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype412_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype412_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype412_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes412 [] = {
g_atype412_0,
g_atype412_1,
g_atype412_2,
};

static const int32 cn_attr412 [] =
{
3791,
3792,
3794,
};

extern const char *names413[];
static const uint32 types413 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags413 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype413_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype413_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype413_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes413 [] = {
g_atype413_0,
g_atype413_1,
g_atype413_2,
};

static const int32 cn_attr413 [] =
{
3791,
3792,
3794,
};

extern const char *names415[];
static const uint32 types415 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags415 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype415_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype415_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes415 [] = {
g_atype415_0,
g_atype415_1,
};

static const int32 cn_attr415 [] =
{
3889,
3890,
};

extern const char *names416[];
static const uint32 types416 [] =
{
SK_BOOL,
};

static const uint16 attr_flags416 [] =
{0,};

static const EIF_TYPE_INDEX g_atype416_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes416 [] = {
g_atype416_0,
};

static const int32 cn_attr416 [] =
{
2327,
};

extern const char *names417[];
static const uint32 types417 [] =
{
SK_BOOL,
};

static const uint16 attr_flags417 [] =
{0,};

static const EIF_TYPE_INDEX g_atype417_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes417 [] = {
g_atype417_0,
};

static const int32 cn_attr417 [] =
{
2327,
};

extern const char *names418[];
static const uint32 types418 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags418 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype418_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype418_1 [] = {0xFF01,384,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype418_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype418_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes418 [] = {
g_atype418_0,
g_atype418_1,
g_atype418_2,
g_atype418_3,
};

static const int32 cn_attr418 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names419[];
static const uint32 types419 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags419 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype419_0 [] = {0xFF01,410,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype419_1 [] = {0xFF01,383,194,0xFFFF};
static const EIF_TYPE_INDEX g_atype419_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype419_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype419_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes419 [] = {
g_atype419_0,
g_atype419_1,
g_atype419_2,
g_atype419_3,
g_atype419_4,
};

static const int32 cn_attr419 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names420[];
static const uint32 types420 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags420 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype420_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype420_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes420 [] = {
g_atype420_0,
g_atype420_1,
};

static const int32 cn_attr420 [] =
{
3889,
3890,
};

extern const char *names421[];
static const uint32 types421 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags421 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype421_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype421_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes421 [] = {
g_atype421_0,
g_atype421_1,
};

static const int32 cn_attr421 [] =
{
3889,
3890,
};

extern const char *names422[];
static const uint32 types422 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags422 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype422_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype422_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype422_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype422_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype422_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype422_5 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes422 [] = {
g_atype422_0,
g_atype422_1,
g_atype422_2,
g_atype422_3,
g_atype422_4,
g_atype422_5,
};

static const int32 cn_attr422 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names423[];
static const uint32 types423 [] =
{
SK_POINTER,
};

static const uint16 attr_flags423 [] =
{0,};

static const EIF_TYPE_INDEX g_atype423_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes423 [] = {
g_atype423_0,
};

static const int32 cn_attr423 [] =
{
4554,
};

extern const char *names424[];
static const uint32 types424 [] =
{
SK_POINTER,
};

static const uint16 attr_flags424 [] =
{0,};

static const EIF_TYPE_INDEX g_atype424_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes424 [] = {
g_atype424_0,
};

static const int32 cn_attr424 [] =
{
4554,
};

extern const char *names425[];
static const uint32 types425 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags425 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype425_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype425_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes425 [] = {
g_atype425_0,
g_atype425_1,
};

static const int32 cn_attr425 [] =
{
3889,
3890,
};

extern const char *names426[];
static const uint32 types426 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags426 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype426_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype426_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype426_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype426_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype426_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype426_5 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes426 [] = {
g_atype426_0,
g_atype426_1,
g_atype426_2,
g_atype426_3,
g_atype426_4,
g_atype426_5,
};

static const int32 cn_attr426 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names428[];
static const uint32 types428 [] =
{
SK_REF,
SK_REF,
SK_INT8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags428 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype428_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype428_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype428_2 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype428_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype428_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype428_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes428 [] = {
g_atype428_0,
g_atype428_1,
g_atype428_2,
g_atype428_3,
g_atype428_4,
g_atype428_5,
};

static const int32 cn_attr428 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names429[];
static const uint32 types429 [] =
{
SK_POINTER,
};

static const uint16 attr_flags429 [] =
{0,};

static const EIF_TYPE_INDEX g_atype429_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes429 [] = {
g_atype429_0,
};

static const int32 cn_attr429 [] =
{
4554,
};

extern const char *names430[];
static const uint32 types430 [] =
{
SK_POINTER,
};

static const uint16 attr_flags430 [] =
{0,};

static const EIF_TYPE_INDEX g_atype430_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes430 [] = {
g_atype430_0,
};

static const int32 cn_attr430 [] =
{
4554,
};

extern const char *names431[];
static const uint32 types431 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags431 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype431_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype431_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes431 [] = {
g_atype431_0,
g_atype431_1,
};

static const int32 cn_attr431 [] =
{
3889,
3890,
};

extern const char *names432[];
static const uint32 types432 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags432 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype432_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype432_5 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes432 [] = {
g_atype432_0,
g_atype432_1,
g_atype432_2,
g_atype432_3,
g_atype432_4,
g_atype432_5,
};

static const int32 cn_attr432 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names433[];
static const uint32 types433 [] =
{
SK_POINTER,
};

static const uint16 attr_flags433 [] =
{0,};

static const EIF_TYPE_INDEX g_atype433_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes433 [] = {
g_atype433_0,
};

static const int32 cn_attr433 [] =
{
4554,
};

extern const char *names434[];
static const uint32 types434 [] =
{
SK_POINTER,
};

static const uint16 attr_flags434 [] =
{0,};

static const EIF_TYPE_INDEX g_atype434_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes434 [] = {
g_atype434_0,
};

static const int32 cn_attr434 [] =
{
4554,
};

extern const char *names435[];
static const uint32 types435 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags435 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype435_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype435_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes435 [] = {
g_atype435_0,
g_atype435_1,
};

static const int32 cn_attr435 [] =
{
3889,
3890,
};

extern const char *names436[];
static const uint32 types436 [] =
{
SK_REF,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags436 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype436_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype436_1 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype436_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype436_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype436_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype436_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes436 [] = {
g_atype436_0,
g_atype436_1,
g_atype436_2,
g_atype436_3,
g_atype436_4,
g_atype436_5,
};

static const int32 cn_attr436 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names437[];
static const uint32 types437 [] =
{
SK_POINTER,
};

static const uint16 attr_flags437 [] =
{0,};

static const EIF_TYPE_INDEX g_atype437_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes437 [] = {
g_atype437_0,
};

static const int32 cn_attr437 [] =
{
4554,
};

extern const char *names438[];
static const uint32 types438 [] =
{
SK_POINTER,
};

static const uint16 attr_flags438 [] =
{0,};

static const EIF_TYPE_INDEX g_atype438_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes438 [] = {
g_atype438_0,
};

static const int32 cn_attr438 [] =
{
4554,
};

extern const char *names439[];
static const uint32 types439 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags439 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype439_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype439_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes439 [] = {
g_atype439_0,
g_atype439_1,
};

static const int32 cn_attr439 [] =
{
3889,
3890,
};

extern const char *names440[];
static const uint32 types440 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags440 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype440_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype440_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype440_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype440_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype440_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype440_5 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes440 [] = {
g_atype440_0,
g_atype440_1,
g_atype440_2,
g_atype440_3,
g_atype440_4,
g_atype440_5,
};

static const int32 cn_attr440 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names441[];
static const uint32 types441 [] =
{
SK_POINTER,
};

static const uint16 attr_flags441 [] =
{0,};

static const EIF_TYPE_INDEX g_atype441_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes441 [] = {
g_atype441_0,
};

static const int32 cn_attr441 [] =
{
4554,
};

extern const char *names442[];
static const uint32 types442 [] =
{
SK_POINTER,
};

static const uint16 attr_flags442 [] =
{0,};

static const EIF_TYPE_INDEX g_atype442_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes442 [] = {
g_atype442_0,
};

static const int32 cn_attr442 [] =
{
4554,
};

extern const char *names443[];
static const uint32 types443 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags443 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype443_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype443_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes443 [] = {
g_atype443_0,
g_atype443_1,
};

static const int32 cn_attr443 [] =
{
3889,
3890,
};

extern const char *names444[];
static const uint32 types444 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags444 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype444_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype444_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype444_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype444_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype444_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes444 [] = {
g_atype444_0,
g_atype444_1,
g_atype444_2,
g_atype444_3,
g_atype444_4,
};

static const int32 cn_attr444 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names445[];
static const uint32 types445 [] =
{
SK_POINTER,
};

static const uint16 attr_flags445 [] =
{0,};

static const EIF_TYPE_INDEX g_atype445_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes445 [] = {
g_atype445_0,
};

static const int32 cn_attr445 [] =
{
4554,
};

extern const char *names446[];
static const uint32 types446 [] =
{
SK_POINTER,
};

static const uint16 attr_flags446 [] =
{0,};

static const EIF_TYPE_INDEX g_atype446_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes446 [] = {
g_atype446_0,
};

static const int32 cn_attr446 [] =
{
4554,
};

extern const char *names447[];
static const uint32 types447 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags447 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype447_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype447_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes447 [] = {
g_atype447_0,
g_atype447_1,
};

static const int32 cn_attr447 [] =
{
3889,
3890,
};

extern const char *names451[];
static const uint32 types451 [] =
{
SK_REF,
SK_REF,
SK_UINT8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags451 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype451_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_2 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype451_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes451 [] = {
g_atype451_0,
g_atype451_1,
g_atype451_2,
g_atype451_3,
g_atype451_4,
};

static const int32 cn_attr451 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names452[];
static const uint32 types452 [] =
{
SK_POINTER,
};

static const uint16 attr_flags452 [] =
{0,};

static const EIF_TYPE_INDEX g_atype452_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes452 [] = {
g_atype452_0,
};

static const int32 cn_attr452 [] =
{
4554,
};

extern const char *names453[];
static const uint32 types453 [] =
{
SK_POINTER,
};

static const uint16 attr_flags453 [] =
{0,};

static const EIF_TYPE_INDEX g_atype453_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes453 [] = {
g_atype453_0,
};

static const int32 cn_attr453 [] =
{
4554,
};

extern const char *names454[];
static const uint32 types454 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags454 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype454_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype454_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes454 [] = {
g_atype454_0,
g_atype454_1,
};

static const int32 cn_attr454 [] =
{
3889,
3890,
};

extern const char *names455[];
static const uint32 types455 [] =
{
SK_REF,
};

static const uint16 attr_flags455 [] =
{0,};

static const EIF_TYPE_INDEX g_atype455_0 [] = {0xFF01,455,206,0xFFFF};

static const EIF_TYPE_INDEX *gtypes455 [] = {
g_atype455_0,
};

static const int32 cn_attr455 [] =
{
2156,
};

extern const char *names457[];
static const uint32 types457 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags457 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype457_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype457_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype457_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes457 [] = {
g_atype457_0,
g_atype457_1,
g_atype457_2,
};

static const int32 cn_attr457 [] =
{
3791,
3792,
3794,
};

extern const char *names458[];
static const uint32 types458 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags458 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype458_0 [] = {0xFF01,458,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype458_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes458 [] = {
g_atype458_0,
g_atype458_1,
g_atype458_2,
g_atype458_3,
g_atype458_4,
g_atype458_5,
g_atype458_6,
};

static const int32 cn_attr458 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names463[];
static const uint32 types463 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags463 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype463_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype463_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype463_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes463 [] = {
g_atype463_0,
g_atype463_1,
g_atype463_2,
};

static const int32 cn_attr463 [] =
{
3791,
3792,
3794,
};

extern const char *names465[];
static const uint32 types465 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags465 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype465_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype465_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes465 [] = {
g_atype465_0,
g_atype465_1,
};

static const int32 cn_attr465 [] =
{
3889,
3890,
};

extern const char *names466[];
static const uint32 types466 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags466 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype466_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype466_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype466_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype466_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes466 [] = {
g_atype466_0,
g_atype466_1,
g_atype466_2,
g_atype466_3,
};

static const int32 cn_attr466 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names467[];
static const uint32 types467 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags467 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype467_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype467_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype467_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes467 [] = {
g_atype467_0,
g_atype467_1,
g_atype467_2,
};

static const int32 cn_attr467 [] =
{
2156,
2327,
3094,
};

extern const char *names468[];
static const uint32 types468 [] =
{
SK_BOOL,
};

static const uint16 attr_flags468 [] =
{0,};

static const EIF_TYPE_INDEX g_atype468_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes468 [] = {
g_atype468_0,
};

static const int32 cn_attr468 [] =
{
2327,
};

extern const char *names469[];
static const uint32 types469 [] =
{
SK_BOOL,
};

static const uint16 attr_flags469 [] =
{0,};

static const EIF_TYPE_INDEX g_atype469_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes469 [] = {
g_atype469_0,
};

static const int32 cn_attr469 [] =
{
2327,
};

extern const char *names470[];
static const uint32 types470 [] =
{
SK_BOOL,
};

static const uint16 attr_flags470 [] =
{0,};

static const EIF_TYPE_INDEX g_atype470_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes470 [] = {
g_atype470_0,
};

static const int32 cn_attr470 [] =
{
2327,
};

extern const char *names471[];
static const uint32 types471 [] =
{
SK_BOOL,
};

static const uint16 attr_flags471 [] =
{0,};

static const EIF_TYPE_INDEX g_atype471_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes471 [] = {
g_atype471_0,
};

static const int32 cn_attr471 [] =
{
2327,
};

extern const char *names472[];
static const uint32 types472 [] =
{
SK_BOOL,
};

static const uint16 attr_flags472 [] =
{0,};

static const EIF_TYPE_INDEX g_atype472_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes472 [] = {
g_atype472_0,
};

static const int32 cn_attr472 [] =
{
2327,
};

extern const char *names473[];
static const uint32 types473 [] =
{
SK_BOOL,
};

static const uint16 attr_flags473 [] =
{0,};

static const EIF_TYPE_INDEX g_atype473_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes473 [] = {
g_atype473_0,
};

static const int32 cn_attr473 [] =
{
2327,
};

extern const char *names474[];
static const uint32 types474 [] =
{
SK_BOOL,
};

static const uint16 attr_flags474 [] =
{0,};

static const EIF_TYPE_INDEX g_atype474_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes474 [] = {
g_atype474_0,
};

static const int32 cn_attr474 [] =
{
2327,
};

extern const char *names475[];
static const uint32 types475 [] =
{
SK_BOOL,
};

static const uint16 attr_flags475 [] =
{0,};

static const EIF_TYPE_INDEX g_atype475_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes475 [] = {
g_atype475_0,
};

static const int32 cn_attr475 [] =
{
2327,
};

extern const char *names476[];
static const uint32 types476 [] =
{
SK_BOOL,
};

static const uint16 attr_flags476 [] =
{0,};

static const EIF_TYPE_INDEX g_atype476_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes476 [] = {
g_atype476_0,
};

static const int32 cn_attr476 [] =
{
2327,
};

extern const char *names477[];
static const uint32 types477 [] =
{
SK_BOOL,
};

static const uint16 attr_flags477 [] =
{0,};

static const EIF_TYPE_INDEX g_atype477_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes477 [] = {
g_atype477_0,
};

static const int32 cn_attr477 [] =
{
2327,
};

extern const char *names478[];
static const uint32 types478 [] =
{
SK_BOOL,
};

static const uint16 attr_flags478 [] =
{0,};

static const EIF_TYPE_INDEX g_atype478_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes478 [] = {
g_atype478_0,
};

static const int32 cn_attr478 [] =
{
2327,
};

extern const char *names479[];
static const uint32 types479 [] =
{
SK_BOOL,
};

static const uint16 attr_flags479 [] =
{0,};

static const EIF_TYPE_INDEX g_atype479_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes479 [] = {
g_atype479_0,
};

static const int32 cn_attr479 [] =
{
2327,
};

extern const char *names480[];
static const uint32 types480 [] =
{
SK_BOOL,
};

static const uint16 attr_flags480 [] =
{0,};

static const EIF_TYPE_INDEX g_atype480_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes480 [] = {
g_atype480_0,
};

static const int32 cn_attr480 [] =
{
2327,
};

extern const char *names481[];
static const uint32 types481 [] =
{
SK_BOOL,
};

static const uint16 attr_flags481 [] =
{0,};

static const EIF_TYPE_INDEX g_atype481_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes481 [] = {
g_atype481_0,
};

static const int32 cn_attr481 [] =
{
2327,
};

extern const char *names482[];
static const uint32 types482 [] =
{
SK_BOOL,
};

static const uint16 attr_flags482 [] =
{0,};

static const EIF_TYPE_INDEX g_atype482_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes482 [] = {
g_atype482_0,
};

static const int32 cn_attr482 [] =
{
2327,
};

extern const char *names483[];
static const uint32 types483 [] =
{
SK_BOOL,
};

static const uint16 attr_flags483 [] =
{0,};

static const EIF_TYPE_INDEX g_atype483_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes483 [] = {
g_atype483_0,
};

static const int32 cn_attr483 [] =
{
2327,
};

extern const char *names484[];
static const uint32 types484 [] =
{
SK_BOOL,
};

static const uint16 attr_flags484 [] =
{0,};

static const EIF_TYPE_INDEX g_atype484_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes484 [] = {
g_atype484_0,
};

static const int32 cn_attr484 [] =
{
2327,
};

extern const char *names485[];
static const uint32 types485 [] =
{
SK_BOOL,
};

static const uint16 attr_flags485 [] =
{0,};

static const EIF_TYPE_INDEX g_atype485_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes485 [] = {
g_atype485_0,
};

static const int32 cn_attr485 [] =
{
2327,
};

extern const char *names486[];
static const uint32 types486 [] =
{
SK_BOOL,
};

static const uint16 attr_flags486 [] =
{0,};

static const EIF_TYPE_INDEX g_atype486_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes486 [] = {
g_atype486_0,
};

static const int32 cn_attr486 [] =
{
2327,
};

extern const char *names487[];
static const uint32 types487 [] =
{
SK_BOOL,
};

static const uint16 attr_flags487 [] =
{0,};

static const EIF_TYPE_INDEX g_atype487_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes487 [] = {
g_atype487_0,
};

static const int32 cn_attr487 [] =
{
2327,
};

extern const char *names488[];
static const uint32 types488 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags488 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype488_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype488_1 [] = {0xFF01,466,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype488_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype488_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes488 [] = {
g_atype488_0,
g_atype488_1,
g_atype488_2,
g_atype488_3,
};

static const int32 cn_attr488 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names489[];
static const uint32 types489 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags489 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype489_0 [] = {0xFF01,455,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype489_1 [] = {0xFF01,465,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype489_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype489_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype489_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes489 [] = {
g_atype489_0,
g_atype489_1,
g_atype489_2,
g_atype489_3,
g_atype489_4,
};

static const int32 cn_attr489 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names490[];
static const uint32 types490 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags490 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype490_0 [] = {0xFF01,490,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype490_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype490_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype490_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype490_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype490_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype490_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes490 [] = {
g_atype490_0,
g_atype490_1,
g_atype490_2,
g_atype490_3,
g_atype490_4,
g_atype490_5,
g_atype490_6,
};

static const int32 cn_attr490 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names493[];
static const uint32 types493 [] =
{
SK_BOOL,
};

static const uint16 attr_flags493 [] =
{0,};

static const EIF_TYPE_INDEX g_atype493_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes493 [] = {
g_atype493_0,
};

static const int32 cn_attr493 [] =
{
2327,
};

extern const char *names494[];
static const uint32 types494 [] =
{
SK_BOOL,
};

static const uint16 attr_flags494 [] =
{0,};

static const EIF_TYPE_INDEX g_atype494_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes494 [] = {
g_atype494_0,
};

static const int32 cn_attr494 [] =
{
2327,
};

extern const char *names495[];
static const uint32 types495 [] =
{
SK_BOOL,
};

static const uint16 attr_flags495 [] =
{0,};

static const EIF_TYPE_INDEX g_atype495_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes495 [] = {
g_atype495_0,
};

static const int32 cn_attr495 [] =
{
2327,
};

extern const char *names496[];
static const uint32 types496 [] =
{
SK_BOOL,
};

static const uint16 attr_flags496 [] =
{0,};

static const EIF_TYPE_INDEX g_atype496_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes496 [] = {
g_atype496_0,
};

static const int32 cn_attr496 [] =
{
2327,
};

extern const char *names497[];
static const uint32 types497 [] =
{
SK_BOOL,
};

static const uint16 attr_flags497 [] =
{0,};

static const EIF_TYPE_INDEX g_atype497_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes497 [] = {
g_atype497_0,
};

static const int32 cn_attr497 [] =
{
2327,
};

extern const char *names498[];
static const uint32 types498 [] =
{
SK_BOOL,
};

static const uint16 attr_flags498 [] =
{0,};

static const EIF_TYPE_INDEX g_atype498_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes498 [] = {
g_atype498_0,
};

static const int32 cn_attr498 [] =
{
2327,
};

extern const char *names499[];
static const uint32 types499 [] =
{
SK_BOOL,
};

static const uint16 attr_flags499 [] =
{0,};

static const EIF_TYPE_INDEX g_atype499_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes499 [] = {
g_atype499_0,
};

static const int32 cn_attr499 [] =
{
2327,
};

extern const char *names500[];
static const uint32 types500 [] =
{
SK_BOOL,
};

static const uint16 attr_flags500 [] =
{0,};

static const EIF_TYPE_INDEX g_atype500_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes500 [] = {
g_atype500_0,
};

static const int32 cn_attr500 [] =
{
2327,
};

extern const char *names501[];
static const uint32 types501 [] =
{
SK_BOOL,
};

static const uint16 attr_flags501 [] =
{0,};

static const EIF_TYPE_INDEX g_atype501_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes501 [] = {
g_atype501_0,
};

static const int32 cn_attr501 [] =
{
2327,
};

extern const char *names502[];
static const uint32 types502 [] =
{
SK_BOOL,
};

static const uint16 attr_flags502 [] =
{0,};

static const EIF_TYPE_INDEX g_atype502_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes502 [] = {
g_atype502_0,
};

static const int32 cn_attr502 [] =
{
2327,
};

extern const char *names503[];
static const uint32 types503 [] =
{
SK_BOOL,
};

static const uint16 attr_flags503 [] =
{0,};

static const EIF_TYPE_INDEX g_atype503_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes503 [] = {
g_atype503_0,
};

static const int32 cn_attr503 [] =
{
2327,
};

extern const char *names504[];
static const uint32 types504 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags504 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype504_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype504_5 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes504 [] = {
g_atype504_0,
g_atype504_1,
g_atype504_2,
g_atype504_3,
g_atype504_4,
g_atype504_5,
};

static const int32 cn_attr504 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names505[];
static const uint32 types505 [] =
{
SK_POINTER,
};

static const uint16 attr_flags505 [] =
{0,};

static const EIF_TYPE_INDEX g_atype505_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes505 [] = {
g_atype505_0,
};

static const int32 cn_attr505 [] =
{
4554,
};

extern const char *names506[];
static const uint32 types506 [] =
{
SK_POINTER,
};

static const uint16 attr_flags506 [] =
{0,};

static const EIF_TYPE_INDEX g_atype506_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes506 [] = {
g_atype506_0,
};

static const int32 cn_attr506 [] =
{
4554,
};

extern const char *names507[];
static const uint32 types507 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags507 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype507_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype507_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes507 [] = {
g_atype507_0,
g_atype507_1,
};

static const int32 cn_attr507 [] =
{
3889,
3890,
};

extern const char *names508[];
static const uint32 types508 [] =
{
SK_REF,
};

static const uint16 attr_flags508 [] =
{0,};

static const EIF_TYPE_INDEX g_atype508_0 [] = {0xFF01,508,191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes508 [] = {
g_atype508_0,
};

static const int32 cn_attr508 [] =
{
2156,
};

extern const char *names510[];
static const uint32 types510 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags510 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype510_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype510_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype510_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes510 [] = {
g_atype510_0,
g_atype510_1,
g_atype510_2,
};

static const int32 cn_attr510 [] =
{
3791,
3792,
3794,
};

extern const char *names511[];
static const uint32 types511 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags511 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype511_0 [] = {0xFF01,511,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype511_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype511_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype511_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype511_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype511_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype511_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes511 [] = {
g_atype511_0,
g_atype511_1,
g_atype511_2,
g_atype511_3,
g_atype511_4,
g_atype511_5,
g_atype511_6,
};

static const int32 cn_attr511 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names517[];
static const uint32 types517 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags517 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype517_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype517_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype517_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes517 [] = {
g_atype517_0,
g_atype517_1,
g_atype517_2,
};

static const int32 cn_attr517 [] =
{
3791,
3792,
3794,
};

extern const char *names519[];
static const uint32 types519 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags519 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype519_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype519_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes519 [] = {
g_atype519_0,
g_atype519_1,
};

static const int32 cn_attr519 [] =
{
3889,
3890,
};

extern const char *names520[];
static const uint32 types520 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags520 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype520_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype520_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype520_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype520_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes520 [] = {
g_atype520_0,
g_atype520_1,
g_atype520_2,
g_atype520_3,
};

static const int32 cn_attr520 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names521[];
static const uint32 types521 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags521 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype521_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype521_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes521 [] = {
g_atype521_0,
g_atype521_1,
g_atype521_2,
};

static const int32 cn_attr521 [] =
{
2156,
2327,
3094,
};

extern const char *names522[];
static const uint32 types522 [] =
{
SK_BOOL,
};

static const uint16 attr_flags522 [] =
{0,};

static const EIF_TYPE_INDEX g_atype522_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes522 [] = {
g_atype522_0,
};

static const int32 cn_attr522 [] =
{
2327,
};

extern const char *names523[];
static const uint32 types523 [] =
{
SK_BOOL,
};

static const uint16 attr_flags523 [] =
{0,};

static const EIF_TYPE_INDEX g_atype523_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes523 [] = {
g_atype523_0,
};

static const int32 cn_attr523 [] =
{
2327,
};

extern const char *names524[];
static const uint32 types524 [] =
{
SK_BOOL,
};

static const uint16 attr_flags524 [] =
{0,};

static const EIF_TYPE_INDEX g_atype524_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes524 [] = {
g_atype524_0,
};

static const int32 cn_attr524 [] =
{
2327,
};

extern const char *names525[];
static const uint32 types525 [] =
{
SK_BOOL,
};

static const uint16 attr_flags525 [] =
{0,};

static const EIF_TYPE_INDEX g_atype525_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes525 [] = {
g_atype525_0,
};

static const int32 cn_attr525 [] =
{
2327,
};

extern const char *names526[];
static const uint32 types526 [] =
{
SK_BOOL,
};

static const uint16 attr_flags526 [] =
{0,};

static const EIF_TYPE_INDEX g_atype526_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes526 [] = {
g_atype526_0,
};

static const int32 cn_attr526 [] =
{
2327,
};

extern const char *names527[];
static const uint32 types527 [] =
{
SK_BOOL,
};

static const uint16 attr_flags527 [] =
{0,};

static const EIF_TYPE_INDEX g_atype527_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes527 [] = {
g_atype527_0,
};

static const int32 cn_attr527 [] =
{
2327,
};

extern const char *names528[];
static const uint32 types528 [] =
{
SK_BOOL,
};

static const uint16 attr_flags528 [] =
{0,};

static const EIF_TYPE_INDEX g_atype528_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes528 [] = {
g_atype528_0,
};

static const int32 cn_attr528 [] =
{
2327,
};

extern const char *names529[];
static const uint32 types529 [] =
{
SK_BOOL,
};

static const uint16 attr_flags529 [] =
{0,};

static const EIF_TYPE_INDEX g_atype529_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes529 [] = {
g_atype529_0,
};

static const int32 cn_attr529 [] =
{
2327,
};

extern const char *names530[];
static const uint32 types530 [] =
{
SK_BOOL,
};

static const uint16 attr_flags530 [] =
{0,};

static const EIF_TYPE_INDEX g_atype530_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes530 [] = {
g_atype530_0,
};

static const int32 cn_attr530 [] =
{
2327,
};

extern const char *names531[];
static const uint32 types531 [] =
{
SK_BOOL,
};

static const uint16 attr_flags531 [] =
{0,};

static const EIF_TYPE_INDEX g_atype531_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes531 [] = {
g_atype531_0,
};

static const int32 cn_attr531 [] =
{
2327,
};

extern const char *names532[];
static const uint32 types532 [] =
{
SK_BOOL,
};

static const uint16 attr_flags532 [] =
{0,};

static const EIF_TYPE_INDEX g_atype532_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes532 [] = {
g_atype532_0,
};

static const int32 cn_attr532 [] =
{
2327,
};

extern const char *names533[];
static const uint32 types533 [] =
{
SK_BOOL,
};

static const uint16 attr_flags533 [] =
{0,};

static const EIF_TYPE_INDEX g_atype533_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes533 [] = {
g_atype533_0,
};

static const int32 cn_attr533 [] =
{
2327,
};

extern const char *names534[];
static const uint32 types534 [] =
{
SK_BOOL,
};

static const uint16 attr_flags534 [] =
{0,};

static const EIF_TYPE_INDEX g_atype534_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes534 [] = {
g_atype534_0,
};

static const int32 cn_attr534 [] =
{
2327,
};

extern const char *names535[];
static const uint32 types535 [] =
{
SK_BOOL,
};

static const uint16 attr_flags535 [] =
{0,};

static const EIF_TYPE_INDEX g_atype535_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes535 [] = {
g_atype535_0,
};

static const int32 cn_attr535 [] =
{
2327,
};

extern const char *names536[];
static const uint32 types536 [] =
{
SK_BOOL,
};

static const uint16 attr_flags536 [] =
{0,};

static const EIF_TYPE_INDEX g_atype536_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes536 [] = {
g_atype536_0,
};

static const int32 cn_attr536 [] =
{
2327,
};

extern const char *names537[];
static const uint32 types537 [] =
{
SK_BOOL,
};

static const uint16 attr_flags537 [] =
{0,};

static const EIF_TYPE_INDEX g_atype537_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes537 [] = {
g_atype537_0,
};

static const int32 cn_attr537 [] =
{
2327,
};

extern const char *names538[];
static const uint32 types538 [] =
{
SK_BOOL,
};

static const uint16 attr_flags538 [] =
{0,};

static const EIF_TYPE_INDEX g_atype538_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes538 [] = {
g_atype538_0,
};

static const int32 cn_attr538 [] =
{
2327,
};

extern const char *names539[];
static const uint32 types539 [] =
{
SK_BOOL,
};

static const uint16 attr_flags539 [] =
{0,};

static const EIF_TYPE_INDEX g_atype539_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes539 [] = {
g_atype539_0,
};

static const int32 cn_attr539 [] =
{
2327,
};

extern const char *names540[];
static const uint32 types540 [] =
{
SK_BOOL,
};

static const uint16 attr_flags540 [] =
{0,};

static const EIF_TYPE_INDEX g_atype540_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes540 [] = {
g_atype540_0,
};

static const int32 cn_attr540 [] =
{
2327,
};

extern const char *names541[];
static const uint32 types541 [] =
{
SK_BOOL,
};

static const uint16 attr_flags541 [] =
{0,};

static const EIF_TYPE_INDEX g_atype541_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes541 [] = {
g_atype541_0,
};

static const int32 cn_attr541 [] =
{
2327,
};

extern const char *names542[];
static const uint32 types542 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags542 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype542_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype542_1 [] = {0xFF01,520,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype542_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype542_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes542 [] = {
g_atype542_0,
g_atype542_1,
g_atype542_2,
g_atype542_3,
};

static const int32 cn_attr542 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names543[];
static const uint32 types543 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags543 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype543_0 [] = {0xFF01,508,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype543_1 [] = {0xFF01,519,191,0xFFFF};
static const EIF_TYPE_INDEX g_atype543_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype543_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype543_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes543 [] = {
g_atype543_0,
g_atype543_1,
g_atype543_2,
g_atype543_3,
g_atype543_4,
};

static const int32 cn_attr543 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names544[];
static const uint32 types544 [] =
{
SK_REF,
};

static const uint16 attr_flags544 [] =
{0,};

static const EIF_TYPE_INDEX g_atype544_0 [] = {0xFF01,544,200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes544 [] = {
g_atype544_0,
};

static const int32 cn_attr544 [] =
{
2156,
};

extern const char *names546[];
static const uint32 types546 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags546 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype546_0 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype546_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype546_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes546 [] = {
g_atype546_0,
g_atype546_1,
g_atype546_2,
};

static const int32 cn_attr546 [] =
{
3791,
3792,
3794,
};

extern const char *names547[];
static const uint32 types547 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags547 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype547_0 [] = {0xFF01,547,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype547_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes547 [] = {
g_atype547_0,
g_atype547_1,
g_atype547_2,
g_atype547_3,
g_atype547_4,
g_atype547_5,
g_atype547_6,
};

static const int32 cn_attr547 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names553[];
static const uint32 types553 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags553 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype553_0 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype553_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype553_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes553 [] = {
g_atype553_0,
g_atype553_1,
g_atype553_2,
};

static const int32 cn_attr553 [] =
{
3791,
3792,
3794,
};

extern const char *names555[];
static const uint32 types555 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags555 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype555_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype555_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes555 [] = {
g_atype555_0,
g_atype555_1,
};

static const int32 cn_attr555 [] =
{
3889,
3890,
};

extern const char *names556[];
static const uint32 types556 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags556 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype556_0 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype556_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype556_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype556_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes556 [] = {
g_atype556_0,
g_atype556_1,
g_atype556_2,
g_atype556_3,
};

static const int32 cn_attr556 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names557[];
static const uint32 types557 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags557 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype557_0 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype557_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes557 [] = {
g_atype557_0,
g_atype557_1,
g_atype557_2,
};

static const int32 cn_attr557 [] =
{
2156,
2327,
3094,
};

extern const char *names558[];
static const uint32 types558 [] =
{
SK_BOOL,
};

static const uint16 attr_flags558 [] =
{0,};

static const EIF_TYPE_INDEX g_atype558_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes558 [] = {
g_atype558_0,
};

static const int32 cn_attr558 [] =
{
2327,
};

extern const char *names559[];
static const uint32 types559 [] =
{
SK_BOOL,
};

static const uint16 attr_flags559 [] =
{0,};

static const EIF_TYPE_INDEX g_atype559_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes559 [] = {
g_atype559_0,
};

static const int32 cn_attr559 [] =
{
2327,
};

extern const char *names560[];
static const uint32 types560 [] =
{
SK_BOOL,
};

static const uint16 attr_flags560 [] =
{0,};

static const EIF_TYPE_INDEX g_atype560_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes560 [] = {
g_atype560_0,
};

static const int32 cn_attr560 [] =
{
2327,
};

extern const char *names561[];
static const uint32 types561 [] =
{
SK_BOOL,
};

static const uint16 attr_flags561 [] =
{0,};

static const EIF_TYPE_INDEX g_atype561_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes561 [] = {
g_atype561_0,
};

static const int32 cn_attr561 [] =
{
2327,
};

extern const char *names562[];
static const uint32 types562 [] =
{
SK_BOOL,
};

static const uint16 attr_flags562 [] =
{0,};

static const EIF_TYPE_INDEX g_atype562_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes562 [] = {
g_atype562_0,
};

static const int32 cn_attr562 [] =
{
2327,
};

extern const char *names563[];
static const uint32 types563 [] =
{
SK_BOOL,
};

static const uint16 attr_flags563 [] =
{0,};

static const EIF_TYPE_INDEX g_atype563_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes563 [] = {
g_atype563_0,
};

static const int32 cn_attr563 [] =
{
2327,
};

extern const char *names564[];
static const uint32 types564 [] =
{
SK_BOOL,
};

static const uint16 attr_flags564 [] =
{0,};

static const EIF_TYPE_INDEX g_atype564_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes564 [] = {
g_atype564_0,
};

static const int32 cn_attr564 [] =
{
2327,
};

extern const char *names565[];
static const uint32 types565 [] =
{
SK_BOOL,
};

static const uint16 attr_flags565 [] =
{0,};

static const EIF_TYPE_INDEX g_atype565_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes565 [] = {
g_atype565_0,
};

static const int32 cn_attr565 [] =
{
2327,
};

extern const char *names566[];
static const uint32 types566 [] =
{
SK_BOOL,
};

static const uint16 attr_flags566 [] =
{0,};

static const EIF_TYPE_INDEX g_atype566_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes566 [] = {
g_atype566_0,
};

static const int32 cn_attr566 [] =
{
2327,
};

extern const char *names567[];
static const uint32 types567 [] =
{
SK_BOOL,
};

static const uint16 attr_flags567 [] =
{0,};

static const EIF_TYPE_INDEX g_atype567_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes567 [] = {
g_atype567_0,
};

static const int32 cn_attr567 [] =
{
2327,
};

extern const char *names568[];
static const uint32 types568 [] =
{
SK_BOOL,
};

static const uint16 attr_flags568 [] =
{0,};

static const EIF_TYPE_INDEX g_atype568_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes568 [] = {
g_atype568_0,
};

static const int32 cn_attr568 [] =
{
2327,
};

extern const char *names569[];
static const uint32 types569 [] =
{
SK_BOOL,
};

static const uint16 attr_flags569 [] =
{0,};

static const EIF_TYPE_INDEX g_atype569_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes569 [] = {
g_atype569_0,
};

static const int32 cn_attr569 [] =
{
2327,
};

extern const char *names570[];
static const uint32 types570 [] =
{
SK_BOOL,
};

static const uint16 attr_flags570 [] =
{0,};

static const EIF_TYPE_INDEX g_atype570_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes570 [] = {
g_atype570_0,
};

static const int32 cn_attr570 [] =
{
2327,
};

extern const char *names571[];
static const uint32 types571 [] =
{
SK_BOOL,
};

static const uint16 attr_flags571 [] =
{0,};

static const EIF_TYPE_INDEX g_atype571_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes571 [] = {
g_atype571_0,
};

static const int32 cn_attr571 [] =
{
2327,
};

extern const char *names572[];
static const uint32 types572 [] =
{
SK_BOOL,
};

static const uint16 attr_flags572 [] =
{0,};

static const EIF_TYPE_INDEX g_atype572_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes572 [] = {
g_atype572_0,
};

static const int32 cn_attr572 [] =
{
2327,
};

extern const char *names573[];
static const uint32 types573 [] =
{
SK_BOOL,
};

static const uint16 attr_flags573 [] =
{0,};

static const EIF_TYPE_INDEX g_atype573_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes573 [] = {
g_atype573_0,
};

static const int32 cn_attr573 [] =
{
2327,
};

extern const char *names574[];
static const uint32 types574 [] =
{
SK_BOOL,
};

static const uint16 attr_flags574 [] =
{0,};

static const EIF_TYPE_INDEX g_atype574_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes574 [] = {
g_atype574_0,
};

static const int32 cn_attr574 [] =
{
2327,
};

extern const char *names575[];
static const uint32 types575 [] =
{
SK_BOOL,
};

static const uint16 attr_flags575 [] =
{0,};

static const EIF_TYPE_INDEX g_atype575_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes575 [] = {
g_atype575_0,
};

static const int32 cn_attr575 [] =
{
2327,
};

extern const char *names576[];
static const uint32 types576 [] =
{
SK_BOOL,
};

static const uint16 attr_flags576 [] =
{0,};

static const EIF_TYPE_INDEX g_atype576_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes576 [] = {
g_atype576_0,
};

static const int32 cn_attr576 [] =
{
2327,
};

extern const char *names577[];
static const uint32 types577 [] =
{
SK_BOOL,
};

static const uint16 attr_flags577 [] =
{0,};

static const EIF_TYPE_INDEX g_atype577_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes577 [] = {
g_atype577_0,
};

static const int32 cn_attr577 [] =
{
2327,
};

extern const char *names578[];
static const uint32 types578 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags578 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype578_0 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype578_1 [] = {0xFF01,556,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype578_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype578_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes578 [] = {
g_atype578_0,
g_atype578_1,
g_atype578_2,
g_atype578_3,
};

static const int32 cn_attr578 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names579[];
static const uint32 types579 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags579 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype579_0 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_1 [] = {0xFF01,555,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype579_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes579 [] = {
g_atype579_0,
g_atype579_1,
g_atype579_2,
g_atype579_3,
g_atype579_4,
};

static const int32 cn_attr579 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names580[];
static const uint32 types580 [] =
{
SK_BOOL,
};

static const uint16 attr_flags580 [] =
{0,};

static const EIF_TYPE_INDEX g_atype580_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes580 [] = {
g_atype580_0,
};

static const int32 cn_attr580 [] =
{
2327,
};

extern const char *names583[];
static const uint32 types583 [] =
{
SK_BOOL,
};

static const uint16 attr_flags583 [] =
{0,};

static const EIF_TYPE_INDEX g_atype583_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes583 [] = {
g_atype583_0,
};

static const int32 cn_attr583 [] =
{
2327,
};

extern const char *names584[];
static const uint32 types584 [] =
{
SK_BOOL,
};

static const uint16 attr_flags584 [] =
{0,};

static const EIF_TYPE_INDEX g_atype584_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes584 [] = {
g_atype584_0,
};

static const int32 cn_attr584 [] =
{
2327,
};

extern const char *names585[];
static const uint32 types585 [] =
{
SK_BOOL,
};

static const uint16 attr_flags585 [] =
{0,};

static const EIF_TYPE_INDEX g_atype585_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes585 [] = {
g_atype585_0,
};

static const int32 cn_attr585 [] =
{
2327,
};

extern const char *names586[];
static const uint32 types586 [] =
{
SK_BOOL,
};

static const uint16 attr_flags586 [] =
{0,};

static const EIF_TYPE_INDEX g_atype586_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes586 [] = {
g_atype586_0,
};

static const int32 cn_attr586 [] =
{
2327,
};

extern const char *names587[];
static const uint32 types587 [] =
{
SK_BOOL,
};

static const uint16 attr_flags587 [] =
{0,};

static const EIF_TYPE_INDEX g_atype587_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes587 [] = {
g_atype587_0,
};

static const int32 cn_attr587 [] =
{
2327,
};

extern const char *names588[];
static const uint32 types588 [] =
{
SK_BOOL,
};

static const uint16 attr_flags588 [] =
{0,};

static const EIF_TYPE_INDEX g_atype588_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes588 [] = {
g_atype588_0,
};

static const int32 cn_attr588 [] =
{
2327,
};

extern const char *names589[];
static const uint32 types589 [] =
{
SK_BOOL,
};

static const uint16 attr_flags589 [] =
{0,};

static const EIF_TYPE_INDEX g_atype589_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes589 [] = {
g_atype589_0,
};

static const int32 cn_attr589 [] =
{
2327,
};

extern const char *names590[];
static const uint32 types590 [] =
{
SK_BOOL,
};

static const uint16 attr_flags590 [] =
{0,};

static const EIF_TYPE_INDEX g_atype590_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes590 [] = {
g_atype590_0,
};

static const int32 cn_attr590 [] =
{
2327,
};

extern const char *names593[];
static const uint32 types593 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags593 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype593_0 [] = {0xFF01,590,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype593_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype593_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype593_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype593_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype593_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype593_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes593 [] = {
g_atype593_0,
g_atype593_1,
g_atype593_2,
g_atype593_3,
g_atype593_4,
g_atype593_5,
g_atype593_6,
};

static const int32 cn_attr593 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names595[];
static const uint32 types595 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags595 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype595_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype595_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype595_2 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype595_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype595_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype595_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes595 [] = {
g_atype595_0,
g_atype595_1,
g_atype595_2,
g_atype595_3,
g_atype595_4,
g_atype595_5,
};

static const int32 cn_attr595 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names596[];
static const uint32 types596 [] =
{
SK_POINTER,
};

static const uint16 attr_flags596 [] =
{0,};

static const EIF_TYPE_INDEX g_atype596_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes596 [] = {
g_atype596_0,
};

static const int32 cn_attr596 [] =
{
4554,
};

extern const char *names597[];
static const uint32 types597 [] =
{
SK_POINTER,
};

static const uint16 attr_flags597 [] =
{0,};

static const EIF_TYPE_INDEX g_atype597_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes597 [] = {
g_atype597_0,
};

static const int32 cn_attr597 [] =
{
4554,
};

extern const char *names598[];
static const uint32 types598 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags598 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype598_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype598_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes598 [] = {
g_atype598_0,
g_atype598_1,
};

static const int32 cn_attr598 [] =
{
3889,
3890,
};

extern const char *names599[];
static const uint32 types599 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags599 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype599_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype599_5 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes599 [] = {
g_atype599_0,
g_atype599_1,
g_atype599_2,
g_atype599_3,
g_atype599_4,
g_atype599_5,
};

static const int32 cn_attr599 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names600[];
static const uint32 types600 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags600 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype600_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype600_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype600_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype600_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype600_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype600_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes600 [] = {
g_atype600_0,
g_atype600_1,
g_atype600_2,
g_atype600_3,
g_atype600_4,
g_atype600_5,
};

static const int32 cn_attr600 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names601[];
static const uint32 types601 [] =
{
SK_POINTER,
};

static const uint16 attr_flags601 [] =
{0,};

static const EIF_TYPE_INDEX g_atype601_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes601 [] = {
g_atype601_0,
};

static const int32 cn_attr601 [] =
{
4554,
};

extern const char *names602[];
static const uint32 types602 [] =
{
SK_POINTER,
};

static const uint16 attr_flags602 [] =
{0,};

static const EIF_TYPE_INDEX g_atype602_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes602 [] = {
g_atype602_0,
};

static const int32 cn_attr602 [] =
{
4554,
};

extern const char *names603[];
static const uint32 types603 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags603 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype603_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype603_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes603 [] = {
g_atype603_0,
g_atype603_1,
};

static const int32 cn_attr603 [] =
{
3889,
3890,
};

extern const char *names604[];
static const uint32 types604 [] =
{
SK_REF,
SK_REF,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags604 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype604_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype604_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype604_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype604_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype604_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype604_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes604 [] = {
g_atype604_0,
g_atype604_1,
g_atype604_2,
g_atype604_3,
g_atype604_4,
g_atype604_5,
};

static const int32 cn_attr604 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names605[];
static const uint32 types605 [] =
{
SK_BOOL,
};

static const uint16 attr_flags605 [] =
{0,};

static const EIF_TYPE_INDEX g_atype605_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes605 [] = {
g_atype605_0,
};

static const int32 cn_attr605 [] =
{
2327,
};

extern const char *names606[];
static const uint32 types606 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_UINT64,
};

static const uint16 attr_flags606 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype606_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype606_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype606_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype606_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype606_4 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes606 [] = {
g_atype606_0,
g_atype606_1,
g_atype606_2,
g_atype606_3,
g_atype606_4,
};

static const int32 cn_attr606 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names607[];
static const uint32 types607 [] =
{
SK_REF,
SK_REF,
SK_INT16,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags607 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype607_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype607_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype607_2 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype607_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype607_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes607 [] = {
g_atype607_0,
g_atype607_1,
g_atype607_2,
g_atype607_3,
g_atype607_4,
};

static const int32 cn_attr607 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names608[];
static const uint32 types608 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags608 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype608_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype608_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype608_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype608_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype608_4 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes608 [] = {
g_atype608_0,
g_atype608_1,
g_atype608_2,
g_atype608_3,
g_atype608_4,
};

static const int32 cn_attr608 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names609[];
static const uint32 types609 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags609 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype609_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype609_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype609_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype609_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes609 [] = {
g_atype609_0,
g_atype609_1,
g_atype609_2,
g_atype609_3,
};

static const int32 cn_attr609 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names610[];
static const uint32 types610 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags610 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype610_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype610_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype610_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes610 [] = {
g_atype610_0,
g_atype610_1,
g_atype610_2,
};

static const int32 cn_attr610 [] =
{
2156,
2327,
3094,
};

extern const char *names612[];
static const uint32 types612 [] =
{
SK_BOOL,
};

static const uint16 attr_flags612 [] =
{0,};

static const EIF_TYPE_INDEX g_atype612_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes612 [] = {
g_atype612_0,
};

static const int32 cn_attr612 [] =
{
2327,
};

extern const char *names613[];
static const uint32 types613 [] =
{
SK_BOOL,
};

static const uint16 attr_flags613 [] =
{0,};

static const EIF_TYPE_INDEX g_atype613_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes613 [] = {
g_atype613_0,
};

static const int32 cn_attr613 [] =
{
2327,
};

extern const char *names614[];
static const uint32 types614 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags614 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype614_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype614_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes614 [] = {
g_atype614_0,
g_atype614_1,
};

static const int32 cn_attr614 [] =
{
3889,
3890,
};

extern const char *names616[];
static const uint32 types616 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags616 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype616_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype616_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype616_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes616 [] = {
g_atype616_0,
g_atype616_1,
g_atype616_2,
};

static const int32 cn_attr616 [] =
{
3791,
3792,
3794,
};

extern const char *names617[];
static const uint32 types617 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags617 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype617_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype617_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype617_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes617 [] = {
g_atype617_0,
g_atype617_1,
g_atype617_2,
};

static const int32 cn_attr617 [] =
{
3791,
3792,
3794,
};

extern const char *names618[];
static const uint32 types618 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags618 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype618_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype618_1 [] = {0xFF01,608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype618_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype618_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype618_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes618 [] = {
g_atype618_0,
g_atype618_1,
g_atype618_2,
g_atype618_3,
g_atype618_4,
};

static const int32 cn_attr618 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names619[];
static const uint32 types619 [] =
{
SK_REF,
};

static const uint16 attr_flags619 [] =
{0,};

static const EIF_TYPE_INDEX g_atype619_0 [] = {0xFF01,614,221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes619 [] = {
g_atype619_0,
};

static const int32 cn_attr619 [] =
{
2156,
};

extern const char *names620[];
static const uint32 types620 [] =
{
SK_BOOL,
};

static const uint16 attr_flags620 [] =
{0,};

static const EIF_TYPE_INDEX g_atype620_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes620 [] = {
g_atype620_0,
};

static const int32 cn_attr620 [] =
{
2327,
};

extern const char *names621[];
static const uint32 types621 [] =
{
SK_BOOL,
};

static const uint16 attr_flags621 [] =
{0,};

static const EIF_TYPE_INDEX g_atype621_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes621 [] = {
g_atype621_0,
};

static const int32 cn_attr621 [] =
{
2327,
};

extern const char *names622[];
static const uint32 types622 [] =
{
SK_BOOL,
};

static const uint16 attr_flags622 [] =
{0,};

static const EIF_TYPE_INDEX g_atype622_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes622 [] = {
g_atype622_0,
};

static const int32 cn_attr622 [] =
{
2327,
};

extern const char *names623[];
static const uint32 types623 [] =
{
SK_BOOL,
};

static const uint16 attr_flags623 [] =
{0,};

static const EIF_TYPE_INDEX g_atype623_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes623 [] = {
g_atype623_0,
};

static const int32 cn_attr623 [] =
{
2327,
};

extern const char *names624[];
static const uint32 types624 [] =
{
SK_BOOL,
};

static const uint16 attr_flags624 [] =
{0,};

static const EIF_TYPE_INDEX g_atype624_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes624 [] = {
g_atype624_0,
};

static const int32 cn_attr624 [] =
{
2327,
};

extern const char *names625[];
static const uint32 types625 [] =
{
SK_BOOL,
};

static const uint16 attr_flags625 [] =
{0,};

static const EIF_TYPE_INDEX g_atype625_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes625 [] = {
g_atype625_0,
};

static const int32 cn_attr625 [] =
{
2327,
};

extern const char *names626[];
static const uint32 types626 [] =
{
SK_BOOL,
};

static const uint16 attr_flags626 [] =
{0,};

static const EIF_TYPE_INDEX g_atype626_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes626 [] = {
g_atype626_0,
};

static const int32 cn_attr626 [] =
{
2327,
};

extern const char *names627[];
static const uint32 types627 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags627 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype627_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_1 [] = {0xFF01,609,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype627_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes627 [] = {
g_atype627_0,
g_atype627_1,
g_atype627_2,
g_atype627_3,
};

static const int32 cn_attr627 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names628[];
static const uint32 types628 [] =
{
SK_INT32,
};

static const uint16 attr_flags628 [] =
{0,};

static const EIF_TYPE_INDEX g_atype628_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes628 [] = {
g_atype628_0,
};

static const int32 cn_attr628 [] =
{
1718,
};

extern const char *names629[];
static const uint32 types629 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags629 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype629_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype629_4 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes629 [] = {
g_atype629_0,
g_atype629_1,
g_atype629_2,
g_atype629_3,
g_atype629_4,
};

static const int32 cn_attr629 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names631[];
static const uint32 types631 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags631 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype631_0 [] = {0xFF01,629,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype631_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype631_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes631 [] = {
g_atype631_0,
g_atype631_1,
g_atype631_2,
};

static const int32 cn_attr631 [] =
{
3791,
3792,
3794,
};

extern const char *names632[];
static const uint32 types632 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags632 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype632_0 [] = {0xFF01,632,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype632_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes632 [] = {
g_atype632_0,
g_atype632_1,
g_atype632_2,
g_atype632_3,
g_atype632_4,
g_atype632_5,
g_atype632_6,
};

static const int32 cn_attr632 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names638[];
static const uint32 types638 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags638 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype638_0 [] = {0xFF01,629,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype638_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype638_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes638 [] = {
g_atype638_0,
g_atype638_1,
g_atype638_2,
};

static const int32 cn_attr638 [] =
{
3791,
3792,
3794,
};

extern const char *names640[];
static const uint32 types640 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags640 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype640_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype640_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes640 [] = {
g_atype640_0,
g_atype640_1,
};

static const int32 cn_attr640 [] =
{
3889,
3890,
};

extern const char *names641[];
static const uint32 types641 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags641 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype641_0 [] = {0xFF01,629,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype641_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype641_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype641_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes641 [] = {
g_atype641_0,
g_atype641_1,
g_atype641_2,
g_atype641_3,
};

static const int32 cn_attr641 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names642[];
static const uint32 types642 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags642 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype642_0 [] = {0xFF01,629,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype642_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype642_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes642 [] = {
g_atype642_0,
g_atype642_1,
g_atype642_2,
};

static const int32 cn_attr642 [] =
{
2156,
2327,
3094,
};

extern const char *names643[];
static const uint32 types643 [] =
{
SK_BOOL,
};

static const uint16 attr_flags643 [] =
{0,};

static const EIF_TYPE_INDEX g_atype643_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes643 [] = {
g_atype643_0,
};

static const int32 cn_attr643 [] =
{
2327,
};

extern const char *names644[];
static const uint32 types644 [] =
{
SK_BOOL,
};

static const uint16 attr_flags644 [] =
{0,};

static const EIF_TYPE_INDEX g_atype644_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes644 [] = {
g_atype644_0,
};

static const int32 cn_attr644 [] =
{
2327,
};

extern const char *names645[];
static const uint32 types645 [] =
{
SK_BOOL,
};

static const uint16 attr_flags645 [] =
{0,};

static const EIF_TYPE_INDEX g_atype645_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes645 [] = {
g_atype645_0,
};

static const int32 cn_attr645 [] =
{
2327,
};

extern const char *names646[];
static const uint32 types646 [] =
{
SK_BOOL,
};

static const uint16 attr_flags646 [] =
{0,};

static const EIF_TYPE_INDEX g_atype646_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes646 [] = {
g_atype646_0,
};

static const int32 cn_attr646 [] =
{
2327,
};

extern const char *names647[];
static const uint32 types647 [] =
{
SK_BOOL,
};

static const uint16 attr_flags647 [] =
{0,};

static const EIF_TYPE_INDEX g_atype647_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes647 [] = {
g_atype647_0,
};

static const int32 cn_attr647 [] =
{
2327,
};

extern const char *names648[];
static const uint32 types648 [] =
{
SK_BOOL,
};

static const uint16 attr_flags648 [] =
{0,};

static const EIF_TYPE_INDEX g_atype648_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes648 [] = {
g_atype648_0,
};

static const int32 cn_attr648 [] =
{
2327,
};

extern const char *names649[];
static const uint32 types649 [] =
{
SK_BOOL,
};

static const uint16 attr_flags649 [] =
{0,};

static const EIF_TYPE_INDEX g_atype649_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes649 [] = {
g_atype649_0,
};

static const int32 cn_attr649 [] =
{
2327,
};

extern const char *names650[];
static const uint32 types650 [] =
{
SK_BOOL,
};

static const uint16 attr_flags650 [] =
{0,};

static const EIF_TYPE_INDEX g_atype650_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes650 [] = {
g_atype650_0,
};

static const int32 cn_attr650 [] =
{
2327,
};

extern const char *names651[];
static const uint32 types651 [] =
{
SK_BOOL,
};

static const uint16 attr_flags651 [] =
{0,};

static const EIF_TYPE_INDEX g_atype651_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes651 [] = {
g_atype651_0,
};

static const int32 cn_attr651 [] =
{
2327,
};

extern const char *names652[];
static const uint32 types652 [] =
{
SK_BOOL,
};

static const uint16 attr_flags652 [] =
{0,};

static const EIF_TYPE_INDEX g_atype652_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes652 [] = {
g_atype652_0,
};

static const int32 cn_attr652 [] =
{
2327,
};

extern const char *names653[];
static const uint32 types653 [] =
{
SK_BOOL,
};

static const uint16 attr_flags653 [] =
{0,};

static const EIF_TYPE_INDEX g_atype653_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes653 [] = {
g_atype653_0,
};

static const int32 cn_attr653 [] =
{
2327,
};

extern const char *names654[];
static const uint32 types654 [] =
{
SK_BOOL,
};

static const uint16 attr_flags654 [] =
{0,};

static const EIF_TYPE_INDEX g_atype654_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes654 [] = {
g_atype654_0,
};

static const int32 cn_attr654 [] =
{
2327,
};

extern const char *names655[];
static const uint32 types655 [] =
{
SK_BOOL,
};

static const uint16 attr_flags655 [] =
{0,};

static const EIF_TYPE_INDEX g_atype655_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes655 [] = {
g_atype655_0,
};

static const int32 cn_attr655 [] =
{
2327,
};

extern const char *names656[];
static const uint32 types656 [] =
{
SK_BOOL,
};

static const uint16 attr_flags656 [] =
{0,};

static const EIF_TYPE_INDEX g_atype656_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes656 [] = {
g_atype656_0,
};

static const int32 cn_attr656 [] =
{
2327,
};

extern const char *names657[];
static const uint32 types657 [] =
{
SK_BOOL,
};

static const uint16 attr_flags657 [] =
{0,};

static const EIF_TYPE_INDEX g_atype657_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes657 [] = {
g_atype657_0,
};

static const int32 cn_attr657 [] =
{
2327,
};

extern const char *names658[];
static const uint32 types658 [] =
{
SK_BOOL,
};

static const uint16 attr_flags658 [] =
{0,};

static const EIF_TYPE_INDEX g_atype658_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes658 [] = {
g_atype658_0,
};

static const int32 cn_attr658 [] =
{
2327,
};

extern const char *names659[];
static const uint32 types659 [] =
{
SK_BOOL,
};

static const uint16 attr_flags659 [] =
{0,};

static const EIF_TYPE_INDEX g_atype659_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes659 [] = {
g_atype659_0,
};

static const int32 cn_attr659 [] =
{
2327,
};

extern const char *names660[];
static const uint32 types660 [] =
{
SK_BOOL,
};

static const uint16 attr_flags660 [] =
{0,};

static const EIF_TYPE_INDEX g_atype660_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes660 [] = {
g_atype660_0,
};

static const int32 cn_attr660 [] =
{
2327,
};

extern const char *names661[];
static const uint32 types661 [] =
{
SK_REF,
};

static const uint16 attr_flags661 [] =
{0,};

static const EIF_TYPE_INDEX g_atype661_0 [] = {0xFF01,629,218,0xFFFF};

static const EIF_TYPE_INDEX *gtypes661 [] = {
g_atype661_0,
};

static const int32 cn_attr661 [] =
{
2156,
};

extern const char *names662[];
static const uint32 types662 [] =
{
SK_BOOL,
};

static const uint16 attr_flags662 [] =
{0,};

static const EIF_TYPE_INDEX g_atype662_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes662 [] = {
g_atype662_0,
};

static const int32 cn_attr662 [] =
{
2327,
};

extern const char *names663[];
static const uint32 types663 [] =
{
SK_BOOL,
};

static const uint16 attr_flags663 [] =
{0,};

static const EIF_TYPE_INDEX g_atype663_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes663 [] = {
g_atype663_0,
};

static const int32 cn_attr663 [] =
{
2327,
};

extern const char *names664[];
static const uint32 types664 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags664 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype664_0 [] = {0xFF01,629,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype664_1 [] = {0xFF01,641,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype664_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype664_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes664 [] = {
g_atype664_0,
g_atype664_1,
g_atype664_2,
g_atype664_3,
};

static const int32 cn_attr664 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names665[];
static const uint32 types665 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags665 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype665_0 [] = {0xFF01,629,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype665_1 [] = {0xFF01,640,218,0xFFFF};
static const EIF_TYPE_INDEX g_atype665_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype665_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype665_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes665 [] = {
g_atype665_0,
g_atype665_1,
g_atype665_2,
g_atype665_3,
g_atype665_4,
};

static const int32 cn_attr665 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names667[];
static const uint32 types667 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags667 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype667_0 [] = {0xFF01,665,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype667_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype667_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes667 [] = {
g_atype667_0,
g_atype667_1,
g_atype667_2,
};

static const int32 cn_attr667 [] =
{
3791,
3792,
3794,
};

extern const char *names668[];
static const uint32 types668 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags668 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype668_0 [] = {0xFF01,668,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype668_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes668 [] = {
g_atype668_0,
g_atype668_1,
g_atype668_2,
g_atype668_3,
g_atype668_4,
g_atype668_5,
g_atype668_6,
};

static const int32 cn_attr668 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names674[];
static const uint32 types674 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags674 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype674_0 [] = {0xFF01,665,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype674_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype674_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes674 [] = {
g_atype674_0,
g_atype674_1,
g_atype674_2,
};

static const int32 cn_attr674 [] =
{
3791,
3792,
3794,
};

extern const char *names676[];
static const uint32 types676 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags676 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype676_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype676_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes676 [] = {
g_atype676_0,
g_atype676_1,
};

static const int32 cn_attr676 [] =
{
3889,
3890,
};

extern const char *names677[];
static const uint32 types677 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags677 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype677_0 [] = {0xFF01,665,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype677_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype677_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype677_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes677 [] = {
g_atype677_0,
g_atype677_1,
g_atype677_2,
g_atype677_3,
};

static const int32 cn_attr677 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names678[];
static const uint32 types678 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags678 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype678_0 [] = {0xFF01,665,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype678_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype678_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes678 [] = {
g_atype678_0,
g_atype678_1,
g_atype678_2,
};

static const int32 cn_attr678 [] =
{
2156,
2327,
3094,
};

extern const char *names679[];
static const uint32 types679 [] =
{
SK_BOOL,
};

static const uint16 attr_flags679 [] =
{0,};

static const EIF_TYPE_INDEX g_atype679_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes679 [] = {
g_atype679_0,
};

static const int32 cn_attr679 [] =
{
2327,
};

extern const char *names680[];
static const uint32 types680 [] =
{
SK_BOOL,
};

static const uint16 attr_flags680 [] =
{0,};

static const EIF_TYPE_INDEX g_atype680_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes680 [] = {
g_atype680_0,
};

static const int32 cn_attr680 [] =
{
2327,
};

extern const char *names681[];
static const uint32 types681 [] =
{
SK_BOOL,
};

static const uint16 attr_flags681 [] =
{0,};

static const EIF_TYPE_INDEX g_atype681_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes681 [] = {
g_atype681_0,
};

static const int32 cn_attr681 [] =
{
2327,
};

extern const char *names682[];
static const uint32 types682 [] =
{
SK_BOOL,
};

static const uint16 attr_flags682 [] =
{0,};

static const EIF_TYPE_INDEX g_atype682_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes682 [] = {
g_atype682_0,
};

static const int32 cn_attr682 [] =
{
2327,
};

extern const char *names683[];
static const uint32 types683 [] =
{
SK_BOOL,
};

static const uint16 attr_flags683 [] =
{0,};

static const EIF_TYPE_INDEX g_atype683_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes683 [] = {
g_atype683_0,
};

static const int32 cn_attr683 [] =
{
2327,
};

extern const char *names684[];
static const uint32 types684 [] =
{
SK_BOOL,
};

static const uint16 attr_flags684 [] =
{0,};

static const EIF_TYPE_INDEX g_atype684_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes684 [] = {
g_atype684_0,
};

static const int32 cn_attr684 [] =
{
2327,
};

extern const char *names685[];
static const uint32 types685 [] =
{
SK_BOOL,
};

static const uint16 attr_flags685 [] =
{0,};

static const EIF_TYPE_INDEX g_atype685_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes685 [] = {
g_atype685_0,
};

static const int32 cn_attr685 [] =
{
2327,
};

extern const char *names686[];
static const uint32 types686 [] =
{
SK_BOOL,
};

static const uint16 attr_flags686 [] =
{0,};

static const EIF_TYPE_INDEX g_atype686_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes686 [] = {
g_atype686_0,
};

static const int32 cn_attr686 [] =
{
2327,
};

extern const char *names687[];
static const uint32 types687 [] =
{
SK_BOOL,
};

static const uint16 attr_flags687 [] =
{0,};

static const EIF_TYPE_INDEX g_atype687_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes687 [] = {
g_atype687_0,
};

static const int32 cn_attr687 [] =
{
2327,
};

extern const char *names688[];
static const uint32 types688 [] =
{
SK_BOOL,
};

static const uint16 attr_flags688 [] =
{0,};

static const EIF_TYPE_INDEX g_atype688_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes688 [] = {
g_atype688_0,
};

static const int32 cn_attr688 [] =
{
2327,
};

extern const char *names689[];
static const uint32 types689 [] =
{
SK_BOOL,
};

static const uint16 attr_flags689 [] =
{0,};

static const EIF_TYPE_INDEX g_atype689_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes689 [] = {
g_atype689_0,
};

static const int32 cn_attr689 [] =
{
2327,
};

extern const char *names690[];
static const uint32 types690 [] =
{
SK_BOOL,
};

static const uint16 attr_flags690 [] =
{0,};

static const EIF_TYPE_INDEX g_atype690_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes690 [] = {
g_atype690_0,
};

static const int32 cn_attr690 [] =
{
2327,
};

extern const char *names691[];
static const uint32 types691 [] =
{
SK_BOOL,
};

static const uint16 attr_flags691 [] =
{0,};

static const EIF_TYPE_INDEX g_atype691_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes691 [] = {
g_atype691_0,
};

static const int32 cn_attr691 [] =
{
2327,
};

extern const char *names692[];
static const uint32 types692 [] =
{
SK_BOOL,
};

static const uint16 attr_flags692 [] =
{0,};

static const EIF_TYPE_INDEX g_atype692_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes692 [] = {
g_atype692_0,
};

static const int32 cn_attr692 [] =
{
2327,
};

extern const char *names693[];
static const uint32 types693 [] =
{
SK_BOOL,
};

static const uint16 attr_flags693 [] =
{0,};

static const EIF_TYPE_INDEX g_atype693_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes693 [] = {
g_atype693_0,
};

static const int32 cn_attr693 [] =
{
2327,
};

extern const char *names694[];
static const uint32 types694 [] =
{
SK_BOOL,
};

static const uint16 attr_flags694 [] =
{0,};

static const EIF_TYPE_INDEX g_atype694_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes694 [] = {
g_atype694_0,
};

static const int32 cn_attr694 [] =
{
2327,
};

extern const char *names695[];
static const uint32 types695 [] =
{
SK_BOOL,
};

static const uint16 attr_flags695 [] =
{0,};

static const EIF_TYPE_INDEX g_atype695_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes695 [] = {
g_atype695_0,
};

static const int32 cn_attr695 [] =
{
2327,
};

extern const char *names696[];
static const uint32 types696 [] =
{
SK_BOOL,
};

static const uint16 attr_flags696 [] =
{0,};

static const EIF_TYPE_INDEX g_atype696_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes696 [] = {
g_atype696_0,
};

static const int32 cn_attr696 [] =
{
2327,
};

extern const char *names697[];
static const uint32 types697 [] =
{
SK_REF,
};

static const uint16 attr_flags697 [] =
{0,};

static const EIF_TYPE_INDEX g_atype697_0 [] = {0xFF01,665,203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes697 [] = {
g_atype697_0,
};

static const int32 cn_attr697 [] =
{
2156,
};

extern const char *names698[];
static const uint32 types698 [] =
{
SK_BOOL,
};

static const uint16 attr_flags698 [] =
{0,};

static const EIF_TYPE_INDEX g_atype698_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes698 [] = {
g_atype698_0,
};

static const int32 cn_attr698 [] =
{
2327,
};

extern const char *names699[];
static const uint32 types699 [] =
{
SK_BOOL,
};

static const uint16 attr_flags699 [] =
{0,};

static const EIF_TYPE_INDEX g_atype699_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes699 [] = {
g_atype699_0,
};

static const int32 cn_attr699 [] =
{
2327,
};

extern const char *names700[];
static const uint32 types700 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags700 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype700_0 [] = {0xFF01,665,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype700_1 [] = {0xFF01,677,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype700_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype700_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes700 [] = {
g_atype700_0,
g_atype700_1,
g_atype700_2,
g_atype700_3,
};

static const int32 cn_attr700 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names701[];
static const uint32 types701 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags701 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype701_0 [] = {0xFF01,665,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype701_1 [] = {0xFF01,676,203,0xFFFF};
static const EIF_TYPE_INDEX g_atype701_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype701_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype701_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes701 [] = {
g_atype701_0,
g_atype701_1,
g_atype701_2,
g_atype701_3,
g_atype701_4,
};

static const int32 cn_attr701 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names703[];
static const uint32 types703 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags703 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype703_0 [] = {0xFF01,701,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype703_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype703_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes703 [] = {
g_atype703_0,
g_atype703_1,
g_atype703_2,
};

static const int32 cn_attr703 [] =
{
3791,
3792,
3794,
};

extern const char *names704[];
static const uint32 types704 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags704 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype704_0 [] = {0xFF01,704,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype704_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes704 [] = {
g_atype704_0,
g_atype704_1,
g_atype704_2,
g_atype704_3,
g_atype704_4,
g_atype704_5,
g_atype704_6,
};

static const int32 cn_attr704 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names710[];
static const uint32 types710 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags710 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype710_0 [] = {0xFF01,701,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype710_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype710_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes710 [] = {
g_atype710_0,
g_atype710_1,
g_atype710_2,
};

static const int32 cn_attr710 [] =
{
3791,
3792,
3794,
};

extern const char *names712[];
static const uint32 types712 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags712 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype712_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype712_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes712 [] = {
g_atype712_0,
g_atype712_1,
};

static const int32 cn_attr712 [] =
{
3889,
3890,
};

extern const char *names713[];
static const uint32 types713 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags713 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype713_0 [] = {0xFF01,701,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype713_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype713_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype713_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes713 [] = {
g_atype713_0,
g_atype713_1,
g_atype713_2,
g_atype713_3,
};

static const int32 cn_attr713 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names714[];
static const uint32 types714 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags714 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype714_0 [] = {0xFF01,701,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype714_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype714_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes714 [] = {
g_atype714_0,
g_atype714_1,
g_atype714_2,
};

static const int32 cn_attr714 [] =
{
2156,
2327,
3094,
};

extern const char *names715[];
static const uint32 types715 [] =
{
SK_BOOL,
};

static const uint16 attr_flags715 [] =
{0,};

static const EIF_TYPE_INDEX g_atype715_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes715 [] = {
g_atype715_0,
};

static const int32 cn_attr715 [] =
{
2327,
};

extern const char *names716[];
static const uint32 types716 [] =
{
SK_BOOL,
};

static const uint16 attr_flags716 [] =
{0,};

static const EIF_TYPE_INDEX g_atype716_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes716 [] = {
g_atype716_0,
};

static const int32 cn_attr716 [] =
{
2327,
};

extern const char *names717[];
static const uint32 types717 [] =
{
SK_BOOL,
};

static const uint16 attr_flags717 [] =
{0,};

static const EIF_TYPE_INDEX g_atype717_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes717 [] = {
g_atype717_0,
};

static const int32 cn_attr717 [] =
{
2327,
};

extern const char *names718[];
static const uint32 types718 [] =
{
SK_BOOL,
};

static const uint16 attr_flags718 [] =
{0,};

static const EIF_TYPE_INDEX g_atype718_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes718 [] = {
g_atype718_0,
};

static const int32 cn_attr718 [] =
{
2327,
};

extern const char *names719[];
static const uint32 types719 [] =
{
SK_BOOL,
};

static const uint16 attr_flags719 [] =
{0,};

static const EIF_TYPE_INDEX g_atype719_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes719 [] = {
g_atype719_0,
};

static const int32 cn_attr719 [] =
{
2327,
};

extern const char *names720[];
static const uint32 types720 [] =
{
SK_BOOL,
};

static const uint16 attr_flags720 [] =
{0,};

static const EIF_TYPE_INDEX g_atype720_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes720 [] = {
g_atype720_0,
};

static const int32 cn_attr720 [] =
{
2327,
};

extern const char *names721[];
static const uint32 types721 [] =
{
SK_BOOL,
};

static const uint16 attr_flags721 [] =
{0,};

static const EIF_TYPE_INDEX g_atype721_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes721 [] = {
g_atype721_0,
};

static const int32 cn_attr721 [] =
{
2327,
};

extern const char *names722[];
static const uint32 types722 [] =
{
SK_BOOL,
};

static const uint16 attr_flags722 [] =
{0,};

static const EIF_TYPE_INDEX g_atype722_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes722 [] = {
g_atype722_0,
};

static const int32 cn_attr722 [] =
{
2327,
};

extern const char *names723[];
static const uint32 types723 [] =
{
SK_BOOL,
};

static const uint16 attr_flags723 [] =
{0,};

static const EIF_TYPE_INDEX g_atype723_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes723 [] = {
g_atype723_0,
};

static const int32 cn_attr723 [] =
{
2327,
};

extern const char *names724[];
static const uint32 types724 [] =
{
SK_BOOL,
};

static const uint16 attr_flags724 [] =
{0,};

static const EIF_TYPE_INDEX g_atype724_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes724 [] = {
g_atype724_0,
};

static const int32 cn_attr724 [] =
{
2327,
};

extern const char *names725[];
static const uint32 types725 [] =
{
SK_BOOL,
};

static const uint16 attr_flags725 [] =
{0,};

static const EIF_TYPE_INDEX g_atype725_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes725 [] = {
g_atype725_0,
};

static const int32 cn_attr725 [] =
{
2327,
};

extern const char *names726[];
static const uint32 types726 [] =
{
SK_BOOL,
};

static const uint16 attr_flags726 [] =
{0,};

static const EIF_TYPE_INDEX g_atype726_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes726 [] = {
g_atype726_0,
};

static const int32 cn_attr726 [] =
{
2327,
};

extern const char *names727[];
static const uint32 types727 [] =
{
SK_BOOL,
};

static const uint16 attr_flags727 [] =
{0,};

static const EIF_TYPE_INDEX g_atype727_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes727 [] = {
g_atype727_0,
};

static const int32 cn_attr727 [] =
{
2327,
};

extern const char *names728[];
static const uint32 types728 [] =
{
SK_BOOL,
};

static const uint16 attr_flags728 [] =
{0,};

static const EIF_TYPE_INDEX g_atype728_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes728 [] = {
g_atype728_0,
};

static const int32 cn_attr728 [] =
{
2327,
};

extern const char *names729[];
static const uint32 types729 [] =
{
SK_BOOL,
};

static const uint16 attr_flags729 [] =
{0,};

static const EIF_TYPE_INDEX g_atype729_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes729 [] = {
g_atype729_0,
};

static const int32 cn_attr729 [] =
{
2327,
};

extern const char *names730[];
static const uint32 types730 [] =
{
SK_BOOL,
};

static const uint16 attr_flags730 [] =
{0,};

static const EIF_TYPE_INDEX g_atype730_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes730 [] = {
g_atype730_0,
};

static const int32 cn_attr730 [] =
{
2327,
};

extern const char *names731[];
static const uint32 types731 [] =
{
SK_BOOL,
};

static const uint16 attr_flags731 [] =
{0,};

static const EIF_TYPE_INDEX g_atype731_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes731 [] = {
g_atype731_0,
};

static const int32 cn_attr731 [] =
{
2327,
};

extern const char *names732[];
static const uint32 types732 [] =
{
SK_BOOL,
};

static const uint16 attr_flags732 [] =
{0,};

static const EIF_TYPE_INDEX g_atype732_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes732 [] = {
g_atype732_0,
};

static const int32 cn_attr732 [] =
{
2327,
};

extern const char *names733[];
static const uint32 types733 [] =
{
SK_REF,
};

static const uint16 attr_flags733 [] =
{0,};

static const EIF_TYPE_INDEX g_atype733_0 [] = {0xFF01,701,188,0xFFFF};

static const EIF_TYPE_INDEX *gtypes733 [] = {
g_atype733_0,
};

static const int32 cn_attr733 [] =
{
2156,
};

extern const char *names734[];
static const uint32 types734 [] =
{
SK_BOOL,
};

static const uint16 attr_flags734 [] =
{0,};

static const EIF_TYPE_INDEX g_atype734_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes734 [] = {
g_atype734_0,
};

static const int32 cn_attr734 [] =
{
2327,
};

extern const char *names735[];
static const uint32 types735 [] =
{
SK_BOOL,
};

static const uint16 attr_flags735 [] =
{0,};

static const EIF_TYPE_INDEX g_atype735_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes735 [] = {
g_atype735_0,
};

static const int32 cn_attr735 [] =
{
2327,
};

extern const char *names736[];
static const uint32 types736 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags736 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype736_0 [] = {0xFF01,701,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype736_1 [] = {0xFF01,713,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype736_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype736_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes736 [] = {
g_atype736_0,
g_atype736_1,
g_atype736_2,
g_atype736_3,
};

static const int32 cn_attr736 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names737[];
static const uint32 types737 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags737 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype737_0 [] = {0xFF01,701,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype737_1 [] = {0xFF01,712,188,0xFFFF};
static const EIF_TYPE_INDEX g_atype737_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype737_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype737_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes737 [] = {
g_atype737_0,
g_atype737_1,
g_atype737_2,
g_atype737_3,
g_atype737_4,
};

static const int32 cn_attr737 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names739[];
static const uint32 types739 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags739 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype739_0 [] = {0xFF01,737,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype739_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype739_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes739 [] = {
g_atype739_0,
g_atype739_1,
g_atype739_2,
};

static const int32 cn_attr739 [] =
{
3791,
3792,
3794,
};

extern const char *names740[];
static const uint32 types740 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags740 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype740_0 [] = {0xFF01,740,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype740_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes740 [] = {
g_atype740_0,
g_atype740_1,
g_atype740_2,
g_atype740_3,
g_atype740_4,
g_atype740_5,
g_atype740_6,
};

static const int32 cn_attr740 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names746[];
static const uint32 types746 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags746 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype746_0 [] = {0xFF01,737,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype746_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype746_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes746 [] = {
g_atype746_0,
g_atype746_1,
g_atype746_2,
};

static const int32 cn_attr746 [] =
{
3791,
3792,
3794,
};

extern const char *names748[];
static const uint32 types748 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags748 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype748_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype748_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes748 [] = {
g_atype748_0,
g_atype748_1,
};

static const int32 cn_attr748 [] =
{
3889,
3890,
};

extern const char *names749[];
static const uint32 types749 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags749 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype749_0 [] = {0xFF01,737,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype749_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype749_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype749_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes749 [] = {
g_atype749_0,
g_atype749_1,
g_atype749_2,
g_atype749_3,
};

static const int32 cn_attr749 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names750[];
static const uint32 types750 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags750 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype750_0 [] = {0xFF01,737,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype750_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype750_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes750 [] = {
g_atype750_0,
g_atype750_1,
g_atype750_2,
};

static const int32 cn_attr750 [] =
{
2156,
2327,
3094,
};

extern const char *names751[];
static const uint32 types751 [] =
{
SK_BOOL,
};

static const uint16 attr_flags751 [] =
{0,};

static const EIF_TYPE_INDEX g_atype751_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes751 [] = {
g_atype751_0,
};

static const int32 cn_attr751 [] =
{
2327,
};

extern const char *names752[];
static const uint32 types752 [] =
{
SK_BOOL,
};

static const uint16 attr_flags752 [] =
{0,};

static const EIF_TYPE_INDEX g_atype752_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes752 [] = {
g_atype752_0,
};

static const int32 cn_attr752 [] =
{
2327,
};

extern const char *names753[];
static const uint32 types753 [] =
{
SK_BOOL,
};

static const uint16 attr_flags753 [] =
{0,};

static const EIF_TYPE_INDEX g_atype753_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes753 [] = {
g_atype753_0,
};

static const int32 cn_attr753 [] =
{
2327,
};

extern const char *names754[];
static const uint32 types754 [] =
{
SK_BOOL,
};

static const uint16 attr_flags754 [] =
{0,};

static const EIF_TYPE_INDEX g_atype754_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes754 [] = {
g_atype754_0,
};

static const int32 cn_attr754 [] =
{
2327,
};

extern const char *names755[];
static const uint32 types755 [] =
{
SK_BOOL,
};

static const uint16 attr_flags755 [] =
{0,};

static const EIF_TYPE_INDEX g_atype755_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes755 [] = {
g_atype755_0,
};

static const int32 cn_attr755 [] =
{
2327,
};

extern const char *names756[];
static const uint32 types756 [] =
{
SK_BOOL,
};

static const uint16 attr_flags756 [] =
{0,};

static const EIF_TYPE_INDEX g_atype756_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes756 [] = {
g_atype756_0,
};

static const int32 cn_attr756 [] =
{
2327,
};

extern const char *names757[];
static const uint32 types757 [] =
{
SK_BOOL,
};

static const uint16 attr_flags757 [] =
{0,};

static const EIF_TYPE_INDEX g_atype757_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes757 [] = {
g_atype757_0,
};

static const int32 cn_attr757 [] =
{
2327,
};

extern const char *names758[];
static const uint32 types758 [] =
{
SK_BOOL,
};

static const uint16 attr_flags758 [] =
{0,};

static const EIF_TYPE_INDEX g_atype758_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes758 [] = {
g_atype758_0,
};

static const int32 cn_attr758 [] =
{
2327,
};

extern const char *names759[];
static const uint32 types759 [] =
{
SK_BOOL,
};

static const uint16 attr_flags759 [] =
{0,};

static const EIF_TYPE_INDEX g_atype759_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes759 [] = {
g_atype759_0,
};

static const int32 cn_attr759 [] =
{
2327,
};

extern const char *names760[];
static const uint32 types760 [] =
{
SK_BOOL,
};

static const uint16 attr_flags760 [] =
{0,};

static const EIF_TYPE_INDEX g_atype760_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes760 [] = {
g_atype760_0,
};

static const int32 cn_attr760 [] =
{
2327,
};

extern const char *names761[];
static const uint32 types761 [] =
{
SK_BOOL,
};

static const uint16 attr_flags761 [] =
{0,};

static const EIF_TYPE_INDEX g_atype761_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes761 [] = {
g_atype761_0,
};

static const int32 cn_attr761 [] =
{
2327,
};

extern const char *names762[];
static const uint32 types762 [] =
{
SK_BOOL,
};

static const uint16 attr_flags762 [] =
{0,};

static const EIF_TYPE_INDEX g_atype762_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes762 [] = {
g_atype762_0,
};

static const int32 cn_attr762 [] =
{
2327,
};

extern const char *names763[];
static const uint32 types763 [] =
{
SK_BOOL,
};

static const uint16 attr_flags763 [] =
{0,};

static const EIF_TYPE_INDEX g_atype763_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes763 [] = {
g_atype763_0,
};

static const int32 cn_attr763 [] =
{
2327,
};

extern const char *names764[];
static const uint32 types764 [] =
{
SK_BOOL,
};

static const uint16 attr_flags764 [] =
{0,};

static const EIF_TYPE_INDEX g_atype764_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes764 [] = {
g_atype764_0,
};

static const int32 cn_attr764 [] =
{
2327,
};

extern const char *names765[];
static const uint32 types765 [] =
{
SK_BOOL,
};

static const uint16 attr_flags765 [] =
{0,};

static const EIF_TYPE_INDEX g_atype765_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes765 [] = {
g_atype765_0,
};

static const int32 cn_attr765 [] =
{
2327,
};

extern const char *names766[];
static const uint32 types766 [] =
{
SK_BOOL,
};

static const uint16 attr_flags766 [] =
{0,};

static const EIF_TYPE_INDEX g_atype766_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes766 [] = {
g_atype766_0,
};

static const int32 cn_attr766 [] =
{
2327,
};

extern const char *names767[];
static const uint32 types767 [] =
{
SK_BOOL,
};

static const uint16 attr_flags767 [] =
{0,};

static const EIF_TYPE_INDEX g_atype767_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes767 [] = {
g_atype767_0,
};

static const int32 cn_attr767 [] =
{
2327,
};

extern const char *names768[];
static const uint32 types768 [] =
{
SK_BOOL,
};

static const uint16 attr_flags768 [] =
{0,};

static const EIF_TYPE_INDEX g_atype768_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes768 [] = {
g_atype768_0,
};

static const int32 cn_attr768 [] =
{
2327,
};

extern const char *names769[];
static const uint32 types769 [] =
{
SK_REF,
};

static const uint16 attr_flags769 [] =
{0,};

static const EIF_TYPE_INDEX g_atype769_0 [] = {0xFF01,737,212,0xFFFF};

static const EIF_TYPE_INDEX *gtypes769 [] = {
g_atype769_0,
};

static const int32 cn_attr769 [] =
{
2156,
};

extern const char *names770[];
static const uint32 types770 [] =
{
SK_BOOL,
};

static const uint16 attr_flags770 [] =
{0,};

static const EIF_TYPE_INDEX g_atype770_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes770 [] = {
g_atype770_0,
};

static const int32 cn_attr770 [] =
{
2327,
};

extern const char *names771[];
static const uint32 types771 [] =
{
SK_BOOL,
};

static const uint16 attr_flags771 [] =
{0,};

static const EIF_TYPE_INDEX g_atype771_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes771 [] = {
g_atype771_0,
};

static const int32 cn_attr771 [] =
{
2327,
};

extern const char *names772[];
static const uint32 types772 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags772 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype772_0 [] = {0xFF01,737,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype772_1 [] = {0xFF01,749,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype772_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype772_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes772 [] = {
g_atype772_0,
g_atype772_1,
g_atype772_2,
g_atype772_3,
};

static const int32 cn_attr772 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names773[];
static const uint32 types773 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags773 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype773_0 [] = {0xFF01,737,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype773_1 [] = {0xFF01,748,212,0xFFFF};
static const EIF_TYPE_INDEX g_atype773_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype773_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype773_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes773 [] = {
g_atype773_0,
g_atype773_1,
g_atype773_2,
g_atype773_3,
g_atype773_4,
};

static const int32 cn_attr773 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names775[];
static const uint32 types775 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags775 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype775_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype775_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes775 [] = {
g_atype775_0,
g_atype775_1,
g_atype775_2,
};

static const int32 cn_attr775 [] =
{
3791,
3792,
3794,
};

extern const char *names776[];
static const uint32 types776 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags776 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype776_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype776_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype776_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes776 [] = {
g_atype776_0,
g_atype776_1,
g_atype776_2,
};

static const int32 cn_attr776 [] =
{
3791,
3792,
3794,
};

extern const char *names778[];
static const uint32 types778 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags778 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype778_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype778_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes778 [] = {
g_atype778_0,
g_atype778_1,
};

static const int32 cn_attr778 [] =
{
3889,
3890,
};

extern const char *names779[];
static const uint32 types779 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags779 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype779_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype779_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype779_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype779_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes779 [] = {
g_atype779_0,
g_atype779_1,
g_atype779_2,
g_atype779_3,
};

static const int32 cn_attr779 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names780[];
static const uint32 types780 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags780 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype780_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype780_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype780_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes780 [] = {
g_atype780_0,
g_atype780_1,
g_atype780_2,
};

static const int32 cn_attr780 [] =
{
2156,
2327,
3094,
};

extern const char *names781[];
static const uint32 types781 [] =
{
SK_BOOL,
};

static const uint16 attr_flags781 [] =
{0,};

static const EIF_TYPE_INDEX g_atype781_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes781 [] = {
g_atype781_0,
};

static const int32 cn_attr781 [] =
{
2327,
};

extern const char *names782[];
static const uint32 types782 [] =
{
SK_BOOL,
};

static const uint16 attr_flags782 [] =
{0,};

static const EIF_TYPE_INDEX g_atype782_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes782 [] = {
g_atype782_0,
};

static const int32 cn_attr782 [] =
{
2327,
};

extern const char *names783[];
static const uint32 types783 [] =
{
SK_BOOL,
};

static const uint16 attr_flags783 [] =
{0,};

static const EIF_TYPE_INDEX g_atype783_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes783 [] = {
g_atype783_0,
};

static const int32 cn_attr783 [] =
{
2327,
};

extern const char *names784[];
static const uint32 types784 [] =
{
SK_BOOL,
};

static const uint16 attr_flags784 [] =
{0,};

static const EIF_TYPE_INDEX g_atype784_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes784 [] = {
g_atype784_0,
};

static const int32 cn_attr784 [] =
{
2327,
};

extern const char *names785[];
static const uint32 types785 [] =
{
SK_BOOL,
};

static const uint16 attr_flags785 [] =
{0,};

static const EIF_TYPE_INDEX g_atype785_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes785 [] = {
g_atype785_0,
};

static const int32 cn_attr785 [] =
{
2327,
};

extern const char *names786[];
static const uint32 types786 [] =
{
SK_BOOL,
};

static const uint16 attr_flags786 [] =
{0,};

static const EIF_TYPE_INDEX g_atype786_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes786 [] = {
g_atype786_0,
};

static const int32 cn_attr786 [] =
{
2327,
};

extern const char *names787[];
static const uint32 types787 [] =
{
SK_BOOL,
};

static const uint16 attr_flags787 [] =
{0,};

static const EIF_TYPE_INDEX g_atype787_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes787 [] = {
g_atype787_0,
};

static const int32 cn_attr787 [] =
{
2327,
};

extern const char *names788[];
static const uint32 types788 [] =
{
SK_BOOL,
};

static const uint16 attr_flags788 [] =
{0,};

static const EIF_TYPE_INDEX g_atype788_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes788 [] = {
g_atype788_0,
};

static const int32 cn_attr788 [] =
{
2327,
};

extern const char *names789[];
static const uint32 types789 [] =
{
SK_BOOL,
};

static const uint16 attr_flags789 [] =
{0,};

static const EIF_TYPE_INDEX g_atype789_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes789 [] = {
g_atype789_0,
};

static const int32 cn_attr789 [] =
{
2327,
};

extern const char *names790[];
static const uint32 types790 [] =
{
SK_REF,
};

static const uint16 attr_flags790 [] =
{0,};

static const EIF_TYPE_INDEX g_atype790_0 [] = {0xFF01,773,215,0xFFFF};

static const EIF_TYPE_INDEX *gtypes790 [] = {
g_atype790_0,
};

static const int32 cn_attr790 [] =
{
2156,
};

extern const char *names791[];
static const uint32 types791 [] =
{
SK_BOOL,
};

static const uint16 attr_flags791 [] =
{0,};

static const EIF_TYPE_INDEX g_atype791_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes791 [] = {
g_atype791_0,
};

static const int32 cn_attr791 [] =
{
2327,
};

extern const char *names792[];
static const uint32 types792 [] =
{
SK_BOOL,
};

static const uint16 attr_flags792 [] =
{0,};

static const EIF_TYPE_INDEX g_atype792_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes792 [] = {
g_atype792_0,
};

static const int32 cn_attr792 [] =
{
2327,
};

extern const char *names793[];
static const uint32 types793 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags793 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype793_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype793_1 [] = {0xFF01,779,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype793_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype793_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes793 [] = {
g_atype793_0,
g_atype793_1,
g_atype793_2,
g_atype793_3,
};

static const int32 cn_attr793 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names794[];
static const uint32 types794 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags794 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype794_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype794_1 [] = {0xFF01,778,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype794_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype794_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype794_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes794 [] = {
g_atype794_0,
g_atype794_1,
g_atype794_2,
g_atype794_3,
g_atype794_4,
};

static const int32 cn_attr794 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names796[];
static const uint32 types796 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags796 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype796_0 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype796_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype796_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes796 [] = {
g_atype796_0,
g_atype796_1,
g_atype796_2,
};

static const int32 cn_attr796 [] =
{
3791,
3792,
3794,
};

extern const char *names797[];
static const uint32 types797 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags797 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype797_0 [] = {0xFF01,797,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype797_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype797_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype797_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype797_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype797_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype797_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes797 [] = {
g_atype797_0,
g_atype797_1,
g_atype797_2,
g_atype797_3,
g_atype797_4,
g_atype797_5,
g_atype797_6,
};

static const int32 cn_attr797 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names803[];
static const uint32 types803 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags803 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype803_0 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype803_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype803_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes803 [] = {
g_atype803_0,
g_atype803_1,
g_atype803_2,
};

static const int32 cn_attr803 [] =
{
3791,
3792,
3794,
};

extern const char *names805[];
static const uint32 types805 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags805 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype805_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype805_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes805 [] = {
g_atype805_0,
g_atype805_1,
};

static const int32 cn_attr805 [] =
{
3889,
3890,
};

extern const char *names806[];
static const uint32 types806 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags806 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype806_0 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype806_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype806_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype806_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes806 [] = {
g_atype806_0,
g_atype806_1,
g_atype806_2,
g_atype806_3,
};

static const int32 cn_attr806 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names807[];
static const uint32 types807 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags807 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype807_0 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype807_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype807_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes807 [] = {
g_atype807_0,
g_atype807_1,
g_atype807_2,
};

static const int32 cn_attr807 [] =
{
2156,
2327,
3094,
};

extern const char *names808[];
static const uint32 types808 [] =
{
SK_BOOL,
};

static const uint16 attr_flags808 [] =
{0,};

static const EIF_TYPE_INDEX g_atype808_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes808 [] = {
g_atype808_0,
};

static const int32 cn_attr808 [] =
{
2327,
};

extern const char *names809[];
static const uint32 types809 [] =
{
SK_BOOL,
};

static const uint16 attr_flags809 [] =
{0,};

static const EIF_TYPE_INDEX g_atype809_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes809 [] = {
g_atype809_0,
};

static const int32 cn_attr809 [] =
{
2327,
};

extern const char *names810[];
static const uint32 types810 [] =
{
SK_BOOL,
};

static const uint16 attr_flags810 [] =
{0,};

static const EIF_TYPE_INDEX g_atype810_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes810 [] = {
g_atype810_0,
};

static const int32 cn_attr810 [] =
{
2327,
};

extern const char *names811[];
static const uint32 types811 [] =
{
SK_BOOL,
};

static const uint16 attr_flags811 [] =
{0,};

static const EIF_TYPE_INDEX g_atype811_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes811 [] = {
g_atype811_0,
};

static const int32 cn_attr811 [] =
{
2327,
};

extern const char *names812[];
static const uint32 types812 [] =
{
SK_BOOL,
};

static const uint16 attr_flags812 [] =
{0,};

static const EIF_TYPE_INDEX g_atype812_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes812 [] = {
g_atype812_0,
};

static const int32 cn_attr812 [] =
{
2327,
};

extern const char *names813[];
static const uint32 types813 [] =
{
SK_BOOL,
};

static const uint16 attr_flags813 [] =
{0,};

static const EIF_TYPE_INDEX g_atype813_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes813 [] = {
g_atype813_0,
};

static const int32 cn_attr813 [] =
{
2327,
};

extern const char *names814[];
static const uint32 types814 [] =
{
SK_BOOL,
};

static const uint16 attr_flags814 [] =
{0,};

static const EIF_TYPE_INDEX g_atype814_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes814 [] = {
g_atype814_0,
};

static const int32 cn_attr814 [] =
{
2327,
};

extern const char *names815[];
static const uint32 types815 [] =
{
SK_BOOL,
};

static const uint16 attr_flags815 [] =
{0,};

static const EIF_TYPE_INDEX g_atype815_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes815 [] = {
g_atype815_0,
};

static const int32 cn_attr815 [] =
{
2327,
};

extern const char *names816[];
static const uint32 types816 [] =
{
SK_BOOL,
};

static const uint16 attr_flags816 [] =
{0,};

static const EIF_TYPE_INDEX g_atype816_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes816 [] = {
g_atype816_0,
};

static const int32 cn_attr816 [] =
{
2327,
};

extern const char *names817[];
static const uint32 types817 [] =
{
SK_BOOL,
};

static const uint16 attr_flags817 [] =
{0,};

static const EIF_TYPE_INDEX g_atype817_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes817 [] = {
g_atype817_0,
};

static const int32 cn_attr817 [] =
{
2327,
};

extern const char *names818[];
static const uint32 types818 [] =
{
SK_BOOL,
};

static const uint16 attr_flags818 [] =
{0,};

static const EIF_TYPE_INDEX g_atype818_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes818 [] = {
g_atype818_0,
};

static const int32 cn_attr818 [] =
{
2327,
};

extern const char *names819[];
static const uint32 types819 [] =
{
SK_BOOL,
};

static const uint16 attr_flags819 [] =
{0,};

static const EIF_TYPE_INDEX g_atype819_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes819 [] = {
g_atype819_0,
};

static const int32 cn_attr819 [] =
{
2327,
};

extern const char *names820[];
static const uint32 types820 [] =
{
SK_BOOL,
};

static const uint16 attr_flags820 [] =
{0,};

static const EIF_TYPE_INDEX g_atype820_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes820 [] = {
g_atype820_0,
};

static const int32 cn_attr820 [] =
{
2327,
};

extern const char *names821[];
static const uint32 types821 [] =
{
SK_BOOL,
};

static const uint16 attr_flags821 [] =
{0,};

static const EIF_TYPE_INDEX g_atype821_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes821 [] = {
g_atype821_0,
};

static const int32 cn_attr821 [] =
{
2327,
};

extern const char *names822[];
static const uint32 types822 [] =
{
SK_BOOL,
};

static const uint16 attr_flags822 [] =
{0,};

static const EIF_TYPE_INDEX g_atype822_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes822 [] = {
g_atype822_0,
};

static const int32 cn_attr822 [] =
{
2327,
};

extern const char *names823[];
static const uint32 types823 [] =
{
SK_BOOL,
};

static const uint16 attr_flags823 [] =
{0,};

static const EIF_TYPE_INDEX g_atype823_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes823 [] = {
g_atype823_0,
};

static const int32 cn_attr823 [] =
{
2327,
};

extern const char *names824[];
static const uint32 types824 [] =
{
SK_BOOL,
};

static const uint16 attr_flags824 [] =
{0,};

static const EIF_TYPE_INDEX g_atype824_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes824 [] = {
g_atype824_0,
};

static const int32 cn_attr824 [] =
{
2327,
};

extern const char *names825[];
static const uint32 types825 [] =
{
SK_BOOL,
};

static const uint16 attr_flags825 [] =
{0,};

static const EIF_TYPE_INDEX g_atype825_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes825 [] = {
g_atype825_0,
};

static const int32 cn_attr825 [] =
{
2327,
};

extern const char *names826[];
static const uint32 types826 [] =
{
SK_REF,
};

static const uint16 attr_flags826 [] =
{0,};

static const EIF_TYPE_INDEX g_atype826_0 [] = {0xFF01,794,224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes826 [] = {
g_atype826_0,
};

static const int32 cn_attr826 [] =
{
2156,
};

extern const char *names827[];
static const uint32 types827 [] =
{
SK_BOOL,
};

static const uint16 attr_flags827 [] =
{0,};

static const EIF_TYPE_INDEX g_atype827_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes827 [] = {
g_atype827_0,
};

static const int32 cn_attr827 [] =
{
2327,
};

extern const char *names828[];
static const uint32 types828 [] =
{
SK_BOOL,
};

static const uint16 attr_flags828 [] =
{0,};

static const EIF_TYPE_INDEX g_atype828_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes828 [] = {
g_atype828_0,
};

static const int32 cn_attr828 [] =
{
2327,
};

extern const char *names829[];
static const uint32 types829 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags829 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype829_0 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype829_1 [] = {0xFF01,806,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype829_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype829_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes829 [] = {
g_atype829_0,
g_atype829_1,
g_atype829_2,
g_atype829_3,
};

static const int32 cn_attr829 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names830[];
static const uint32 types830 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags830 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype830_0 [] = {0xFF01,794,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype830_1 [] = {0xFF01,805,224,0xFFFF};
static const EIF_TYPE_INDEX g_atype830_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype830_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype830_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes830 [] = {
g_atype830_0,
g_atype830_1,
g_atype830_2,
g_atype830_3,
g_atype830_4,
};

static const int32 cn_attr830 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names831[];
static const uint32 types831 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags831 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype831_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_1 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_2 [] = {0xFF01,273,0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_3 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_4 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_6 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_16 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype831_17 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes831 [] = {
g_atype831_0,
g_atype831_1,
g_atype831_2,
g_atype831_3,
g_atype831_4,
g_atype831_5,
g_atype831_6,
g_atype831_7,
g_atype831_8,
g_atype831_9,
g_atype831_10,
g_atype831_11,
g_atype831_12,
g_atype831_13,
g_atype831_14,
g_atype831_15,
g_atype831_16,
g_atype831_17,
};

static const int32 cn_attr831 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
2327,
3008,
3014,
3062,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names832[];
static const uint32 types832 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags832 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype832_0 [] = {0xFF01,832,0xFFF8,1,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype832_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype832_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype832_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype832_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype832_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype832_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes832 [] = {
g_atype832_0,
g_atype832_1,
g_atype832_2,
g_atype832_3,
g_atype832_4,
g_atype832_5,
g_atype832_6,
};

static const int32 cn_attr832 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names833[];
static const uint32 types833 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags833 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype833_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_1 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_2 [] = {0xFF01,273,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_3 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_4 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_6 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype833_16 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes833 [] = {
g_atype833_0,
g_atype833_1,
g_atype833_2,
g_atype833_3,
g_atype833_4,
g_atype833_5,
g_atype833_6,
g_atype833_7,
g_atype833_8,
g_atype833_9,
g_atype833_10,
g_atype833_11,
g_atype833_12,
g_atype833_13,
g_atype833_14,
g_atype833_15,
g_atype833_16,
};

static const int32 cn_attr833 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
3025,
2327,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
};

extern const char *names834[];
static const uint32 types834 [] =
{
SK_BOOL,
};

static const uint16 attr_flags834 [] =
{0,};

static const EIF_TYPE_INDEX g_atype834_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes834 [] = {
g_atype834_0,
};

static const int32 cn_attr834 [] =
{
2327,
};

extern const char *names837[];
static const uint32 types837 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags837 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype837_0 [] = {0xFF01,837,215,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype837_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype837_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype837_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype837_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype837_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype837_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes837 [] = {
g_atype837_0,
g_atype837_1,
g_atype837_2,
g_atype837_3,
g_atype837_4,
g_atype837_5,
g_atype837_6,
};

static const int32 cn_attr837 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names838[];
static const uint32 types838 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags838 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype838_0 [] = {0xFF01,773,215,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_1 [] = {0xFF01,328,227,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_2 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_3 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_7 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_8 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype838_16 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes838 [] = {
g_atype838_0,
g_atype838_1,
g_atype838_2,
g_atype838_3,
g_atype838_4,
g_atype838_5,
g_atype838_6,
g_atype838_7,
g_atype838_8,
g_atype838_9,
g_atype838_10,
g_atype838_11,
g_atype838_12,
g_atype838_13,
g_atype838_14,
g_atype838_15,
g_atype838_16,
};

static const int32 cn_attr838 [] =
{
3009,
3010,
3011,
3012,
2327,
3008,
3014,
2972,
3024,
2981,
3013,
3015,
3018,
3019,
3023,
3059,
3025,
};

extern const char *names839[];
static const uint32 types839 [] =
{
SK_BOOL,
};

static const uint16 attr_flags839 [] =
{0,};

static const EIF_TYPE_INDEX g_atype839_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes839 [] = {
g_atype839_0,
};

static const int32 cn_attr839 [] =
{
2327,
};

extern const char *names842[];
static const uint32 types842 [] =
{
SK_UINT64,
};

static const uint16 attr_flags842 [] =
{0,};

static const EIF_TYPE_INDEX g_atype842_0 [] = {224,0xFFFF};

static const EIF_TYPE_INDEX *gtypes842 [] = {
g_atype842_0,
};

static const int32 cn_attr842 [] =
{
1718,
};

extern const char *names843[];
static const uint32 types843 [] =
{
SK_CHAR32,
};

static const uint16 attr_flags843 [] =
{0,};

static const EIF_TYPE_INDEX g_atype843_0 [] = {191,0xFFFF};

static const EIF_TYPE_INDEX *gtypes843 [] = {
g_atype843_0,
};

static const int32 cn_attr843 [] =
{
1718,
};

extern const char *names844[];
static const uint32 types844 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags844 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype844_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_1 [] = {0xFF01,273,0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_2 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_3 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_4 [] = {229,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_16 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype844_17 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes844 [] = {
g_atype844_0,
g_atype844_1,
g_atype844_2,
g_atype844_3,
g_atype844_4,
g_atype844_5,
g_atype844_6,
g_atype844_7,
g_atype844_8,
g_atype844_9,
g_atype844_10,
g_atype844_11,
g_atype844_12,
g_atype844_13,
g_atype844_14,
g_atype844_15,
g_atype844_16,
g_atype844_17,
};

static const int32 cn_attr844 [] =
{
3009,
3010,
3011,
3012,
3025,
2327,
3008,
3014,
3062,
2972,
2981,
3013,
3015,
3018,
3019,
3023,
3024,
3059,
};

extern const char *names845[];
static const uint32 types845 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags845 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype845_0 [] = {0xFF01,845,221,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype845_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype845_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype845_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype845_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype845_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype845_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes845 [] = {
g_atype845_0,
g_atype845_1,
g_atype845_2,
g_atype845_3,
g_atype845_4,
g_atype845_5,
g_atype845_6,
};

static const int32 cn_attr845 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names846[];
static const uint32 types846 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags846 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype846_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_1 [] = {0xFF01,273,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_2 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_3 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_4 [] = {0xFF02,0xFFF8,2,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype846_16 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes846 [] = {
g_atype846_0,
g_atype846_1,
g_atype846_2,
g_atype846_3,
g_atype846_4,
g_atype846_5,
g_atype846_6,
g_atype846_7,
g_atype846_8,
g_atype846_9,
g_atype846_10,
g_atype846_11,
g_atype846_12,
g_atype846_13,
g_atype846_14,
g_atype846_15,
g_atype846_16,
};

static const int32 cn_attr846 [] =
{
3009,
3010,
3011,
3012,
3025,
2327,
3008,
3014,
2972,
2981,
3013,
3015,
3018,
3019,
3023,
3024,
3059,
};

extern const char *names847[];
static const uint32 types847 [] =
{
SK_BOOL,
};

static const uint16 attr_flags847 [] =
{0,};

static const EIF_TYPE_INDEX g_atype847_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes847 [] = {
g_atype847_0,
};

static const int32 cn_attr847 [] =
{
2327,
};

extern const char *names850[];
static const uint32 types850 [] =
{
SK_REF,
SK_REF,
SK_UINT16,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags850 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype850_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype850_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes850 [] = {
g_atype850_0,
g_atype850_1,
g_atype850_2,
g_atype850_3,
g_atype850_4,
};

static const int32 cn_attr850 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names851[];
static const uint32 types851 [] =
{
SK_POINTER,
};

static const uint16 attr_flags851 [] =
{0,};

static const EIF_TYPE_INDEX g_atype851_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes851 [] = {
g_atype851_0,
};

static const int32 cn_attr851 [] =
{
4554,
};

extern const char *names852[];
static const uint32 types852 [] =
{
SK_POINTER,
};

static const uint16 attr_flags852 [] =
{0,};

static const EIF_TYPE_INDEX g_atype852_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes852 [] = {
g_atype852_0,
};

static const int32 cn_attr852 [] =
{
4554,
};

extern const char *names853[];
static const uint32 types853 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags853 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype853_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype853_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes853 [] = {
g_atype853_0,
g_atype853_1,
};

static const int32 cn_attr853 [] =
{
3889,
3890,
};

extern const char *names854[];
static const uint32 types854 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_POINTER,
SK_POINTER,
SK_POINTER,
};

static const uint16 attr_flags854 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype854_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_1 [] = {0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_2 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_3 [] = {608,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_10 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_11 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype854_12 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes854 [] = {
g_atype854_0,
g_atype854_1,
g_atype854_2,
g_atype854_3,
g_atype854_4,
g_atype854_5,
g_atype854_6,
g_atype854_7,
g_atype854_8,
g_atype854_9,
g_atype854_10,
g_atype854_11,
g_atype854_12,
};

static const int32 cn_attr854 [] =
{
4579,
4594,
4598,
4606,
4587,
4601,
4615,
4588,
4600,
4602,
4596,
4597,
4599,
};

extern const char *names855[];
static const uint32 types855 [] =
{
SK_REF,
SK_INT8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags855 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype855_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_1 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype855_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes855 [] = {
g_atype855_0,
g_atype855_1,
g_atype855_2,
g_atype855_3,
g_atype855_4,
g_atype855_5,
};

static const int32 cn_attr855 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names856[];
static const uint32 types856 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags856 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype856_0 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_1 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_2 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_3 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_4 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype856_16 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes856 [] = {
g_atype856_0,
g_atype856_1,
g_atype856_2,
g_atype856_3,
g_atype856_4,
g_atype856_5,
g_atype856_6,
g_atype856_7,
g_atype856_8,
g_atype856_9,
g_atype856_10,
g_atype856_11,
g_atype856_12,
g_atype856_13,
g_atype856_14,
g_atype856_15,
g_atype856_16,
};

static const int32 cn_attr856 [] =
{
2972,
3009,
3010,
3011,
3012,
3024,
2327,
3008,
3014,
2981,
3013,
3015,
3018,
3019,
3023,
3025,
3059,
};

extern const char *names859[];
static const uint32 types859 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags859 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype859_0 [] = {0xFF01,855,0xFFF8,1,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype859_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes859 [] = {
g_atype859_0,
g_atype859_1,
g_atype859_2,
g_atype859_3,
g_atype859_4,
g_atype859_5,
g_atype859_6,
};

static const int32 cn_attr859 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names860[];
static const uint32 types860 [] =
{
SK_REF,
SK_CHAR8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags860 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype860_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_1 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype860_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes860 [] = {
g_atype860_0,
g_atype860_1,
g_atype860_2,
g_atype860_3,
g_atype860_4,
g_atype860_5,
};

static const int32 cn_attr860 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names861[];
static const uint32 types861 [] =
{
SK_REF,
SK_CHAR32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags861 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype861_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype861_1 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype861_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype861_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype861_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype861_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes861 [] = {
g_atype861_0,
g_atype861_1,
g_atype861_2,
g_atype861_3,
g_atype861_4,
g_atype861_5,
};

static const int32 cn_attr861 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names862[];
static const uint32 types862 [] =
{
SK_POINTER,
};

static const uint16 attr_flags862 [] =
{0,};

static const EIF_TYPE_INDEX g_atype862_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes862 [] = {
g_atype862_0,
};

static const int32 cn_attr862 [] =
{
4554,
};

extern const char *names863[];
static const uint32 types863 [] =
{
SK_POINTER,
};

static const uint16 attr_flags863 [] =
{0,};

static const EIF_TYPE_INDEX g_atype863_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes863 [] = {
g_atype863_0,
};

static const int32 cn_attr863 [] =
{
4554,
};

extern const char *names864[];
static const uint32 types864 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags864 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype864_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype864_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes864 [] = {
g_atype864_0,
g_atype864_1,
};

static const int32 cn_attr864 [] =
{
3889,
3890,
};

extern const char *names865[];
static const uint32 types865 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags865 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype865_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype865_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype865_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype865_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype865_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype865_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes865 [] = {
g_atype865_0,
g_atype865_1,
g_atype865_2,
g_atype865_3,
g_atype865_4,
g_atype865_5,
};

static const int32 cn_attr865 [] =
{
2084,
2099,
2082,
2083,
2097,
2098,
};

extern const char *names866[];
static const uint32 types866 [] =
{
SK_REF,
SK_REF,
SK_CHAR32,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags866 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype866_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype866_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype866_2 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype866_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype866_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype866_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes866 [] = {
g_atype866_0,
g_atype866_1,
g_atype866_2,
g_atype866_3,
g_atype866_4,
g_atype866_5,
};

static const int32 cn_attr866 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names867[];
static const uint32 types867 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags867 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype867_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype867_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype867_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype867_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype867_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes867 [] = {
g_atype867_0,
g_atype867_1,
g_atype867_2,
g_atype867_3,
g_atype867_4,
};

static const int32 cn_attr867 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names868[];
static const uint32 types868 [] =
{
SK_POINTER,
};

static const uint16 attr_flags868 [] =
{0,};

static const EIF_TYPE_INDEX g_atype868_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes868 [] = {
g_atype868_0,
};

static const int32 cn_attr868 [] =
{
4554,
};

extern const char *names869[];
static const uint32 types869 [] =
{
SK_POINTER,
};

static const uint16 attr_flags869 [] =
{0,};

static const EIF_TYPE_INDEX g_atype869_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes869 [] = {
g_atype869_0,
};

static const int32 cn_attr869 [] =
{
4554,
};

extern const char *names870[];
static const uint32 types870 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags870 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype870_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype870_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes870 [] = {
g_atype870_0,
g_atype870_1,
};

static const int32 cn_attr870 [] =
{
3889,
3890,
};

extern const char *names871[];
static const uint32 types871 [] =
{
SK_REF,
SK_REF,
SK_INT8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags871 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype871_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_2 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype871_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes871 [] = {
g_atype871_0,
g_atype871_1,
g_atype871_2,
g_atype871_3,
g_atype871_4,
};

static const int32 cn_attr871 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names872[];
static const uint32 types872 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags872 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype872_0 [] = {0xFF01,273,0xFF01,248,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_1 [] = {944,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_2 [] = {872,0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_3 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_4 [] = {247,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_5 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_6 [] = {253,0xFF01,248,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_7 [] = {253,0xFF01,248,0xFF01,0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_8 [] = {253,0xFF01,248,0xFF01,0xFFF9,0,186,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype872_12 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes872 [] = {
g_atype872_0,
g_atype872_1,
g_atype872_2,
g_atype872_3,
g_atype872_4,
g_atype872_5,
g_atype872_6,
g_atype872_7,
g_atype872_8,
g_atype872_9,
g_atype872_10,
g_atype872_11,
g_atype872_12,
};

static const int32 cn_attr872 [] =
{
2156,
3133,
3135,
3136,
3137,
3138,
3140,
3141,
3142,
2327,
3106,
3094,
3120,
};

extern const char *names873[];
static const uint32 types873 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags873 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype873_0 [] = {342,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype873_1 [] = {342,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype873_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype873_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype873_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype873_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes873 [] = {
g_atype873_0,
g_atype873_1,
g_atype873_2,
g_atype873_3,
g_atype873_4,
g_atype873_5,
};

static const int32 cn_attr873 [] =
{
2881,
2885,
2327,
2889,
2890,
2891,
};

extern const char *names874[];
static const uint32 types874 [] =
{
SK_BOOL,
};

static const uint16 attr_flags874 [] =
{0,};

static const EIF_TYPE_INDEX g_atype874_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes874 [] = {
g_atype874_0,
};

static const int32 cn_attr874 [] =
{
2327,
};

extern const char *names875[];
static const uint32 types875 [] =
{
SK_BOOL,
};

static const uint16 attr_flags875 [] =
{0,};

static const EIF_TYPE_INDEX g_atype875_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes875 [] = {
g_atype875_0,
};

static const int32 cn_attr875 [] =
{
2327,
};

extern const char *names876[];
static const uint32 types876 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags876 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype876_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype876_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype876_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype876_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes876 [] = {
g_atype876_0,
g_atype876_1,
g_atype876_2,
g_atype876_3,
};

static const int32 cn_attr876 [] =
{
2156,
2327,
3106,
3094,
};

extern const char *names877[];
static const uint32 types877 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags877 [] =
{1,1,};

static const EIF_TYPE_INDEX g_atype877_0 [] = {241,0xFFFF};
static const EIF_TYPE_INDEX g_atype877_1 [] = {240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes877 [] = {
g_atype877_0,
g_atype877_1,
};

static const int32 cn_attr877 [] =
{
3889,
3890,
};

extern const char *names878[];
static const uint32 types878 [] =
{
SK_REF,
SK_REF,
SK_UINT8,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags878 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype878_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype878_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype878_2 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype878_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype878_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype878_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes878 [] = {
g_atype878_0,
g_atype878_1,
g_atype878_2,
g_atype878_3,
g_atype878_4,
g_atype878_5,
};

static const int32 cn_attr878 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names879[];
static const uint32 types879 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags879 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype879_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_1 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_2 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_3 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype879_16 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes879 [] = {
g_atype879_0,
g_atype879_1,
g_atype879_2,
g_atype879_3,
g_atype879_4,
g_atype879_5,
g_atype879_6,
g_atype879_7,
g_atype879_8,
g_atype879_9,
g_atype879_10,
g_atype879_11,
g_atype879_12,
g_atype879_13,
g_atype879_14,
g_atype879_15,
g_atype879_16,
};

static const int32 cn_attr879 [] =
{
3009,
3010,
3011,
3012,
2327,
3008,
3014,
2972,
2981,
3013,
3015,
3018,
3019,
3023,
3024,
3025,
3059,
};

extern const char *names882[];
static const uint32 types882 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags882 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype882_0 [] = {0xFF01,878,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype882_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype882_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype882_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype882_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype882_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype882_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes882 [] = {
g_atype882_0,
g_atype882_1,
g_atype882_2,
g_atype882_3,
g_atype882_4,
g_atype882_5,
g_atype882_6,
};

static const int32 cn_attr882 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names883[];
static const uint32 types883 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags883 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype883_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype883_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype883_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes883 [] = {
g_atype883_0,
g_atype883_1,
g_atype883_2,
};

static const int32 cn_attr883 [] =
{
2156,
2327,
3094,
};

extern const char *names884[];
static const uint32 types884 [] =
{
SK_BOOL,
};

static const uint16 attr_flags884 [] =
{0,};

static const EIF_TYPE_INDEX g_atype884_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes884 [] = {
g_atype884_0,
};

static const int32 cn_attr884 [] =
{
2327,
};

extern const char *names885[];
static const uint32 types885 [] =
{
SK_REF,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags885 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype885_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype885_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype885_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype885_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype885_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype885_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes885 [] = {
g_atype885_0,
g_atype885_1,
g_atype885_2,
g_atype885_3,
g_atype885_4,
g_atype885_5,
};

static const int32 cn_attr885 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names886[];
static const uint32 types886 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags886 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype886_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype886_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype886_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype886_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype886_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype886_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes886 [] = {
g_atype886_0,
g_atype886_1,
g_atype886_2,
g_atype886_3,
g_atype886_4,
g_atype886_5,
};

static const int32 cn_attr886 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names887[];
static const uint32 types887 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_REAL32,
};

static const uint16 attr_flags887 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype887_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype887_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype887_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype887_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype887_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype887_5 [] = {209,0xFFFF};

static const EIF_TYPE_INDEX *gtypes887 [] = {
g_atype887_0,
g_atype887_1,
g_atype887_2,
g_atype887_3,
g_atype887_4,
g_atype887_5,
};

static const int32 cn_attr887 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names888[];
static const uint32 types888 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags888 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype888_0 [] = {889,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype888_1 [] = {889,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype888_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype888_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype888_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype888_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes888 [] = {
g_atype888_0,
g_atype888_1,
g_atype888_2,
g_atype888_3,
g_atype888_4,
g_atype888_5,
};

static const int32 cn_attr888 [] =
{
2881,
2885,
2327,
2889,
2890,
2891,
};

extern const char *names889[];
static const uint32 types889 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags889 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype889_0 [] = {889,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype889_1 [] = {889,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype889_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype889_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype889_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype889_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes889 [] = {
g_atype889_0,
g_atype889_1,
g_atype889_2,
g_atype889_3,
g_atype889_4,
g_atype889_5,
};

static const int32 cn_attr889 [] =
{
2881,
2885,
2327,
2889,
2890,
2891,
};

extern const char *names890[];
static const uint32 types890 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags890 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype890_0 [] = {889,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype890_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes890 [] = {
g_atype890_0,
g_atype890_1,
};

static const int32 cn_attr890 [] =
{
1722,
1718,
};

extern const char *names891[];
static const uint32 types891 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags891 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype891_0 [] = {0xFF01,888,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_1 [] = {889,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype891_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes891 [] = {
g_atype891_0,
g_atype891_1,
g_atype891_2,
g_atype891_3,
g_atype891_4,
g_atype891_5,
g_atype891_6,
g_atype891_7,
};

static const int32 cn_attr891 [] =
{
3784,
3790,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names892[];
static const uint32 types892 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags892 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype892_0 [] = {889,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype892_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype892_2 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes892 [] = {
g_atype892_0,
g_atype892_1,
g_atype892_2,
};

static const int32 cn_attr892 [] =
{
2150,
2151,
2152,
};

extern const char *names893[];
static const uint32 types893 [] =
{
SK_BOOL,
};

static const uint16 attr_flags893 [] =
{0,};

static const EIF_TYPE_INDEX g_atype893_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes893 [] = {
g_atype893_0,
};

static const int32 cn_attr893 [] =
{
2327,
};

extern const char *names894[];
static const uint32 types894 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags894 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype894_0 [] = {0xFF01,614,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype894_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype894_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes894 [] = {
g_atype894_0,
g_atype894_1,
g_atype894_2,
};

static const int32 cn_attr894 [] =
{
2156,
2327,
3094,
};

extern const char *names895[];
static const uint32 types895 [] =
{
SK_BOOL,
};

static const uint16 attr_flags895 [] =
{0,};

static const EIF_TYPE_INDEX g_atype895_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes895 [] = {
g_atype895_0,
};

static const int32 cn_attr895 [] =
{
2327,
};

extern const char *names896[];
static const uint32 types896 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_POINTER,
};

static const uint16 attr_flags896 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype896_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype896_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype896_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype896_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype896_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype896_5 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes896 [] = {
g_atype896_0,
g_atype896_1,
g_atype896_2,
g_atype896_3,
g_atype896_4,
g_atype896_5,
};

static const int32 cn_attr896 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names897[];
static const uint32 types897 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags897 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype897_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype897_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype897_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype897_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype897_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes897 [] = {
g_atype897_0,
g_atype897_1,
g_atype897_2,
g_atype897_3,
g_atype897_4,
};

static const int32 cn_attr897 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names898[];
static const uint32 types898 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags898 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype898_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype898_1 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype898_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype898_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype898_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype898_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes898 [] = {
g_atype898_0,
g_atype898_1,
g_atype898_2,
g_atype898_3,
g_atype898_4,
g_atype898_5,
};

static const int32 cn_attr898 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names899[];
static const uint32 types899 [] =
{
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags899 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype899_0 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype899_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes899 [] = {
g_atype899_0,
g_atype899_1,
};

static const int32 cn_attr899 [] =
{
2327,
2491,
};

extern const char *names900[];
static const uint32 types900 [] =
{
SK_BOOL,
};

static const uint16 attr_flags900 [] =
{0,};

static const EIF_TYPE_INDEX g_atype900_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes900 [] = {
g_atype900_0,
};

static const int32 cn_attr900 [] =
{
2327,
};

extern const char *names901[];
static const uint32 types901 [] =
{
SK_BOOL,
};

static const uint16 attr_flags901 [] =
{0,};

static const EIF_TYPE_INDEX g_atype901_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes901 [] = {
g_atype901_0,
};

static const int32 cn_attr901 [] =
{
2327,
};

extern const char *names902[];
static const uint32 types902 [] =
{
SK_REF,
SK_UINT8,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags902 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype902_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype902_1 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype902_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype902_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype902_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype902_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes902 [] = {
g_atype902_0,
g_atype902_1,
g_atype902_2,
g_atype902_3,
g_atype902_4,
g_atype902_5,
};

static const int32 cn_attr902 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names904[];
static const uint32 types904 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags904 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype904_0 [] = {0xFF01,902,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype904_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype904_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes904 [] = {
g_atype904_0,
g_atype904_1,
g_atype904_2,
};

static const int32 cn_attr904 [] =
{
3791,
3792,
3794,
};

extern const char *names905[];
static const uint32 types905 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags905 [] =
{0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype905_0 [] = {0xFF01,905,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype905_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes905 [] = {
g_atype905_0,
g_atype905_1,
g_atype905_2,
g_atype905_3,
g_atype905_4,
g_atype905_5,
g_atype905_6,
};

static const int32 cn_attr905 [] =
{
3784,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names911[];
static const uint32 types911 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags911 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype911_0 [] = {0xFF01,902,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype911_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype911_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes911 [] = {
g_atype911_0,
g_atype911_1,
g_atype911_2,
};

static const int32 cn_attr911 [] =
{
3791,
3792,
3794,
};

extern const char *names913[];
static const uint32 types913 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags913 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype913_0 [] = {0xFF01,902,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype913_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype913_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype913_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes913 [] = {
g_atype913_0,
g_atype913_1,
g_atype913_2,
g_atype913_3,
};

static const int32 cn_attr913 [] =
{
2156,
2327,
2779,
2780,
};

extern const char *names914[];
static const uint32 types914 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags914 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype914_0 [] = {0xFF01,902,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype914_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype914_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes914 [] = {
g_atype914_0,
g_atype914_1,
g_atype914_2,
};

static const int32 cn_attr914 [] =
{
2156,
2327,
3094,
};

extern const char *names915[];
static const uint32 types915 [] =
{
SK_BOOL,
};

static const uint16 attr_flags915 [] =
{0,};

static const EIF_TYPE_INDEX g_atype915_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes915 [] = {
g_atype915_0,
};

static const int32 cn_attr915 [] =
{
2327,
};

extern const char *names916[];
static const uint32 types916 [] =
{
SK_BOOL,
};

static const uint16 attr_flags916 [] =
{0,};

static const EIF_TYPE_INDEX g_atype916_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes916 [] = {
g_atype916_0,
};

static const int32 cn_attr916 [] =
{
2327,
};

extern const char *names917[];
static const uint32 types917 [] =
{
SK_BOOL,
};

static const uint16 attr_flags917 [] =
{0,};

static const EIF_TYPE_INDEX g_atype917_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes917 [] = {
g_atype917_0,
};

static const int32 cn_attr917 [] =
{
2327,
};

extern const char *names918[];
static const uint32 types918 [] =
{
SK_BOOL,
};

static const uint16 attr_flags918 [] =
{0,};

static const EIF_TYPE_INDEX g_atype918_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes918 [] = {
g_atype918_0,
};

static const int32 cn_attr918 [] =
{
2327,
};

extern const char *names919[];
static const uint32 types919 [] =
{
SK_BOOL,
};

static const uint16 attr_flags919 [] =
{0,};

static const EIF_TYPE_INDEX g_atype919_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes919 [] = {
g_atype919_0,
};

static const int32 cn_attr919 [] =
{
2327,
};

extern const char *names920[];
static const uint32 types920 [] =
{
SK_BOOL,
};

static const uint16 attr_flags920 [] =
{0,};

static const EIF_TYPE_INDEX g_atype920_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes920 [] = {
g_atype920_0,
};

static const int32 cn_attr920 [] =
{
2327,
};

extern const char *names921[];
static const uint32 types921 [] =
{
SK_BOOL,
};

static const uint16 attr_flags921 [] =
{0,};

static const EIF_TYPE_INDEX g_atype921_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes921 [] = {
g_atype921_0,
};

static const int32 cn_attr921 [] =
{
2327,
};

extern const char *names922[];
static const uint32 types922 [] =
{
SK_BOOL,
};

static const uint16 attr_flags922 [] =
{0,};

static const EIF_TYPE_INDEX g_atype922_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes922 [] = {
g_atype922_0,
};

static const int32 cn_attr922 [] =
{
2327,
};

extern const char *names923[];
static const uint32 types923 [] =
{
SK_BOOL,
};

static const uint16 attr_flags923 [] =
{0,};

static const EIF_TYPE_INDEX g_atype923_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes923 [] = {
g_atype923_0,
};

static const int32 cn_attr923 [] =
{
2327,
};

extern const char *names924[];
static const uint32 types924 [] =
{
SK_BOOL,
};

static const uint16 attr_flags924 [] =
{0,};

static const EIF_TYPE_INDEX g_atype924_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes924 [] = {
g_atype924_0,
};

static const int32 cn_attr924 [] =
{
2327,
};

extern const char *names925[];
static const uint32 types925 [] =
{
SK_BOOL,
};

static const uint16 attr_flags925 [] =
{0,};

static const EIF_TYPE_INDEX g_atype925_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes925 [] = {
g_atype925_0,
};

static const int32 cn_attr925 [] =
{
2327,
};

extern const char *names926[];
static const uint32 types926 [] =
{
SK_BOOL,
};

static const uint16 attr_flags926 [] =
{0,};

static const EIF_TYPE_INDEX g_atype926_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes926 [] = {
g_atype926_0,
};

static const int32 cn_attr926 [] =
{
2327,
};

extern const char *names927[];
static const uint32 types927 [] =
{
SK_BOOL,
};

static const uint16 attr_flags927 [] =
{0,};

static const EIF_TYPE_INDEX g_atype927_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes927 [] = {
g_atype927_0,
};

static const int32 cn_attr927 [] =
{
2327,
};

extern const char *names928[];
static const uint32 types928 [] =
{
SK_BOOL,
};

static const uint16 attr_flags928 [] =
{0,};

static const EIF_TYPE_INDEX g_atype928_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes928 [] = {
g_atype928_0,
};

static const int32 cn_attr928 [] =
{
2327,
};

extern const char *names929[];
static const uint32 types929 [] =
{
SK_BOOL,
};

static const uint16 attr_flags929 [] =
{0,};

static const EIF_TYPE_INDEX g_atype929_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes929 [] = {
g_atype929_0,
};

static const int32 cn_attr929 [] =
{
2327,
};

extern const char *names930[];
static const uint32 types930 [] =
{
SK_BOOL,
};

static const uint16 attr_flags930 [] =
{0,};

static const EIF_TYPE_INDEX g_atype930_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes930 [] = {
g_atype930_0,
};

static const int32 cn_attr930 [] =
{
2327,
};

extern const char *names931[];
static const uint32 types931 [] =
{
SK_BOOL,
};

static const uint16 attr_flags931 [] =
{0,};

static const EIF_TYPE_INDEX g_atype931_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes931 [] = {
g_atype931_0,
};

static const int32 cn_attr931 [] =
{
2327,
};

extern const char *names932[];
static const uint32 types932 [] =
{
SK_BOOL,
};

static const uint16 attr_flags932 [] =
{0,};

static const EIF_TYPE_INDEX g_atype932_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes932 [] = {
g_atype932_0,
};

static const int32 cn_attr932 [] =
{
2327,
};

extern const char *names933[];
static const uint32 types933 [] =
{
SK_REF,
};

static const uint16 attr_flags933 [] =
{0,};

static const EIF_TYPE_INDEX g_atype933_0 [] = {0xFF01,902,197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes933 [] = {
g_atype933_0,
};

static const int32 cn_attr933 [] =
{
2156,
};

extern const char *names934[];
static const uint32 types934 [] =
{
SK_BOOL,
};

static const uint16 attr_flags934 [] =
{0,};

static const EIF_TYPE_INDEX g_atype934_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes934 [] = {
g_atype934_0,
};

static const int32 cn_attr934 [] =
{
2327,
};

extern const char *names935[];
static const uint32 types935 [] =
{
SK_BOOL,
};

static const uint16 attr_flags935 [] =
{0,};

static const EIF_TYPE_INDEX g_atype935_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes935 [] = {
g_atype935_0,
};

static const int32 cn_attr935 [] =
{
2327,
};

extern const char *names936[];
static const uint32 types936 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags936 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype936_0 [] = {0xFF01,902,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype936_1 [] = {0xFF01,913,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype936_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype936_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes936 [] = {
g_atype936_0,
g_atype936_1,
g_atype936_2,
g_atype936_3,
};

static const int32 cn_attr936 [] =
{
3791,
3796,
3792,
3794,
};

extern const char *names937[];
static const uint32 types937 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags937 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype937_0 [] = {0xFF01,902,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_1 [] = {0xFF01,912,197,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype937_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes937 [] = {
g_atype937_0,
g_atype937_1,
g_atype937_2,
g_atype937_3,
g_atype937_4,
};

static const int32 cn_attr937 [] =
{
3791,
3804,
3792,
3794,
3805,
};

extern const char *names938[];
static const uint32 types938 [] =
{
SK_REF,
SK_UINT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags938 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype938_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_1 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype938_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes938 [] = {
g_atype938_0,
g_atype938_1,
g_atype938_2,
g_atype938_3,
g_atype938_4,
g_atype938_5,
};

static const int32 cn_attr938 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names939[];
static const uint32 types939 [] =
{
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags939 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype939_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_1 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype939_5 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes939 [] = {
g_atype939_0,
g_atype939_1,
g_atype939_2,
g_atype939_3,
g_atype939_4,
g_atype939_5,
};

static const int32 cn_attr939 [] =
{
2084,
2099,
2082,
2083,
2098,
2097,
};

extern const char *names940[];
static const uint32 types940 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags940 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype940_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_2 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype940_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes940 [] = {
g_atype940_0,
g_atype940_1,
g_atype940_2,
g_atype940_3,
g_atype940_4,
};

static const int32 cn_attr940 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names941[];
static const uint32 types941 [] =
{
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags941 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype941_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype941_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes941 [] = {
g_atype941_0,
g_atype941_1,
g_atype941_2,
g_atype941_3,
g_atype941_4,
g_atype941_5,
};

static const int32 cn_attr941 [] =
{
2084,
2097,
2099,
2082,
2083,
2098,
};

extern const char *names942[];
static const uint32 types942 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags942 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype942_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype942_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype942_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype942_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype942_4 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes942 [] = {
g_atype942_0,
g_atype942_1,
g_atype942_2,
g_atype942_3,
g_atype942_4,
};

static const int32 cn_attr942 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names943[];
static const uint32 types943 [] =
{
SK_REF,
SK_REF,
SK_UINT16,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags943 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype943_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_2 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype943_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes943 [] = {
g_atype943_0,
g_atype943_1,
g_atype943_2,
g_atype943_3,
g_atype943_4,
g_atype943_5,
};

static const int32 cn_attr943 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names944[];
static const uint32 types944 [] =
{
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags944 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype944_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype944_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype944_2 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype944_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype944_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype944_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes944 [] = {
g_atype944_0,
g_atype944_1,
g_atype944_2,
g_atype944_3,
g_atype944_4,
g_atype944_5,
};

static const int32 cn_attr944 [] =
{
2084,
2112,
2114,
2082,
2083,
2113,
};

extern const char *names945[];
static const uint32 types945 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags945 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype945_0 [] = {946,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype945_1 [] = {946,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype945_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype945_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype945_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype945_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes945 [] = {
g_atype945_0,
g_atype945_1,
g_atype945_2,
g_atype945_3,
g_atype945_4,
g_atype945_5,
};

static const int32 cn_attr945 [] =
{
2881,
2885,
2327,
2889,
2890,
2891,
};

extern const char *names946[];
static const uint32 types946 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags946 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype946_0 [] = {946,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_1 [] = {946,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype946_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes946 [] = {
g_atype946_0,
g_atype946_1,
g_atype946_2,
g_atype946_3,
g_atype946_4,
g_atype946_5,
};

static const int32 cn_attr946 [] =
{
2881,
2885,
2327,
2889,
2890,
2891,
};

extern const char *names947[];
static const uint32 types947 [] =
{
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags947 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype947_0 [] = {946,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype947_1 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes947 [] = {
g_atype947_0,
g_atype947_1,
};

static const int32 cn_attr947 [] =
{
1722,
1718,
};

extern const char *names948[];
static const uint32 types948 [] =
{
SK_BOOL,
};

static const uint16 attr_flags948 [] =
{0,};

static const EIF_TYPE_INDEX g_atype948_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes948 [] = {
g_atype948_0,
};

static const int32 cn_attr948 [] =
{
1718,
};

extern const char *names949[];
static const uint32 types949 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags949 [] =
{0,0,0,1,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype949_0 [] = {0xFF01,945,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_1 [] = {946,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype949_7 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes949 [] = {
g_atype949_0,
g_atype949_1,
g_atype949_2,
g_atype949_3,
g_atype949_4,
g_atype949_5,
g_atype949_6,
g_atype949_7,
};

static const int32 cn_attr949 [] =
{
3784,
3790,
3785,
3781,
3786,
3787,
3788,
3789,
};

extern const char *names950[];
static const uint32 types950 [] =
{
SK_REF,
SK_BOOL,
SK_BOOL,
};

static const uint16 attr_flags950 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype950_0 [] = {946,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype950_2 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes950 [] = {
g_atype950_0,
g_atype950_1,
g_atype950_2,
};

static const int32 cn_attr950 [] =
{
2150,
2151,
2152,
};

extern const char *names951[];
static const uint32 types951 [] =
{
SK_BOOL,
};

static const uint16 attr_flags951 [] =
{0,};

static const EIF_TYPE_INDEX g_atype951_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes951 [] = {
g_atype951_0,
};

static const int32 cn_attr951 [] =
{
2327,
};

extern const char *names952[];
static const uint32 types952 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags952 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype952_0 [] = {0xFF01,544,200,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype952_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes952 [] = {
g_atype952_0,
g_atype952_1,
g_atype952_2,
};

static const int32 cn_attr952 [] =
{
2156,
2327,
3094,
};

extern const char *names953[];
static const uint32 types953 [] =
{
SK_BOOL,
};

static const uint16 attr_flags953 [] =
{0,};

static const EIF_TYPE_INDEX g_atype953_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes953 [] = {
g_atype953_0,
};

static const int32 cn_attr953 [] =
{
2327,
};

extern const char *names954[];
static const uint32 types954 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags954 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype954_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype954_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype954_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype954_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes954 [] = {
g_atype954_0,
g_atype954_1,
g_atype954_2,
g_atype954_3,
};

static const int32 cn_attr954 [] =
{
2962,
2327,
2963,
2968,
};

extern const char *names955[];
static const uint32 types955 [] =
{
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags955 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype955_0 [] = {0xFF01,273,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype955_1 [] = {0xFFF5,1,0xFF01,273,0xFFF8,1,2673,0xFFFF};
static const EIF_TYPE_INDEX g_atype955_2 [] = {0xFFF5,1,0xFF01,953,0xFFF8,1,2968,0xFFFF};

static const EIF_TYPE_INDEX *gtypes955 [] = {
g_atype955_0,
g_atype955_1,
g_atype955_2,
};

static const int32 cn_attr955 [] =
{
3748,
3749,
3750,
};

extern const char *names956[];
static const uint32 types956 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_UINT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags956 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype956_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_2 [] = {0xFF02,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_3 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype956_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes956 [] = {
g_atype956_0,
g_atype956_1,
g_atype956_2,
g_atype956_3,
g_atype956_4,
g_atype956_5,
};

static const int32 cn_attr956 [] =
{
2084,
2112,
2113,
2114,
2082,
2083,
};

extern const char *names957[];
static const uint32 types957 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_INT64,
};

static const uint16 attr_flags957 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype957_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype957_4 [] = {197,0xFFFF};

static const EIF_TYPE_INDEX *gtypes957 [] = {
g_atype957_0,
g_atype957_1,
g_atype957_2,
g_atype957_3,
g_atype957_4,
};

static const int32 cn_attr957 [] =
{
2084,
2105,
2082,
2083,
2106,
};

extern const char *names958[];
static const uint32 types958 [] =
{
SK_REF,
SK_REF,
SK_CHAR8,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags958 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype958_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_2 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype958_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes958 [] = {
g_atype958_0,
g_atype958_1,
g_atype958_2,
g_atype958_3,
g_atype958_4,
};

static const int32 cn_attr958 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names959[];
static const uint32 types959 [] =
{
SK_REF,
SK_REF,
SK_CHAR32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags959 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype959_0 [] = {0xFFF9,2,186,221,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_1 [] = {0xFF01,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_2 [] = {191,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype959_4 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes959 [] = {
g_atype959_0,
g_atype959_1,
g_atype959_2,
g_atype959_3,
g_atype959_4,
};

static const int32 cn_attr959 [] =
{
2084,
2105,
2106,
2082,
2083,
};

extern const char *names960[];
static const uint32 types960 [] =
{
SK_INT32,
};

static const uint16 attr_flags960 [] =
{0,};

static const EIF_TYPE_INDEX g_atype960_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes960 [] = {
g_atype960_0,
};

static const int32 cn_attr960 [] =
{
4981,
};

extern const char *names961[];
static const uint32 types961 [] =
{
SK_REF,
};

static const uint16 attr_flags961 [] =
{0,};

static const EIF_TYPE_INDEX g_atype961_0 [] = {0xFF01,163,0xFFFF};

static const EIF_TYPE_INDEX *gtypes961 [] = {
g_atype961_0,
};

static const int32 cn_attr961 [] =
{
4989,
};

extern const char *names963[];
static const uint32 types963 [] =
{
SK_REF,
};

static const uint16 attr_flags963 [] =
{0,};

static const EIF_TYPE_INDEX g_atype963_0 [] = {0xFF01,253,0xFF01,229,0xFFFF};

static const EIF_TYPE_INDEX *gtypes963 [] = {
g_atype963_0,
};

static const int32 cn_attr963 [] =
{
4999,
};

extern const char *names964[];
static const uint32 types964 [] =
{
SK_REF,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags964 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype964_0 [] = {0xFF01,163,0xFFFF};
static const EIF_TYPE_INDEX g_atype964_1 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype964_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes964 [] = {
g_atype964_0,
g_atype964_1,
g_atype964_2,
};

static const int32 cn_attr964 [] =
{
5022,
5006,
5015,
};

extern const char *names965[];
static const uint32 types965 [] =
{
SK_REF,
};

static const uint16 attr_flags965 [] =
{0,};

static const EIF_TYPE_INDEX g_atype965_0 [] = {0xFF01,965,0xFFFF};

static const EIF_TYPE_INDEX *gtypes965 [] = {
g_atype965_0,
};

static const int32 cn_attr965 [] =
{
5032,
};

extern const char *names966[];
static const uint32 types966 [] =
{
SK_BOOL,
};

static const uint16 attr_flags966 [] =
{0,};

static const EIF_TYPE_INDEX g_atype966_0 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes966 [] = {
g_atype966_0,
};

static const int32 cn_attr966 [] =
{
5050,
};

extern const char *names967[];
static const uint32 types967 [] =
{
SK_REF,
};

static const uint16 attr_flags967 [] =
{0,};

static const EIF_TYPE_INDEX g_atype967_0 [] = {1030,0xFFFF};

static const EIF_TYPE_INDEX *gtypes967 [] = {
g_atype967_0,
};

static const int32 cn_attr967 [] =
{
5054,
};

extern const char *names972[];
static const uint32 types972 [] =
{
SK_REF,
};

static const uint16 attr_flags972 [] =
{0,};

static const EIF_TYPE_INDEX g_atype972_0 [] = {0xFF01,163,0xFFFF};

static const EIF_TYPE_INDEX *gtypes972 [] = {
g_atype972_0,
};

static const int32 cn_attr972 [] =
{
5083,
};

extern const char *names977[];
static const uint32 types977 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags977 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype977_0 [] = {0xFF01,964,0xFFFF};
static const EIF_TYPE_INDEX g_atype977_1 [] = {0xFF01,967,0xFFFF};
static const EIF_TYPE_INDEX g_atype977_2 [] = {965,0xFFFF};
static const EIF_TYPE_INDEX g_atype977_3 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes977 [] = {
g_atype977_0,
g_atype977_1,
g_atype977_2,
g_atype977_3,
};

static const int32 cn_attr977 [] =
{
5120,
5121,
5131,
5124,
};

extern const char *names978[];
static const uint32 types978 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
};

static const uint16 attr_flags978 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype978_0 [] = {0xFF01,964,0xFFFF};
static const EIF_TYPE_INDEX g_atype978_1 [] = {0xFF01,967,0xFFFF};
static const EIF_TYPE_INDEX g_atype978_2 [] = {965,0xFFFF};
static const EIF_TYPE_INDEX g_atype978_3 [] = {200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes978 [] = {
g_atype978_0,
g_atype978_1,
g_atype978_2,
g_atype978_3,
};

static const int32 cn_attr978 [] =
{
5120,
5121,
5131,
5124,
};

extern const char *names983[];
static const uint32 types983 [] =
{
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags983 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype983_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype983_1 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes983 [] = {
g_atype983_0,
g_atype983_1,
};

static const int32 cn_attr983 [] =
{
5185,
5184,
};

extern const char *names987[];
static const uint32 types987 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags987 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype987_0 [] = {0xFF01,982,0xFFFF};
static const EIF_TYPE_INDEX g_atype987_1 [] = {0xFF01,1011,0xFFFF};

static const EIF_TYPE_INDEX *gtypes987 [] = {
g_atype987_0,
g_atype987_1,
};

static const int32 cn_attr987 [] =
{
5226,
5227,
};

extern const char *names994[];
static const uint32 types994 [] =
{
SK_REF,
};

static const uint16 attr_flags994 [] =
{0,};

static const EIF_TYPE_INDEX g_atype994_0 [] = {0,0xFFFF};

static const EIF_TYPE_INDEX *gtypes994 [] = {
g_atype994_0,
};

static const int32 cn_attr994 [] =
{
5239,
};

extern const char *names995[];
static const uint32 types995 [] =
{
SK_REF,
SK_INT32,
};

static const uint16 attr_flags995 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype995_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype995_1 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes995 [] = {
g_atype995_0,
g_atype995_1,
};

static const int32 cn_attr995 [] =
{
5258,
5243,
};

extern const char *names996[];
static const uint32 types996 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags996 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype996_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype996_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype996_2 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes996 [] = {
g_atype996_0,
g_atype996_1,
g_atype996_2,
};

static const int32 cn_attr996 [] =
{
5258,
5243,
5264,
};

extern const char *names997[];
static const uint32 types997 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags997 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype997_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype997_1 [] = {0xFF01,465,206,0xFFFF};
static const EIF_TYPE_INDEX g_atype997_2 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype997_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype997_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype997_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes997 [] = {
g_atype997_0,
g_atype997_1,
g_atype997_2,
g_atype997_3,
g_atype997_4,
g_atype997_5,
};

static const int32 cn_attr997 [] =
{
5258,
5274,
5276,
5278,
5243,
5275,
};

extern const char *names999[];
static const uint32 types999 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags999 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype999_0 [] = {0xFF01,247,0,0xFFFF};
static const EIF_TYPE_INDEX g_atype999_1 [] = {0xFF01,555,200,0xFFFF};

static const EIF_TYPE_INDEX *gtypes999 [] = {
g_atype999_0,
g_atype999_1,
};

static const int32 cn_attr999 [] =
{
5294,
5295,
};

extern const char *names1002[];
static const uint32 types1002 [] =
{
SK_REF,
};

static const uint16 attr_flags1002 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1002_0 [] = {0xFF01,163,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1002 [] = {
g_atype1002_0,
};

static const int32 cn_attr1002 [] =
{
5083,
};

extern const char *names1004[];
static const uint32 types1004 [] =
{
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags1004 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype1004_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1004_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1004_2 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1004 [] = {
g_atype1004_0,
g_atype1004_1,
g_atype1004_2,
};

static const int32 cn_attr1004 [] =
{
5471,
5472,
5470,
};

extern const char *names1005[];
static const uint32 types1005 [] =
{
SK_REF,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags1005 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1005_0 [] = {1026,0xFFFF};
static const EIF_TYPE_INDEX g_atype1005_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1005_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1005_3 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1005 [] = {
g_atype1005_0,
g_atype1005_1,
g_atype1005_2,
g_atype1005_3,
};

static const int32 cn_attr1005 [] =
{
5475,
5487,
5488,
5489,
};

extern const char *names1006[];
static const uint32 types1006 [] =
{
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags1006 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype1006_0 [] = {1029,0xFFFF};
static const EIF_TYPE_INDEX g_atype1006_1 [] = {0xFF01,1003,0xFFFF};
static const EIF_TYPE_INDEX g_atype1006_2 [] = {0xFF01,1004,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1006 [] = {
g_atype1006_0,
g_atype1006_1,
g_atype1006_2,
};

static const int32 cn_attr1006 [] =
{
5495,
5507,
5508,
};

extern const char *names1008[];
static const uint32 types1008 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags1008 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1008_0 [] = {253,0xFF01,13,0xFFFF};
static const EIF_TYPE_INDEX g_atype1008_1 [] = {0xFF01,1018,0xFFFF};
static const EIF_TYPE_INDEX g_atype1008_2 [] = {0xFF01,185,0xFFFF};
static const EIF_TYPE_INDEX g_atype1008_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1008_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1008_5 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1008 [] = {
g_atype1008_0,
g_atype1008_1,
g_atype1008_2,
g_atype1008_3,
g_atype1008_4,
g_atype1008_5,
};

static const int32 cn_attr1008 [] =
{
967,
5521,
5523,
5522,
2201,
5520,
};

extern const char *names1009[];
static const uint32 types1009 [] =
{
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags1009 [] =
{0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1009_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_1 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_2 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_3 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_4 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_5 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1009_8 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1009 [] = {
g_atype1009_0,
g_atype1009_1,
g_atype1009_2,
g_atype1009_3,
g_atype1009_4,
g_atype1009_5,
g_atype1009_6,
g_atype1009_7,
g_atype1009_8,
};

static const int32 cn_attr1009 [] =
{
5530,
5533,
5527,
5528,
5531,
5532,
5534,
5535,
5536,
};

extern const char *names1010[];
static const uint32 types1010 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags1010 [] =
{0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1010_0 [] = {0xFF01,1029,0xFFFF};
static const EIF_TYPE_INDEX g_atype1010_1 [] = {0xFF01,1029,0xFFFF};
static const EIF_TYPE_INDEX g_atype1010_2 [] = {0xFF01,966,0xFFFF};
static const EIF_TYPE_INDEX g_atype1010_3 [] = {0xFF01,240,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1010 [] = {
g_atype1010_0,
g_atype1010_1,
g_atype1010_2,
g_atype1010_3,
};

static const int32 cn_attr1010 [] =
{
5562,
5563,
5565,
5566,
};

extern const char *names1011[];
static const uint32 types1011 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
};

static const uint16 attr_flags1011 [] =
{0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1011_0 [] = {0xFF01,1029,0xFFFF};
static const EIF_TYPE_INDEX g_atype1011_1 [] = {0xFF01,1029,0xFFFF};
static const EIF_TYPE_INDEX g_atype1011_2 [] = {0xFF01,966,0xFFFF};
static const EIF_TYPE_INDEX g_atype1011_3 [] = {0xFF01,240,0xFFFF};
static const EIF_TYPE_INDEX g_atype1011_4 [] = {0xFF01,966,0xFFFF};
static const EIF_TYPE_INDEX g_atype1011_5 [] = {0xFF01,966,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1011 [] = {
g_atype1011_0,
g_atype1011_1,
g_atype1011_2,
g_atype1011_3,
g_atype1011_4,
g_atype1011_5,
};

static const int32 cn_attr1011 [] =
{
5562,
5563,
5565,
5566,
5574,
5575,
};

extern const char *names1012[];
static const uint32 types1012 [] =
{
SK_INT32,
};

static const uint16 attr_flags1012 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1012_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1012 [] = {
g_atype1012_0,
};

static const int32 cn_attr1012 [] =
{
5578,
};

extern const char *names1013[];
static const uint32 types1013 [] =
{
SK_POINTER,
};

static const uint16 attr_flags1013 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1013_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1013 [] = {
g_atype1013_0,
};

static const int32 cn_attr1013 [] =
{
5590,
};

extern const char *names1014[];
static const uint32 types1014 [] =
{
SK_POINTER,
};

static const uint16 attr_flags1014 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1014_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1014 [] = {
g_atype1014_0,
};

static const int32 cn_attr1014 [] =
{
5590,
};

extern const char *names1015[];
static const uint32 types1015 [] =
{
SK_POINTER,
};

static const uint16 attr_flags1015 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1015_0 [] = {227,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1015 [] = {
g_atype1015_0,
};

static const int32 cn_attr1015 [] =
{
5590,
};

extern const char *names1016[];
static const uint32 types1016 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags1016 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1016_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_1 [] = {971,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_2 [] = {971,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_3 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_4 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_5 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_6 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_12 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_13 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_14 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_15 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_16 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_17 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_18 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_19 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_20 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_21 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_22 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_23 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_24 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype1016_25 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1016 [] = {
g_atype1016_0,
g_atype1016_1,
g_atype1016_2,
g_atype1016_3,
g_atype1016_4,
g_atype1016_5,
g_atype1016_6,
g_atype1016_7,
g_atype1016_8,
g_atype1016_9,
g_atype1016_10,
g_atype1016_11,
g_atype1016_12,
g_atype1016_13,
g_atype1016_14,
g_atype1016_15,
g_atype1016_16,
g_atype1016_17,
g_atype1016_18,
g_atype1016_19,
g_atype1016_20,
g_atype1016_21,
g_atype1016_22,
g_atype1016_23,
g_atype1016_24,
g_atype1016_25,
};

static const int32 cn_attr1016 [] =
{
3428,
5611,
5612,
5645,
5650,
5652,
3427,
5621,
5639,
5674,
5675,
5676,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
5608,
5609,
5610,
3439,
3434,
3431,
3440,
};

extern const char *names1017[];
static const uint32 types1017 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags1017 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1017_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_1 [] = {971,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_2 [] = {971,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_3 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_4 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_5 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_6 [] = {1016,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_7 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_12 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_13 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_14 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_15 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_16 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_17 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_18 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_19 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_20 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_21 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_22 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_23 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_24 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_25 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype1017_26 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1017 [] = {
g_atype1017_0,
g_atype1017_1,
g_atype1017_2,
g_atype1017_3,
g_atype1017_4,
g_atype1017_5,
g_atype1017_6,
g_atype1017_7,
g_atype1017_8,
g_atype1017_9,
g_atype1017_10,
g_atype1017_11,
g_atype1017_12,
g_atype1017_13,
g_atype1017_14,
g_atype1017_15,
g_atype1017_16,
g_atype1017_17,
g_atype1017_18,
g_atype1017_19,
g_atype1017_20,
g_atype1017_21,
g_atype1017_22,
g_atype1017_23,
g_atype1017_24,
g_atype1017_25,
g_atype1017_26,
};

static const int32 cn_attr1017 [] =
{
3428,
5611,
5612,
5645,
5650,
5652,
5678,
3427,
5621,
5639,
5674,
5675,
5676,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
5608,
5609,
5610,
3439,
3434,
3431,
3440,
};

extern const char *names1018[];
static const uint32 types1018 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_UINT64,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags1018 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1018_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_1 [] = {1001,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_2 [] = {1001,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_3 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_4 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_5 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_6 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_12 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_13 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_14 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_15 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_16 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_17 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_18 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_19 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_20 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_21 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_22 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_23 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_24 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_25 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_26 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_27 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_28 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_29 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_30 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_31 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_32 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_33 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_34 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype1018_35 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1018 [] = {
g_atype1018_0,
g_atype1018_1,
g_atype1018_2,
g_atype1018_3,
g_atype1018_4,
g_atype1018_5,
g_atype1018_6,
g_atype1018_7,
g_atype1018_8,
g_atype1018_9,
g_atype1018_10,
g_atype1018_11,
g_atype1018_12,
g_atype1018_13,
g_atype1018_14,
g_atype1018_15,
g_atype1018_16,
g_atype1018_17,
g_atype1018_18,
g_atype1018_19,
g_atype1018_20,
g_atype1018_21,
g_atype1018_22,
g_atype1018_23,
g_atype1018_24,
g_atype1018_25,
g_atype1018_26,
g_atype1018_27,
g_atype1018_28,
g_atype1018_29,
g_atype1018_30,
g_atype1018_31,
g_atype1018_32,
g_atype1018_33,
g_atype1018_34,
g_atype1018_35,
};

static const int32 cn_attr1018 [] =
{
3428,
5611,
5612,
5645,
5650,
5652,
3427,
5621,
5639,
5674,
5675,
5676,
5680,
5681,
5682,
5719,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
5608,
5609,
5610,
5705,
5706,
5707,
5708,
5712,
3439,
3434,
5690,
3431,
3440,
};

extern const char *names1019[];
static const uint32 types1019 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_UINT64,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags1019 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1019_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_1 [] = {1001,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_2 [] = {1001,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_3 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_4 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_5 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_6 [] = {1018,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_7 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_12 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_13 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_14 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_15 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_16 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_17 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_18 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_19 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_20 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_21 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_22 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_23 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_24 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_25 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_26 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_27 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_28 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_29 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_30 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_31 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_32 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_33 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_34 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_35 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_36 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_37 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype1019_38 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1019 [] = {
g_atype1019_0,
g_atype1019_1,
g_atype1019_2,
g_atype1019_3,
g_atype1019_4,
g_atype1019_5,
g_atype1019_6,
g_atype1019_7,
g_atype1019_8,
g_atype1019_9,
g_atype1019_10,
g_atype1019_11,
g_atype1019_12,
g_atype1019_13,
g_atype1019_14,
g_atype1019_15,
g_atype1019_16,
g_atype1019_17,
g_atype1019_18,
g_atype1019_19,
g_atype1019_20,
g_atype1019_21,
g_atype1019_22,
g_atype1019_23,
g_atype1019_24,
g_atype1019_25,
g_atype1019_26,
g_atype1019_27,
g_atype1019_28,
g_atype1019_29,
g_atype1019_30,
g_atype1019_31,
g_atype1019_32,
g_atype1019_33,
g_atype1019_34,
g_atype1019_35,
g_atype1019_36,
g_atype1019_37,
g_atype1019_38,
};

static const int32 cn_attr1019 [] =
{
3428,
5611,
5612,
5645,
5650,
5652,
5678,
3427,
5621,
5639,
5674,
5675,
5676,
5680,
5681,
5682,
5719,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
5608,
5609,
5610,
5705,
5706,
5707,
5708,
5712,
5729,
5731,
3439,
3434,
5690,
3431,
3440,
};

extern const char *names1020[];
static const uint32 types1020 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_CHAR8,
SK_CHAR8,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT8,
SK_INT8,
SK_UINT16,
SK_INT16,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL32,
SK_POINTER,
SK_UINT64,
SK_INT64,
SK_REAL64,
};

static const uint16 attr_flags1020 [] =
{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1020_0 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_1 [] = {0xFF01,229,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_2 [] = {163,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_3 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_4 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_5 [] = {194,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_9 [] = {206,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_10 [] = {188,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_11 [] = {218,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_12 [] = {212,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_13 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_16 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_17 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_18 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_19 [] = {209,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_20 [] = {227,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_21 [] = {224,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_22 [] = {197,0xFFFF};
static const EIF_TYPE_INDEX g_atype1020_23 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1020 [] = {
g_atype1020_0,
g_atype1020_1,
g_atype1020_2,
g_atype1020_3,
g_atype1020_4,
g_atype1020_5,
g_atype1020_6,
g_atype1020_7,
g_atype1020_8,
g_atype1020_9,
g_atype1020_10,
g_atype1020_11,
g_atype1020_12,
g_atype1020_13,
g_atype1020_14,
g_atype1020_15,
g_atype1020_16,
g_atype1020_17,
g_atype1020_18,
g_atype1020_19,
g_atype1020_20,
g_atype1020_21,
g_atype1020_22,
g_atype1020_23,
};

static const int32 cn_attr1020 [] =
{
3428,
3616,
3618,
5763,
3427,
3534,
2327,
3680,
3694,
3438,
3433,
3437,
3432,
3435,
3429,
3441,
3671,
5758,
5765,
3439,
3535,
3434,
3431,
3440,
};

extern const char *names1021[];
static const uint32 types1021 [] =
{
SK_INT32,
};

static const uint16 attr_flags1021 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1021_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1021 [] = {
g_atype1021_0,
};

static const int32 cn_attr1021 [] =
{
5771,
};

extern const char *names1023[];
static const uint32 types1023 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
};

static const uint16 attr_flags1023 [] =
{0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1023_0 [] = {0xFF01,855,0xFF01,1008,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1023_1 [] = {0xFF01,247,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1023_2 [] = {0xFF01,247,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1023_3 [] = {1028,0xFFFF};
static const EIF_TYPE_INDEX g_atype1023_4 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1023_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1023_6 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1023 [] = {
g_atype1023_0,
g_atype1023_1,
g_atype1023_2,
g_atype1023_3,
g_atype1023_4,
g_atype1023_5,
g_atype1023_6,
};

static const int32 cn_attr1023 [] =
{
5777,
5796,
5797,
5800,
5784,
5798,
5779,
};

extern const char *names1024[];
static const uint32 types1024 [] =
{
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags1024 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype1024_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1024_1 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1024 [] = {
g_atype1024_0,
g_atype1024_1,
};

static const int32 cn_attr1024 [] =
{
5185,
5184,
};

extern const char *names1025[];
static const uint32 types1025 [] =
{
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags1025 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype1025_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1025_1 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1025 [] = {
g_atype1025_0,
g_atype1025_1,
};

static const int32 cn_attr1025 [] =
{
5185,
5184,
};

extern const char *names1026[];
static const uint32 types1026 [] =
{
SK_INT32,
};

static const uint16 attr_flags1026 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1026_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1026 [] = {
g_atype1026_0,
};

static const int32 cn_attr1026 [] =
{
5578,
};

extern const char *names1027[];
static const uint32 types1027 [] =
{
SK_INT32,
};

static const uint16 attr_flags1027 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1027_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1027 [] = {
g_atype1027_0,
};

static const int32 cn_attr1027 [] =
{
5578,
};

extern const char *names1028[];
static const uint32 types1028 [] =
{
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags1028 [] =
{0,0,0,};

static const EIF_TYPE_INDEX g_atype1028_0 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1028_1 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1028_2 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1028 [] = {
g_atype1028_0,
g_atype1028_1,
g_atype1028_2,
};

static const int32 cn_attr1028 [] =
{
5185,
5578,
5184,
};

extern const char *names1029[];
static const uint32 types1029 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_INT32,
SK_REAL64,
SK_REAL64,
};

static const uint16 attr_flags1029 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1029_0 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_1 [] = {232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_2 [] = {0xFF01,855,0xFF01,1008,221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_3 [] = {247,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_4 [] = {247,0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_5 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_6 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_7 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_10 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_11 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_12 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_13 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_14 [] = {203,0xFFFF};
static const EIF_TYPE_INDEX g_atype1029_15 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1029 [] = {
g_atype1029_0,
g_atype1029_1,
g_atype1029_2,
g_atype1029_3,
g_atype1029_4,
g_atype1029_5,
g_atype1029_6,
g_atype1029_7,
g_atype1029_8,
g_atype1029_9,
g_atype1029_10,
g_atype1029_11,
g_atype1029_12,
g_atype1029_13,
g_atype1029_14,
g_atype1029_15,
};

static const int32 cn_attr1029 [] =
{
5874,
5899,
5900,
5901,
5902,
5882,
5185,
5578,
5893,
5894,
5895,
5896,
5897,
5903,
5184,
5898,
};

extern const char *names1030[];
static const uint32 types1030 [] =
{
SK_REF,
SK_REF,
SK_INT32,
SK_INT32,
SK_REAL64,
};

static const uint16 attr_flags1030 [] =
{0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1030_0 [] = {0xFF01,1024,0xFFFF};
static const EIF_TYPE_INDEX g_atype1030_1 [] = {0xFF01,1026,0xFFFF};
static const EIF_TYPE_INDEX g_atype1030_2 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1030_3 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1030_4 [] = {203,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1030 [] = {
g_atype1030_0,
g_atype1030_1,
g_atype1030_2,
g_atype1030_3,
g_atype1030_4,
};

static const int32 cn_attr1030 [] =
{
5226,
5227,
5185,
5578,
5184,
};

extern const char *names1031[];
static const uint32 types1031 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags1031 [] =
{0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1031_0 [] = {0xFF01,230,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_1 [] = {0xFF01,230,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_2 [] = {0xFF01,237,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_3 [] = {0xFF01,237,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_4 [] = {237,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_5 [] = {237,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_6 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_7 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_8 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_9 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1031_10 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1031 [] = {
g_atype1031_0,
g_atype1031_1,
g_atype1031_2,
g_atype1031_3,
g_atype1031_4,
g_atype1031_5,
g_atype1031_6,
g_atype1031_7,
g_atype1031_8,
g_atype1031_9,
g_atype1031_10,
};

static const int32 cn_attr1031 [] =
{
5933,
5934,
5935,
5936,
5938,
5939,
5941,
5942,
5932,
5937,
5940,
};

extern const char *names1032[];
static const uint32 types1032 [] =
{
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_REF,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_BOOL,
SK_UINT32,
SK_UINT32,
SK_INT32,
SK_INT32,
SK_INT32,
};

static const uint16 attr_flags1032 [] =
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};

static const EIF_TYPE_INDEX g_atype1032_0 [] = {253,0xFF01,13,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_1 [] = {0xFF01,175,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_2 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_3 [] = {0xFF01,232,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_4 [] = {0xFF01,1018,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_5 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_6 [] = {0,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_7 [] = {0xFF01,998,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_8 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_9 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_10 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_11 [] = {200,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_12 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_13 [] = {215,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_14 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_15 [] = {221,0xFFFF};
static const EIF_TYPE_INDEX g_atype1032_16 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1032 [] = {
g_atype1032_0,
g_atype1032_1,
g_atype1032_2,
g_atype1032_3,
g_atype1032_4,
g_atype1032_5,
g_atype1032_6,
g_atype1032_7,
g_atype1032_8,
g_atype1032_9,
g_atype1032_10,
g_atype1032_11,
g_atype1032_12,
g_atype1032_13,
g_atype1032_14,
g_atype1032_15,
g_atype1032_16,
};

static const int32 cn_attr1032 [] =
{
967,
5963,
5967,
5968,
5971,
5973,
5975,
5980,
5952,
5953,
5954,
5992,
5972,
5974,
2201,
5981,
5982,
};

extern const char *names1033[];
static const uint32 types1033 [] =
{
SK_REF,
SK_REF,
};

static const uint16 attr_flags1033 [] =
{0,0,};

static const EIF_TYPE_INDEX g_atype1033_0 [] = {0xFF01,0xFFF8,1,0xFFFF};
static const EIF_TYPE_INDEX g_atype1033_1 [] = {0xFF01,0xFFF8,1,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1033 [] = {
g_atype1033_0,
g_atype1033_1,
};

static const int32 cn_attr1033 [] =
{
5427,
5428,
};

extern const char *names1034[];
static const uint32 types1034 [] =
{
SK_INT32,
};

static const uint16 attr_flags1034 [] =
{0,};

static const EIF_TYPE_INDEX g_atype1034_0 [] = {221,0xFFFF};

static const EIF_TYPE_INDEX *gtypes1034 [] = {
g_atype1034_0,
};

static const int32 cn_attr1034 [] =
{
2201,
};

const struct cnode egc_fsystem_init[] = {
{
	(long) 0,
	(long) 0,
	"ANY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TEST_CASE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SYSTEM_STRING_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SHARED_EXECUTION_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_SCOOP_RUNTIME",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_META_MODEL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"RT_DBG_EXECUTION_PARAMETERS",
	names7,
	types7,
	attr_flags7,
	gtypes7,
	(uint16) 0,
	cn_attr7,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"IDENTIFIED_CONTROLLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DEBUGGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 14,
	(long) 14,
	"DECLARATOR",
	names10,
	types10,
	attr_flags10,
	gtypes10,
	(uint16) 0,
	cn_attr10,
	112,
	14L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"SED_TYPE_MISMATCH",
	names11,
	types11,
	attr_flags11,
	gtypes11,
	(uint16) 0,
	cn_attr11,
	48,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"FILE_UTILITIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 768,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"FILE_UTILITIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 256,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_ERROR",
	names14,
	types14,
	attr_flags14,
	gtypes14,
	(uint16) 0,
	cn_attr14,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"CHARACTER_PROPERTY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STD_FILES",
	names16,
	types16,
	attr_flags16,
	gtypes16,
	(uint16) 0,
	cn_attr16,
	8,
	1L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"UNIX_SIGNALS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 0,
	"VERSIONABLE",
	names18,
	types18,
	attr_flags18,
	gtypes18,
	(uint16) 4096,
	cn_attr18,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_ERROR_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_VERSIONS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_RUNTIME",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"UTF_CONVERTER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 768,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"UTF_CONVERTER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 256,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"PROFILING_SETTING",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TRACING_SETTING",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"OPERATING_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SYSTEM_STRING",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"BASIC_ROUTINES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ASCII",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"FORMAT_INTEGER",
	names30,
	types30,
	attr_flags30,
	gtypes30,
	(uint16) 0,
	cn_attr30,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEP_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TRACING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"STRING_TRACING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"AGENT_TRACING_HANDLER",
	names34,
	types34,
	attr_flags34,
	gtypes34,
	(uint16) 0,
	cn_attr34,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"IDENTIFIED_ROUTINES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_ABSTRACT_OBJECTS_TABLE",
	names36,
	types36,
	attr_flags36,
	gtypes36,
	(uint16) 4096,
	cn_attr36,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_STORABLE_FACILITIES",
	names37,
	types37,
	attr_flags37,
	gtypes37,
	(uint16) 0,
	cn_attr37,
	8,
	1L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"OBJECT_GRAPH_MARKER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SED_READER_WRITER",
	names39,
	types39,
	attr_flags39,
	gtypes39,
	(uint16) 4096,
	cn_attr39,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_BINARY_READER_WRITER",
	names40,
	types40,
	attr_flags40,
	gtypes40,
	(uint16) 4096,
	cn_attr40,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_MEMORY_READER_WRITER",
	names41,
	types41,
	attr_flags41,
	gtypes41,
	(uint16) 0,
	cn_attr41,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFACTORING_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"THREAD_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION_COMMON",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION_GENERAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_EXTENSION",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DBG_COMMON",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_TRAVERSABLE",
	names48,
	types48,
	attr_flags48,
	gtypes48,
	(uint16) 4096,
	cn_attr48,
	56,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_DEPTH_FIRST_TRAVERSABLE",
	names49,
	types49,
	attr_flags49,
	gtypes49,
	(uint16) 0,
	cn_attr49,
	56,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"OBJECT_GRAPH_BREADTH_FIRST_TRAVERSABLE",
	names50,
	types50,
	attr_flags50,
	gtypes50,
	(uint16) 0,
	cn_attr50,
	56,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"MEMORY_STRUCTURE",
	names51,
	types51,
	attr_flags51,
	gtypes51,
	(uint16) 4096,
	cn_attr51,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INTERNAL_HELPER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NUMERIC_INFORMATION",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"INTEGER_OVERFLOW_CHECKER",
	names55,
	types55,
	attr_flags55,
	gtypes55,
	(uint16) 0,
	cn_attr55,
	32,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"STRING_TO_NUMERIC_CONVERTOR",
	names56,
	types56,
	attr_flags56,
	gtypes56,
	(uint16) 4096,
	cn_attr56,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 11,
	(long) 11,
	"HEXADECIMAL_STRING_TO_INTEGER_CONVERTER",
	names57,
	types57,
	attr_flags57,
	gtypes57,
	(uint16) 8192,
	cn_attr57,
	48,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 15,
	(long) 15,
	"STRING_TO_REAL_CONVERTOR",
	names58,
	types58,
	attr_flags58,
	gtypes58,
	(uint16) 8192,
	cn_attr58,
	64,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"STRING_TO_INTEGER_CONVERTOR",
	names59,
	types59,
	attr_flags59,
	gtypes59,
	(uint16) 8192,
	cn_attr59,
	48,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTION_MANAGER_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTIONS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"STORABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXCEPTION",
	names63,
	types63,
	attr_flags63,
	gtypes63,
	(uint16) 0,
	cn_attr63,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"DEVELOPER_EXCEPTION",
	names64,
	types64,
	attr_flags64,
	gtypes64,
	(uint16) 0,
	cn_attr64,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"MACHINE_EXCEPTION",
	names65,
	types65,
	attr_flags65,
	gtypes65,
	(uint16) 4096,
	cn_attr65,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"HARDWARE_EXCEPTION",
	names66,
	types66,
	attr_flags66,
	gtypes66,
	(uint16) 4096,
	cn_attr66,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"FLOATING_POINT_FAILURE",
	names67,
	types67,
	attr_flags67,
	gtypes67,
	(uint16) 0,
	cn_attr67,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"OPERATING_SYSTEM_EXCEPTION",
	names68,
	types68,
	attr_flags68,
	gtypes68,
	(uint16) 4096,
	cn_attr68,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"COM_FAILURE",
	names69,
	types69,
	attr_flags69,
	gtypes69,
	(uint16) 0,
	cn_attr69,
	64,
	6L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"OPERATING_SYSTEM_FAILURE",
	names70,
	types70,
	attr_flags70,
	gtypes70,
	(uint16) 0,
	cn_attr70,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"OPERATING_SYSTEM_SIGNAL_FAILURE",
	names71,
	types71,
	attr_flags71,
	gtypes71,
	(uint16) 0,
	cn_attr71,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"OBSOLETE_EXCEPTION",
	names72,
	types72,
	attr_flags72,
	gtypes72,
	(uint16) 4096,
	cn_attr72,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"RESUMPTION_FAILURE",
	names73,
	types73,
	attr_flags73,
	gtypes73,
	(uint16) 0,
	cn_attr73,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"RESCUE_FAILURE",
	names74,
	types74,
	attr_flags74,
	gtypes74,
	(uint16) 0,
	cn_attr74,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXCEPTION_IN_SIGNAL_HANDLER_FAILURE",
	names75,
	types75,
	attr_flags75,
	gtypes75,
	(uint16) 0,
	cn_attr75,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"SYS_EXCEPTION",
	names76,
	types76,
	attr_flags76,
	gtypes76,
	(uint16) 4096,
	cn_attr76,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"EIFFEL_RUNTIME_PANIC",
	names77,
	types77,
	attr_flags77,
	gtypes77,
	(uint16) 0,
	cn_attr77,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"OLD_VIOLATION",
	names78,
	types78,
	attr_flags78,
	gtypes78,
	(uint16) 0,
	cn_attr78,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIF_EXCEPTION",
	names79,
	types79,
	attr_flags79,
	gtypes79,
	(uint16) 4096,
	cn_attr79,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIFFEL_RUNTIME_EXCEPTION",
	names80,
	types80,
	attr_flags80,
	gtypes80,
	(uint16) 4096,
	cn_attr80,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EXTERNAL_FAILURE",
	names81,
	types81,
	attr_flags81,
	gtypes81,
	(uint16) 0,
	cn_attr81,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"NO_MORE_MEMORY",
	names82,
	types82,
	attr_flags82,
	gtypes82,
	(uint16) 0,
	cn_attr82,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"DATA_EXCEPTION",
	names83,
	types83,
	attr_flags83,
	gtypes83,
	(uint16) 4096,
	cn_attr83,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"SERIALIZATION_FAILURE",
	names84,
	types84,
	attr_flags84,
	gtypes84,
	(uint16) 0,
	cn_attr84,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"MISMATCH_FAILURE",
	names85,
	types85,
	attr_flags85,
	gtypes85,
	(uint16) 0,
	cn_attr85,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"IO_FAILURE",
	names86,
	types86,
	attr_flags86,
	gtypes86,
	(uint16) 0,
	cn_attr86,
	56,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"LANGUAGE_EXCEPTION",
	names87,
	types87,
	attr_flags87,
	gtypes87,
	(uint16) 4096,
	cn_attr87,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"VOID_TARGET",
	names88,
	types88,
	attr_flags88,
	gtypes88,
	(uint16) 0,
	cn_attr88,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"VOID_ASSIGNED_TO_EXPANDED",
	names89,
	types89,
	attr_flags89,
	gtypes89,
	(uint16) 0,
	cn_attr89,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"ROUTINE_FAILURE",
	names90,
	types90,
	attr_flags90,
	gtypes90,
	(uint16) 0,
	cn_attr90,
	64,
	7L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"BAD_INSPECT_VALUE",
	names91,
	types91,
	attr_flags91,
	gtypes91,
	(uint16) 0,
	cn_attr91,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"EIFFELSTUDIO_SPECIFIC_LANGUAGE_EXCEPTION",
	names92,
	types92,
	attr_flags92,
	gtypes92,
	(uint16) 4096,
	cn_attr92,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"CREATE_ON_DEFERRED",
	names93,
	types93,
	attr_flags93,
	gtypes93,
	(uint16) 0,
	cn_attr93,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"ADDRESS_APPLIED_TO_MELTED_FEATURE",
	names94,
	types94,
	attr_flags94,
	gtypes94,
	(uint16) 0,
	cn_attr94,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"ASSERTION_VIOLATION",
	names95,
	types95,
	attr_flags95,
	gtypes95,
	(uint16) 4096,
	cn_attr95,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"LOOP_INVARIANT_VIOLATION",
	names96,
	types96,
	attr_flags96,
	gtypes96,
	(uint16) 0,
	cn_attr96,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"PRECONDITION_VIOLATION",
	names97,
	types97,
	attr_flags97,
	gtypes97,
	(uint16) 0,
	cn_attr97,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"POSTCONDITION_VIOLATION",
	names98,
	types98,
	attr_flags98,
	gtypes98,
	(uint16) 0,
	cn_attr98,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"VARIANT_VIOLATION",
	names99,
	types99,
	attr_flags99,
	gtypes99,
	(uint16) 0,
	cn_attr99,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"CHECK_VIOLATION",
	names100,
	types100,
	attr_flags100,
	gtypes100,
	(uint16) 0,
	cn_attr100,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"INVARIANT_VIOLATION",
	names101,
	types101,
	attr_flags101,
	gtypes101,
	(uint16) 0,
	cn_attr101,
	48,
	5L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_SEARCHER",
	names102,
	types102,
	attr_flags102,
	gtypes102,
	(uint16) 4096,
	cn_attr102,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_8_SEARCHER",
	names103,
	types103,
	attr_flags103,
	gtypes103,
	(uint16) 8192,
	cn_attr103,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_32_SEARCHER",
	names104,
	types104,
	attr_flags104,
	gtypes104,
	(uint16) 8192,
	cn_attr104,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"PART_COMPARABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"COMPARABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SED_UTILITIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_SESSION_SERIALIZER",
	names108,
	types108,
	attr_flags108,
	gtypes108,
	(uint16) 0,
	cn_attr108,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_BASIC_SERIALIZER",
	names109,
	types109,
	attr_flags109,
	gtypes109,
	(uint16) 0,
	cn_attr109,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_RECOVERABLE_SERIALIZER",
	names110,
	types110,
	attr_flags110,
	gtypes110,
	(uint16) 0,
	cn_attr110,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_INDEPENDENT_SERIALIZER",
	names111,
	types111,
	attr_flags111,
	gtypes111,
	(uint16) 0,
	cn_attr111,
	48,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EXCEPTION_MANAGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ISE_EXCEPTION_MANAGER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"SED_SESSION_DESERIALIZER",
	names114,
	types114,
	attr_flags114,
	gtypes114,
	(uint16) 0,
	cn_attr114,
	64,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"SED_BASIC_DESERIALIZER",
	names115,
	types115,
	attr_flags115,
	gtypes115,
	(uint16) 0,
	cn_attr115,
	64,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 10,
	(long) 10,
	"SED_INDEPENDENT_DESERIALIZER",
	names116,
	types116,
	attr_flags116,
	gtypes116,
	(uint16) 0,
	cn_attr116,
	72,
	8L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MATH_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SINGLE_MATH",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DOUBLE_MATH",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"FORMAT_DOUBLE",
	names120,
	types120,
	attr_flags120,
	gtypes120,
	(uint16) 0,
	cn_attr120,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"PLATFORM",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"SED_MEDIUM_READER_WRITER",
	names122,
	types122,
	attr_flags122,
	gtypes122,
	(uint16) 0,
	cn_attr122,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 8,
	"SED_MEDIUM_READER_WRITER_1",
	names123,
	types123,
	attr_flags123,
	gtypes123,
	(uint16) 0,
	cn_attr123,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"STRING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"C_STRING",
	names125,
	types125,
	attr_flags125,
	gtypes125,
	(uint16) 0,
	cn_attr125,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MEM_CONST",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"MEM_INFO",
	names127,
	types127,
	attr_flags127,
	gtypes127,
	(uint16) 0,
	cn_attr127,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"GC_INFO",
	names128,
	types128,
	attr_flags128,
	gtypes128,
	(uint16) 0,
	cn_attr128,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REFLECTOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ECMA_INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REFLECTED_OBJECT",
	names133,
	types133,
	attr_flags133,
	gtypes133,
	(uint16) 4096,
	cn_attr133,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"RT_DBG_INTERNAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 11,
	(long) 11,
	"RT_DBG_EXECUTION_RECORDER",
	names135,
	types135,
	attr_flags135,
	gtypes135,
	(uint16) 0,
	cn_attr135,
	48,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"REFLECTED_COPY_SEMANTICS_OBJECT",
	names136,
	types136,
	attr_flags136,
	gtypes136,
	(uint16) 0,
	cn_attr136,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"REFLECTED_REFERENCE_OBJECT",
	names137,
	types137,
	attr_flags137,
	gtypes137,
	(uint16) 0,
	cn_attr137,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DEBUG_OUTPUT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 15,
	(long) 15,
	"RT_DBG_CALL_RECORD",
	names139,
	types139,
	attr_flags139,
	gtypes139,
	(uint16) 0,
	cn_attr139,
	80,
	8L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ABSTRACT_SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"RT_DBG_VALUE_RECORD",
	names141,
	types141,
	attr_flags141,
	gtypes141,
	(uint16) 4096,
	cn_attr141,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NUMERIC",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"CIRCULAR_CURSOR",
	names144,
	types144,
	attr_flags144,
	gtypes144,
	(uint16) 0,
	cn_attr144,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"HASH_TABLE_CURSOR",
	names145,
	types145,
	attr_flags145,
	gtypes145,
	(uint16) 0,
	cn_attr145,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ARRAYED_LIST_CURSOR",
	names146,
	types146,
	attr_flags146,
	gtypes146,
	(uint16) 0,
	cn_attr146,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_STRING_HANDLER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"NATIVE_STRING",
	names148,
	types148,
	attr_flags148,
	gtypes148,
	(uint16) 0,
	cn_attr148,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"FILE_COMPARER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EXECUTION_ENVIRONMENT",
	names150,
	types150,
	attr_flags150,
	gtypes150,
	(uint16) 0,
	cn_attr150,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"FILE_INFO",
	names151,
	types151,
	attr_flags151,
	gtypes151,
	(uint16) 0,
	cn_attr151,
	32,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"UNIX_FILE_INFO",
	names152,
	types152,
	attr_flags152,
	gtypes152,
	(uint16) 0,
	cn_attr152,
	32,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ARGUMENTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ARGUMENTS_32",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"INTEGER_INTERVAL",
	names155,
	types155,
	attr_flags155,
	gtypes155,
	(uint16) 0,
	cn_attr155,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ACTIVE_INTEGER_INTERVAL",
	names156,
	types156,
	attr_flags156,
	gtypes156,
	(uint16) 0,
	cn_attr156,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MISMATCH_CORRECTOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"SED_RECOVERABLE_DESERIALIZER",
	names158,
	types158,
	attr_flags158,
	gtypes158,
	(uint16) 0,
	cn_attr158,
	112,
	12L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 19,
	(long) 19,
	"MISMATCH_INFORMATION",
	names159,
	types159,
	attr_flags159,
	gtypes159,
	(uint16) 0,
	cn_attr159,
	104,
	9L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"CLASS_NAME_TRANSLATIONS",
	names160,
	types160,
	attr_flags160,
	gtypes160,
	(uint16) 0,
	cn_attr160,
	88,
	7L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"SED_OBJECTS_TABLE",
	names161,
	types161,
	attr_flags161,
	gtypes161,
	(uint16) 0,
	cn_attr161,
	88,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DISPOSABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"READ_WRITE_LOCK",
	names163,
	types163,
	attr_flags163,
	gtypes163,
	(uint16) 1024,
	cn_attr163,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 2,
	"MANAGED_POINTER",
	names164,
	types164,
	attr_flags164,
	gtypes164,
	(uint16) 1024,
	cn_attr164,
	24,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"MEMORY_STREAM",
	names165,
	types165,
	attr_flags165,
	gtypes165,
	(uint16) 1024,
	cn_attr165,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"MEMORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 1024,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONDITION_VARIABLE",
	names167,
	types167,
	attr_flags167,
	gtypes167,
	(uint16) 1024,
	cn_attr167,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"DIRECTORY",
	names168,
	types168,
	attr_flags168,
	gtypes168,
	(uint16) 1024,
	cn_attr168,
	48,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEMAPHORE",
	names169,
	types169,
	attr_flags169,
	gtypes169,
	(uint16) 1024,
	cn_attr169,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"MUTEX",
	names170,
	types170,
	attr_flags170,
	gtypes170,
	(uint16) 1024,
	cn_attr170,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"IDENTIFIED",
	names171,
	types171,
	attr_flags171,
	gtypes171,
	(uint16) 1024,
	cn_attr171,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"IO_MEDIUM",
	names172,
	types172,
	attr_flags172,
	gtypes172,
	(uint16) 5120,
	cn_attr172,
	56,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"STREAM",
	names173,
	types173,
	attr_flags173,
	gtypes173,
	(uint16) 1024,
	cn_attr173,
	72,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 20,
	(long) 19,
	"FILE",
	names174,
	types174,
	attr_flags174,
	gtypes174,
	(uint16) 5120,
	cn_attr174,
	88,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 21,
	(long) 20,
	"RAW_FILE",
	names175,
	types175,
	attr_flags175,
	gtypes175,
	(uint16) 1024,
	cn_attr175,
	96,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 21,
	(long) 20,
	"PLAIN_TEXT_FILE",
	names176,
	types176,
	attr_flags176,
	gtypes176,
	(uint16) 1024,
	cn_attr176,
	88,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"FILE_ITERATION_CURSOR",
	names177,
	types177,
	attr_flags177,
	gtypes177,
	(uint16) 1024,
	cn_attr177,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RANDOM",
	names178,
	types178,
	attr_flags178,
	gtypes178,
	(uint16) 0,
	cn_attr178,
	24,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"STRING_ITERATION_CURSOR",
	names179,
	types179,
	attr_flags179,
	gtypes179,
	(uint16) 0,
	cn_attr179,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"PRIMES",
	names180,
	types180,
	attr_flags180,
	gtypes180,
	(uint16) 0,
	cn_attr180,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"FIBONACCI",
	names181,
	types181,
	attr_flags181,
	gtypes181,
	(uint16) 0,
	cn_attr181,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"REPEATABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_32_ITERATION_CURSOR",
	names183,
	types183,
	attr_flags183,
	gtypes183,
	(uint16) 0,
	cn_attr183,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_8_ITERATION_CURSOR",
	names184,
	types184,
	attr_flags184,
	gtypes184,
	(uint16) 0,
	cn_attr184,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"HASHABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"PATH",
	names186,
	types186,
	attr_flags186,
	gtypes186,
	(uint16) 0,
	cn_attr186,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TUPLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8_REF",
	names188,
	types188,
	attr_flags188,
	gtypes188,
	(uint16) 0,
	cn_attr188,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8",
	names189,
	types189,
	attr_flags189,
	gtypes189,
	(uint16) 8966,
	cn_attr189,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_8",
	names190,
	types190,
	attr_flags190,
	gtypes190,
	(uint16) 8448,
	cn_attr190,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32_REF",
	names191,
	types191,
	attr_flags191,
	gtypes191,
	(uint16) 0,
	cn_attr191,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32",
	names192,
	types192,
	attr_flags192,
	gtypes192,
	(uint16) 8974,
	cn_attr192,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_32",
	names193,
	types193,
	attr_flags193,
	gtypes193,
	(uint16) 8448,
	cn_attr193,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8_REF",
	names194,
	types194,
	attr_flags194,
	gtypes194,
	(uint16) 0,
	cn_attr194,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8",
	names195,
	types195,
	attr_flags195,
	gtypes195,
	(uint16) 8962,
	cn_attr195,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHARACTER_8",
	names196,
	types196,
	attr_flags196,
	gtypes196,
	(uint16) 8448,
	cn_attr196,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64_REF",
	names197,
	types197,
	attr_flags197,
	gtypes197,
	(uint16) 0,
	cn_attr197,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64",
	names198,
	types198,
	attr_flags198,
	gtypes198,
	(uint16) 8969,
	cn_attr198,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_64",
	names199,
	types199,
	attr_flags199,
	gtypes199,
	(uint16) 8448,
	cn_attr199,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN_REF",
	names200,
	types200,
	attr_flags200,
	gtypes200,
	(uint16) 0,
	cn_attr200,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN",
	names201,
	types201,
	attr_flags201,
	gtypes201,
	(uint16) 8961,
	cn_attr201,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOLEAN",
	names202,
	types202,
	attr_flags202,
	gtypes202,
	(uint16) 8448,
	cn_attr202,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64_REF",
	names203,
	types203,
	attr_flags203,
	gtypes203,
	(uint16) 0,
	cn_attr203,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64",
	names204,
	types204,
	attr_flags204,
	gtypes204,
	(uint16) 8963,
	cn_attr204,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_64",
	names205,
	types205,
	attr_flags205,
	gtypes205,
	(uint16) 8448,
	cn_attr205,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8_REF",
	names206,
	types206,
	attr_flags206,
	gtypes206,
	(uint16) 0,
	cn_attr206,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8",
	names207,
	types207,
	attr_flags207,
	gtypes207,
	(uint16) 8970,
	cn_attr207,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_8",
	names208,
	types208,
	attr_flags208,
	gtypes208,
	(uint16) 8448,
	cn_attr208,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32_REF",
	names209,
	types209,
	attr_flags209,
	gtypes209,
	(uint16) 0,
	cn_attr209,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32",
	names210,
	types210,
	attr_flags210,
	gtypes210,
	(uint16) 8964,
	cn_attr210,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"REAL_32",
	names211,
	types211,
	attr_flags211,
	gtypes211,
	(uint16) 8448,
	cn_attr211,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16_REF",
	names212,
	types212,
	attr_flags212,
	gtypes212,
	(uint16) 0,
	cn_attr212,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16",
	names213,
	types213,
	attr_flags213,
	gtypes213,
	(uint16) 8967,
	cn_attr213,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_16",
	names214,
	types214,
	attr_flags214,
	gtypes214,
	(uint16) 8448,
	cn_attr214,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32_REF",
	names215,
	types215,
	attr_flags215,
	gtypes215,
	(uint16) 0,
	cn_attr215,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32",
	names216,
	types216,
	attr_flags216,
	gtypes216,
	(uint16) 8972,
	cn_attr216,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_32",
	names217,
	types217,
	attr_flags217,
	gtypes217,
	(uint16) 8448,
	cn_attr217,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16_REF",
	names218,
	types218,
	attr_flags218,
	gtypes218,
	(uint16) 0,
	cn_attr218,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16",
	names219,
	types219,
	attr_flags219,
	gtypes219,
	(uint16) 8971,
	cn_attr219,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_16",
	names220,
	types220,
	attr_flags220,
	gtypes220,
	(uint16) 8448,
	cn_attr220,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32_REF",
	names221,
	types221,
	attr_flags221,
	gtypes221,
	(uint16) 0,
	cn_attr221,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32",
	names222,
	types222,
	attr_flags222,
	gtypes222,
	(uint16) 8968,
	cn_attr222,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INTEGER_32",
	names223,
	types223,
	attr_flags223,
	gtypes223,
	(uint16) 8448,
	cn_attr223,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64_REF",
	names224,
	types224,
	attr_flags224,
	gtypes224,
	(uint16) 0,
	cn_attr224,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64",
	names225,
	types225,
	attr_flags225,
	gtypes225,
	(uint16) 8973,
	cn_attr225,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NATURAL_64",
	names226,
	types226,
	attr_flags226,
	gtypes226,
	(uint16) 8448,
	cn_attr226,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER_REF",
	names227,
	types227,
	attr_flags227,
	gtypes227,
	(uint16) 0,
	cn_attr227,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER",
	names228,
	types228,
	attr_flags228,
	gtypes228,
	(uint16) 8965,
	cn_attr228,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"POINTER",
	names229,
	types229,
	attr_flags229,
	gtypes229,
	(uint16) 8448,
	cn_attr229,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"READABLE_STRING_GENERAL",
	names230,
	types230,
	attr_flags230,
	gtypes230,
	(uint16) 4096,
	cn_attr230,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"READABLE_STRING_8",
	names231,
	types231,
	attr_flags231,
	gtypes231,
	(uint16) 4096,
	cn_attr231,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"STRING_GENERAL",
	names232,
	types232,
	attr_flags232,
	gtypes232,
	(uint16) 4096,
	cn_attr232,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_8",
	names233,
	types233,
	attr_flags233,
	gtypes233,
	(uint16) 0,
	cn_attr233,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"SEQ_STRING",
	names234,
	types234,
	attr_flags234,
	gtypes234,
	(uint16) 0,
	cn_attr234,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"PATH_NAME",
	names235,
	types235,
	attr_flags235,
	gtypes235,
	(uint16) 4096,
	cn_attr235,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"DIRECTORY_NAME",
	names236,
	types236,
	attr_flags236,
	gtypes236,
	(uint16) 0,
	cn_attr236,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"FILE_NAME",
	names237,
	types237,
	attr_flags237,
	gtypes237,
	(uint16) 0,
	cn_attr237,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"READABLE_STRING_32",
	names238,
	types238,
	attr_flags238,
	gtypes238,
	(uint16) 4096,
	cn_attr238,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"STRING_32",
	names239,
	types239,
	attr_flags239,
	gtypes239,
	(uint16) 0,
	cn_attr239,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"IMMUTABLE_STRING_GENERAL",
	names240,
	types240,
	attr_flags240,
	gtypes240,
	(uint16) 4096,
	cn_attr240,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"IMMUTABLE_STRING_8",
	names241,
	types241,
	attr_flags241,
	gtypes241,
	(uint16) 8192,
	cn_attr241,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"IMMUTABLE_STRING_32",
	names242,
	types242,
	attr_flags242,
	gtypes242,
	(uint16) 8192,
	cn_attr242,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 21,
	(long) 20,
	"CONSOLE",
	names243,
	types243,
	attr_flags243,
	gtypes243,
	(uint16) 1024,
	cn_attr243,
	88,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"SED_MULTI_OBJECT_SERIALIZATION",
	names244,
	types244,
	attr_flags244,
	gtypes244,
	(uint16) 0,
	cn_attr244,
	32,
	3L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOOL_STRING",
	names245,
	types245,
	attr_flags245,
	gtypes245,
	(uint16) 0,
	cn_attr245,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names246,
	types246,
	attr_flags246,
	gtypes246,
	(uint16) 8192,
	cn_attr246,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names247,
	types247,
	attr_flags247,
	gtypes247,
	(uint16) 8192,
	cn_attr247,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names248,
	types248,
	attr_flags248,
	gtypes248,
	(uint16) 0,
	cn_attr248,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"PROCEDURE",
	names249,
	types249,
	attr_flags249,
	gtypes249,
	(uint16) 0,
	cn_attr249,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names250,
	types250,
	attr_flags250,
	gtypes250,
	(uint16) 8965,
	cn_attr250,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names251,
	types251,
	attr_flags251,
	gtypes251,
	(uint16) 8448,
	cn_attr251,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names252,
	types252,
	attr_flags252,
	gtypes252,
	(uint16) 8192,
	cn_attr252,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 12,
	(long) 12,
	"ROUTINE",
	names253,
	types253,
	attr_flags253,
	gtypes253,
	(uint16) 4096,
	cn_attr253,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names254,
	types254,
	attr_flags254,
	gtypes254,
	(uint16) 0,
	cn_attr254,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names256,
	types256,
	attr_flags256,
	gtypes256,
	(uint16) 4096,
	cn_attr256,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names259,
	types259,
	attr_flags259,
	gtypes259,
	(uint16) 4096,
	cn_attr259,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names260,
	types260,
	attr_flags260,
	gtypes260,
	(uint16) 4096,
	cn_attr260,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names261,
	types261,
	attr_flags261,
	gtypes261,
	(uint16) 4096,
	cn_attr261,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names262,
	types262,
	attr_flags262,
	gtypes262,
	(uint16) 4096,
	cn_attr262,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names265,
	types265,
	attr_flags265,
	gtypes265,
	(uint16) 0,
	cn_attr265,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names267,
	types267,
	attr_flags267,
	gtypes267,
	(uint16) 4096,
	cn_attr267,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names268,
	types268,
	attr_flags268,
	gtypes268,
	(uint16) 4096,
	cn_attr268,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names269,
	types269,
	attr_flags269,
	gtypes269,
	(uint16) 4096,
	cn_attr269,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names270,
	types270,
	attr_flags270,
	gtypes270,
	(uint16) 4096,
	cn_attr270,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names271,
	types271,
	attr_flags271,
	gtypes271,
	(uint16) 4096,
	cn_attr271,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"FUNCTION",
	names272,
	types272,
	attr_flags272,
	gtypes272,
	(uint16) 0,
	cn_attr272,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names273,
	types273,
	attr_flags273,
	gtypes273,
	(uint16) 4096,
	cn_attr273,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names275,
	types275,
	attr_flags275,
	gtypes275,
	(uint16) 0,
	cn_attr275,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names276,
	types276,
	attr_flags276,
	gtypes276,
	(uint16) 4096,
	cn_attr276,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names277,
	types277,
	attr_flags277,
	gtypes277,
	(uint16) 0,
	cn_attr277,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names278,
	types278,
	attr_flags278,
	gtypes278,
	(uint16) 0,
	cn_attr278,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names279,
	types279,
	attr_flags279,
	gtypes279,
	(uint16) 4096,
	cn_attr279,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names280,
	types280,
	attr_flags280,
	gtypes280,
	(uint16) 4096,
	cn_attr280,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names281,
	types281,
	attr_flags281,
	gtypes281,
	(uint16) 4096,
	cn_attr281,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names282,
	types282,
	attr_flags282,
	gtypes282,
	(uint16) 4096,
	cn_attr282,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names283,
	types283,
	attr_flags283,
	gtypes283,
	(uint16) 4096,
	cn_attr283,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names284,
	types284,
	attr_flags284,
	gtypes284,
	(uint16) 4096,
	cn_attr284,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names285,
	types285,
	attr_flags285,
	gtypes285,
	(uint16) 4096,
	cn_attr285,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names286,
	types286,
	attr_flags286,
	gtypes286,
	(uint16) 4096,
	cn_attr286,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names287,
	types287,
	attr_flags287,
	gtypes287,
	(uint16) 4096,
	cn_attr287,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names288,
	types288,
	attr_flags288,
	gtypes288,
	(uint16) 0,
	cn_attr288,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names289,
	types289,
	attr_flags289,
	gtypes289,
	(uint16) 8192,
	cn_attr289,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names290,
	types290,
	attr_flags290,
	gtypes290,
	(uint16) 8192,
	cn_attr290,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names291,
	types291,
	attr_flags291,
	gtypes291,
	(uint16) 8192,
	cn_attr291,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names292,
	types292,
	attr_flags292,
	gtypes292,
	(uint16) 8192,
	cn_attr292,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names293,
	types293,
	attr_flags293,
	gtypes293,
	(uint16) 8192,
	cn_attr293,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names294,
	types294,
	attr_flags294,
	gtypes294,
	(uint16) 8192,
	cn_attr294,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names295,
	types295,
	attr_flags295,
	gtypes295,
	(uint16) 8192,
	cn_attr295,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names296,
	types296,
	attr_flags296,
	gtypes296,
	(uint16) 8192,
	cn_attr296,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names297,
	types297,
	attr_flags297,
	gtypes297,
	(uint16) 8192,
	cn_attr297,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names298,
	types298,
	attr_flags298,
	gtypes298,
	(uint16) 8192,
	cn_attr298,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names299,
	types299,
	attr_flags299,
	gtypes299,
	(uint16) 8192,
	cn_attr299,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names300,
	types300,
	attr_flags300,
	gtypes300,
	(uint16) 8192,
	cn_attr300,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names301,
	types301,
	attr_flags301,
	gtypes301,
	(uint16) 8192,
	cn_attr301,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names302,
	types302,
	attr_flags302,
	gtypes302,
	(uint16) 0,
	cn_attr302,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names303,
	types303,
	attr_flags303,
	gtypes303,
	(uint16) 0,
	cn_attr303,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names304,
	types304,
	attr_flags304,
	gtypes304,
	(uint16) 4096,
	cn_attr304,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names305,
	types305,
	attr_flags305,
	gtypes305,
	(uint16) 4096,
	cn_attr305,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names306,
	types306,
	attr_flags306,
	gtypes306,
	(uint16) 4096,
	cn_attr306,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names307,
	types307,
	attr_flags307,
	gtypes307,
	(uint16) 4096,
	cn_attr307,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names309,
	types309,
	attr_flags309,
	gtypes309,
	(uint16) 4096,
	cn_attr309,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names311,
	types311,
	attr_flags311,
	gtypes311,
	(uint16) 4096,
	cn_attr311,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names314,
	types314,
	attr_flags314,
	gtypes314,
	(uint16) 0,
	cn_attr314,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names316,
	types316,
	attr_flags316,
	gtypes316,
	(uint16) 4096,
	cn_attr316,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names317,
	types317,
	attr_flags317,
	gtypes317,
	(uint16) 4096,
	cn_attr317,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names318,
	types318,
	attr_flags318,
	gtypes318,
	(uint16) 4096,
	cn_attr318,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names319,
	types319,
	attr_flags319,
	gtypes319,
	(uint16) 4096,
	cn_attr319,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names320,
	types320,
	attr_flags320,
	gtypes320,
	(uint16) 4096,
	cn_attr320,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names321,
	types321,
	attr_flags321,
	gtypes321,
	(uint16) 4096,
	cn_attr321,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names322,
	types322,
	attr_flags322,
	gtypes322,
	(uint16) 4096,
	cn_attr322,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names323,
	types323,
	attr_flags323,
	gtypes323,
	(uint16) 4096,
	cn_attr323,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names324,
	types324,
	attr_flags324,
	gtypes324,
	(uint16) 4096,
	cn_attr324,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names325,
	types325,
	attr_flags325,
	gtypes325,
	(uint16) 4096,
	cn_attr325,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names326,
	types326,
	attr_flags326,
	gtypes326,
	(uint16) 4096,
	cn_attr326,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names327,
	types327,
	attr_flags327,
	gtypes327,
	(uint16) 4096,
	cn_attr327,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names328,
	types328,
	attr_flags328,
	gtypes328,
	(uint16) 0,
	cn_attr328,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names330,
	types330,
	attr_flags330,
	gtypes330,
	(uint16) 0,
	cn_attr330,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names331,
	types331,
	attr_flags331,
	gtypes331,
	(uint16) 4096,
	cn_attr331,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names333,
	types333,
	attr_flags333,
	gtypes333,
	(uint16) 8192,
	cn_attr333,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names334,
	types334,
	attr_flags334,
	gtypes334,
	(uint16) 4096,
	cn_attr334,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names335,
	types335,
	attr_flags335,
	gtypes335,
	(uint16) 4096,
	cn_attr335,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names336,
	types336,
	attr_flags336,
	gtypes336,
	(uint16) 0,
	cn_attr336,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names337,
	types337,
	attr_flags337,
	gtypes337,
	(uint16) 0,
	cn_attr337,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names338,
	types338,
	attr_flags338,
	gtypes338,
	(uint16) 0,
	cn_attr338,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names339,
	types339,
	attr_flags339,
	gtypes339,
	(uint16) 8965,
	cn_attr339,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names340,
	types340,
	attr_flags340,
	gtypes340,
	(uint16) 8448,
	cn_attr340,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names341,
	types341,
	attr_flags341,
	gtypes341,
	(uint16) 8192,
	cn_attr341,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names342,
	types342,
	attr_flags342,
	gtypes342,
	(uint16) 0,
	cn_attr342,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names343,
	types343,
	attr_flags343,
	gtypes343,
	(uint16) 0,
	cn_attr343,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names344,
	types344,
	attr_flags344,
	gtypes344,
	(uint16) 0,
	cn_attr344,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names345,
	types345,
	attr_flags345,
	gtypes345,
	(uint16) 0,
	cn_attr345,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names346,
	types346,
	attr_flags346,
	gtypes346,
	(uint16) 0,
	cn_attr346,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"FUNCTION",
	names347,
	types347,
	attr_flags347,
	gtypes347,
	(uint16) 0,
	cn_attr347,
	80,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names348,
	types348,
	attr_flags348,
	gtypes348,
	(uint16) 0,
	cn_attr348,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names349,
	types349,
	attr_flags349,
	gtypes349,
	(uint16) 0,
	cn_attr349,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names350,
	types350,
	attr_flags350,
	gtypes350,
	(uint16) 4096,
	cn_attr350,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names351,
	types351,
	attr_flags351,
	gtypes351,
	(uint16) 4096,
	cn_attr351,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names352,
	types352,
	attr_flags352,
	gtypes352,
	(uint16) 4096,
	cn_attr352,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names353,
	types353,
	attr_flags353,
	gtypes353,
	(uint16) 4096,
	cn_attr353,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names355,
	types355,
	attr_flags355,
	gtypes355,
	(uint16) 4096,
	cn_attr355,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names357,
	types357,
	attr_flags357,
	gtypes357,
	(uint16) 4096,
	cn_attr357,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names360,
	types360,
	attr_flags360,
	gtypes360,
	(uint16) 0,
	cn_attr360,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names362,
	types362,
	attr_flags362,
	gtypes362,
	(uint16) 4096,
	cn_attr362,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names363,
	types363,
	attr_flags363,
	gtypes363,
	(uint16) 4096,
	cn_attr363,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names364,
	types364,
	attr_flags364,
	gtypes364,
	(uint16) 4096,
	cn_attr364,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names365,
	types365,
	attr_flags365,
	gtypes365,
	(uint16) 4096,
	cn_attr365,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names366,
	types366,
	attr_flags366,
	gtypes366,
	(uint16) 4096,
	cn_attr366,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names367,
	types367,
	attr_flags367,
	gtypes367,
	(uint16) 4096,
	cn_attr367,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names368,
	types368,
	attr_flags368,
	gtypes368,
	(uint16) 4096,
	cn_attr368,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names369,
	types369,
	attr_flags369,
	gtypes369,
	(uint16) 4096,
	cn_attr369,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names370,
	types370,
	attr_flags370,
	gtypes370,
	(uint16) 4096,
	cn_attr370,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names371,
	types371,
	attr_flags371,
	gtypes371,
	(uint16) 4096,
	cn_attr371,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names372,
	types372,
	attr_flags372,
	gtypes372,
	(uint16) 4096,
	cn_attr372,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names373,
	types373,
	attr_flags373,
	gtypes373,
	(uint16) 4096,
	cn_attr373,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names374,
	types374,
	attr_flags374,
	gtypes374,
	(uint16) 0,
	cn_attr374,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names376,
	types376,
	attr_flags376,
	gtypes376,
	(uint16) 0,
	cn_attr376,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names377,
	types377,
	attr_flags377,
	gtypes377,
	(uint16) 4096,
	cn_attr377,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names379,
	types379,
	attr_flags379,
	gtypes379,
	(uint16) 8192,
	cn_attr379,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names380,
	types380,
	attr_flags380,
	gtypes380,
	(uint16) 4096,
	cn_attr380,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names381,
	types381,
	attr_flags381,
	gtypes381,
	(uint16) 4096,
	cn_attr381,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names382,
	types382,
	attr_flags382,
	gtypes382,
	(uint16) 0,
	cn_attr382,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names383,
	types383,
	attr_flags383,
	gtypes383,
	(uint16) 0,
	cn_attr383,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names384,
	types384,
	attr_flags384,
	gtypes384,
	(uint16) 0,
	cn_attr384,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names385,
	types385,
	attr_flags385,
	gtypes385,
	(uint16) 0,
	cn_attr385,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names386,
	types386,
	attr_flags386,
	gtypes386,
	(uint16) 4096,
	cn_attr386,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names387,
	types387,
	attr_flags387,
	gtypes387,
	(uint16) 4096,
	cn_attr387,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names388,
	types388,
	attr_flags388,
	gtypes388,
	(uint16) 4096,
	cn_attr388,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names389,
	types389,
	attr_flags389,
	gtypes389,
	(uint16) 4096,
	cn_attr389,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names391,
	types391,
	attr_flags391,
	gtypes391,
	(uint16) 4096,
	cn_attr391,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names393,
	types393,
	attr_flags393,
	gtypes393,
	(uint16) 4096,
	cn_attr393,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names396,
	types396,
	attr_flags396,
	gtypes396,
	(uint16) 0,
	cn_attr396,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names398,
	types398,
	attr_flags398,
	gtypes398,
	(uint16) 4096,
	cn_attr398,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names399,
	types399,
	attr_flags399,
	gtypes399,
	(uint16) 4096,
	cn_attr399,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names400,
	types400,
	attr_flags400,
	gtypes400,
	(uint16) 4096,
	cn_attr400,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names401,
	types401,
	attr_flags401,
	gtypes401,
	(uint16) 4096,
	cn_attr401,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names402,
	types402,
	attr_flags402,
	gtypes402,
	(uint16) 4096,
	cn_attr402,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names403,
	types403,
	attr_flags403,
	gtypes403,
	(uint16) 4096,
	cn_attr403,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names404,
	types404,
	attr_flags404,
	gtypes404,
	(uint16) 4096,
	cn_attr404,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names405,
	types405,
	attr_flags405,
	gtypes405,
	(uint16) 4096,
	cn_attr405,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names406,
	types406,
	attr_flags406,
	gtypes406,
	(uint16) 4096,
	cn_attr406,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names407,
	types407,
	attr_flags407,
	gtypes407,
	(uint16) 4096,
	cn_attr407,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names408,
	types408,
	attr_flags408,
	gtypes408,
	(uint16) 4096,
	cn_attr408,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names409,
	types409,
	attr_flags409,
	gtypes409,
	(uint16) 4096,
	cn_attr409,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names410,
	types410,
	attr_flags410,
	gtypes410,
	(uint16) 0,
	cn_attr410,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names412,
	types412,
	attr_flags412,
	gtypes412,
	(uint16) 0,
	cn_attr412,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names413,
	types413,
	attr_flags413,
	gtypes413,
	(uint16) 4096,
	cn_attr413,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names415,
	types415,
	attr_flags415,
	gtypes415,
	(uint16) 8192,
	cn_attr415,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names416,
	types416,
	attr_flags416,
	gtypes416,
	(uint16) 4096,
	cn_attr416,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names417,
	types417,
	attr_flags417,
	gtypes417,
	(uint16) 4096,
	cn_attr417,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names418,
	types418,
	attr_flags418,
	gtypes418,
	(uint16) 0,
	cn_attr418,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names419,
	types419,
	attr_flags419,
	gtypes419,
	(uint16) 0,
	cn_attr419,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names420,
	types420,
	attr_flags420,
	gtypes420,
	(uint16) 8192,
	cn_attr420,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names421,
	types421,
	attr_flags421,
	gtypes421,
	(uint16) 8192,
	cn_attr421,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names422,
	types422,
	attr_flags422,
	gtypes422,
	(uint16) 0,
	cn_attr422,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names423,
	types423,
	attr_flags423,
	gtypes423,
	(uint16) 8965,
	cn_attr423,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names424,
	types424,
	attr_flags424,
	gtypes424,
	(uint16) 8448,
	cn_attr424,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names425,
	types425,
	attr_flags425,
	gtypes425,
	(uint16) 8192,
	cn_attr425,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names426,
	types426,
	attr_flags426,
	gtypes426,
	(uint16) 0,
	cn_attr426,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names428,
	types428,
	attr_flags428,
	gtypes428,
	(uint16) 0,
	cn_attr428,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names429,
	types429,
	attr_flags429,
	gtypes429,
	(uint16) 8965,
	cn_attr429,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names430,
	types430,
	attr_flags430,
	gtypes430,
	(uint16) 8448,
	cn_attr430,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names431,
	types431,
	attr_flags431,
	gtypes431,
	(uint16) 8192,
	cn_attr431,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names432,
	types432,
	attr_flags432,
	gtypes432,
	(uint16) 0,
	cn_attr432,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names433,
	types433,
	attr_flags433,
	gtypes433,
	(uint16) 8965,
	cn_attr433,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names434,
	types434,
	attr_flags434,
	gtypes434,
	(uint16) 8448,
	cn_attr434,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names435,
	types435,
	attr_flags435,
	gtypes435,
	(uint16) 8192,
	cn_attr435,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names436,
	types436,
	attr_flags436,
	gtypes436,
	(uint16) 0,
	cn_attr436,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names437,
	types437,
	attr_flags437,
	gtypes437,
	(uint16) 8965,
	cn_attr437,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names438,
	types438,
	attr_flags438,
	gtypes438,
	(uint16) 8448,
	cn_attr438,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names439,
	types439,
	attr_flags439,
	gtypes439,
	(uint16) 8192,
	cn_attr439,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names440,
	types440,
	attr_flags440,
	gtypes440,
	(uint16) 0,
	cn_attr440,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names441,
	types441,
	attr_flags441,
	gtypes441,
	(uint16) 8965,
	cn_attr441,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names442,
	types442,
	attr_flags442,
	gtypes442,
	(uint16) 8448,
	cn_attr442,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names443,
	types443,
	attr_flags443,
	gtypes443,
	(uint16) 8192,
	cn_attr443,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names444,
	types444,
	attr_flags444,
	gtypes444,
	(uint16) 0,
	cn_attr444,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names445,
	types445,
	attr_flags445,
	gtypes445,
	(uint16) 8965,
	cn_attr445,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names446,
	types446,
	attr_flags446,
	gtypes446,
	(uint16) 8448,
	cn_attr446,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names447,
	types447,
	attr_flags447,
	gtypes447,
	(uint16) 8192,
	cn_attr447,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names451,
	types451,
	attr_flags451,
	gtypes451,
	(uint16) 0,
	cn_attr451,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names452,
	types452,
	attr_flags452,
	gtypes452,
	(uint16) 8965,
	cn_attr452,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names453,
	types453,
	attr_flags453,
	gtypes453,
	(uint16) 8448,
	cn_attr453,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names454,
	types454,
	attr_flags454,
	gtypes454,
	(uint16) 8192,
	cn_attr454,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names455,
	types455,
	attr_flags455,
	gtypes455,
	(uint16) 0,
	cn_attr455,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names457,
	types457,
	attr_flags457,
	gtypes457,
	(uint16) 0,
	cn_attr457,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names458,
	types458,
	attr_flags458,
	gtypes458,
	(uint16) 0,
	cn_attr458,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names463,
	types463,
	attr_flags463,
	gtypes463,
	(uint16) 4096,
	cn_attr463,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names465,
	types465,
	attr_flags465,
	gtypes465,
	(uint16) 8192,
	cn_attr465,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names466,
	types466,
	attr_flags466,
	gtypes466,
	(uint16) 0,
	cn_attr466,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names467,
	types467,
	attr_flags467,
	gtypes467,
	(uint16) 0,
	cn_attr467,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names468,
	types468,
	attr_flags468,
	gtypes468,
	(uint16) 4096,
	cn_attr468,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names469,
	types469,
	attr_flags469,
	gtypes469,
	(uint16) 4096,
	cn_attr469,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names470,
	types470,
	attr_flags470,
	gtypes470,
	(uint16) 4096,
	cn_attr470,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names471,
	types471,
	attr_flags471,
	gtypes471,
	(uint16) 4096,
	cn_attr471,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names472,
	types472,
	attr_flags472,
	gtypes472,
	(uint16) 4096,
	cn_attr472,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names473,
	types473,
	attr_flags473,
	gtypes473,
	(uint16) 4096,
	cn_attr473,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names474,
	types474,
	attr_flags474,
	gtypes474,
	(uint16) 4096,
	cn_attr474,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names475,
	types475,
	attr_flags475,
	gtypes475,
	(uint16) 4096,
	cn_attr475,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names476,
	types476,
	attr_flags476,
	gtypes476,
	(uint16) 4096,
	cn_attr476,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names477,
	types477,
	attr_flags477,
	gtypes477,
	(uint16) 4096,
	cn_attr477,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names478,
	types478,
	attr_flags478,
	gtypes478,
	(uint16) 4096,
	cn_attr478,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names479,
	types479,
	attr_flags479,
	gtypes479,
	(uint16) 4096,
	cn_attr479,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names480,
	types480,
	attr_flags480,
	gtypes480,
	(uint16) 4096,
	cn_attr480,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names481,
	types481,
	attr_flags481,
	gtypes481,
	(uint16) 4096,
	cn_attr481,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names482,
	types482,
	attr_flags482,
	gtypes482,
	(uint16) 4096,
	cn_attr482,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names483,
	types483,
	attr_flags483,
	gtypes483,
	(uint16) 4096,
	cn_attr483,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names484,
	types484,
	attr_flags484,
	gtypes484,
	(uint16) 4096,
	cn_attr484,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names485,
	types485,
	attr_flags485,
	gtypes485,
	(uint16) 4096,
	cn_attr485,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names486,
	types486,
	attr_flags486,
	gtypes486,
	(uint16) 4096,
	cn_attr486,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names487,
	types487,
	attr_flags487,
	gtypes487,
	(uint16) 4096,
	cn_attr487,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names488,
	types488,
	attr_flags488,
	gtypes488,
	(uint16) 0,
	cn_attr488,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names489,
	types489,
	attr_flags489,
	gtypes489,
	(uint16) 0,
	cn_attr489,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names490,
	types490,
	attr_flags490,
	gtypes490,
	(uint16) 0,
	cn_attr490,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names493,
	types493,
	attr_flags493,
	gtypes493,
	(uint16) 4096,
	cn_attr493,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names494,
	types494,
	attr_flags494,
	gtypes494,
	(uint16) 4096,
	cn_attr494,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names495,
	types495,
	attr_flags495,
	gtypes495,
	(uint16) 4096,
	cn_attr495,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names496,
	types496,
	attr_flags496,
	gtypes496,
	(uint16) 4096,
	cn_attr496,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names497,
	types497,
	attr_flags497,
	gtypes497,
	(uint16) 4096,
	cn_attr497,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names498,
	types498,
	attr_flags498,
	gtypes498,
	(uint16) 4096,
	cn_attr498,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names499,
	types499,
	attr_flags499,
	gtypes499,
	(uint16) 4096,
	cn_attr499,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names500,
	types500,
	attr_flags500,
	gtypes500,
	(uint16) 4096,
	cn_attr500,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names501,
	types501,
	attr_flags501,
	gtypes501,
	(uint16) 4096,
	cn_attr501,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names502,
	types502,
	attr_flags502,
	gtypes502,
	(uint16) 4096,
	cn_attr502,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names503,
	types503,
	attr_flags503,
	gtypes503,
	(uint16) 4096,
	cn_attr503,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names504,
	types504,
	attr_flags504,
	gtypes504,
	(uint16) 0,
	cn_attr504,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names505,
	types505,
	attr_flags505,
	gtypes505,
	(uint16) 8965,
	cn_attr505,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names506,
	types506,
	attr_flags506,
	gtypes506,
	(uint16) 8448,
	cn_attr506,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names507,
	types507,
	attr_flags507,
	gtypes507,
	(uint16) 8192,
	cn_attr507,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names508,
	types508,
	attr_flags508,
	gtypes508,
	(uint16) 0,
	cn_attr508,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names510,
	types510,
	attr_flags510,
	gtypes510,
	(uint16) 0,
	cn_attr510,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names511,
	types511,
	attr_flags511,
	gtypes511,
	(uint16) 0,
	cn_attr511,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names517,
	types517,
	attr_flags517,
	gtypes517,
	(uint16) 4096,
	cn_attr517,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names519,
	types519,
	attr_flags519,
	gtypes519,
	(uint16) 8192,
	cn_attr519,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names520,
	types520,
	attr_flags520,
	gtypes520,
	(uint16) 0,
	cn_attr520,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names521,
	types521,
	attr_flags521,
	gtypes521,
	(uint16) 0,
	cn_attr521,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names522,
	types522,
	attr_flags522,
	gtypes522,
	(uint16) 4096,
	cn_attr522,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names523,
	types523,
	attr_flags523,
	gtypes523,
	(uint16) 4096,
	cn_attr523,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names524,
	types524,
	attr_flags524,
	gtypes524,
	(uint16) 4096,
	cn_attr524,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names525,
	types525,
	attr_flags525,
	gtypes525,
	(uint16) 4096,
	cn_attr525,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names526,
	types526,
	attr_flags526,
	gtypes526,
	(uint16) 4096,
	cn_attr526,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names527,
	types527,
	attr_flags527,
	gtypes527,
	(uint16) 4096,
	cn_attr527,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names528,
	types528,
	attr_flags528,
	gtypes528,
	(uint16) 4096,
	cn_attr528,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names529,
	types529,
	attr_flags529,
	gtypes529,
	(uint16) 4096,
	cn_attr529,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names530,
	types530,
	attr_flags530,
	gtypes530,
	(uint16) 4096,
	cn_attr530,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names531,
	types531,
	attr_flags531,
	gtypes531,
	(uint16) 4096,
	cn_attr531,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names532,
	types532,
	attr_flags532,
	gtypes532,
	(uint16) 4096,
	cn_attr532,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names533,
	types533,
	attr_flags533,
	gtypes533,
	(uint16) 4096,
	cn_attr533,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names534,
	types534,
	attr_flags534,
	gtypes534,
	(uint16) 4096,
	cn_attr534,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names535,
	types535,
	attr_flags535,
	gtypes535,
	(uint16) 4096,
	cn_attr535,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names536,
	types536,
	attr_flags536,
	gtypes536,
	(uint16) 4096,
	cn_attr536,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names537,
	types537,
	attr_flags537,
	gtypes537,
	(uint16) 4096,
	cn_attr537,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names538,
	types538,
	attr_flags538,
	gtypes538,
	(uint16) 4096,
	cn_attr538,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names539,
	types539,
	attr_flags539,
	gtypes539,
	(uint16) 4096,
	cn_attr539,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names540,
	types540,
	attr_flags540,
	gtypes540,
	(uint16) 4096,
	cn_attr540,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names541,
	types541,
	attr_flags541,
	gtypes541,
	(uint16) 4096,
	cn_attr541,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names542,
	types542,
	attr_flags542,
	gtypes542,
	(uint16) 0,
	cn_attr542,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names543,
	types543,
	attr_flags543,
	gtypes543,
	(uint16) 0,
	cn_attr543,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names544,
	types544,
	attr_flags544,
	gtypes544,
	(uint16) 0,
	cn_attr544,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names546,
	types546,
	attr_flags546,
	gtypes546,
	(uint16) 0,
	cn_attr546,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names547,
	types547,
	attr_flags547,
	gtypes547,
	(uint16) 0,
	cn_attr547,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names553,
	types553,
	attr_flags553,
	gtypes553,
	(uint16) 4096,
	cn_attr553,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names555,
	types555,
	attr_flags555,
	gtypes555,
	(uint16) 8192,
	cn_attr555,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names556,
	types556,
	attr_flags556,
	gtypes556,
	(uint16) 0,
	cn_attr556,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names557,
	types557,
	attr_flags557,
	gtypes557,
	(uint16) 0,
	cn_attr557,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names558,
	types558,
	attr_flags558,
	gtypes558,
	(uint16) 4096,
	cn_attr558,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names559,
	types559,
	attr_flags559,
	gtypes559,
	(uint16) 4096,
	cn_attr559,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names560,
	types560,
	attr_flags560,
	gtypes560,
	(uint16) 4096,
	cn_attr560,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names561,
	types561,
	attr_flags561,
	gtypes561,
	(uint16) 4096,
	cn_attr561,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names562,
	types562,
	attr_flags562,
	gtypes562,
	(uint16) 4096,
	cn_attr562,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names563,
	types563,
	attr_flags563,
	gtypes563,
	(uint16) 4096,
	cn_attr563,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names564,
	types564,
	attr_flags564,
	gtypes564,
	(uint16) 4096,
	cn_attr564,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names565,
	types565,
	attr_flags565,
	gtypes565,
	(uint16) 4096,
	cn_attr565,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names566,
	types566,
	attr_flags566,
	gtypes566,
	(uint16) 4096,
	cn_attr566,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names567,
	types567,
	attr_flags567,
	gtypes567,
	(uint16) 4096,
	cn_attr567,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names568,
	types568,
	attr_flags568,
	gtypes568,
	(uint16) 4096,
	cn_attr568,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names569,
	types569,
	attr_flags569,
	gtypes569,
	(uint16) 4096,
	cn_attr569,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names570,
	types570,
	attr_flags570,
	gtypes570,
	(uint16) 4096,
	cn_attr570,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names571,
	types571,
	attr_flags571,
	gtypes571,
	(uint16) 4096,
	cn_attr571,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names572,
	types572,
	attr_flags572,
	gtypes572,
	(uint16) 4096,
	cn_attr572,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names573,
	types573,
	attr_flags573,
	gtypes573,
	(uint16) 4096,
	cn_attr573,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names574,
	types574,
	attr_flags574,
	gtypes574,
	(uint16) 4096,
	cn_attr574,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names575,
	types575,
	attr_flags575,
	gtypes575,
	(uint16) 4096,
	cn_attr575,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names576,
	types576,
	attr_flags576,
	gtypes576,
	(uint16) 4096,
	cn_attr576,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names577,
	types577,
	attr_flags577,
	gtypes577,
	(uint16) 4096,
	cn_attr577,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names578,
	types578,
	attr_flags578,
	gtypes578,
	(uint16) 0,
	cn_attr578,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names579,
	types579,
	attr_flags579,
	gtypes579,
	(uint16) 0,
	cn_attr579,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names580,
	types580,
	attr_flags580,
	gtypes580,
	(uint16) 4096,
	cn_attr580,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names583,
	types583,
	attr_flags583,
	gtypes583,
	(uint16) 4096,
	cn_attr583,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names584,
	types584,
	attr_flags584,
	gtypes584,
	(uint16) 4096,
	cn_attr584,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names585,
	types585,
	attr_flags585,
	gtypes585,
	(uint16) 4096,
	cn_attr585,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names586,
	types586,
	attr_flags586,
	gtypes586,
	(uint16) 4096,
	cn_attr586,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names587,
	types587,
	attr_flags587,
	gtypes587,
	(uint16) 4096,
	cn_attr587,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names588,
	types588,
	attr_flags588,
	gtypes588,
	(uint16) 4096,
	cn_attr588,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names589,
	types589,
	attr_flags589,
	gtypes589,
	(uint16) 4096,
	cn_attr589,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names590,
	types590,
	attr_flags590,
	gtypes590,
	(uint16) 4096,
	cn_attr590,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names593,
	types593,
	attr_flags593,
	gtypes593,
	(uint16) 0,
	cn_attr593,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names595,
	types595,
	attr_flags595,
	gtypes595,
	(uint16) 0,
	cn_attr595,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names596,
	types596,
	attr_flags596,
	gtypes596,
	(uint16) 8965,
	cn_attr596,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names597,
	types597,
	attr_flags597,
	gtypes597,
	(uint16) 8448,
	cn_attr597,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names598,
	types598,
	attr_flags598,
	gtypes598,
	(uint16) 8192,
	cn_attr598,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names599,
	types599,
	attr_flags599,
	gtypes599,
	(uint16) 0,
	cn_attr599,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names600,
	types600,
	attr_flags600,
	gtypes600,
	(uint16) 0,
	cn_attr600,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names601,
	types601,
	attr_flags601,
	gtypes601,
	(uint16) 8965,
	cn_attr601,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names602,
	types602,
	attr_flags602,
	gtypes602,
	(uint16) 8448,
	cn_attr602,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names603,
	types603,
	attr_flags603,
	gtypes603,
	(uint16) 8192,
	cn_attr603,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names604,
	types604,
	attr_flags604,
	gtypes604,
	(uint16) 0,
	cn_attr604,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SET",
	names605,
	types605,
	attr_flags605,
	gtypes605,
	(uint16) 4096,
	cn_attr605,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names606,
	types606,
	attr_flags606,
	gtypes606,
	(uint16) 0,
	cn_attr606,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names607,
	types607,
	attr_flags607,
	gtypes607,
	(uint16) 0,
	cn_attr607,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names608,
	types608,
	attr_flags608,
	gtypes608,
	(uint16) 0,
	cn_attr608,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names609,
	types609,
	attr_flags609,
	gtypes609,
	(uint16) 0,
	cn_attr609,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names610,
	types610,
	attr_flags610,
	gtypes610,
	(uint16) 0,
	cn_attr610,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names612,
	types612,
	attr_flags612,
	gtypes612,
	(uint16) 4096,
	cn_attr612,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names613,
	types613,
	attr_flags613,
	gtypes613,
	(uint16) 4096,
	cn_attr613,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names614,
	types614,
	attr_flags614,
	gtypes614,
	(uint16) 8192,
	cn_attr614,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names616,
	types616,
	attr_flags616,
	gtypes616,
	(uint16) 0,
	cn_attr616,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names617,
	types617,
	attr_flags617,
	gtypes617,
	(uint16) 4096,
	cn_attr617,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names618,
	types618,
	attr_flags618,
	gtypes618,
	(uint16) 0,
	cn_attr618,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names619,
	types619,
	attr_flags619,
	gtypes619,
	(uint16) 0,
	cn_attr619,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names620,
	types620,
	attr_flags620,
	gtypes620,
	(uint16) 4096,
	cn_attr620,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names621,
	types621,
	attr_flags621,
	gtypes621,
	(uint16) 4096,
	cn_attr621,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names622,
	types622,
	attr_flags622,
	gtypes622,
	(uint16) 4096,
	cn_attr622,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names623,
	types623,
	attr_flags623,
	gtypes623,
	(uint16) 4096,
	cn_attr623,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names624,
	types624,
	attr_flags624,
	gtypes624,
	(uint16) 4096,
	cn_attr624,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names625,
	types625,
	attr_flags625,
	gtypes625,
	(uint16) 4096,
	cn_attr625,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names626,
	types626,
	attr_flags626,
	gtypes626,
	(uint16) 4096,
	cn_attr626,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names627,
	types627,
	attr_flags627,
	gtypes627,
	(uint16) 0,
	cn_attr627,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names628,
	types628,
	attr_flags628,
	gtypes628,
	(uint16) 0,
	cn_attr628,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names629,
	types629,
	attr_flags629,
	gtypes629,
	(uint16) 0,
	cn_attr629,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names631,
	types631,
	attr_flags631,
	gtypes631,
	(uint16) 0,
	cn_attr631,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names632,
	types632,
	attr_flags632,
	gtypes632,
	(uint16) 0,
	cn_attr632,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names638,
	types638,
	attr_flags638,
	gtypes638,
	(uint16) 4096,
	cn_attr638,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names640,
	types640,
	attr_flags640,
	gtypes640,
	(uint16) 8192,
	cn_attr640,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names641,
	types641,
	attr_flags641,
	gtypes641,
	(uint16) 0,
	cn_attr641,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names642,
	types642,
	attr_flags642,
	gtypes642,
	(uint16) 0,
	cn_attr642,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names643,
	types643,
	attr_flags643,
	gtypes643,
	(uint16) 4096,
	cn_attr643,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names644,
	types644,
	attr_flags644,
	gtypes644,
	(uint16) 4096,
	cn_attr644,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names645,
	types645,
	attr_flags645,
	gtypes645,
	(uint16) 4096,
	cn_attr645,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names646,
	types646,
	attr_flags646,
	gtypes646,
	(uint16) 4096,
	cn_attr646,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names647,
	types647,
	attr_flags647,
	gtypes647,
	(uint16) 4096,
	cn_attr647,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names648,
	types648,
	attr_flags648,
	gtypes648,
	(uint16) 4096,
	cn_attr648,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names649,
	types649,
	attr_flags649,
	gtypes649,
	(uint16) 4096,
	cn_attr649,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names650,
	types650,
	attr_flags650,
	gtypes650,
	(uint16) 4096,
	cn_attr650,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names651,
	types651,
	attr_flags651,
	gtypes651,
	(uint16) 4096,
	cn_attr651,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names652,
	types652,
	attr_flags652,
	gtypes652,
	(uint16) 4096,
	cn_attr652,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names653,
	types653,
	attr_flags653,
	gtypes653,
	(uint16) 4096,
	cn_attr653,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names654,
	types654,
	attr_flags654,
	gtypes654,
	(uint16) 4096,
	cn_attr654,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names655,
	types655,
	attr_flags655,
	gtypes655,
	(uint16) 4096,
	cn_attr655,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names656,
	types656,
	attr_flags656,
	gtypes656,
	(uint16) 4096,
	cn_attr656,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names657,
	types657,
	attr_flags657,
	gtypes657,
	(uint16) 4096,
	cn_attr657,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names658,
	types658,
	attr_flags658,
	gtypes658,
	(uint16) 4096,
	cn_attr658,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names659,
	types659,
	attr_flags659,
	gtypes659,
	(uint16) 4096,
	cn_attr659,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names660,
	types660,
	attr_flags660,
	gtypes660,
	(uint16) 4096,
	cn_attr660,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names661,
	types661,
	attr_flags661,
	gtypes661,
	(uint16) 0,
	cn_attr661,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names662,
	types662,
	attr_flags662,
	gtypes662,
	(uint16) 4096,
	cn_attr662,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names663,
	types663,
	attr_flags663,
	gtypes663,
	(uint16) 4096,
	cn_attr663,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names664,
	types664,
	attr_flags664,
	gtypes664,
	(uint16) 0,
	cn_attr664,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names665,
	types665,
	attr_flags665,
	gtypes665,
	(uint16) 0,
	cn_attr665,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names667,
	types667,
	attr_flags667,
	gtypes667,
	(uint16) 0,
	cn_attr667,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names668,
	types668,
	attr_flags668,
	gtypes668,
	(uint16) 0,
	cn_attr668,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names674,
	types674,
	attr_flags674,
	gtypes674,
	(uint16) 4096,
	cn_attr674,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names676,
	types676,
	attr_flags676,
	gtypes676,
	(uint16) 8192,
	cn_attr676,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names677,
	types677,
	attr_flags677,
	gtypes677,
	(uint16) 0,
	cn_attr677,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names678,
	types678,
	attr_flags678,
	gtypes678,
	(uint16) 0,
	cn_attr678,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names679,
	types679,
	attr_flags679,
	gtypes679,
	(uint16) 4096,
	cn_attr679,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names680,
	types680,
	attr_flags680,
	gtypes680,
	(uint16) 4096,
	cn_attr680,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names681,
	types681,
	attr_flags681,
	gtypes681,
	(uint16) 4096,
	cn_attr681,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names682,
	types682,
	attr_flags682,
	gtypes682,
	(uint16) 4096,
	cn_attr682,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names683,
	types683,
	attr_flags683,
	gtypes683,
	(uint16) 4096,
	cn_attr683,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names684,
	types684,
	attr_flags684,
	gtypes684,
	(uint16) 4096,
	cn_attr684,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names685,
	types685,
	attr_flags685,
	gtypes685,
	(uint16) 4096,
	cn_attr685,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names686,
	types686,
	attr_flags686,
	gtypes686,
	(uint16) 4096,
	cn_attr686,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names687,
	types687,
	attr_flags687,
	gtypes687,
	(uint16) 4096,
	cn_attr687,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names688,
	types688,
	attr_flags688,
	gtypes688,
	(uint16) 4096,
	cn_attr688,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names689,
	types689,
	attr_flags689,
	gtypes689,
	(uint16) 4096,
	cn_attr689,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names690,
	types690,
	attr_flags690,
	gtypes690,
	(uint16) 4096,
	cn_attr690,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names691,
	types691,
	attr_flags691,
	gtypes691,
	(uint16) 4096,
	cn_attr691,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names692,
	types692,
	attr_flags692,
	gtypes692,
	(uint16) 4096,
	cn_attr692,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names693,
	types693,
	attr_flags693,
	gtypes693,
	(uint16) 4096,
	cn_attr693,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names694,
	types694,
	attr_flags694,
	gtypes694,
	(uint16) 4096,
	cn_attr694,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names695,
	types695,
	attr_flags695,
	gtypes695,
	(uint16) 4096,
	cn_attr695,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names696,
	types696,
	attr_flags696,
	gtypes696,
	(uint16) 4096,
	cn_attr696,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names697,
	types697,
	attr_flags697,
	gtypes697,
	(uint16) 0,
	cn_attr697,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names698,
	types698,
	attr_flags698,
	gtypes698,
	(uint16) 4096,
	cn_attr698,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names699,
	types699,
	attr_flags699,
	gtypes699,
	(uint16) 4096,
	cn_attr699,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names700,
	types700,
	attr_flags700,
	gtypes700,
	(uint16) 0,
	cn_attr700,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names701,
	types701,
	attr_flags701,
	gtypes701,
	(uint16) 0,
	cn_attr701,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names703,
	types703,
	attr_flags703,
	gtypes703,
	(uint16) 0,
	cn_attr703,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names704,
	types704,
	attr_flags704,
	gtypes704,
	(uint16) 0,
	cn_attr704,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names710,
	types710,
	attr_flags710,
	gtypes710,
	(uint16) 4096,
	cn_attr710,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names712,
	types712,
	attr_flags712,
	gtypes712,
	(uint16) 8192,
	cn_attr712,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names713,
	types713,
	attr_flags713,
	gtypes713,
	(uint16) 0,
	cn_attr713,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names714,
	types714,
	attr_flags714,
	gtypes714,
	(uint16) 0,
	cn_attr714,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names715,
	types715,
	attr_flags715,
	gtypes715,
	(uint16) 4096,
	cn_attr715,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names716,
	types716,
	attr_flags716,
	gtypes716,
	(uint16) 4096,
	cn_attr716,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names717,
	types717,
	attr_flags717,
	gtypes717,
	(uint16) 4096,
	cn_attr717,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names718,
	types718,
	attr_flags718,
	gtypes718,
	(uint16) 4096,
	cn_attr718,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names719,
	types719,
	attr_flags719,
	gtypes719,
	(uint16) 4096,
	cn_attr719,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names720,
	types720,
	attr_flags720,
	gtypes720,
	(uint16) 4096,
	cn_attr720,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names721,
	types721,
	attr_flags721,
	gtypes721,
	(uint16) 4096,
	cn_attr721,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names722,
	types722,
	attr_flags722,
	gtypes722,
	(uint16) 4096,
	cn_attr722,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names723,
	types723,
	attr_flags723,
	gtypes723,
	(uint16) 4096,
	cn_attr723,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names724,
	types724,
	attr_flags724,
	gtypes724,
	(uint16) 4096,
	cn_attr724,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names725,
	types725,
	attr_flags725,
	gtypes725,
	(uint16) 4096,
	cn_attr725,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names726,
	types726,
	attr_flags726,
	gtypes726,
	(uint16) 4096,
	cn_attr726,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names727,
	types727,
	attr_flags727,
	gtypes727,
	(uint16) 4096,
	cn_attr727,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names728,
	types728,
	attr_flags728,
	gtypes728,
	(uint16) 4096,
	cn_attr728,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names729,
	types729,
	attr_flags729,
	gtypes729,
	(uint16) 4096,
	cn_attr729,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names730,
	types730,
	attr_flags730,
	gtypes730,
	(uint16) 4096,
	cn_attr730,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names731,
	types731,
	attr_flags731,
	gtypes731,
	(uint16) 4096,
	cn_attr731,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names732,
	types732,
	attr_flags732,
	gtypes732,
	(uint16) 4096,
	cn_attr732,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names733,
	types733,
	attr_flags733,
	gtypes733,
	(uint16) 0,
	cn_attr733,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names734,
	types734,
	attr_flags734,
	gtypes734,
	(uint16) 4096,
	cn_attr734,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names735,
	types735,
	attr_flags735,
	gtypes735,
	(uint16) 4096,
	cn_attr735,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names736,
	types736,
	attr_flags736,
	gtypes736,
	(uint16) 0,
	cn_attr736,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names737,
	types737,
	attr_flags737,
	gtypes737,
	(uint16) 0,
	cn_attr737,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names739,
	types739,
	attr_flags739,
	gtypes739,
	(uint16) 0,
	cn_attr739,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names740,
	types740,
	attr_flags740,
	gtypes740,
	(uint16) 0,
	cn_attr740,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names746,
	types746,
	attr_flags746,
	gtypes746,
	(uint16) 4096,
	cn_attr746,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names748,
	types748,
	attr_flags748,
	gtypes748,
	(uint16) 8192,
	cn_attr748,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names749,
	types749,
	attr_flags749,
	gtypes749,
	(uint16) 0,
	cn_attr749,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names750,
	types750,
	attr_flags750,
	gtypes750,
	(uint16) 0,
	cn_attr750,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names751,
	types751,
	attr_flags751,
	gtypes751,
	(uint16) 4096,
	cn_attr751,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names752,
	types752,
	attr_flags752,
	gtypes752,
	(uint16) 4096,
	cn_attr752,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names753,
	types753,
	attr_flags753,
	gtypes753,
	(uint16) 4096,
	cn_attr753,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names754,
	types754,
	attr_flags754,
	gtypes754,
	(uint16) 4096,
	cn_attr754,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names755,
	types755,
	attr_flags755,
	gtypes755,
	(uint16) 4096,
	cn_attr755,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names756,
	types756,
	attr_flags756,
	gtypes756,
	(uint16) 4096,
	cn_attr756,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names757,
	types757,
	attr_flags757,
	gtypes757,
	(uint16) 4096,
	cn_attr757,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names758,
	types758,
	attr_flags758,
	gtypes758,
	(uint16) 4096,
	cn_attr758,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names759,
	types759,
	attr_flags759,
	gtypes759,
	(uint16) 4096,
	cn_attr759,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names760,
	types760,
	attr_flags760,
	gtypes760,
	(uint16) 4096,
	cn_attr760,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names761,
	types761,
	attr_flags761,
	gtypes761,
	(uint16) 4096,
	cn_attr761,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names762,
	types762,
	attr_flags762,
	gtypes762,
	(uint16) 4096,
	cn_attr762,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names763,
	types763,
	attr_flags763,
	gtypes763,
	(uint16) 4096,
	cn_attr763,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names764,
	types764,
	attr_flags764,
	gtypes764,
	(uint16) 4096,
	cn_attr764,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names765,
	types765,
	attr_flags765,
	gtypes765,
	(uint16) 4096,
	cn_attr765,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names766,
	types766,
	attr_flags766,
	gtypes766,
	(uint16) 4096,
	cn_attr766,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names767,
	types767,
	attr_flags767,
	gtypes767,
	(uint16) 4096,
	cn_attr767,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names768,
	types768,
	attr_flags768,
	gtypes768,
	(uint16) 4096,
	cn_attr768,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names769,
	types769,
	attr_flags769,
	gtypes769,
	(uint16) 0,
	cn_attr769,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names770,
	types770,
	attr_flags770,
	gtypes770,
	(uint16) 4096,
	cn_attr770,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names771,
	types771,
	attr_flags771,
	gtypes771,
	(uint16) 4096,
	cn_attr771,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names772,
	types772,
	attr_flags772,
	gtypes772,
	(uint16) 0,
	cn_attr772,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names773,
	types773,
	attr_flags773,
	gtypes773,
	(uint16) 0,
	cn_attr773,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names775,
	types775,
	attr_flags775,
	gtypes775,
	(uint16) 0,
	cn_attr775,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names776,
	types776,
	attr_flags776,
	gtypes776,
	(uint16) 4096,
	cn_attr776,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names778,
	types778,
	attr_flags778,
	gtypes778,
	(uint16) 8192,
	cn_attr778,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names779,
	types779,
	attr_flags779,
	gtypes779,
	(uint16) 0,
	cn_attr779,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names780,
	types780,
	attr_flags780,
	gtypes780,
	(uint16) 0,
	cn_attr780,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names781,
	types781,
	attr_flags781,
	gtypes781,
	(uint16) 4096,
	cn_attr781,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names782,
	types782,
	attr_flags782,
	gtypes782,
	(uint16) 4096,
	cn_attr782,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names783,
	types783,
	attr_flags783,
	gtypes783,
	(uint16) 4096,
	cn_attr783,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names784,
	types784,
	attr_flags784,
	gtypes784,
	(uint16) 4096,
	cn_attr784,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names785,
	types785,
	attr_flags785,
	gtypes785,
	(uint16) 4096,
	cn_attr785,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names786,
	types786,
	attr_flags786,
	gtypes786,
	(uint16) 4096,
	cn_attr786,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names787,
	types787,
	attr_flags787,
	gtypes787,
	(uint16) 4096,
	cn_attr787,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names788,
	types788,
	attr_flags788,
	gtypes788,
	(uint16) 4096,
	cn_attr788,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names789,
	types789,
	attr_flags789,
	gtypes789,
	(uint16) 4096,
	cn_attr789,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names790,
	types790,
	attr_flags790,
	gtypes790,
	(uint16) 0,
	cn_attr790,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names791,
	types791,
	attr_flags791,
	gtypes791,
	(uint16) 4096,
	cn_attr791,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names792,
	types792,
	attr_flags792,
	gtypes792,
	(uint16) 4096,
	cn_attr792,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names793,
	types793,
	attr_flags793,
	gtypes793,
	(uint16) 0,
	cn_attr793,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names794,
	types794,
	attr_flags794,
	gtypes794,
	(uint16) 0,
	cn_attr794,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names796,
	types796,
	attr_flags796,
	gtypes796,
	(uint16) 0,
	cn_attr796,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names797,
	types797,
	attr_flags797,
	gtypes797,
	(uint16) 0,
	cn_attr797,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names803,
	types803,
	attr_flags803,
	gtypes803,
	(uint16) 4096,
	cn_attr803,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names805,
	types805,
	attr_flags805,
	gtypes805,
	(uint16) 8192,
	cn_attr805,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names806,
	types806,
	attr_flags806,
	gtypes806,
	(uint16) 0,
	cn_attr806,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names807,
	types807,
	attr_flags807,
	gtypes807,
	(uint16) 0,
	cn_attr807,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names808,
	types808,
	attr_flags808,
	gtypes808,
	(uint16) 4096,
	cn_attr808,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names809,
	types809,
	attr_flags809,
	gtypes809,
	(uint16) 4096,
	cn_attr809,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names810,
	types810,
	attr_flags810,
	gtypes810,
	(uint16) 4096,
	cn_attr810,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names811,
	types811,
	attr_flags811,
	gtypes811,
	(uint16) 4096,
	cn_attr811,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names812,
	types812,
	attr_flags812,
	gtypes812,
	(uint16) 4096,
	cn_attr812,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names813,
	types813,
	attr_flags813,
	gtypes813,
	(uint16) 4096,
	cn_attr813,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names814,
	types814,
	attr_flags814,
	gtypes814,
	(uint16) 4096,
	cn_attr814,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names815,
	types815,
	attr_flags815,
	gtypes815,
	(uint16) 4096,
	cn_attr815,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names816,
	types816,
	attr_flags816,
	gtypes816,
	(uint16) 4096,
	cn_attr816,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names817,
	types817,
	attr_flags817,
	gtypes817,
	(uint16) 4096,
	cn_attr817,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names818,
	types818,
	attr_flags818,
	gtypes818,
	(uint16) 4096,
	cn_attr818,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names819,
	types819,
	attr_flags819,
	gtypes819,
	(uint16) 4096,
	cn_attr819,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names820,
	types820,
	attr_flags820,
	gtypes820,
	(uint16) 4096,
	cn_attr820,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names821,
	types821,
	attr_flags821,
	gtypes821,
	(uint16) 4096,
	cn_attr821,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names822,
	types822,
	attr_flags822,
	gtypes822,
	(uint16) 4096,
	cn_attr822,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names823,
	types823,
	attr_flags823,
	gtypes823,
	(uint16) 4096,
	cn_attr823,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names824,
	types824,
	attr_flags824,
	gtypes824,
	(uint16) 4096,
	cn_attr824,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names825,
	types825,
	attr_flags825,
	gtypes825,
	(uint16) 4096,
	cn_attr825,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names826,
	types826,
	attr_flags826,
	gtypes826,
	(uint16) 0,
	cn_attr826,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names827,
	types827,
	attr_flags827,
	gtypes827,
	(uint16) 4096,
	cn_attr827,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names828,
	types828,
	attr_flags828,
	gtypes828,
	(uint16) 4096,
	cn_attr828,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names829,
	types829,
	attr_flags829,
	gtypes829,
	(uint16) 0,
	cn_attr829,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names830,
	types830,
	attr_flags830,
	gtypes830,
	(uint16) 0,
	cn_attr830,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"STRING_TABLE",
	names831,
	types831,
	attr_flags831,
	gtypes831,
	(uint16) 0,
	cn_attr831,
	88,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names832,
	types832,
	attr_flags832,
	gtypes832,
	(uint16) 0,
	cn_attr832,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names833,
	types833,
	attr_flags833,
	gtypes833,
	(uint16) 0,
	cn_attr833,
	88,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names834,
	types834,
	attr_flags834,
	gtypes834,
	(uint16) 4096,
	cn_attr834,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names837,
	types837,
	attr_flags837,
	gtypes837,
	(uint16) 0,
	cn_attr837,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names838,
	types838,
	attr_flags838,
	gtypes838,
	(uint16) 0,
	cn_attr838,
	80,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names839,
	types839,
	attr_flags839,
	gtypes839,
	(uint16) 4096,
	cn_attr839,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names842,
	types842,
	attr_flags842,
	gtypes842,
	(uint16) 0,
	cn_attr842,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names843,
	types843,
	attr_flags843,
	gtypes843,
	(uint16) 0,
	cn_attr843,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 18,
	(long) 18,
	"STRING_TABLE",
	names844,
	types844,
	attr_flags844,
	gtypes844,
	(uint16) 0,
	cn_attr844,
	80,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names845,
	types845,
	attr_flags845,
	gtypes845,
	(uint16) 0,
	cn_attr845,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names846,
	types846,
	attr_flags846,
	gtypes846,
	(uint16) 0,
	cn_attr846,
	80,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names847,
	types847,
	attr_flags847,
	gtypes847,
	(uint16) 4096,
	cn_attr847,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names850,
	types850,
	attr_flags850,
	gtypes850,
	(uint16) 0,
	cn_attr850,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names851,
	types851,
	attr_flags851,
	gtypes851,
	(uint16) 8965,
	cn_attr851,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names852,
	types852,
	attr_flags852,
	gtypes852,
	(uint16) 8448,
	cn_attr852,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names853,
	types853,
	attr_flags853,
	gtypes853,
	(uint16) 8192,
	cn_attr853,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"PREDICATE",
	names854,
	types854,
	attr_flags854,
	gtypes854,
	(uint16) 0,
	cn_attr854,
	72,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names855,
	types855,
	attr_flags855,
	gtypes855,
	(uint16) 0,
	cn_attr855,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names856,
	types856,
	attr_flags856,
	gtypes856,
	(uint16) 0,
	cn_attr856,
	88,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names859,
	types859,
	attr_flags859,
	gtypes859,
	(uint16) 0,
	cn_attr859,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names860,
	types860,
	attr_flags860,
	gtypes860,
	(uint16) 0,
	cn_attr860,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names861,
	types861,
	attr_flags861,
	gtypes861,
	(uint16) 0,
	cn_attr861,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names862,
	types862,
	attr_flags862,
	gtypes862,
	(uint16) 8965,
	cn_attr862,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names863,
	types863,
	attr_flags863,
	gtypes863,
	(uint16) 8448,
	cn_attr863,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names864,
	types864,
	attr_flags864,
	gtypes864,
	(uint16) 8192,
	cn_attr864,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names865,
	types865,
	attr_flags865,
	gtypes865,
	(uint16) 0,
	cn_attr865,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names866,
	types866,
	attr_flags866,
	gtypes866,
	(uint16) 0,
	cn_attr866,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names867,
	types867,
	attr_flags867,
	gtypes867,
	(uint16) 0,
	cn_attr867,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names868,
	types868,
	attr_flags868,
	gtypes868,
	(uint16) 8965,
	cn_attr868,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TYPED_POINTER",
	names869,
	types869,
	attr_flags869,
	gtypes869,
	(uint16) 8448,
	cn_attr869,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names870,
	types870,
	attr_flags870,
	gtypes870,
	(uint16) 8192,
	cn_attr870,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names871,
	types871,
	attr_flags871,
	gtypes871,
	(uint16) 0,
	cn_attr871,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 13,
	(long) 13,
	"ACTION_SEQUENCE",
	names872,
	types872,
	attr_flags872,
	gtypes872,
	(uint16) 0,
	cn_attr872,
	88,
	9L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_QUEUE",
	names873,
	types873,
	attr_flags873,
	gtypes873,
	(uint16) 0,
	cn_attr873,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"QUEUE",
	names874,
	types874,
	attr_flags874,
	gtypes874,
	(uint16) 4096,
	cn_attr874,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DISPENSER",
	names875,
	types875,
	attr_flags875,
	gtypes875,
	(uint16) 4096,
	cn_attr875,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"INTERACTIVE_LIST",
	names876,
	types876,
	attr_flags876,
	gtypes876,
	(uint16) 4096,
	cn_attr876,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 0,
	"TYPE",
	names877,
	types877,
	attr_flags877,
	gtypes877,
	(uint16) 8192,
	cn_attr877,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names878,
	types878,
	attr_flags878,
	gtypes878,
	(uint16) 0,
	cn_attr878,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"HASH_TABLE",
	names879,
	types879,
	attr_flags879,
	gtypes879,
	(uint16) 0,
	cn_attr879,
	80,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"HASH_TABLE_ITERATION_CURSOR",
	names882,
	types882,
	attr_flags882,
	gtypes882,
	(uint16) 0,
	cn_attr882,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_STACK",
	names883,
	types883,
	attr_flags883,
	gtypes883,
	(uint16) 0,
	cn_attr883,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STACK",
	names884,
	types884,
	attr_flags884,
	gtypes884,
	(uint16) 4096,
	cn_attr884,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names885,
	types885,
	attr_flags885,
	gtypes885,
	(uint16) 0,
	cn_attr885,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names886,
	types886,
	attr_flags886,
	gtypes886,
	(uint16) 0,
	cn_attr886,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names887,
	types887,
	attr_flags887,
	gtypes887,
	(uint16) 0,
	cn_attr887,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_STACK",
	names888,
	types888,
	attr_flags888,
	gtypes888,
	(uint16) 0,
	cn_attr888,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names889,
	types889,
	attr_flags889,
	gtypes889,
	(uint16) 0,
	cn_attr889,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names890,
	types890,
	attr_flags890,
	gtypes890,
	(uint16) 0,
	cn_attr890,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names891,
	types891,
	attr_flags891,
	gtypes891,
	(uint16) 0,
	cn_attr891,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names892,
	types892,
	attr_flags892,
	gtypes892,
	(uint16) 0,
	cn_attr892,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STACK",
	names893,
	types893,
	attr_flags893,
	gtypes893,
	(uint16) 4096,
	cn_attr893,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_STACK",
	names894,
	types894,
	attr_flags894,
	gtypes894,
	(uint16) 0,
	cn_attr894,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DISPENSER",
	names895,
	types895,
	attr_flags895,
	gtypes895,
	(uint16) 4096,
	cn_attr895,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names896,
	types896,
	attr_flags896,
	gtypes896,
	(uint16) 0,
	cn_attr896,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names897,
	types897,
	attr_flags897,
	gtypes897,
	(uint16) 0,
	cn_attr897,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names898,
	types898,
	attr_flags898,
	gtypes898,
	(uint16) 0,
	cn_attr898,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"COUNTABLE_SEQUENCE",
	names899,
	types899,
	attr_flags899,
	gtypes899,
	(uint16) 4096,
	cn_attr899,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COUNTABLE",
	names900,
	types900,
	attr_flags900,
	gtypes900,
	(uint16) 4096,
	cn_attr900,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INFINITE",
	names901,
	types901,
	attr_flags901,
	gtypes901,
	(uint16) 4096,
	cn_attr901,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names902,
	types902,
	attr_flags902,
	gtypes902,
	(uint16) 0,
	cn_attr902,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SPECIAL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"SPECIAL_ITERATION_CURSOR",
	names904,
	types904,
	attr_flags904,
	gtypes904,
	(uint16) 0,
	cn_attr904,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 6,
	"READABLE_INDEXABLE_ITERATION_CURSOR",
	names905,
	types905,
	attr_flags905,
	gtypes905,
	(uint16) 0,
	cn_attr905,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"READABLE_INDEXABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERABLE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TYPED_INDEXABLE_ITERATION_CURSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"GENERAL_SPECIAL_ITERATION_CURSOR",
	names911,
	types911,
	attr_flags911,
	gtypes911,
	(uint16) 4096,
	cn_attr911,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"NATIVE_ARRAY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 8192,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAY",
	names913,
	types913,
	attr_flags913,
	gtypes913,
	(uint16) 0,
	cn_attr913,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_LIST",
	names914,
	types914,
	attr_flags914,
	gtypes914,
	(uint16) 0,
	cn_attr914,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_LIST",
	names915,
	types915,
	attr_flags915,
	gtypes915,
	(uint16) 4096,
	cn_attr915,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LIST",
	names916,
	types916,
	attr_flags916,
	gtypes916,
	(uint16) 4096,
	cn_attr916,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SEQUENCE",
	names917,
	types917,
	attr_flags917,
	gtypes917,
	(uint16) 4096,
	cn_attr917,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"FINITE",
	names918,
	types918,
	attr_flags918,
	gtypes918,
	(uint16) 4096,
	cn_attr918,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOX",
	names919,
	types919,
	attr_flags919,
	gtypes919,
	(uint16) 4096,
	cn_attr919,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CONTAINER",
	names920,
	types920,
	attr_flags920,
	gtypes920,
	(uint16) 4096,
	cn_attr920,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"LINEAR",
	names921,
	types921,
	attr_flags921,
	gtypes921,
	(uint16) 4096,
	cn_attr921,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CURSOR_STRUCTURE",
	names922,
	types922,
	attr_flags922,
	gtypes922,
	(uint16) 4096,
	cn_attr922,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ACTIVE",
	names923,
	types923,
	attr_flags923,
	gtypes923,
	(uint16) 4096,
	cn_attr923,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BAG",
	names924,
	types924,
	attr_flags924,
	gtypes924,
	(uint16) 4096,
	cn_attr924,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"COLLECTION",
	names925,
	types925,
	attr_flags925,
	gtypes925,
	(uint16) 4096,
	cn_attr925,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TRAVERSABLE",
	names926,
	types926,
	attr_flags926,
	gtypes926,
	(uint16) 4096,
	cn_attr926,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BILINEAR",
	names927,
	types927,
	attr_flags927,
	gtypes927,
	(uint16) 4096,
	cn_attr927,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CHAIN",
	names928,
	types928,
	attr_flags928,
	gtypes928,
	(uint16) 4096,
	cn_attr928,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"INDEXABLE",
	names929,
	types929,
	attr_flags929,
	gtypes929,
	(uint16) 4096,
	cn_attr929,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TABLE",
	names930,
	types930,
	attr_flags930,
	gtypes930,
	(uint16) 4096,
	cn_attr930,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DYNAMIC_CHAIN",
	names931,
	types931,
	attr_flags931,
	gtypes931,
	(uint16) 4096,
	cn_attr931,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"UNBOUNDED",
	names932,
	types932,
	attr_flags932,
	gtypes932,
	(uint16) 4096,
	cn_attr932,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"TO_SPECIAL",
	names933,
	types933,
	attr_flags933,
	gtypes933,
	(uint16) 0,
	cn_attr933,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"RESIZABLE",
	names934,
	types934,
	attr_flags934,
	gtypes934,
	(uint16) 4096,
	cn_attr934,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"BOUNDED",
	names935,
	types935,
	attr_flags935,
	gtypes935,
	(uint16) 4096,
	cn_attr935,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_LIST_ITERATION_CURSOR",
	names936,
	types936,
	attr_flags936,
	gtypes936,
	(uint16) 0,
	cn_attr936,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"ARRAY_ITERATION_CURSOR",
	names937,
	types937,
	attr_flags937,
	gtypes937,
	(uint16) 0,
	cn_attr937,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names938,
	types938,
	attr_flags938,
	gtypes938,
	(uint16) 0,
	cn_attr938,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names939,
	types939,
	attr_flags939,
	gtypes939,
	(uint16) 0,
	cn_attr939,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names940,
	types940,
	attr_flags940,
	gtypes940,
	(uint16) 0,
	cn_attr940,
	32,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_LOCAL_RECORD",
	names941,
	types941,
	attr_flags941,
	gtypes941,
	(uint16) 0,
	cn_attr941,
	32,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names942,
	types942,
	attr_flags942,
	gtypes942,
	(uint16) 0,
	cn_attr942,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names943,
	types943,
	attr_flags943,
	gtypes943,
	(uint16) 0,
	cn_attr943,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names944,
	types944,
	attr_flags944,
	gtypes944,
	(uint16) 0,
	cn_attr944,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_STACK",
	names945,
	types945,
	attr_flags945,
	gtypes945,
	(uint16) 0,
	cn_attr945,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"LINKED_LIST",
	names946,
	types946,
	attr_flags946,
	gtypes946,
	(uint16) 0,
	cn_attr946,
	24,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"LINKABLE",
	names947,
	types947,
	attr_flags947,
	gtypes947,
	(uint16) 0,
	cn_attr947,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"CELL",
	names948,
	types948,
	attr_flags948,
	gtypes948,
	(uint16) 0,
	cn_attr948,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 8,
	(long) 7,
	"LINKED_LIST_ITERATION_CURSOR",
	names949,
	types949,
	attr_flags949,
	gtypes949,
	(uint16) 0,
	cn_attr949,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"LINKED_LIST_CURSOR",
	names950,
	types950,
	attr_flags950,
	gtypes950,
	(uint16) 0,
	cn_attr950,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"STACK",
	names951,
	types951,
	attr_flags951,
	gtypes951,
	(uint16) 4096,
	cn_attr951,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_STACK",
	names952,
	types952,
	attr_flags952,
	gtypes952,
	(uint16) 0,
	cn_attr952,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DISPENSER",
	names953,
	types953,
	attr_flags953,
	gtypes953,
	(uint16) 4096,
	cn_attr953,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"ARRAYED_QUEUE",
	names954,
	types954,
	attr_flags954,
	gtypes954,
	(uint16) 0,
	cn_attr954,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"ARRAYED_QUEUE_ITERATION_CURSOR",
	names955,
	types955,
	attr_flags955,
	gtypes955,
	(uint16) 0,
	cn_attr955,
	24,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"RT_DBG_ATTRIBUTE_RECORD",
	names956,
	types956,
	attr_flags956,
	gtypes956,
	(uint16) 0,
	cn_attr956,
	40,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names957,
	types957,
	attr_flags957,
	gtypes957,
	(uint16) 0,
	cn_attr957,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names958,
	types958,
	attr_flags958,
	gtypes958,
	(uint16) 0,
	cn_attr958,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"RT_DBG_FIELD_RECORD",
	names959,
	types959,
	attr_flags959,
	gtypes959,
	(uint16) 0,
	cn_attr959,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"GAME",
	names960,
	types960,
	attr_flags960,
	gtypes960,
	(uint16) 0,
	cn_attr960,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"PACKET",
	names961,
	types961,
	attr_flags961,
	gtypes961,
	(uint16) 0,
	cn_attr961,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITP_EXPRESSION_PROCESSOR",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EQA_SYSTEM_PATH",
	names963,
	types963,
	attr_flags963,
	gtypes963,
	(uint16) 0,
	cn_attr963,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"C_DATE",
	names964,
	types964,
	attr_flags964,
	gtypes964,
	(uint16) 0,
	cn_attr964,
	16,
	1L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EQA_FILE_SYSTEM",
	names965,
	types965,
	attr_flags965,
	gtypes965,
	(uint16) 0,
	cn_attr965,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EQA_ASSERTIONS",
	names966,
	types966,
	attr_flags966,
	gtypes966,
	(uint16) 0,
	cn_attr966,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EQA_TEST_INVOCATION_RESPONSE",
	names967,
	types967,
	attr_flags967,
	gtypes967,
	(uint16) 0,
	cn_attr967,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EQA_ENVIRONMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"HELLO",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DATE_TIME_LANGUAGE_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DATE_TIME_TOOLS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"SOCKET_ADDRESS",
	names972,
	types972,
	attr_flags972,
	gtypes972,
	(uint16) 0,
	cn_attr972,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"GROUP_ELEMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SOCKET_TIMEOUT_UTILITIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ADDRINFO",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITP_SHARED_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"EQA_TEST_SET",
	names977,
	types977,
	attr_flags977,
	gtypes977,
	(uint16) 4096,
	cn_attr977,
	32,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"TEST1",
	names978,
	types978,
	attr_flags978,
	gtypes978,
	(uint16) 0,
	cn_attr978,
	32,
	3L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"CODE_VALIDITY_CHECKER",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TIME_UTILITY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TIME_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"TIME_MEASUREMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"TIME_VALUE",
	names983,
	types983,
	attr_flags983,
	gtypes983,
	(uint16) 0,
	cn_attr983,
	16,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DATE_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DATE_MEASUREMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DATE_TIME_MEASUREMENT",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"DATE_TIME_VALUE",
	names987,
	types987,
	attr_flags987,
	gtypes987,
	(uint16) 4096,
	cn_attr987,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ITP_EXPRESSION",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INET_PROPERTIES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INET_ADDRESS_IMPL",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INET_ADDRESS_IMPL_V6",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INET_ADDRESS_IMPL_V4",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ERL_CONSTANTS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ITP_CONSTANT",
	names994,
	types994,
	attr_flags994,
	gtypes994,
	(uint16) 0,
	cn_attr994,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"INET_ADDRESS",
	names995,
	types995,
	attr_flags995,
	gtypes995,
	(uint16) 4096,
	cn_attr995,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"INET4_ADDRESS",
	names996,
	types996,
	attr_flags996,
	gtypes996,
	(uint16) 0,
	cn_attr996,
	16,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"INET6_ADDRESS",
	names997,
	types997,
	attr_flags997,
	gtypes997,
	(uint16) 0,
	cn_attr997,
	40,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"EQA_EXTERNALS",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"ITP_STORE",
	names999,
	types999,
	attr_flags999,
	gtypes999,
	(uint16) 0,
	cn_attr999,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"SOCKET_RESOURCES",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"INET_ADDRESS_FACTORY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"NETWORK_SOCKET_ADDRESS",
	names1002,
	types1002,
	attr_flags1002,
	gtypes1002,
	(uint16) 0,
	cn_attr1002,
	8,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"DURATION",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"TIME_DURATION",
	names1004,
	types1004,
	attr_flags1004,
	gtypes1004,
	(uint16) 0,
	cn_attr1004,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"DATE_DURATION",
	names1005,
	types1005,
	attr_flags1005,
	gtypes1005,
	(uint16) 0,
	cn_attr1005,
	24,
	1L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"DATE_TIME_DURATION",
	names1006,
	types1006,
	attr_flags1006,
	gtypes1006,
	(uint16) 0,
	cn_attr1006,
	24,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"ABSOLUTE",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 4096,
	(int32 *) 0,
	0,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"EQA_EVALUATOR",
	names1008,
	types1008,
	attr_flags1008,
	gtypes1008,
	(uint16) 8192,
	cn_attr1008,
	40,
	3L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 9,
	(long) 9,
	"DATE_TIME_CODE",
	names1009,
	types1009,
	attr_flags1009,
	gtypes1009,
	(uint16) 0,
	cn_attr1009,
	40,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 4,
	(long) 4,
	"EQA_PARTIAL_RESULT",
	names1010,
	types1010,
	attr_flags1010,
	gtypes1010,
	(uint16) 0,
	cn_attr1010,
	32,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 6,
	(long) 6,
	"EQA_RESULT",
	names1011,
	types1011,
	attr_flags1011,
	gtypes1011,
	(uint16) 0,
	cn_attr1011,
	48,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DATE_VALUE",
	names1012,
	types1012,
	attr_flags1012,
	gtypes1012,
	(uint16) 0,
	cn_attr1012,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EXTERNAL_OBJECT",
	names1013,
	types1013,
	attr_flags1013,
	gtypes1013,
	(uint16) 5120,
	cn_attr1013,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ADDRINFO_1",
	names1014,
	types1014,
	attr_flags1014,
	gtypes1014,
	(uint16) 1024,
	cn_attr1014,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ADDRINFO_2",
	names1015,
	types1015,
	attr_flags1015,
	gtypes1015,
	(uint16) 1024,
	cn_attr1015,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 26,
	(long) 26,
	"SOCKET",
	names1016,
	types1016,
	attr_flags1016,
	gtypes1016,
	(uint16) 5120,
	cn_attr1016,
	112,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 27,
	(long) 27,
	"STREAM_SOCKET",
	names1017,
	types1017,
	attr_flags1017,
	gtypes1017,
	(uint16) 5120,
	cn_attr1017,
	120,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 36,
	(long) 36,
	"NETWORK_SOCKET",
	names1018,
	types1018,
	attr_flags1018,
	gtypes1018,
	(uint16) 5120,
	cn_attr1018,
	144,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 39,
	(long) 39,
	"NETWORK_STREAM_SOCKET",
	names1019,
	types1019,
	attr_flags1019,
	gtypes1019,
	(uint16) 1024,
	cn_attr1019,
	160,
	7L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 24,
	(long) 23,
	"EQA_TEST_OUTPUT_BUFFER",
	names1020,
	types1020,
	attr_flags1020,
	gtypes1020,
	(uint16) 1024,
	cn_attr1020,
	104,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"ITP_VARIABLE",
	names1021,
	types1021,
	attr_flags1021,
	gtypes1021,
	(uint16) 0,
	cn_attr1021,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 0,
	(long) 0,
	"FIND_SEPARATOR_FACILITY",
	NULL,
	NULL,
	NULL,
	NULL,
	(uint16) 0,
	(int32 *) 0,
	0,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 7,
	(long) 7,
	"DATE_TIME_CODE_STRING",
	names1023,
	types1023,
	attr_flags1023,
	gtypes1023,
	(uint16) 0,
	cn_attr1023,
	40,
	4L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"TIME_VALIDITY_CHECKER",
	names1024,
	types1024,
	attr_flags1024,
	gtypes1024,
	(uint16) 0,
	cn_attr1024,
	16,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"TIME",
	names1025,
	types1025,
	attr_flags1025,
	gtypes1025,
	(uint16) 0,
	cn_attr1025,
	16,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DATE_VALIDITY_CHECKER",
	names1026,
	types1026,
	attr_flags1026,
	gtypes1026,
	(uint16) 0,
	cn_attr1026,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"DATE",
	names1027,
	types1027,
	attr_flags1027,
	gtypes1027,
	(uint16) 0,
	cn_attr1027,
	8,
	0L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 3,
	(long) 3,
	"DATE_TIME_VALIDITY_CHECKER",
	names1028,
	types1028,
	attr_flags1028,
	gtypes1028,
	(uint16) 0,
	cn_attr1028,
	16,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 16,
	(long) 16,
	"DATE_TIME_PARSER",
	names1029,
	types1029,
	attr_flags1029,
	gtypes1029,
	(uint16) 0,
	cn_attr1029,
	96,
	5L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 5,
	(long) 5,
	"DATE_TIME",
	names1030,
	types1030,
	attr_flags1030,
	gtypes1030,
	(uint16) 0,
	cn_attr1030,
	32,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 11,
	(long) 11,
	"EQA_TEST_INVOCATION_EXCEPTION",
	names1031,
	types1031,
	attr_flags1031,
	gtypes1031,
	(uint16) 0,
	cn_attr1031,
	64,
	6L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 17,
	(long) 17,
	"ITP_INTERPRETER",
	names1032,
	types1032,
	attr_flags1032,
	gtypes1032,
	(uint16) 0,
	cn_attr1032,
	88,
	8L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 2,
	(long) 2,
	"INTERVAL",
	names1033,
	types1033,
	attr_flags1033,
	gtypes1033,
	(uint16) 0,
	cn_attr1033,
	16,
	2L,
	(int32) 0,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},
{
	(long) 1,
	(long) 1,
	"EQA_TEST_EVALUATOR",
	names1034,
	types1034,
	attr_flags1034,
	gtypes1034,
	(uint16) 0,
	cn_attr1034,
	8,
	0L,
	(int32) 32,
	{(int32) 0, (int) 0, (char **) 0, (char *) 0},
	NULL
},};


#ifdef __cplusplus
}
#endif
