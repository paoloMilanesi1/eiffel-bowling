

#ifdef __cplusplus
extern "C" {
#endif

char *names7 [] =
{
"flatten_when_closing",
"keep_calls_records",
"recording_values",
"maximum_record_count",
};

char *names10 [] =
{
"s1",
"s1_2",
"s1_3",
"s1_4",
"s2",
"s3",
"s4",
"s5",
"s6",
"s7",
"s8",
"s8_2",
"s8_3",
"s8_4",
};

char *names11 [] =
{
"old_version",
"new_version",
"mismatches_by_name",
"mismatches_by_stored_position",
"has_version_mismatch",
"has_new_attribute",
"has_new_attached_attribute",
"type_id",
"old_count",
"new_count",
};

char *names14 [] =
{
"message",
};

char *names16 [] =
{
"default_output",
};

char *names18 [] =
{
"version",
};

char *names30 [] =
{
"sign_string",
"fill_character",
"separator",
"trailing_sign",
"bracketted_negative",
"width",
"justification",
"sign_format",
};

char *names34 [] =
{
"action",
};

char *names36 [] =
{
"last_index",
};

char *names37 [] =
{
"retrieved_errors",
};

char *names39 [] =
{
"is_pointer_value_stored",
};

char *names40 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"buffer_size",
"buffer_position",
"stored_pointer_bytes",
};

char *names41 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"buffer_size",
"buffer_position",
"stored_pointer_bytes",
};

char *names48 [] =
{
"root_object",
"on_processing_object_action",
"on_processing_reference_action",
"object_action",
"visited_objects",
"visited_types",
"has_failed",
"has_reference_with_copy_semantics",
"is_skip_transient",
"is_skip_copy_semantics_reference",
"is_exception_on_copy_suppressed",
"is_exception_propagated",
};

char *names49 [] =
{
"root_object",
"on_processing_object_action",
"on_processing_reference_action",
"object_action",
"visited_objects",
"visited_types",
"has_failed",
"has_reference_with_copy_semantics",
"is_skip_transient",
"is_skip_copy_semantics_reference",
"is_exception_on_copy_suppressed",
"is_exception_propagated",
};

char *names50 [] =
{
"root_object",
"on_processing_object_action",
"on_processing_reference_action",
"object_action",
"visited_objects",
"visited_types",
"has_failed",
"has_reference_with_copy_semantics",
"is_skip_transient",
"is_skip_copy_semantics_reference",
"is_exception_on_copy_suppressed",
"is_exception_propagated",
};

char *names51 [] =
{
"managed_pointer",
"shared",
"internal_item",
};

char *names55 [] =
{
"integer_overflow_state1",
"integer_overflow_state2",
"natural_overflow_state1",
"natural_overflow_state2",
};

char *names56 [] =
{
"leading_separators",
"trailing_separators",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"conversion_type",
"last_state",
"sign",
};

char *names57 [] =
{
"leading_separators",
"trailing_separators",
"internal_lookahead",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"internal_overflowed",
"conversion_type",
"last_state",
"sign",
"part1",
"part2",
};

char *names58 [] =
{
"leading_separators",
"trailing_separators",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"is_negative",
"has_negative_exponent",
"has_fractional_part",
"needs_digit",
"conversion_type",
"last_state",
"sign",
"exponent",
"natural_part",
"fractional_part",
"fractional_divider",
};

char *names59 [] =
{
"leading_separators",
"trailing_separators",
"trailing_separators_acceptable",
"leading_separators_acceptable",
"internal_overflowed",
"conversion_type",
"last_state",
"sign",
"part1",
"part2",
};

char *names63 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names64 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names65 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names66 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names67 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names68 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names69 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"exception_information",
"internal_is_ignorable",
"line_number",
"hresult",
"hresult_code",
};

char *names70 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"error_code",
};

char *names71 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"signal_code",
};

char *names72 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names73 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names74 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names75 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names76 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names77 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"internal_code",
};

char *names78 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names79 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names80 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names81 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names82 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"internal_code",
};

char *names83 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names84 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names85 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names86 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
"error_code",
"internal_code",
};

char *names87 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names88 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names89 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names90 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"routine_name",
"class_name",
"internal_is_ignorable",
"line_number",
};

char *names91 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names92 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names93 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names94 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names95 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names96 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names97 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names98 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names99 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names100 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"line_number",
};

char *names101 [] =
{
"recipient_name",
"type_name",
"throwing_exception",
"c_description",
"internal_trace",
"internal_is_ignorable",
"is_entry",
"line_number",
};

char *names102 [] =
{
"deltas",
"deltas_array",
};

char *names103 [] =
{
"deltas",
"deltas_array",
};

char *names104 [] =
{
"deltas",
"deltas_array",
};

char *names108 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names109 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names110 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names111 [] =
{
"serializer",
"reflector",
"reflected_object",
"traversable",
"object_indexes",
"is_root_object_set",
"has_reference_with_copy_semantics",
"version",
};

char *names114 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"has_reference_with_copy_semantics",
"version",
};

char *names115 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"has_reference_with_copy_semantics",
"version",
};

char *names116 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"attributes_mapping",
"has_reference_with_copy_semantics",
"version",
};

char *names120 [] =
{
"sign_string",
"fill_character",
"separator",
"decimal",
"trailing_sign",
"bracketted_negative",
"after_decimal_separate",
"zero_not_shown",
"trailing_zeros_shown",
"width",
"justification",
"sign_format",
"decimals",
};

char *names122 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"is_last_chunk",
"buffer_size",
"buffer_position",
"stored_pointer_bytes",
};

char *names123 [] =
{
"buffer",
"medium",
"is_pointer_value_stored",
"is_for_reading",
"is_little_endian_storable",
"buffer_size",
"buffer_position",
"stored_pointer_bytes",
};

char *names125 [] =
{
"managed_data",
"count",
};

char *names127 [] =
{
"managed_pointer",
"shared",
"type",
"internal_item",
};

char *names128 [] =
{
"managed_pointer",
"shared",
"type",
"internal_item",
};

char *names133 [] =
{
"dynamic_type",
};

char *names135 [] =
{
"top_callstack_record",
"bottom_callstack_record",
"replayed_call",
"replay_stack",
"flatten_when_closing",
"keep_calls_records",
"recording_values",
"is_replaying",
"last_replay_operation_failed",
"record_count",
"maximum_record_count",
};

char *names136 [] =
{
"referring_object",
"dynamic_type",
"physical_offset",
"referring_physical_offset",
};

char *names137 [] =
{
"enclosing_object",
"dynamic_type",
"physical_offset",
};

char *names139 [] =
{
"recorder",
"object",
"breakable_info",
"parent",
"steps",
"call_records",
"value_records",
"last_position",
"rt_information_available",
"is_expanded",
"is_flat",
"is_closed",
"class_type_id",
"feature_rout_id",
"depth",
};

char *names141 [] =
{
"breakable_info",
"position",
"type",
};

char *names144 [] =
{
"cursor",
"internal_exhausted",
"starter",
};

char *names145 [] =
{
"position",
};

char *names146 [] =
{
"index",
};

char *names148 [] =
{
"managed_data",
"unit_count",
};

char *names150 [] =
{
"return_code",
};

char *names151 [] =
{
"buffered_file_info",
"internal_file_name",
"internal_name_pointer",
"exists",
"is_following_symlinks",
};

char *names152 [] =
{
"buffered_file_info",
"internal_file_name",
"internal_name_pointer",
"exists",
"is_following_symlinks",
};

char *names155 [] =
{
"object_comparison",
"upper",
"lower",
};

char *names156 [] =
{
"opo_change_actions",
"object_comparison",
"upper",
"lower",
};

char *names158 [] =
{
"deserializer",
"last_decoded_object",
"errors",
"reflector",
"reflected_object",
"object_references",
"dynamic_type_table",
"attributes_mapping",
"class_type_translator",
"attribute_name_translator",
"mismatches",
"mismatched_object",
"has_reference_with_copy_semantics",
"is_conforming_mismatch_allowed",
"is_attribute_removal_allowed",
"is_stopping_on_data_retrieval_error",
"is_checking_data_consistency",
"version",
};

char *names159 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"stored_version",
"current_version",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names160 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names161 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"object_comparison",
"hash_table_version_64",
"has_default",
"last_index",
"found_item",
"ht_deleted_item",
"table_capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"capacity",
"ht_deleted_key",
};

char *names163 [] =
{
"item",
};

char *names164 [] =
{
"is_shared",
"count",
"item",
"counter",
};

char *names165 [] =
{
"internal_area",
"is_resizable",
};

char *names167 [] =
{
"cond_pointer",
};

char *names168 [] =
{
"lastentry",
"internal_name",
"internal_detachable_name_pointer",
"mode",
"directory_pointer",
"last_entry_pointer",
};

char *names169 [] =
{
"sem_pointer",
};

char *names170 [] =
{
"owner_thread_id",
"mutex_pointer",
};

char *names171 [] =
{
"internal_id",
};

char *names172 [] =
{
"last_string",
"last_character",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"last_real",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names173 [] =
{
"last_string",
"last_character",
"is_closed",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"buffer_size",
"object_stored_size",
"last_real",
"internal_buffer_access",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names174 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names175 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"internal_integer_buffer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names176 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"is_sequence_an_expected_numeric",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names177 [] =
{
"byte",
"file_pointer",
};

char *names178 [] =
{
"object_comparison",
"index",
"seed",
"last_item",
"last_result",
};

char *names179 [] =
{
"target",
"target_index",
"start_index",
"end_index",
};

char *names180 [] =
{
"object_comparison",
"index",
};

char *names181 [] =
{
"object_comparison",
"index",
};

char *names183 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"area_first_index",
};

char *names184 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"area_first_index",
};

char *names186 [] =
{
"storage",
"internal_name",
"is_normalized",
};

char *names188 [] =
{
"item",
};

char *names189 [] =
{
"item",
};

char *names190 [] =
{
"item",
};

char *names191 [] =
{
"item",
};

char *names192 [] =
{
"item",
};

char *names193 [] =
{
"item",
};

char *names194 [] =
{
"item",
};

char *names195 [] =
{
"item",
};

char *names196 [] =
{
"item",
};

char *names197 [] =
{
"item",
};

char *names198 [] =
{
"item",
};

char *names199 [] =
{
"item",
};

char *names200 [] =
{
"item",
};

char *names201 [] =
{
"item",
};

char *names202 [] =
{
"item",
};

char *names203 [] =
{
"item",
};

char *names204 [] =
{
"item",
};

char *names205 [] =
{
"item",
};

char *names206 [] =
{
"item",
};

char *names207 [] =
{
"item",
};

char *names208 [] =
{
"item",
};

char *names209 [] =
{
"item",
};

char *names210 [] =
{
"item",
};

char *names211 [] =
{
"item",
};

char *names212 [] =
{
"item",
};

char *names213 [] =
{
"item",
};

char *names214 [] =
{
"item",
};

char *names215 [] =
{
"item",
};

char *names216 [] =
{
"item",
};

char *names217 [] =
{
"item",
};

char *names218 [] =
{
"item",
};

char *names219 [] =
{
"item",
};

char *names220 [] =
{
"item",
};

char *names221 [] =
{
"item",
};

char *names222 [] =
{
"item",
};

char *names223 [] =
{
"item",
};

char *names224 [] =
{
"item",
};

char *names225 [] =
{
"item",
};

char *names226 [] =
{
"item",
};

char *names227 [] =
{
"item",
};

char *names228 [] =
{
"item",
};

char *names229 [] =
{
"item",
};

char *names230 [] =
{
"internal_hash_code",
"internal_case_insensitive_hash_code",
};

char *names231 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names232 [] =
{
"internal_hash_code",
"internal_case_insensitive_hash_code",
};

char *names233 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names234 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
"index",
};

char *names235 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names236 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names237 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names238 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names239 [] =
{
"area",
"object_comparison",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
};

char *names240 [] =
{
"internal_hash_code",
"internal_case_insensitive_hash_code",
};

char *names241 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
"area_lower",
};

char *names242 [] =
{
"area",
"internal_hash_code",
"internal_case_insensitive_hash_code",
"count",
"area_lower",
};

char *names243 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"is_sequence_an_expected_numeric",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names244 [] =
{
"retrieved_errors",
"deserialized_object",
"error_message",
"successful",
"last_file_position",
};

char *names245 [] =
{
"area",
};

char *names246 [] =
{
"internal_name_32",
"internal_name",
};

char *names247 [] =
{
"internal_name_32",
"internal_name",
};

char *names248 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names249 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names250 [] =
{
"to_pointer",
};

char *names251 [] =
{
"to_pointer",
};

char *names252 [] =
{
"internal_name_32",
"internal_name",
};

char *names253 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names254 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names256 [] =
{
"object_comparison",
};

char *names259 [] =
{
"object_comparison",
};

char *names260 [] =
{
"object_comparison",
};

char *names261 [] =
{
"object_comparison",
};

char *names262 [] =
{
"object_comparison",
};

char *names265 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names267 [] =
{
"object_comparison",
};

char *names268 [] =
{
"object_comparison",
};

char *names269 [] =
{
"object_comparison",
};

char *names270 [] =
{
"object_comparison",
};

char *names271 [] =
{
"object_comparison",
};

char *names272 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"last_result",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names273 [] =
{
"object_comparison",
};

char *names275 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names276 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names277 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names278 [] =
{
"area",
};

char *names279 [] =
{
"object_comparison",
};

char *names280 [] =
{
"object_comparison",
};

char *names281 [] =
{
"object_comparison",
};

char *names282 [] =
{
"object_comparison",
};

char *names283 [] =
{
"object_comparison",
};

char *names284 [] =
{
"object_comparison",
};

char *names285 [] =
{
"object_comparison",
};

char *names286 [] =
{
"object_comparison",
};

char *names287 [] =
{
"object_comparison",
};

char *names288 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names289 [] =
{
"internal_name_32",
"internal_name",
};

char *names290 [] =
{
"internal_name_32",
"internal_name",
};

char *names291 [] =
{
"internal_name_32",
"internal_name",
};

char *names292 [] =
{
"internal_name_32",
"internal_name",
};

char *names293 [] =
{
"internal_name_32",
"internal_name",
};

char *names294 [] =
{
"internal_name_32",
"internal_name",
};

char *names295 [] =
{
"internal_name_32",
"internal_name",
};

char *names296 [] =
{
"internal_name_32",
"internal_name",
};

char *names297 [] =
{
"internal_name_32",
"internal_name",
};

char *names298 [] =
{
"internal_name_32",
"internal_name",
};

char *names299 [] =
{
"internal_name_32",
"internal_name",
};

char *names300 [] =
{
"internal_name_32",
"internal_name",
};

char *names301 [] =
{
"internal_name_32",
"internal_name",
};

char *names302 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names303 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names304 [] =
{
"object_comparison",
};

char *names305 [] =
{
"object_comparison",
};

char *names306 [] =
{
"object_comparison",
};

char *names307 [] =
{
"object_comparison",
};

char *names309 [] =
{
"object_comparison",
};

char *names311 [] =
{
"object_comparison",
};

char *names314 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names316 [] =
{
"object_comparison",
};

char *names317 [] =
{
"object_comparison",
};

char *names318 [] =
{
"object_comparison",
};

char *names319 [] =
{
"object_comparison",
};

char *names320 [] =
{
"object_comparison",
};

char *names321 [] =
{
"object_comparison",
};

char *names322 [] =
{
"object_comparison",
};

char *names323 [] =
{
"object_comparison",
};

char *names324 [] =
{
"object_comparison",
};

char *names325 [] =
{
"object_comparison",
};

char *names326 [] =
{
"object_comparison",
};

char *names327 [] =
{
"object_comparison",
};

char *names328 [] =
{
"area",
};

char *names330 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names331 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names333 [] =
{
"internal_name_32",
"internal_name",
};

char *names334 [] =
{
"object_comparison",
};

char *names335 [] =
{
"object_comparison",
};

char *names336 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names337 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names338 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names339 [] =
{
"to_pointer",
};

char *names340 [] =
{
"to_pointer",
};

char *names341 [] =
{
"internal_name_32",
"internal_name",
};

char *names342 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names343 [] =
{
"item",
"right",
};

char *names344 [] =
{
"item",
};

char *names345 [] =
{
"target",
"active",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names346 [] =
{
"active",
"after",
"before",
};

char *names347 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"last_result",
"is_target_closed",
"is_basic",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names348 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names349 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names350 [] =
{
"object_comparison",
};

char *names351 [] =
{
"object_comparison",
};

char *names352 [] =
{
"object_comparison",
};

char *names353 [] =
{
"object_comparison",
};

char *names355 [] =
{
"object_comparison",
};

char *names357 [] =
{
"object_comparison",
};

char *names360 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names362 [] =
{
"object_comparison",
};

char *names363 [] =
{
"object_comparison",
};

char *names364 [] =
{
"object_comparison",
};

char *names365 [] =
{
"object_comparison",
};

char *names366 [] =
{
"object_comparison",
};

char *names367 [] =
{
"object_comparison",
};

char *names368 [] =
{
"object_comparison",
};

char *names369 [] =
{
"object_comparison",
};

char *names370 [] =
{
"object_comparison",
};

char *names371 [] =
{
"object_comparison",
};

char *names372 [] =
{
"object_comparison",
};

char *names373 [] =
{
"object_comparison",
};

char *names374 [] =
{
"area",
};

char *names376 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names377 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names379 [] =
{
"internal_name_32",
"internal_name",
};

char *names380 [] =
{
"object_comparison",
};

char *names381 [] =
{
"object_comparison",
};

char *names382 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names383 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names384 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names385 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names386 [] =
{
"object_comparison",
};

char *names387 [] =
{
"object_comparison",
};

char *names388 [] =
{
"object_comparison",
};

char *names389 [] =
{
"object_comparison",
};

char *names391 [] =
{
"object_comparison",
};

char *names393 [] =
{
"object_comparison",
};

char *names396 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names398 [] =
{
"object_comparison",
};

char *names399 [] =
{
"object_comparison",
};

char *names400 [] =
{
"object_comparison",
};

char *names401 [] =
{
"object_comparison",
};

char *names402 [] =
{
"object_comparison",
};

char *names403 [] =
{
"object_comparison",
};

char *names404 [] =
{
"object_comparison",
};

char *names405 [] =
{
"object_comparison",
};

char *names406 [] =
{
"object_comparison",
};

char *names407 [] =
{
"object_comparison",
};

char *names408 [] =
{
"object_comparison",
};

char *names409 [] =
{
"object_comparison",
};

char *names410 [] =
{
"area",
};

char *names412 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names413 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names415 [] =
{
"internal_name_32",
"internal_name",
};

char *names416 [] =
{
"object_comparison",
};

char *names417 [] =
{
"object_comparison",
};

char *names418 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names419 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names420 [] =
{
"internal_name_32",
"internal_name",
};

char *names421 [] =
{
"internal_name_32",
"internal_name",
};

char *names422 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names423 [] =
{
"to_pointer",
};

char *names424 [] =
{
"to_pointer",
};

char *names425 [] =
{
"internal_name_32",
"internal_name",
};

char *names426 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names428 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names429 [] =
{
"to_pointer",
};

char *names430 [] =
{
"to_pointer",
};

char *names431 [] =
{
"internal_name_32",
"internal_name",
};

char *names432 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names433 [] =
{
"to_pointer",
};

char *names434 [] =
{
"to_pointer",
};

char *names435 [] =
{
"internal_name_32",
"internal_name",
};

char *names436 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names437 [] =
{
"to_pointer",
};

char *names438 [] =
{
"to_pointer",
};

char *names439 [] =
{
"internal_name_32",
"internal_name",
};

char *names440 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names441 [] =
{
"to_pointer",
};

char *names442 [] =
{
"to_pointer",
};

char *names443 [] =
{
"internal_name_32",
"internal_name",
};

char *names444 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names445 [] =
{
"to_pointer",
};

char *names446 [] =
{
"to_pointer",
};

char *names447 [] =
{
"internal_name_32",
"internal_name",
};

char *names451 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names452 [] =
{
"to_pointer",
};

char *names453 [] =
{
"to_pointer",
};

char *names454 [] =
{
"internal_name_32",
"internal_name",
};

char *names455 [] =
{
"area",
};

char *names457 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names458 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names463 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names465 [] =
{
"internal_name_32",
"internal_name",
};

char *names466 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names467 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names468 [] =
{
"object_comparison",
};

char *names469 [] =
{
"object_comparison",
};

char *names470 [] =
{
"object_comparison",
};

char *names471 [] =
{
"object_comparison",
};

char *names472 [] =
{
"object_comparison",
};

char *names473 [] =
{
"object_comparison",
};

char *names474 [] =
{
"object_comparison",
};

char *names475 [] =
{
"object_comparison",
};

char *names476 [] =
{
"object_comparison",
};

char *names477 [] =
{
"object_comparison",
};

char *names478 [] =
{
"object_comparison",
};

char *names479 [] =
{
"object_comparison",
};

char *names480 [] =
{
"object_comparison",
};

char *names481 [] =
{
"object_comparison",
};

char *names482 [] =
{
"object_comparison",
};

char *names483 [] =
{
"object_comparison",
};

char *names484 [] =
{
"object_comparison",
};

char *names485 [] =
{
"object_comparison",
};

char *names486 [] =
{
"object_comparison",
};

char *names487 [] =
{
"object_comparison",
};

char *names488 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names489 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names490 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names493 [] =
{
"object_comparison",
};

char *names494 [] =
{
"object_comparison",
};

char *names495 [] =
{
"object_comparison",
};

char *names496 [] =
{
"object_comparison",
};

char *names497 [] =
{
"object_comparison",
};

char *names498 [] =
{
"object_comparison",
};

char *names499 [] =
{
"object_comparison",
};

char *names500 [] =
{
"object_comparison",
};

char *names501 [] =
{
"object_comparison",
};

char *names502 [] =
{
"object_comparison",
};

char *names503 [] =
{
"object_comparison",
};

char *names504 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names505 [] =
{
"to_pointer",
};

char *names506 [] =
{
"to_pointer",
};

char *names507 [] =
{
"internal_name_32",
"internal_name",
};

char *names508 [] =
{
"area",
};

char *names510 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names511 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names517 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names519 [] =
{
"internal_name_32",
"internal_name",
};

char *names520 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names521 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names522 [] =
{
"object_comparison",
};

char *names523 [] =
{
"object_comparison",
};

char *names524 [] =
{
"object_comparison",
};

char *names525 [] =
{
"object_comparison",
};

char *names526 [] =
{
"object_comparison",
};

char *names527 [] =
{
"object_comparison",
};

char *names528 [] =
{
"object_comparison",
};

char *names529 [] =
{
"object_comparison",
};

char *names530 [] =
{
"object_comparison",
};

char *names531 [] =
{
"object_comparison",
};

char *names532 [] =
{
"object_comparison",
};

char *names533 [] =
{
"object_comparison",
};

char *names534 [] =
{
"object_comparison",
};

char *names535 [] =
{
"object_comparison",
};

char *names536 [] =
{
"object_comparison",
};

char *names537 [] =
{
"object_comparison",
};

char *names538 [] =
{
"object_comparison",
};

char *names539 [] =
{
"object_comparison",
};

char *names540 [] =
{
"object_comparison",
};

char *names541 [] =
{
"object_comparison",
};

char *names542 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names543 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names544 [] =
{
"area",
};

char *names546 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names547 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names553 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names555 [] =
{
"internal_name_32",
"internal_name",
};

char *names556 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names557 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names558 [] =
{
"object_comparison",
};

char *names559 [] =
{
"object_comparison",
};

char *names560 [] =
{
"object_comparison",
};

char *names561 [] =
{
"object_comparison",
};

char *names562 [] =
{
"object_comparison",
};

char *names563 [] =
{
"object_comparison",
};

char *names564 [] =
{
"object_comparison",
};

char *names565 [] =
{
"object_comparison",
};

char *names566 [] =
{
"object_comparison",
};

char *names567 [] =
{
"object_comparison",
};

char *names568 [] =
{
"object_comparison",
};

char *names569 [] =
{
"object_comparison",
};

char *names570 [] =
{
"object_comparison",
};

char *names571 [] =
{
"object_comparison",
};

char *names572 [] =
{
"object_comparison",
};

char *names573 [] =
{
"object_comparison",
};

char *names574 [] =
{
"object_comparison",
};

char *names575 [] =
{
"object_comparison",
};

char *names576 [] =
{
"object_comparison",
};

char *names577 [] =
{
"object_comparison",
};

char *names578 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names579 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names580 [] =
{
"object_comparison",
};

char *names583 [] =
{
"object_comparison",
};

char *names584 [] =
{
"object_comparison",
};

char *names585 [] =
{
"object_comparison",
};

char *names586 [] =
{
"object_comparison",
};

char *names587 [] =
{
"object_comparison",
};

char *names588 [] =
{
"object_comparison",
};

char *names589 [] =
{
"object_comparison",
};

char *names590 [] =
{
"object_comparison",
};

char *names593 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names595 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names596 [] =
{
"to_pointer",
};

char *names597 [] =
{
"to_pointer",
};

char *names598 [] =
{
"internal_name_32",
"internal_name",
};

char *names599 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names600 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names601 [] =
{
"to_pointer",
};

char *names602 [] =
{
"to_pointer",
};

char *names603 [] =
{
"internal_name_32",
"internal_name",
};

char *names604 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names605 [] =
{
"object_comparison",
};

char *names606 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names607 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names608 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names609 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names610 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names612 [] =
{
"object_comparison",
};

char *names613 [] =
{
"object_comparison",
};

char *names614 [] =
{
"internal_name_32",
"internal_name",
};

char *names616 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names617 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names618 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names619 [] =
{
"area",
};

char *names620 [] =
{
"object_comparison",
};

char *names621 [] =
{
"object_comparison",
};

char *names622 [] =
{
"object_comparison",
};

char *names623 [] =
{
"object_comparison",
};

char *names624 [] =
{
"object_comparison",
};

char *names625 [] =
{
"object_comparison",
};

char *names626 [] =
{
"object_comparison",
};

char *names627 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names628 [] =
{
"item",
};

char *names629 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names631 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names632 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names638 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names640 [] =
{
"internal_name_32",
"internal_name",
};

char *names641 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names642 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names643 [] =
{
"object_comparison",
};

char *names644 [] =
{
"object_comparison",
};

char *names645 [] =
{
"object_comparison",
};

char *names646 [] =
{
"object_comparison",
};

char *names647 [] =
{
"object_comparison",
};

char *names648 [] =
{
"object_comparison",
};

char *names649 [] =
{
"object_comparison",
};

char *names650 [] =
{
"object_comparison",
};

char *names651 [] =
{
"object_comparison",
};

char *names652 [] =
{
"object_comparison",
};

char *names653 [] =
{
"object_comparison",
};

char *names654 [] =
{
"object_comparison",
};

char *names655 [] =
{
"object_comparison",
};

char *names656 [] =
{
"object_comparison",
};

char *names657 [] =
{
"object_comparison",
};

char *names658 [] =
{
"object_comparison",
};

char *names659 [] =
{
"object_comparison",
};

char *names660 [] =
{
"object_comparison",
};

char *names661 [] =
{
"area",
};

char *names662 [] =
{
"object_comparison",
};

char *names663 [] =
{
"object_comparison",
};

char *names664 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names665 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names667 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names668 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names674 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names676 [] =
{
"internal_name_32",
"internal_name",
};

char *names677 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names678 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names679 [] =
{
"object_comparison",
};

char *names680 [] =
{
"object_comparison",
};

char *names681 [] =
{
"object_comparison",
};

char *names682 [] =
{
"object_comparison",
};

char *names683 [] =
{
"object_comparison",
};

char *names684 [] =
{
"object_comparison",
};

char *names685 [] =
{
"object_comparison",
};

char *names686 [] =
{
"object_comparison",
};

char *names687 [] =
{
"object_comparison",
};

char *names688 [] =
{
"object_comparison",
};

char *names689 [] =
{
"object_comparison",
};

char *names690 [] =
{
"object_comparison",
};

char *names691 [] =
{
"object_comparison",
};

char *names692 [] =
{
"object_comparison",
};

char *names693 [] =
{
"object_comparison",
};

char *names694 [] =
{
"object_comparison",
};

char *names695 [] =
{
"object_comparison",
};

char *names696 [] =
{
"object_comparison",
};

char *names697 [] =
{
"area",
};

char *names698 [] =
{
"object_comparison",
};

char *names699 [] =
{
"object_comparison",
};

char *names700 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names701 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names703 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names704 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names710 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names712 [] =
{
"internal_name_32",
"internal_name",
};

char *names713 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names714 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names715 [] =
{
"object_comparison",
};

char *names716 [] =
{
"object_comparison",
};

char *names717 [] =
{
"object_comparison",
};

char *names718 [] =
{
"object_comparison",
};

char *names719 [] =
{
"object_comparison",
};

char *names720 [] =
{
"object_comparison",
};

char *names721 [] =
{
"object_comparison",
};

char *names722 [] =
{
"object_comparison",
};

char *names723 [] =
{
"object_comparison",
};

char *names724 [] =
{
"object_comparison",
};

char *names725 [] =
{
"object_comparison",
};

char *names726 [] =
{
"object_comparison",
};

char *names727 [] =
{
"object_comparison",
};

char *names728 [] =
{
"object_comparison",
};

char *names729 [] =
{
"object_comparison",
};

char *names730 [] =
{
"object_comparison",
};

char *names731 [] =
{
"object_comparison",
};

char *names732 [] =
{
"object_comparison",
};

char *names733 [] =
{
"area",
};

char *names734 [] =
{
"object_comparison",
};

char *names735 [] =
{
"object_comparison",
};

char *names736 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names737 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names739 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names740 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names746 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names748 [] =
{
"internal_name_32",
"internal_name",
};

char *names749 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names750 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names751 [] =
{
"object_comparison",
};

char *names752 [] =
{
"object_comparison",
};

char *names753 [] =
{
"object_comparison",
};

char *names754 [] =
{
"object_comparison",
};

char *names755 [] =
{
"object_comparison",
};

char *names756 [] =
{
"object_comparison",
};

char *names757 [] =
{
"object_comparison",
};

char *names758 [] =
{
"object_comparison",
};

char *names759 [] =
{
"object_comparison",
};

char *names760 [] =
{
"object_comparison",
};

char *names761 [] =
{
"object_comparison",
};

char *names762 [] =
{
"object_comparison",
};

char *names763 [] =
{
"object_comparison",
};

char *names764 [] =
{
"object_comparison",
};

char *names765 [] =
{
"object_comparison",
};

char *names766 [] =
{
"object_comparison",
};

char *names767 [] =
{
"object_comparison",
};

char *names768 [] =
{
"object_comparison",
};

char *names769 [] =
{
"area",
};

char *names770 [] =
{
"object_comparison",
};

char *names771 [] =
{
"object_comparison",
};

char *names772 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names773 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names775 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names776 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names778 [] =
{
"internal_name_32",
"internal_name",
};

char *names779 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names780 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names781 [] =
{
"object_comparison",
};

char *names782 [] =
{
"object_comparison",
};

char *names783 [] =
{
"object_comparison",
};

char *names784 [] =
{
"object_comparison",
};

char *names785 [] =
{
"object_comparison",
};

char *names786 [] =
{
"object_comparison",
};

char *names787 [] =
{
"object_comparison",
};

char *names788 [] =
{
"object_comparison",
};

char *names789 [] =
{
"object_comparison",
};

char *names790 [] =
{
"area",
};

char *names791 [] =
{
"object_comparison",
};

char *names792 [] =
{
"object_comparison",
};

char *names793 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names794 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names796 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names797 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names803 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names805 [] =
{
"internal_name_32",
"internal_name",
};

char *names806 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names807 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names808 [] =
{
"object_comparison",
};

char *names809 [] =
{
"object_comparison",
};

char *names810 [] =
{
"object_comparison",
};

char *names811 [] =
{
"object_comparison",
};

char *names812 [] =
{
"object_comparison",
};

char *names813 [] =
{
"object_comparison",
};

char *names814 [] =
{
"object_comparison",
};

char *names815 [] =
{
"object_comparison",
};

char *names816 [] =
{
"object_comparison",
};

char *names817 [] =
{
"object_comparison",
};

char *names818 [] =
{
"object_comparison",
};

char *names819 [] =
{
"object_comparison",
};

char *names820 [] =
{
"object_comparison",
};

char *names821 [] =
{
"object_comparison",
};

char *names822 [] =
{
"object_comparison",
};

char *names823 [] =
{
"object_comparison",
};

char *names824 [] =
{
"object_comparison",
};

char *names825 [] =
{
"object_comparison",
};

char *names826 [] =
{
"area",
};

char *names827 [] =
{
"object_comparison",
};

char *names828 [] =
{
"object_comparison",
};

char *names829 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names830 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names831 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"is_case_insensitive",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names832 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"iteration_position",
};

char *names833 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
};

char *names834 [] =
{
"object_comparison",
};

char *names837 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"iteration_position",
};

char *names838 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"object_comparison",
"hash_table_version_64",
"has_default",
"found_item",
"ht_deleted_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"count",
"ht_deleted_key",
};

char *names839 [] =
{
"object_comparison",
};

char *names842 [] =
{
"item",
};

char *names843 [] =
{
"item",
};

char *names844 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"is_case_insensitive",
"found_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_item",
"count",
};

char *names845 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"iteration_position",
};

char *names846 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_key",
"object_comparison",
"hash_table_version_64",
"has_default",
"found_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_item",
"count",
};

char *names847 [] =
{
"object_comparison",
};

char *names850 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names851 [] =
{
"to_pointer",
};

char *names852 [] =
{
"to_pointer",
};

char *names853 [] =
{
"internal_name_32",
"internal_name",
};

char *names854 [] =
{
"operands",
"closed_operands",
"open_map",
"open_types",
"is_target_closed",
"is_basic",
"last_result",
"open_count",
"routine_id",
"written_type_id_inline_agent",
"rout_disp",
"calc_rout_addr",
"encaps_rout_disp",
};

char *names855 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names856 [] =
{
"found_item",
"content",
"keys",
"indexes_map",
"deleted_marks",
"ht_deleted_item",
"object_comparison",
"hash_table_version_64",
"has_default",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_key",
"count",
};

char *names859 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"iteration_position",
};

char *names860 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names861 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names862 [] =
{
"to_pointer",
};

char *names863 [] =
{
"to_pointer",
};

char *names864 [] =
{
"internal_name_32",
"internal_name",
};

char *names865 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"value",
"callstack_depth",
};

char *names866 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names867 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names868 [] =
{
"to_pointer",
};

char *names869 [] =
{
"to_pointer",
};

char *names870 [] =
{
"internal_name_32",
"internal_name",
};

char *names871 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names872 [] =
{
"area_v2",
"is_aborted_stack_internal",
"call_buffer_internal",
"name_internal",
"event_data_names_internal",
"dummy_event_data_internal",
"kamikazes_internal",
"not_empty_actions_internal",
"empty_actions_internal",
"object_comparison",
"in_operation",
"index",
"state",
};

char *names873 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names874 [] =
{
"object_comparison",
};

char *names875 [] =
{
"object_comparison",
};

char *names876 [] =
{
"area_v2",
"object_comparison",
"in_operation",
"index",
};

char *names877 [] =
{
"internal_name_32",
"internal_name",
};

char *names878 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names879 [] =
{
"content",
"keys",
"indexes_map",
"deleted_marks",
"object_comparison",
"hash_table_version_64",
"has_default",
"found_item",
"capacity",
"item_position",
"iteration_position",
"control",
"deleted_item_position",
"ht_lowest_deleted_position",
"ht_deleted_item",
"ht_deleted_key",
"count",
};

char *names882 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"iteration_position",
};

char *names883 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names884 [] =
{
"object_comparison",
};

char *names885 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names886 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names887 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names888 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names889 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names890 [] =
{
"right",
"item",
};

char *names891 [] =
{
"target",
"active",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names892 [] =
{
"active",
"after",
"before",
};

char *names893 [] =
{
"object_comparison",
};

char *names894 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names895 [] =
{
"object_comparison",
};

char *names896 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names897 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names898 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names899 [] =
{
"object_comparison",
"index",
};

char *names900 [] =
{
"object_comparison",
};

char *names901 [] =
{
"object_comparison",
};

char *names902 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names904 [] =
{
"target",
"area_index",
"area_last_index",
};

char *names905 [] =
{
"target",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names911 [] =
{
"area",
"area_index",
"area_last_index",
};

char *names913 [] =
{
"area",
"object_comparison",
"upper",
"lower",
};

char *names914 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names915 [] =
{
"object_comparison",
};

char *names916 [] =
{
"object_comparison",
};

char *names917 [] =
{
"object_comparison",
};

char *names918 [] =
{
"object_comparison",
};

char *names919 [] =
{
"object_comparison",
};

char *names920 [] =
{
"object_comparison",
};

char *names921 [] =
{
"object_comparison",
};

char *names922 [] =
{
"object_comparison",
};

char *names923 [] =
{
"object_comparison",
};

char *names924 [] =
{
"object_comparison",
};

char *names925 [] =
{
"object_comparison",
};

char *names926 [] =
{
"object_comparison",
};

char *names927 [] =
{
"object_comparison",
};

char *names928 [] =
{
"object_comparison",
};

char *names929 [] =
{
"object_comparison",
};

char *names930 [] =
{
"object_comparison",
};

char *names931 [] =
{
"object_comparison",
};

char *names932 [] =
{
"object_comparison",
};

char *names933 [] =
{
"area",
};

char *names934 [] =
{
"object_comparison",
};

char *names935 [] =
{
"object_comparison",
};

char *names936 [] =
{
"area",
"target",
"area_index",
"area_last_index",
};

char *names937 [] =
{
"area",
"target",
"area_index",
"area_last_index",
"first_index",
};

char *names938 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names939 [] =
{
"breakable_info",
"rt_type",
"position",
"type",
"callstack_depth",
"value",
};

char *names940 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names941 [] =
{
"breakable_info",
"value",
"rt_type",
"position",
"type",
"callstack_depth",
};

char *names942 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names943 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names944 [] =
{
"breakable_info",
"object",
"rt_type",
"offset",
"type",
"value",
};

char *names945 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names946 [] =
{
"first_element",
"active",
"object_comparison",
"before",
"after",
"count",
};

char *names947 [] =
{
"right",
"item",
};

char *names948 [] =
{
"item",
};

char *names949 [] =
{
"target",
"active",
"is_reversed",
"version",
"step",
"last_index",
"first_index",
"target_index",
};

char *names950 [] =
{
"active",
"after",
"before",
};

char *names951 [] =
{
"object_comparison",
};

char *names952 [] =
{
"area_v2",
"object_comparison",
"index",
};

char *names953 [] =
{
"object_comparison",
};

char *names954 [] =
{
"area",
"object_comparison",
"out_index",
"count",
};

char *names955 [] =
{
"area",
"area_index",
"remaining_count",
};

char *names956 [] =
{
"breakable_info",
"object",
"value",
"rt_type",
"offset",
"type",
};

char *names957 [] =
{
"breakable_info",
"object",
"index",
"type",
"value",
};

char *names958 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names959 [] =
{
"breakable_info",
"object",
"value",
"index",
"type",
};

char *names960 [] =
{
"pippo",
};

char *names961 [] =
{
"data",
};

char *names963 [] =
{
"items",
};

char *names964 [] =
{
"internal_item",
"is_utc",
"microseconds_now",
};

char *names965 [] =
{
"asserter",
};

char *names966 [] =
{
"last_assertion_failed",
};

char *names967 [] =
{
"internal_exception",
};

char *names972 [] =
{
"socket_address",
};

char *names977 [] =
{
"file_system",
"environment",
"internal_asserter",
"has_failed",
};

char *names978 [] =
{
"file_system",
"environment",
"internal_asserter",
"has_failed",
};

char *names983 [] =
{
"compact_time",
"fractional_second",
};

char *names987 [] =
{
"time",
"date",
};

char *names994 [] =
{
"value",
};

char *names995 [] =
{
"internal_host_name",
"family",
};

char *names996 [] =
{
"internal_host_name",
"family",
"the_address",
};

char *names997 [] =
{
"internal_host_name",
"the_address",
"the_scope_ifname",
"is_scope_id_set",
"family",
"the_scope_id",
};

char *names999 [] =
{
"storage",
"storage_flag",
};

char *names1002 [] =
{
"socket_address",
};

char *names1004 [] =
{
"minute",
"hour",
"fine_second",
};

char *names1005 [] =
{
"origin_date",
"year",
"month",
"day",
};

char *names1006 [] =
{
"origin_date_time",
"time",
"date",
};

char *names1008 [] =
{
"retrieved_errors",
"socket",
"testing_directory",
"is_stream_invalid",
"return_code",
"port",
};

char *names1009 [] =
{
"value",
"name",
"is_text",
"is_numeric",
"count_max",
"count_min",
"value_max",
"value_min",
"type",
};

char *names1010 [] =
{
"start_date",
"finish_date",
"setup_response",
"output",
};

char *names1011 [] =
{
"start_date",
"finish_date",
"setup_response",
"output",
"test_response",
"teardown_response",
};

char *names1012 [] =
{
"ordered_compact_date",
};

char *names1013 [] =
{
"object_ptr",
};

char *names1014 [] =
{
"object_ptr",
};

char *names1015 [] =
{
"object_ptr",
};

char *names1016 [] =
{
"last_string",
"address",
"peer_address",
"socket_error",
"put_internal_socket_buffer",
"read_internal_socket_buffer",
"last_character",
"last_boolean",
"is_blocking",
"is_open_read",
"is_open_write",
"descriptor_available",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"family",
"protocol",
"type",
"last_real",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names1017 [] =
{
"last_string",
"address",
"peer_address",
"socket_error",
"put_internal_socket_buffer",
"read_internal_socket_buffer",
"accepted",
"last_character",
"last_boolean",
"is_blocking",
"is_open_read",
"is_open_write",
"descriptor_available",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"family",
"protocol",
"type",
"last_real",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names1018 [] =
{
"last_string",
"address",
"peer_address",
"socket_error",
"put_internal_socket_buffer",
"read_internal_socket_buffer",
"last_character",
"last_boolean",
"is_blocking",
"is_open_read",
"is_open_write",
"descriptor_available",
"is_created",
"is_connected",
"is_bound",
"is_closed",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"family",
"protocol",
"type",
"fd",
"fd1",
"last_fd",
"internal_port",
"default_timeout",
"last_real",
"last_natural_64",
"timeout_ns",
"last_integer_64",
"last_double",
};

char *names1019 [] =
{
"last_string",
"address",
"peer_address",
"socket_error",
"put_internal_socket_buffer",
"read_internal_socket_buffer",
"accepted",
"last_character",
"last_boolean",
"is_blocking",
"is_open_read",
"is_open_write",
"descriptor_available",
"is_created",
"is_connected",
"is_bound",
"is_closed",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"family",
"protocol",
"type",
"fd",
"fd1",
"last_fd",
"internal_port",
"default_timeout",
"connect_timeout",
"accept_timeout",
"last_real",
"last_natural_64",
"timeout_ns",
"last_integer_64",
"last_double",
};

char *names1020 [] =
{
"last_string",
"internal_name",
"internal_detachable_name_pointer",
"buffer",
"last_character",
"separator",
"object_comparison",
"descriptor_available",
"is_sequence_an_expected_numeric",
"last_natural_8",
"last_integer_8",
"last_natural_16",
"last_integer_16",
"last_natural",
"last_integer",
"bytes_read",
"mode",
"buffer_size",
"truncated_start_position",
"last_real",
"file_pointer",
"last_natural_64",
"last_integer_64",
"last_double",
};

char *names1021 [] =
{
"index",
};

char *names1023 [] =
{
"value",
"days",
"months",
"internal_parser",
"separators_used",
"right_day_text",
"base_century",
};

char *names1024 [] =
{
"compact_time",
"fractional_second",
};

char *names1025 [] =
{
"compact_time",
"fractional_second",
};

char *names1026 [] =
{
"ordered_compact_date",
};

char *names1027 [] =
{
"ordered_compact_date",
};

char *names1028 [] =
{
"compact_time",
"ordered_compact_date",
"fractional_second",
};

char *names1029 [] =
{
"source_string",
"day_text_val",
"code",
"months",
"days",
"parsed",
"compact_time",
"ordered_compact_date",
"year_val",
"month_val",
"day_val",
"hour_val",
"minute_val",
"base_century",
"fractional_second",
"fine_second_val",
};

char *names1030 [] =
{
"time",
"date",
"compact_time",
"ordered_compact_date",
"fractional_second",
};

char *names1031 [] =
{
"recipient_name",
"class_name",
"tag_name",
"trace",
"last_class_name",
"last_routine_name",
"is_trace_valid",
"is_test_invalid",
"code",
"break_point_slot",
"last_break_point_slot",
};

char *names1032 [] =
{
"retrieved_errors",
"log_file",
"output_buffer",
"error_buffer",
"socket",
"last_request",
"last_response",
"object_store",
"has_error",
"is_last_protected_execution_successfull",
"should_quit",
"is_last_invariant_violated",
"last_request_type",
"last_response_flag",
"return_code",
"byte_code_feature_body_id",
"byte_code_feature_pattern_id",
};

char *names1033 [] =
{
"start_bound",
"end_bound",
};

char *names1034 [] =
{
"return_code",
};



#ifdef __cplusplus
}
#endif
